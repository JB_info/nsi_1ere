# Histoire de l'informatique



Chapitre transversal : tout au long de l'année, vous allez compléter la [frise](./frise.pdf) avec les évènements clés de l'histoire de l'informatique.



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des documents : *production personnelle*