# Algorithmique : k plus proches voisins

Dernier chapitre d'algorithmique cette année : les *k plus proches voisins*, ou *kNN*. Je vous rassure, il n'y aura pas de code cette fois !

## I. Principe

### 1. À quoi servent les algos kNN ?

L’algorithme des k plus proches voisins, appelé **kNN** pour *k* *n*earest *n*eighbors en anglais, est un algorithme de classification qui appartient à la famille des **algorithmes d’apprentissage automatique (machine learning)**.

Le terme de « machine learning » a été utilisé pour la première fois par l’informaticien américain Arthur Samuel en 1959. Les algorithmes d’apprentissage automatique ont connu un fort regain d’intérêt au début des années 2000 notamment grâce à la quantité de données disponibles sur internet : de nombreuses sociétés, commes les GAFAM (*G*oogle *A*pple *F*acebok *A*mazon *M*icrosoft) utilisent les données concernant leurs utilisateurs afin de "nourrir" des algorithmes de machine learning qui permettront à ces sociétés d’en savoir toujours plus sur nous et ainsi de mieux cerner nos "besoins" en termes de consommation ... au détriment de la vie privée.

> **exercice 1 :**  
> * Expliquez avec vos mots en quoi le machine learning met en danger votre vie privée.
> * Lorsque vous naviguez sur le Web, par quel moyen les entreprises récupèrent-elles vos données ?


### 2. Comment ça marche ?

Un algorithme des k plus proches voisins se base sur un **ensemble de données d'apprentissage**. Chaque donnée d'apprentissage :
* appartient à une **classe**
* possède un certain nombre de **caractéristiques**

```txt
EXEMPLE :

Un site Web a récupéré les données de clients suivantes :
- un homme de 23 ans a confirmé être étudiant
- une femme de 18 ans a confirmé être étudiante
- un homme de 49 ans a confirmé ne pas être étudiant
- un homme de 21 ans a confirmé être étudiant
- une femme de 21 ans a confirmé être étudiante
- une femme de 38 ans a confirmé ne pas être étudiante
- un homme de 14 ans a confirmé ne pas être étudiant

Les caractéristiques sont le genre et l'âge des personnes. La classe est le statut (étudiant ou non).
```

L'algorithme kNN va alors rencontrer un individu non identifié, c'est-à-dire une **donnée dont on possède les caractéristiques mais dont la classe est inconnue**.

```txt
EXEMPLE :

Un nouvel utilisateur se connecte au même site Web que celui de l'exemple précédent.
On sait que c'est un homme de 22 ans, mais il n'a pas renseigné son statut donc on ne sait pas s'il est étudiant ou non :
on connaît ses caractéristiques mais pas sa classe.
```

L'objectif de l'algorithme sera alors de **deviner la classe de la donnée inconnue en regardant les classes des "k" données d'apprentissage qui partagent le plus de caractéristiques similaires**, c'est-à-dire en regardant les "k" plus proches voisins.

> **exercice 2 :**  
> * À votre avis, l'homme de classe inconnue de l'exemple précédent est-il étudiant ou non ?
> * Expliquez votre stratégie (quelles lignes des données d'apprentissage avez-vous regardées ? pourquoi ?) .


### 3. Quel est ce "k" ?

Le nombre « k » représente le nombre de données d'apprentissage que l'on va prendre en compte, c'est-à-dire le nombre de voisins dont on va se servir pour devenir la classe de notre inconnu.

Prenons un exemple :

![](img/ronds_carres.jpg)

Nous avons un ensemble de données d'apprentissage, qui ont deux classes possibles :
- rond rouge
- carré bleu.

Nous avons une donnée de classe inconnue, le "?" vert sur l'image.

Nous allons essayer de deviner si la forme inconnue est un rond rouge ou un carré bleu en regardant ses "k" voisins.

Essayons avec plusieurs valeurs de "k".

**k = 3 :**
![](img/ronds_carres_k3.jpg)

En prenant k=3 voisins, on obtient deux ronds rouges et un seul carré bleu dans les données d'apprentissage. On devine donc que notre donnée inconnue est aussi un rond rouge.

**k = 7 :**
![](img/ronds_carres_k7.jpg)

> **exercice 3 :**  
> En regardant k=7 voisins, quelle est la classe de notre forme inconnue ?

**k = 13 :**
![](img/ronds_carres_k13.jpg)

En prenant k=13 voisins, notre forme inconnue possède 6 voisins ronds rouges pour 7 voisins carrés bleus. On devine donc que c'est un carré bleu (alors qu'avec k=3 on devinait rond rouge !).

Il faut donc **choisir un k suffisamment grand pour éliminer les coïncidences et suffisamment petit pour ne pas regarder des données qui ne seraient plus de vrais voisins**.

> **exercice 4 :**  
> Pas trop petit, pas trop grand, facile non !  
> Parmi les k testés (k=3, k=7, k=13), lequel vaut-il donc mieux garder ?

En réalité, il existe des techniques basées sur des études statistiques des données d'apprentissage pour choisir le "k", mais nous ne le verrons pas cette année.

> **exercice 5 :**  
> Pourquoi aurait-il est déjà dangereux de prendre un "k" pair ?

Même si on choisit un "k" approprié, avec cet algorithme on fait un choix éclairé mais on ne peut pas être à 100% certain d'avoir fait le bon.


## II. Exemples concrets

### 1. Classifier les fleurs

Il est assez compliqué de déterminer l’espèce d’un iris lorsqu’on est face à un spécimen dans la nature. Nous considérerons trois espèces d’iris :

![](img/trois_iris.jpg)

Edgar Anderson (un botaniste Américain) a collecté les largeurs des pétales et les longueurs des pétales de 150 fleurs et leur a attribué leur bonne espèce.

> **exercice 6 :**  
> Quelles sont les caractéristiques des données ? Quelle est la classe ?

Le fichier avec les données d'apprentissage d'Anderson est disponible [ici](src/iris.csv).

> **exercice 7 :**  
> * De quel type de fichier s'agit-il ?
> * Ouvrez-le pour voir ce qu'il contient.

Pour simplifier la lecture, le graphique suivant a été créé :

![](img/iris.png)

Les iris y sont placées dans un graphique avec comme coordonnées les longueurs et les largeurs des pétales. Pour faciliter le repérage des classes, on utilise une couleur différente pour chaque espèce d'iris.

Nous pouvons maintenant nous servir de nos données d'apprentissage pour classifier nos iris inconnues.

Nous avons trouvé 3 iris dont nous ne connaissons pas la classe, alors on mesure leurs pétales :
* pour la première on obtient 1.3cm sur 0.4cm
* pour la seconde on obtient 6.5cm sur 2.2 cm
* pour la troisième on obtient 2.5cm sur 0.75cm

On ne connait pas la classe des trois iris donc on les représente en noir :

![](img/iris_inconnues.png)

> **exercice 8 :**  
> Les trois iris ont-elles été correctement placées sur le graphique ?

On admet que *k=3* est le meilleur k pour classifier nos iris.

Quelques fois, pas besoin de sortir les outils : la classe est facile à deviner.

Par exemple pour l'iris inconnue (1.3, 0.4) : elle est entourée de setosa (vert) donc on peut deviner que c'est aussi une setosa.
 
> **exercice 9 :**  
> Quelle est l'espèce de l'iris (6.5, 2.2) ?

> **exercice 10 :**  
> * Peut-on trancher aussi nettement pour la troisième iris (2.5, 0.75) ?
> * Proposez une méthode simple pour trouver la classe de cette iris avec k=3.


### 2. Cibler les publicités

> **exercice 11 :**  
> Maintenant que vous êtes des experts en algorithmes de k plus proches voisins, pouvez-vous expliquer comment les sites Web ou réseaux sociaux font pour vous proposer des publicités appropriées ?

### 3. Les planètes

Les planètes du système solaire sont soit *telluriques* (composées de roches), soit *gazeuses*.

Le type d'une planète dépend de sa distance par rapport au soleil (en millions de kilomètres).

Voici nos données d'apprentissage :

| planète | distance au soleil |    type    |
|:-------:|:------------------:|:----------:|
| Jupiter |         778        |   gazeuse  |
|  Uranus |        2870        |   gazeuse  |
|  Terre  |         150        | tellurique |
|  Vénus  |         108        | tellurique |
| Saturne |        1457        |   gazeuse  |
| Neptune |        4500        |   gazeuse  |
| Mercure |         58         | tellurique |

> **exercice 12 :**  
> * Recopiez et complétez le graphique suivant permettant de représenter les planètes :
> ![](img/planetes.png)
> * Ajoutez-y la planète Mars (227 millions de km du soleil) dont le type est inconnu.
> * À votre avis, quel k faut-il choisir ?
> * Déterminez le type de Mars.


## Pour aller plus loin : des kNN avec Python

Les graphiques des données sur les iris ont été créés avec Python !

Si vous le souhaitez, vous pouvez aller faire [cette activité](knn_python.md) pour découvrir comment.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*

Fichier `iris.csv` issu de ["The iris dataset"](https://gist.github.com/curran/a08a1080b88344b0c8a7)
