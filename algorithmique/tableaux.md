# Algorithmique : parcours de tableaux

Le mot algorithme vient d'Al-Khwârizmî, nom d'un mathématicien persan du IXème siècle. Les algorithmes existaient donc bien avant les ordinateurs...

## I. Algorithmes

**Pour chaque fonction, vous écrirez des doctests.**

*RAPPEL*  
Pour exécuter les doctests il faut mettre ceci à la fin de votre programme :
```python
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
```

### 1. Somme

> **exercice 1 :**  
> Écrivez une fonction qui prend en paramètre un tableau d'entiers et renvoie la somme des éléments.
> 
> ```python
> def somme(tab):
>     """
>     Calcule la somme des éléments du tableau.
>     :param tab: (list) un tableau
>     :return: (int) somme des éléments
>     
>     """
> ```

### 2. Moyenne

> **exercice 2 :**  
> Écrivez une fonction qui prend en paramètre un tableau d'entiers et renvoie la moyenne des éléments.
> *Indice : moyenne = somme divisée par le nombre d'éléments.*
> 
> ```python
> def moyenne(tab):
>     """
>     Calcule la moyenne des éléments du tableau.
>     :param tab: (list) un tableau
>     :return: (int) moyenne des éléments
>     
>     """
> ```

### 3. Nombre d'occurrences

> **exercice 3 :**  
> Écrivez une fonction qui prend en paramètre un tableau d'entiers et qui renvoie le nombre de 0 du tableau.
> 
> ```python
> def nombre_zeros(tab):
>     """
>     Calcule le nombre de zéros du tableau.
>     :param tab: (list) un tableau
>     :return: (int) nombre de zéros
>     
>     """
> ```

> **exercice 4 :**  
> Écrivez une fonction qui prend en paramètre un tableau et une valeur et qui renvoie le nombre d'occurrences de la valeur dans le tableau.
> *Indice : nombre d'occurrences = nombre de fois où cette valeur apparaît.*
> 
> ```python
> def nombre_occurrences(tab, val):
>     """
>     Renvoie le nombre d'occurrences de la valeur dans le tableau.
>     :param tab: (list) un tableau
>     :param val: (int ou str) une valeur
>     :return: (int) nombre d'occurrences de la valeur
>     
>     """
> ```

### 4. Indice

> **exercice 5 :**  
> Écrivez une fonction qui prend en paramètre un tableau et une valeur et qui renvoie l'indice de la première occurrence de la valeur dans le tableau.
> *Indice : première occurrence = première fois où cette valeur apparaît.*
> 
> ```python
> def premiere_occurrence(tab, val):
>     """
>     Renvoie l'indice de la première occurrence de la valeur dans le tableau.
>     :param tab: (list) un tableau
>     :param val: (int ou str) une valeur
>     :return: (int) indice de la première occurrence de la valeur
>     
>     """
> ```

> **exercice 6 :**  
> Écrivez une fonction qui prend en paramètre un tableau et une valeur et qui renvoie l'indice de la dernière occurrence de la valeur dans le tableau.
> 
> ```python
> def derniere_occurrence(tab, val):
>     """
>     Renvoie l'indice de la dernière occurrence de la valeur dans le tableau.
>     :param tab: (list) un tableau
>     :param val: (int ou str) une valeur
>     :return: (int) indice de la dernière occurrence de la valeur
>     
>     """
> ```

### 5. Extremum (maximum et minimum)

> **exercice 7 :**  
> Écrivez une fonction qui prend en paramètre un tableau d'entiers et renvoie le maximum.
> 
> ```python
> def maximum(tab):
>     """
>     Trouve le maximum des éléments du tableau.
>     :param tab: (list) un tableau
>     :return: (int) le maximum
>     
>     """
> ```

> **exercice 8 :**  
> Écrivez une fonction qui prend en paramètre un tableau d'entiers et renvoie le minimum.
> 
> ```python
> def minimum(tab):
>     """
>     Trouve le minimum des éléments du tableau.
>     :param tab: (list) un tableau
>     :return: (int) le minimum
>     
>     """
> ```

> **exercice 9 :**  
> En vous inspirant de l'exercice 7, écrivez une fonction qui renvoie l'INDICE du maximum.
> 
> ```python
> def indice_maximum(tab):
>     """
>     Trouve l'indice du maximum des éléments du tableau.
>     :param tab: (list) un tableau
>     :return: (int) l'indice du maximum
>     
>     """
> ```

> **exercice 10 :**  
> En vous inspirant de l'exercice 8, écrivez une fonction qui renvoie l'INDICE du minimum.
> 
> ```python
> def indice_minimum(tab):
>     """
>     Trouve l'indice du minimum des éléments du tableau.
>     :param tab: (list) un tableau
>     :return: (int) l'indice du minimum
>     
>     """
> ```

## II. Coût

Le **coût** de l'algorithme est le nombre d'«&nbsp;étapes&nbsp;» effectuées.

Pour les tableaux une «&nbsp;étape&nbsp;» consiste à accéder à une nouvelle valeur.

Tous nos algorithmes se réalisent avec une seule boucle for. La boucle for parcourt le tableau du premier au dernier élément.

Si on note `n` la taille du tableau, la boucle for parcourt alors `n` élément. Donc on fait `n` étapes.

On dit donc que le coût de l'algorithme est d'**ordre n**, noté **O(n)**. Un algorithme qui a un coût en O(n) est dit **linéaire**.

> **exercice 11 :**  
> Justifiez que la fonction maximum de l'exercice 7 est linéaire. Donnez donc son coût.


## III. Terminaison

La **terminaison** d'un algorithme est le fait que l'algorithme se termine bien et donc ne boucle pas à l'infini.

Ce qui nous permet d'être certain de la terminaison est appelé **variant** de boucle.

Un variant est un **entier qui va de a à b en se rapprochant de b à chaque étape** (étape = tour de boucle).

Tous nos algorithmes se réalisent avec une seule boucle for. La boucle for va du premier indice au dernier indice du tableau. À chaque tour de boucle on regarde l'indice suivant donc l'indice se rapproche bien de la fin.  
Notre variant de boucle est donc l'indice !

> **exercice 12 :**  
> Donnez le variant de boucle pour la fonction maximum de l'exercice 7.


## IV. Correction

La **correction** d'un algorithme est l'assurance qu'il réalise bien ce pourquoi il est fait.

Ce qui garantit cette correction est appelé **invariant** : c'est une propriété qui doit rester vraie du début à la fin de l'algorithme.

Par exemple pour la fonction somme de l'exercice 1, l'invariant est "peu importe où on en est dans le parcours du tableau, on a déjà calculé la somme des éléments précédents".

Cette propriété est toujours vraie puisqu'on calcule la somme au fur et à mesure du parcours. Et elle est encore vraie quand on a fini notre parcours.

> **exercice 13 :**  
> Donnez l'invariant pour la fonction maximum de l'exercice 7.

## Pour aller plus loin

> **exercice 14 :**  
> Donnez le coût, un variant et un invariant pour chaque algorithme du I.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
