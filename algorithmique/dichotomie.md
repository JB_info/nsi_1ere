# Algorithmique : recherche dichotomique

La recherche dichotomique est formalisée dans un article de John Mauchly en 1946, mais on trouve des traces de cette technique de recherche bien avant J.-C. : encore une preuve que les algorithmes existaient bien avant les ordinateurs...

## I. Principe

Il faut d'abord noter que la **recherche dichotomique se réalise dans un tableau trié dans l'ordre croissant et ne comportant pas plusieurs fois la même valeur**.

> **exercice 1 :**  
> Parmi les tableaux Python suivants, sur lesquels peut-on effectuer une recherche dichotomique ?
> * [1, 4, 8, 11]
> * [-7, -3, 0, 2, 6, 3602]
> * [47, 21, 12, 4, 0]
> * [0, 4, 5, 5, 7, 8]
> * [4, 6, 8, 3, 5, 7]

*Dichotomique* vient du grec "dikhotomos" qui signifie *couper en deux*. Cela résume bien l'idée de l'algorithme, car à chaque étape nous allons couper notre tableau en 2.

Le principe de l'algorithme pour rechercher une valeur `val` dans un tableau `t` est de **regarder la valeur *au milieu* de t** :
* si la valeur au milieu de t est égale à val : c'est fini, on a trouvé !
* si val est plus petite que la valeur au milieu de t : **on continue notre recherche dans la moitié gauche** du tableau
* si val est plus grande que la valeur au milieu de t : **on continue notre recherche dans la moitié droite** du tableau

Un exemple est toujours plus parlant :

![dichotomie_true](img/dichotomie_true.png)

Dans l'image précédente, on cherche 4 dans le tableau [1, 3, 4, 7, 8, 11, 12, 13, 20] :
* on regarde le milieu : 8. 4 est plus petit que 8 donc on cherche à gauche (dans [1, 3, 4, 7]).
* on regarde le nouveau milieu : 3 (s'il y a un nombre pair de chiffres, il y a deux "milieux" donc on prend le nombre de gauche). 4 est plus grand que 3 donc on cherche à droite (dans [4, 7]).
* on regarde le nouveau milieu : 4. 4 est la valeur qu'on cherchait donc c'est fini : on a bien trouvé !

> **exercice 2 :**  
> Réalisez un schéma similaire pour la recherche dichotomique de 11 dans le tableau [1, 3, 4, 7, 8, 11, 12, 13, 20].

Regardons ce qu'il se passe si on cherche un nombre qui n'est pas présent dans le tableau :

![dichotomie_false](img/dichotomie_false.png)

* on regarde le milieu : 8. 5 est plus petit que 8 donc on cherche à gauche (dans [1, 3, 4, 7]).
* on regarde le nouveau milieu : 3. 5 est plus grand que 3 donc on cherche à droite (dans [4, 7]).
* on regarde le nouveau milieu : 4. 5 est plus grand que 4 donc on cherche à droite (dans [7]).
* on regarde le nouveau milieu : 7. 5 est plus petit que 7 donc on cherche à gauche (dans []).
* il n'y a plus rien : c'est fini, on a pas trouvé !

> **exercice 3 :**  
> Réalisez un schéma similaire pour la recherche dichotomique de 9 dans le tableau [1, 3, 4, 7, 8, 11, 12, 13, 20].


## II. Algorithme

*Pour cette partie, vous n'êtes pas obligés de recopier le code dans votre cahier.*

L'algorithme pour la recherche dichotomique va renvoyer `True` s'il trouve la valeur, et `False` s'il ne la trouve pas.

> **exercice 4 :**  
> Enregistrez le code suivant dans un fichier python (si possible, au même endroit que les algorithmes sur les tableaux).
> ```python
> def recherche_dichotomique(tab, val):
>     """
>     Effectue une recherche dichotomique.
>     :param tab: (list) un tableau
>     :param val: (int) un entier
>     :return: (bool) True si on trouve la valeur, False sinon
>     
>     """
> ```

> **exercice 5 :**  
> En utilisant les valeurs des exercices 2 et 3, écrivez deux doctests pour la fonction recherche_dichotomique.
> N'oubliez pas d'ajouter les 3 lignes à la fin de votre fichier :
> ```python
> if __name__ == '__main__':
>     import doctest
>     doctest.testmod(verbose=True)
> ```

> **exercice 6 :**  
> Il manque un test pour le cas particulier du tableau vide. Rajoutez-en un dans votre fonction recherche_dichotomique.

Voici l'algorithme en pseudo-code pour la recherche dichotomique :

```
debut <- 0
fin <- taille de tab - 1
    
tant que debut est inférieur ou égal à fin :
    milieu <- (debut + fin) divisé par 2
    val_milieu <- tab[milieu]

	si val est égale à val_milieu :
		renvoyer True

	si val est inférieure à val_milieu :
		debut <- debut
		fin <- milieu - 1

	si val est supérieure à val_milieu :
		debut <- milieu + 1
		fin <- fin

renvoyer False
```

> **exercice 7 :**  
> Transformez ce pseudo-code en Python dans votre fonction recherche_dichotomique.
> Les doctests doivent normalement vous permettre de vérifier que votre fonction est correcte, pensez à exécuter votre programme.

Quelques explications pour cet algorithme :

* debut vaut 0 et fin vaut taille de tab - 1 car ce sont les premiers et derniers indices qui existent dans un tableau.
* On renvoie False à la toute fin car cela signifie que l'on a pas trouvé notre valeur.
* Si val est inférieure à val_milieu, cela signifie qu'on doit chercher à gauche. Donc le debut ne change pas. Par contre, la fin change : on s'arrête au milieu du tableau - 1.
*  Si val est supérieure à val_milieu, cela signifie qu'on doit chercher à droite. Donc la fin ne change pas. Par contre, le debut change : on commence au milieu du tableau + 1.


## III. Coût

On rappelle que le **coût** d'un algorithme est le nombre d'«&nbsp;étapes&nbsp;» effectuées.

Pour la recherche dichotomique, comme on travaille sur les tableaux, une étape consiste à accéder à une nouvelle valeur.

Après avoir regardé une valeur, on ne s'intéresse à chaque fois plus qu'à *la moitié* du tableau d'avant.

Si on note `n` la taille du tableau, cela signifie que la fois suivante on travaille sur un tableau de taille $`\frac{n}{2}`$, puis sur un tableau de taille $`\frac{n}{4}`$, puis $`\frac{n}{8}`$, etc.

Un tel algorithme a dit coût d'ordre **$`log_2(n)`$**, donc noté **O($`log_2(n)`$)**. On dit que l'algorithme est **logarithmique**.

> **exercice 8 :**  
> Quel est donc le coût de la recherche dichotomique sur le tableau [0, 2, 3, 4, 10, 11, 17, 38] ?

*Remarque : pour ceux qui choisissent de continuer les maths en Terminale, vous étudierez en détail le logarithme.*

Pour calculer un $`log_2`$, c'est assez simple : c'est l'inverse de la puissance de 2 !

Par exemple, calculons $`log_2(8)`$ : on sait que $`8 = 2^3`$, donc $`log_2(8) = 3`$.

> **exercice 9 :**  
> Calculez $`log_2(16)`$ et $`log_2(64)`$.

> **exercice 10 :**  
> En calculant maintenant, quel est donc le coût de la recherche dichotomique sur le tableau [0, 2, 3, 4, 10, 11, 17, 38] ?


## IV. Terminaison & correction

*Rappel : La **terminaison** d'un algorithme est le fait que l'algorithme se termine bien et donc ne boucle pas à l'infini. Ce qui nous permet d'être certain de la terminaison est appelé **variant** de boucle. Un variant est un **entier qui va de a à b en se rapprochant de b à chaque étape** (étape = tour de boucle).*

> **exercice 11 :**  
> Trouvez le variant pour l'algorithme de recherche dichotomique.

*Rappel : La **correction** d'un algorithme est l'assurance qu'il réalise bien ce pourquoi il est fait. Ce qui garantit cette correction est appelé **invariant** : c'est une propriété qui doit rester vraie du début à la fin de l'algorithme.*

> **exercice 12 :**  
> Justifiez que "la valeur que je recherche se situe forcément entre l'indice `debut` et l'indice `fin`" est un invariant pour la recherche dichotomique.

## Pour aller plus loin

> **exercice 13 : entraînement au principe**  
> Pour chacune des situations suivantes, dessinez un schéma représentant la recherche dichotomique :
> * recherche de -1 dans [-4, -3, 0, 1, 2, 7, 8]
> * recherche de 2 dans [1, 3, 5, 7, 9, 11]
> * recherche de 10 dans [1, 2, 3, 4, 5, 6, 7, 8, 9]

> **exercice 14 : entraînement à l'algorithme**  
> Écrivez un algorithme de recherche dichotomique qui renvoie l'indice du nombre recherché (au lieu de True/False).

> **exercice 15 :**  
> En utilisant un exemple de tableau, montrez que la recherche dichotomique (en O($`log_2(n)`$)) est plus efficace que la recherche classique (en O(n)).

> **exercice 16 :**  
> Dessinez le tableau d'évolution d'état de la mémoire (avec quatre colonnes : debut, fin, milieu et val_milieu) pour la recherche_dichotomique de 11 dans [1, 3, 4, 7, 8, 11, 12, 13, 20].

> **exercice 17 :**  
> Recherchez rapidement en quoi consiste une recherche trichotomique.  
> À votre avis, serait-elle plus ou moins efficace que la recherche dichotomique ?

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
