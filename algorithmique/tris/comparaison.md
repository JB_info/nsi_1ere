# Algorithmique : comparaison des tris

Nous avons vu deux algorithmes de tri différents :
* le tri par sélection
* le tri par insertion

Nous avons établi leur coût :
* tri par sélection en O($`n^2`$)
* tri par insertion allant de O(n) à O($`n^2`$)

Nous allons essayer de mieux comprendre d'où viennent ces coûts et comparer l'efficacité des deux tris.

## I. À la main

Pour trouver le coût d'un tri, nous allons nous mettre à la place de l'ordinateur.

L'ordinateur ne "voit" pas les cartes comme nous. Il est incapable de savoir ce qu'il y a dans *tout* le tableau, il ne peut regarder qu'*une* carte à la fois.

Pour nous mettre à la place de l'ordinateur, nous allons donc réaliser des tris mais avec les cartes retournées, faces cachées.

### Par sélection

> **exercice 1 :**  
> 1) Placez les cartes devant vous comme ceci :
> ![selection](img/selection_1.png)
> 
> 2) Retournez les cartes, faces contre la table :
> ![faces cachées](img/faces_cachees.png)
> 
> Nous pouvons maintenant effectuer un tri par sélection du point de vue de l'ordinateur. Pour trouver le coût, nous allons compter "+1" à chaque fois que l'on retourne une carte.
> 
> Commençons. Il faut d'abord trouver la carte la plus petite. Rappelez-vous comment l'algorithme fait cela :
> ![recherche mini](img/minimum/animation.gif)
> 
> 3) Réalisez cette étape en comptant "+1" à chaque fois que vous retournez une nouvelle carte :
> * vous retournez la première carte, vous constatez que c'est un 6, vous comptez +1 et vous la reposez face visible car elle est plus petite
> * puis vous retournez la deuxième carte, vous constatez que c'est un 10, vous comptez +2 et vous la reposez face contre la table car elle plus grande
> * puis vous retournez la troisième carte, vous constatez que c'est un 5, vous comptez +3 et vous la reposez face visible car elle plus petite et vous remettez alors 6 face contre la table
> * ... continuez !
> * Arrivez-vous bien à +10 ?
> 
> Vous devez à ce stade avoir ceci devant vous :
> ![echange](img/echange_cache.png)
> Il faut maintenant échanger la carte plus petite avec la première :
> ![echange](img/echange_cache_2.png)
> 
> 4) Faîtes l'échange. Pourquoi n'avez vous pas eu besoin de retournez la première carte ?
> 
> Vous devez donc maintenant avoir ceci devant vous :
> ![echange](img/echange_cache_3.png)
> 
> 5) Recommencez l'étape de recherche de la plus petite carte (on était arrivé au coût "+10" avant). À quel coût sommes-nous maintenant ?
> 6) Faîtes l'échange puis terminez le tri en continuant de compter les cartes que vous devez retourner.
> 7) Arrivez-vous bien à +55 ? Calculez le coût théorique O($`n^2`$). Comparez les deux résultats.

> **exercice 2 :**  
> 1) Placez les cartes devant vous dans l'ordre décroissant :
> ![selection](img/selection_2.png)
> 
> 2) Retournez les cartes, faces contre la table.
> 
> 3) Effectuez un tri par sélection en comptant +1 à chaque fois que vous retournez une carte (comme dans l'exercice précédent).
> 4) Quel coût avez-vous obtenu ? Comparez avec le coût trouvé à l'exercice précédent.

> **exercice 3 :**  
> 1) Placez les cartes devant vous dans l'ordre croissant :
> ![selection](img/selection_3.png)
> 
> 2) Retournez les cartes, faces contre la table.
> 
> 3) Effectuez un tri par sélection en comptant +1 à chaque fois que vous retournez une carte (comme dans les exercices précédents).
> 4) Quel coût avez-vous obtenu ? Comparez avec les coûts trouvé à l'exercice précédent.

Nous avons calculé le coût du tri par sélection avec tous les cas possibles :
* des cartes dans un ordre aléatoire
* des cartes dans l'ordre décroissant
* des cartes dans l'ordre croissant

Et nous avons trouvé le même coût à chaque fois !  
On peut donc dire que **l'ordre de départ des cartes n'a pas d'influence sur le coût du tri par sélection**.

### Par insertion

Peut-on dire la même chose du tri par insertion ? Vérifions !

> **exercice 4:**  
> 1) Placez les cartes devant vous dans l'ordre suivant :
> ![selection](img/insertion_1.png)
> 
> 2) Retournez les cartes, faces contre la table :
> ![faces cachées](img/faces_cachees.png)
> 
> 3) Effectuez un tri par insertion en comptant +1 à chaque fois que vous retournez une carte :
> * on retourne la première carte (+1), c'est un 6, il n'y a rien à décaler donc on la repose face cachée
> * on retourne la deuxième carte (+2), c'est un 3, on regarde la carte à sa gauche (+3), 6 est plus grand donc on décale le 6 et on replace le 3 face cachée
> * on retourne la troisième carte (+4), c'est un 5, on regarde la carte à sa gauche (+5), 6 est plus grand donc on décale le 6, on regarde encore à gauche (+6), 3 est plus petit donc on ne décale pas et on replace le 5 face cachée
> * ...
> 
> 4) Quel coût avez-vous obtenu ?


> **exercice 5 :**  
> 1) Placez les cartes devant vous dans l'ordre décroissant :
> ![selection](img/selection_2.png)
> 
> 2) Retournez les cartes, faces contre la table.
> 
> 3) Effectuez un tri par insertion en comptant +1 à chaque fois que vous retournez une carte (comme dans l'exercice précédent).
> 4) Quel coût avez-vous obtenu ? Comparez avec le coût trouvé à l'exercice précédent.

> **exercice 6 :**  
> 1) Placez les cartes devant vous dans l'ordre croissant :
> ![selection](img/selection_3.png)
> 
> 2) Retournez les cartes, faces contre la table.
> 
> 3) Effectuez un tri par insertion en comptant +1 à chaque fois que vous retournez une carte (comme dans les exercices précédents).
> 4) Quel coût avez-vous obtenu ? Comparez avec les coûts trouvé à l'exercice précédent.

Nous avons à nouveau calculé le coût du tri par sélection avec tous les cas possibles :
* des cartes dans un ordre aléatoire
* des cartes dans l'ordre décroissant
* des cartes dans l'ordre croissant

Et nous n'avons pas trouvé le même coût à chaque fois !  
On peut donc dire que **l'ordre de départ des cartes a une influence sur le coût du tri par insertion**.

> **exercice 7 :**  
> 1) Quel ordre a donné le meilleur coût ? Comparez avec le coût théorique O($`n`$) du meilleur des cas.
> 2) Quel ordre a donné le pire coût ? Comparez avec le coût théorique O($`n^2`$) du pire des cas.

## II. Graphique avec Python

Tous les calculs précédents ont été effectués sur des paquets de 10 cartes. Pour être plus rigoureux, il faut tester sur des paquets de différentes tailles.

Évidemment, ce serait beaucoup trop long à faire à la main, nous allons donc utiliser Python pour dessiner des courbes symbolisant le coût en fonction de la taille du paquet.

### Générer des tableaux

On va avoir besoin de générer des tableaux de différentes tailles.

> **exercice 8 :**  
> Écrivez une fonction `croissant(n)` qui renvoie un tableau contenant des nombres dans l'ordre croissant allant de 1 à n.  
> *(par exemple croissant(10) doit renvoyer [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])*

> **exercice 9 :**  
> Écrivez une fonction `decroissant(n)` qui renvoie un tableau contenant des nombres dans l'ordre décroissant allant de n à 1.  
> *(par exemple decroissant(10) doit renvoyer [10, 9, 8, 7, 6, 5, 4, 3, 2, 1])*


### Tracer les courbes

> **exercice 10 :**  
> * Téléchargez [ce fichier Python](graphiques.py) *à un endroit que vous allez retrouver*.
> * Copiez-y en haut vos fonctions `croissant` et `decroissant`.
> * Exécutez le programme. *(s'il vous indique une erreur "No module named Matplotlib" appelez le professeur)*
> * Dans le même dossier que là où vous avez enregistré le fichier Python doivent normalement être apparues 2 images.

> **exercice 11 :**  
> * Ouvrez l'image `selection.png`.
> * Pourquoi n'y a-t-il qu'une courbe de visible alors qu'on en a dessiné 3 ?
> * Vérifiez que le coût trouvé précédemment pour les tableaux de taille 10 (+55) est correct.
> * La courbe correspond-elle bien à la *forme* d'une courbe $`n^2`$ (c'est-à-dire $`f(x) = x^2`$) ?

> **exercice 12 :**  
> * Ouvrez l'image `insertion.png`.
> * Pourquoi y a-t-il bien 3 courbes cette fois ?
> * Vérifiez que le coût trouvé précédemment pour les tableaux de taille 10 est correct pour chacune des 3 courbes.
> * La courbe décroissant correspond-elle bien à la *forme* d'une courbe $`n^2`$ (c'est-à-dire $`f(x) = x^2`$) ?
> * La courbe croissant correspond-elle bien à la *forme* d'une courbe $`n`$ (c'est-à-dire $`f(x) = x`$) ?
> * Pourquoi la courbe aléatoire est-elle forcément située entre les 2 autres ?

> **Pour aller plus loin :**
> * Trouvez la ligne de code du programme qui détermine la taille des paquets de cartes que l'on va trier.
> * Modifiez cette ligne pour trier des paquets allant de 170 à 200 cartes.
> * Exécutez le programme et regardez les nouvelles courbes. Est-ce toujours cohérent ?

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
