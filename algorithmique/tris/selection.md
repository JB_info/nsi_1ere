# Algorithmique : tri par sélection

En informatique, nous avons très souvent besoin de travailler sur des tableaux triés. Il est donc primordial d'avoir des algorithmes de tri.

> **exercice 1 :**  
> Donnez un exemple d'algorithme de recherche qui ne fonctionne que sur des tableaux triés.

Nous allons voir aujourd'hui une première façon de trier un tableau, le **tri par sélection**.

## I. Principe

Nous allons nous servir d'un jeu de cartes comme exemple.

Le principe du tri par sélection est de :
* **sélectionner la plus petite carte**
* **échanger la plus petite carte avec la première carte du paquet**
* **recommencer avec le reste des cartes jusqu'à ce que tout soit trié**

> **exercice 2 :**  
> 1) Placez les cartes devant vous comme ceci :
> ![mélange](img/selection_1.png)
> 
> La première étape est de sélectionner la plus petite carte. Pour cela on regarde toutes les cartes une par une en conservant la plus petite de côté :
> 
> ![animation minimum](img/minimum/animation.gif)
> 
> 2) Reproduisez ces étapes (vous pouvez utiliser un stylo pour symboliser la flèche).
> 
> La deuxième étape consiste à échanger la plus petite carte avec la première :
> ![premier échange](img/echange_1.png)
> 
> Comme vous pouvez le voir, après avoir échangé les 2 cartes l'As se retrouve devant, donc il est bien rangé : on n'a plus besoin de s'en occuper.
> 
> 3) Échangez les 2 cartes comme dans l'image ci-dessus.
> 
> Il faut maintenant recommencer avec le reste du tableau :
> ![sélection du 2](img/minimum_2.png)
> 
> 4) En utilisant un stylo pour représenter la flèche, détaillez les étapes permettant de sélectionner le minimum.
> 5) Échangez les deux cartes appropriées.
> 6) Reproduisez ces étapes jusqu'à ce que toutes les cartes soit trié.  
> Vous devriez avoir trouvé l'évolution suivante :
> ![tri selection](img/selection.png)
> 

> **exercice 3 :**  
> 1) Placez les cartes devant vous dans l'ordre décroissant :
> ![selection décroissant](img/selection_2.png)
> 
> 2) En utilisant la méthode du tri par sélection, triez les cartes dans l'ordre croissant.
> 
> (sur votre cahier, vous pourrez résumer le tri en n'écrivant que les évolutions comme dans l'image à la fin de l'exercice précédent)


## II. Terminaison & Correction

*Rappel : La terminaison d'un algorithme est le fait que l'algorithme se termine bien et donc ne boucle pas à l'infini. Ce qui nous permet d'être certain de la terminaison est appelé variant de boucle. Un variant est un entier qui va de a à b en se rapprochant de b à chaque étape (étape = tour de boucle).*

Le **variant** qui permet de justifier que l'algorithme de tri par sélection se termine est **le nombre de cartes triées**.  

> **exercice 4 :**  
> Pour justifiez cela, il faut montrer que le nombre de cartes triées va bien de a à b en se rapprochant de b à chaque étape.  
> Reprenons notre tout premier tri :
> ![tri selection](img/selection.png)
> Le nombre de cartes triées est entouré en vert à chaque fois.
> 1) Combien y a-t-il de cartes triées au départ ?
> 2) Combien y a-t-il de cartes triées à la fin ?
> 3) Si on prend :
> * `a` = le nombre de cartes triées au départ 
> * `b` = le nombre de cartes triées à la fin
> 
> Peut-on affirmer qu'on se rapproche de `b` à chaque étape ?

*Rappel : La correction d'un algorithme est l'assurance qu'il réalise bien ce pourquoi il est fait. Ce qui garantit cette correction est appelé invariant : c'est une propriété qui doit rester vraie du début à la fin de l'algorithme.*

L'**invariant** qui permet de justifier que le tri par sélection est correct est **"à l'étape `i`, on a déjà trié `i` cartes"**.

> **exercice 5 :**  
> Pour justifiez cela, il faut montrer que cette propriété est *toujours* vraie (c'est-à-dire qu'elle est vraie *AVANT*, *PENDANT*, et *APRES*).
> Reprenons notre tout premier tri :
> ![tri selection](img/selection.png)
> 1) *AVANT* : Le départ est l'étape `i = 0`. Peut-on donc affirmer que "à l'étape `0`, on a déjà trié `0` cartes" ?
> 2) *PENDANT* : Si on prend une étape `i` quelconque, l'invariant est-il vrai ? (aidez vous de l'image)
> 3) *APRES* : La fin du tri est l'étape `i = 10`. Peut-on donc affirmer que "à l'étape `10`, on a déjà trié `10` cartes" ?
> 4) Justifiez donc que l'invariant "à l'étape `i`, on a déjà trié `i` cartes" est correct pour n'importe quel tri par sélection.

## III. Algorithme

Avec Python, on ne va pas trier des cartes, mais des tableaux contenant des entiers. Mais le principe reste le même.

Par exemple le tableau `[6, 10, 5, 4, 1, 3, 2, 9, 7, 8]` correspond aux cartes suivantes :
![mélange](img/selection_1.png)

> **exercice 6 :**  
> Donnez le tableau qui correspond aux cartes suivantes :
> ![décroissant](img/selection_2.png)

Avant de pouvoir réaliser un tri par sélection, nous allons avoir besoin de 2 autres fonctions :
* une pour trouver où se situe la plus petite carte
* une pour échanger deux cartes

> **exercice 7 :**  
> Donnez le code de la fonction suivante, qui renvoie l'indice du minimum du tableau :
> ```python
> def indice_minimum(tab):
>     '''
>     Renvoie l'indice de l'élement minimum du tableau.
>     :param tab: (list) un tableau non vide
>     :return: (int) l'indice du minimum
> 
>     >>> indice_minimum([4, 5, 7, 8, 0, 2])
>     4
>     >>> indice_minimum([-4, -5, -7, -8, -2])
>     3
>     >>> indice_minimum([4, 5, -7, 8, 0, 2])
>     2
>     '''
>     
> ```
> 
> N'oubliez pas les 3 lignes nécessaires pour les tests :
> ```python
> if __name__ == "__main__":
>     import doctest
>     doctest.testmod(verbose=True)
> ```

> **exercice 8 :**  
> Complétez le code de la fonction suivante, qui permet d'échanger deux éléments :
> ```python
> def echange(tab, indice_1, indice_2):
>     '''
>     Echange les élements d'indices indice_1 et indice_2 du tableau.
>     :param tab: (list) un tableau non vide
>     :param indice_1: (int) l'indice du premier élément
>     :param indice_2: (int) l'indice du deuxième élément
>     :return: (list) le tableau avec deux éléments échangés
>     
>     >>> echange([1, 7, 4, 5, 8, 2], 2, 4)
>     [1, 7, 8, 5, 4, 2]
>     >>> echange([2, 3, 4, 5, 6], 0, 3)
>     [5, 3, 4, 2, 6]
>     '''
>     valeur_1 = tab[indice_1]
>     valeur_2 = tab[indice_2]
>     tab[indice_1] = # COMPLETEZ ICI
>     tab[indice_2] = # COMPLETEZ ICI
>     return tab
> ```

Une fois qu'une carte est triée, on ne doit plus s'intéresser à elle. Il va donc falloir apprendre à ne garder qu'une partie du tableau.

> **exercice 9 :**  
> Dites ce que renvoient les instructions suivantes :
> ```python
> >>> tab = [6, 10, 5, 4, 1, 3, 2, 9, 7, 8]
> >>> tab[1:]
> ?
> >>> tab[4:]
> ?
> >>> tab[9:]
> ?
> ```

La notation `tab[x:]` permet de renvoyer le tableau sans les `x` premiers éléments.

Voici donc le code de la fonction permettant de trier un tableau par sélection :

```python
def tri_selection(tab):
    '''
    Effectue un tri par sélection du tableau.
    :param tab: (list) le tableau à trier
    :return: (list) le tableau trié

    '''
    indice_1 = 0
    for x in tab:
        reste_tab = tab[indice_1:]
        indice_2 = indice_1 + indice_minimum(reste_tab)
        tab = echange(tab, indice_1, indice_2)
        indice_1 = indice_1 + 1
    return tab
```

> **exercice 10 :**  
> Ajoutez un doctest, puis exécutez le programme pour vérifier que tout fonctionne.

> **exercice 11 :**  
> Expliquez *ligne par ligne* ce que fait la fonction `tri_selection`.


## IV. Coût

On rappelle que le **coût** d'un algorithme est le nombre d'«&nbsp;étapes&nbsp;» effectuées.

Reprenons notre tout premier tri :
![tri selection](img/selection.png)

Il y a 10 cartes à trier et 10 étapes.

Mais chaque étape se fait elle-même en plusieurs étapes :
![animation minimum](img/minimum/animation.gif)

Pour chercher la carte la plus petite parmi les 10 on doit à nouveau faire 10 étapes.

Au final, on a donc un coût d'ordre $`10 \times 10`$, soit $`10^2`$.

De manière générale, pour un tableau de taille `n`, cela nous donne donc un coût d'ordre $`n^2`$, noté **O($`n^2`$)**. Un algorithme qui a une complexité en O($`n^2`$) est dit **quadratique**.

> **exercice 12 :**  
> Donnez donc le coût pour un tri par sélection des cartes suivantes :
> ![décroissant](img/selection_2.png)
> Et pour celles-ci ?
> ![croissant](img/selection_3.png)

Globalement, le tri par sélection n'est pas considéré comme très efficace, il existe des méthodes de tri avec de meilleurs coûts. Nous reviendrons dessus plus tard.

> **Pour aller plus loin : tri décroissant**  
> Il existe une variante du tri par sélection qui permet de trier un tableau dans l'ordre décroissant.
> 1) En vous servant des cartes, réfléchissez à la méthode permettant de faire ce tri.
> 2) Parmi les 3 fonctions écrites, quelle est celle qu'il faudrait modifier pour réaliser un tri décroissant ?
> 3) Programmez le !

> **Pour aller plus loin : peut-on tout trier ?**  
> Notre fonction `tri_selection` peut elle être appliquée à n'importe quel tableau ? Essayez avec :
> * un tableau de chaînes de caractères
> * un tableau de flottants
> * un tableau de tableau
> 
> ... et expliquez ce qu'il se passe / pourrait se passer à chaque fois.
> 

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
