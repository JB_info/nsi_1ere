from matplotlib.pyplot import *
from random import shuffle
import math

global cpt_selection
global cpt_insertion

# COPIEZ ICI VOTRE FONCTION CROISSANT :


# COPIEZ ICI VOTRE FONCTION DECROISSANT :



def aleatoire(n):
    """
    Renvoie un tableau de nombres allant de 1 à n dans un ordre aléatoire.
    :param n: (int) la plus grande carte
    :return: (list) un tableau de n cartes
    """
    l = croissant(n)
    shuffle(l)
    return l

def indice_minimum(tab):
    '''
    Renvoie l'indice de l'élement minimum du tableau.
    :param tab: (list) un tableau non vide
    :return: (int) l'indice du minimum

    >>> indice_minimum([4, 5, 7, 8, 0, 2])
    4
    >>> indice_minimum([-4, -5, -7, -8, -2])
    3
    >>> indice_minimum([4, 5, -7, 8, 0, 2])
    2
    '''
    minimum = +math.inf
    indice = 0
    resultat = 0
    for x in tab:
        global cpt_selection
        cpt_selection = cpt_selection + 1
        if x < minimum:
            minimum = x
            resultat = indice
        indice = indice + 1
    return resultat

def echange(tab, indice_1, indice_2):
    '''
    Echange les élements d'indices indice_1 et indice_2 du tableau.
    :param tab: (list) un tableau non vide
    :param indice_1: (int) l'indice du premier élément
    :param indice_2: (int) l'indice du deuxième élément
    :return: (list) le tableau avec deux éléments échangés
    
    >>> echange([1, 7, 4, 5, 8, 2], 2, 4)
    [1, 7, 8, 5, 4, 2]
    >>> echange([2, 3, 4, 5, 6], 0, 3)
    [5, 3, 4, 2, 6]
    '''
    valeur_1 = tab[indice_1]
    valeur_2 = tab[indice_2]
    
    tab[indice_1] = valeur_2
    tab[indice_2] = valeur_1
    
    return tab
    

def tri_selection(tab):
    '''
    Effectue un tri par sélection du tableau.
    :param tab: (list) le tableau à trier
    :return: (list) le tableau trié

    >>> tri_selection([8, 5, 2, 6, 9, 3, 1, 4, 0, 7])
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    '''
    indice_1 = 0
    
    for x in tab:
        reste_tab = tab[indice_1:]
        indice_2 = indice_1 + indice_minimum(reste_tab)
        tab = echange(tab, indice_1, indice_2)
        indice_1 = indice_1 + 1
    
    return tab

def decaler_precedents(tab, indice, x):
    '''
    Décale vers la droite les valeurs précédentes du tableau qui sont supérieures à x.
    On renvoie la position vide finale.
    
    :param tab: (list) un tableau
    :param indice: (int) l'indice de x (on ne doit décaler que les valeurs à gauche de l'indice)
    :param x: la valeur à insérer
    :return: (int) la position vide à la fin des décalages
    '''
    global cpt_insertion
    cpt_insertion = cpt_insertion + 1
    while indice > 0 and tab[indice - 1] > x:
        cpt_insertion = cpt_insertion + 1
        tab[indice] = tab[indice - 1]
        indice = indice - 1
    return indice

def tri_insertion(tab):
    '''
    Effectue un tri par insertion du tableau.
    :param tab: (list) le tableau à trier
    :return: (list) le tableau trié

    >>> tri_insertion([8, 5, 2, 6, 9, 3, 1, 4, 0, 7])
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    '''
    indice = 0
    for x in tab:
        position_x = decaler_precedents(tab, indice, x)
        tab[position_x] = x
        indice = indice + 1
    return tab

# -----------------------------------------------------------------
# COURBES

# on va trier des paquets allant de 1 à 30 cartes
abscisses = [i for i in range(1, 30+1)]
y_croissant_selection = []
y_decroissant_selection = []
y_aleatoire_selection = []
y_croissant_insertion = []
y_decroissant_insertion = []
y_aleatoire_insertion = []

for x in abscisses:
    
    # on effectue un tri par sélection et un par insertion
    # de tableaux dans l'ordre croissant
    # pour calculer les coûts obtenus
    cpt_selection = 0
    cpt_insertion = 0
    tri_selection(croissant(x))
    tri_insertion(croissant(x))
    y_croissant_selection.append(cpt_selection)
    y_croissant_insertion.append(cpt_insertion)
    
    # on effectue un tri par sélection et un par insertion
    # de tableaux dans l'ordre décroissant
    # pour calculer les coûts obtenus
    cpt_selection = 0
    cpt_insertion = 0
    tri_selection(decroissant(x))
    tri_insertion(decroissant(x))
    y_decroissant_selection.append(cpt_selection)
    y_decroissant_insertion.append(cpt_insertion)
    
    # on effectue 100 tris par sélection et par insertion
    # de tableaux dans un ordre aléatoire
    # pour calculer la moyenne des coûts obtenus
    cpt_selection = 0
    cpt_insertion = 0
    for i in range(100):
        tri_selection(aleatoire(x))
        tri_insertion(aleatoire(x))
    y_aleatoire_selection.append(cpt_selection/100)
    y_aleatoire_insertion.append(cpt_insertion/100)
    

# permet de tracer le graphique pour le tri par sélection
figure(1)
title("sélection")
xlabel('nombre de cartes')
ylabel('coût')
plot(abscisses, y_croissant_selection, label='croissant')
plot(abscisses, y_decroissant_selection, label='decroissant')
plot(abscisses, y_aleatoire_selection, label='aleatoire')
legend()
savefig('selection.png')

# permet de tracer le graphique pour le tri par insertion
figure(2)
title("insertion")
xlabel('nombre de cartes')
ylabel('coût')
plot(abscisses, y_croissant_insertion, label='croissant')
plot(abscisses, y_decroissant_insertion, label='decroissant')
plot(abscisses, y_aleatoire_insertion, label='aleatoire')
legend()
savefig('insertion.png')
    

# effectue les tests
if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)