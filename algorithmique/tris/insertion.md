# Algorithmique : tri par insertion

Il existe de très nombreuses façons de trier des nombres.
Certaines sont plus efficaces que d'autres.

> **exercice 1 :**  
> Donnez le nom d'un algorithme de tri que vous connaissez déjà. Quel est son coût ?

Nous allons voir aujourd'hui une deuxième façon de trier un tableau, qui peut dans certains cas être plus efficace, le **tri par insertion**.

## I. Principe

Nous allons à nouveau nous servir d'un jeu de cartes comme exemple.

Le principe du tri par insertion est de :
* **parcourir une par une chaque carte du paquet**
* **décaler vers la droite toutes les cartes précédentes qui sont plus grandes qu'elle**
* **insérer la carte à la position trouvée une fois toutes les autres décalées**

> **exercice 2 :**  
> 1) Placez les cartes devant vous comme ceci :
> ![insertion mélange](img/insertion_1.png)
> On va parcourir une par une chaque carte du paquet.
> On commence donc par la première carte :
> ![insere 1](img/inserer/1.png)
> 2) En utilisant un stylo pour représenter la flèche, reproduisez l'image ci-dessus.
> 
> Il faut ensuite décaler vers la droite toutes les cartes précédentes qui sont plus grandes. Ici, il n'y a aucune cartes précédentes donc on ne fait rien.
> 
> Pour finir, on insère la carte dans la position vide :
> ![insere](img/inserer/2.png)
> Puis on passe à la carte suivante :
> ![insere](img/inserer/3.png)
> On décale à droite toutes les cartes plus grandes : cette fois, il y en a une à décaler&nbsp;!
> ![insere](img/inserer/4.gif)
> Puis on insère la carte à la position vide :
> ![insere](img/inserer/5.png)
>
> 3) Insérer la carte 3 à la bonne place comme sur les images ci-dessus.
> 
> À nouveau, on passe à la carte suivante :
> ![insere](img/inserer/6.png)
> On décale les cartes plus grandes :
> ![insere](img/inserer/7.gif)
>
> 4) Pourquoi ne décale-t-on que le 6 et non le 3 ?
> 
> Et enfin on insère la carte à la position vide :
> ![insere](img/inserer/8.png)
> On continue encore avec la carte suivante :
> ![insere](img/inserer/9.gif)
>
> 5) Terminez le tri par insertion du paquet.  
> 
> Vous devriez avoir trouvé l'évolution suivante :
> ![insere](img/insertion.png)

> **exercice 3 :**  
> 1) Placez les cartes devant vous dans l'ordre décroissant :
> ![décroissant](img/selection_2.png)
> 2) En utilisant la méthode du tri par insertion, triez les cartes dans l'ordre croissant.  
> 
> (sur votre cahier, vous pourrez résumer le tri en n'écrivant que les évolutions comme dans l'image à la fin de l'exercice précédent)


## II. Terminaison et Correction

*Rappel : La terminaison d'un algorithme est le fait que l'algorithme se termine bien et donc ne boucle pas à l'infini. Ce qui nous permet d'être certain de la terminaison est appelé variant de boucle. Un variant est un entier qui va de a à b en se rapprochant de b à chaque étape (étape = tour de boucle).*

Le **variant** qui permet de justifier que l'algorithme de tri par sélection se termine est **le nombre de cartes triées**.

> **exercice 4 :**  
> Pour justifiez cela, il faut montrer que le nombre de cartes triées va bien de a à b en se rapprochant de b à chaque étape.  
> Reprenons notre tout premier tri :
> ![tri selection](img/insertion.png)
> Le nombre de cartes triées est entouré en vert à chaque fois.
> 1) Combien y a-t-il de cartes triées au départ ?
> 2) Combien y a-t-il de cartes triées à la fin ?
> 3) Si on prend :
> * `a` = le nombre de cartes triées au départ 
> * `b` = le nombre de cartes triées à la fin
> 
> Peut-on affirmer qu'on se rapproche de `b` à chaque étape ?

*Rappel : La correction d'un algorithme est l'assurance qu'il réalise bien ce pourquoi il est fait. Ce qui garantit cette correction est appelé invariant : c'est une propriété qui doit rester vraie du début à la fin de l'algorithme.*

L'**invariant** qui permet de justifier que le tri par insertion est correct est **"à l'étape `i`, on a déjà trié `i` cartes"**.

> **exercice 5 :**  
> Pour justifiez cela, il faut montrer que cette propriété est *toujours* vraie (c'est-à-dire qu'elle est vraie *AVANT*, *PENDANT*, et *APRES*).
> Reprenons notre tout premier tri :
> ![tri selection](img/insertion.png)
> 1) *AVANT* : Le départ est l'étape `i = 0`. Peut-on donc affirmer que "à l'étape `0`, on a déjà trié `0` carte" ?
> 2) *PENDANT* : Si on prend une étape `i` quelconque, l'invariant est-il vrai ? (aidez vous de l'image)
> 3) *APRES* : La fin du tri est l'étape `i = 10`. Peut-on donc affirmer que "à l'étape `10`, on a déjà trié `10` cartes" ?
> 4) Justifiez donc que l'invariant "à l'étape `i`, on a déjà trié `i` cartes" est correct pour n'importe quel tri par insertion.


## III. Algorithme

À nouveau, ce ne sont pas des cartes que nous allons trier, mais des tableaux.

> **exercice 6 :**  
> Donnez le tableau qui correspond aux cartes suivantes :
> ![décroissant](img/insertion_1.png)

Avant de pouvoir réaliser un tri par insertion, nous allons avoir besoin d'une autre fonction qui permet de décaler des cartes vers la droite.

Voici le code de cette fonction :
```python
def decaler_precedents(tab, indice, x):
    '''
    Décale vers la droite les valeurs précédentes du tableau qui sont supérieures à x.
    On renvoie la position vide finale.
    
    :param tab: (list) un tableau
    :param indice: (int) l'indice de x (on ne doit décaler que les valeurs à gauche de l'indice)
    :param x: la valeur à insérer
    :return: (int) la position vide à la fin des décalages
    '''
    while indice > 0 and tab[indice - 1] > x:
        tab[indice] = tab[indice - 1]
        indice = indice - 1
    return indice
```

Par exemple pour le tableau `[3, 5, 6, 4, 1, 10, 2, 9, 7, 8]`, on veut décaler les valeurs précédant x = 4 qui se situe à l'indice = 3 :
* on décale le 6 (indice 2) à droite
* on décale le 5 (indice 1) à droite
* 3 (indice 0) est inférieur donc on s'arrête là
* on renvoie l'indice = 1 car c'est à cet endroit qu'on devra remettre le x = 4 après

> **exercice 7 :**  
> Détaillez ce qu'il se passerait avec le tableau `[1, 3, 7, 8, 10, 4, 2, 9, 6, 5]` pour décaler les valeurs précédant x = 4 qui se situe à l'indice = 5.

> **exercice 8 :**  
> Expliquez *ligne par ligne* ce que fait la fonction `decaler_precedents`.

Nous pouvons donc maintenant donc écrire le tri par insertion.

Voici le pseudo-code :

```
tri_insertion (tab):
    indice <- 0
    pour chaque valeur x de tab :
        position_x <- decaler_precedents(tab, indice, x)
        mettre x à la position_x dans le tableau
        passer à l'indice suivant
    renvoyer le tableau
```

> **exercice 9 :**  
> Donnez le code de la fonction `tri_insertion` suivante (en vous appuyant sur le pseudo-code au dessus) :
> ```python
> def tri_insertion(tab):
>     '''
>     Effectue un tri par insertion du tableau.
>     :param tab: (list) le tableau à trier
>     :return: (list) le tableau trié
>     '''
> ```

> **exercice 10 :**  
> Sans oublier d'ajouter les 3 lignes suivantes à la fin de votre fichier, ajoutez un doctest à la fonction `tri_selection`.
> ```python
> if __name__ == "__main__":
>     import doctest
>     doctest.testmod(verbose=True)
> ```

## IV. Coût

On rappelle que le **coût** d'un algorithme est le nombre d'«&nbsp;étapes&nbsp;» effectuées.

Reprenons notre tout premier tri :
![tri selection](img/insertion.png)

Il y a 10 cartes à trier et 10 étapes.

Mais chaque étape se fait elle-même en plusieurs étapes :

![insere](img/inserer/9.gif)

Pour insérer le 4 ci-dessus, il a fallu faire 2 décalages.

Et le nombre de décalages à faire dépend de la carte à insérer ! Impossible à prévoir donc...

> **exercice 11 :**  
> Pour trier le paquet suivant, combien de décalages va t-il falloir faire à chaque fois ?
> ![croissant](img/selection_3.png)

Dans le **meilleur des cas** (tableau déjà trié dans l'ordre croissant), on a aucun décalage à faire donc il y a juste `n` étapes. Le coût est donc **O(n)**.

> **exercice 12 :**  
> Pour trier le paquet suivant, combien de décalages va t-il falloir faire à chaque fois ?
> ![décroissant](img/selection_2.png)

Dans le **pire des cas** (tableau dans l'ordre décroissant), on doit décaler tout à gauche à chaque fois donc il y a `n` étapes qui font chacune `n` décalages. Le coût est donc **O($`n^2`$)**.

Au final, on peut donc dire que le tri par insertion a un coût allant de **O($`n`$)** à **O($`n^2`$)**.

> **exercice 13 :**  
> Parmi les propositions suivantes, laquelle permet donc de qualifier l'algorithme de tri par insertion ?
> * de linéaire à logarithmique
> * de logarithmique à quadratique
> * de linéaire à quadratique

Si vous avez terminé, vous pouvez aller faire l'[activité de comparaison des tris par sélection et par insertion](./comparaison.md).

> **Pour aller plus loin : tri décroissant**  
> Il existe une variante du tri par insertion qui permet de trier un tableau dans l'ordre décroissant.
> 1) En vous servant des cartes, réfléchissez à la méthode permettant de faire ce tri.
> 2) Parmi les 2 fonctions écrites, quelle est celle qu'il faudrait modifier pour réaliser un tri décroissant ?
> 3) Programmez le !

> **Pour aller plus loin : peut-on tout trier ?**  
> Notre fonction `tri_insertion` peut elle être appliquée à n'importe quel tableau ? Essayez avec :
> * un tableau de chaînes de caractères
> * un tableau de flottants
> * un tableau de tableau
> 
> ... et expliquez ce qu'il se passe / pourrait se passer à chaque fois.




---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
