# Algorithmique : algorithmes gloutons


## I. Pourquoi Glouton ?

### 1. Exemple

Imaginons un voyageur de commerce qui doit passer dans 15 villes afin de livrer ses produits. Comment rendre son trajet le plus court possible ?  
Il n'y a qu'une seule façon de le savoir : lister tous les trajets possibles, les mesurer et choisir le plus court.

Sur le papier, le principe est simple. Mais ce n'est pas réalisable. Comptons un peu :

* Considérons que le voyageur se situe dans l'une des villes pour commencer le circuit.
* Pour sa première étape, il lui reste 14 possibilités : le nombre de trajets possibles à ce stade est donc $`14`$
* Pour sa deuxième étape, il lui reste 13 possibilités : le nombre de trajets possibles à ce stade est donc $` 14 \times 13`$
* etc ...
* Pour son avant dernière étape, il ne lui reste que 2 possibilités : le nombre de trajets possibles à ce stade est donc $` 14 \times 13 \times ... \times 2`$
* Il n'a plus qu'à relier la ville restante pour terminer son périple.

> **exercice 1 :**  
> * Calculez donc (avec une calculatrice ou avec la console Python) combien de trajets sont possibles au total.
> * Est-ce envisageable de les lister un par un ?

### 2. Définition

Puisqu'il n'est pas possible de lister toutes les possibilités, il nous faut trouver une méthode conduisant à une réponse convenable. Celle-ci consistera à relier la ville la plus proche de la position du voyageur à chaque étape, c'est à dire à choisir une solution *localement meilleure* en espérant que la solution finale soit *proche de la meilleure solution au final*.

Notre algorithme va donc prendre, à chaque étape, le meilleur cas possible. C'est pour cela qu'on dit qu'il est glouton, gourmand en quelque sorte, qui illustre bien l'idée de cet algorithme qui est de choisir la plus grande valeur possible à chaque étape.

"Meilleure solution" est un terme subjectif, on utilise plutôt le terme *solution optimale*.

**Un algorithme glouton est donc un algorithme qui, à chaque étape, fait le choix qui semble le plus pertinent en espérant obtenir une solution optimale au problème.**

### 3. Optimalité

Il faut bien comprendre que choisir toujours le meilleur parti à chaque étape n'a pas forcément pour conséquence d'aboutir au meilleur résultat possible, c'est à dire à un résultat *optimal*.

Regardez l'arbre ci-dessous. Si on le parcours de façon gloutonne de manière à obtenir la plus grande somme possible, on ne peut parvenir à ce résultat optimal :

![glouton - source wikipédia - CCBYSA](https://upload.wikimedia.org/wikipedia/commons/8/8c/Greedy-search-path-example.gif)

> **exercice 2 :**  
> Expliquez ce qu'il se passe dans l'image ci-dessus.



## II. Le problème de rendu de monnaie

### 1. Enoncé

On dispose de pièces de monnaie (ou billets) dont les montants sont des nombres entiers de l’unité de monnaie : $`p_0, p_1, p_2, ..., p_n`$.  
Pour payer une certaine somme `S` (entier positif), on aimerait utiliser le moins de pièces/billets possibles.

*Exemple avec l'euro :*

Dans le système monétaire européen on utilise les montants suivants : $`p_0 = 200€, p_1 = 100€, p_2 = 50€, p_3 = 20€, p_4 = 10€, p_5 = 5€, p_6 = 2€, p_7 = 1€`$.

> **exercice 3 :**  
> 1. Peut-on payer n'importe quelle somme entière positive avec ce système ? Pourquoi ?
> 2. Comment payer la somme `S = 528€` en utilisant le moins de pièces possibles ?
> 3. N'y-a-t-il qu'une seule façon de payer cette somme ?
> 4. Pouvez-vous lister toutes les façons différentes de payer cette somme ?

L'algorithme glouton consiste ici à **prendre la plus grande pièce possible à chaque fois**.  
C'est peut-être déjà ce que vous faîtes quand vous devez payer quelque chose à la caisse ?


### 2. Implémentation

Nous allons implémenter cet algorithme avec Python.

> **exercice 4 :**  
> 1. Créez un fichier `glouton.py`.
> 2. Ajoutez-y cette ligne en haut : 
> ```python
> PIECES = [200, 100, 50, 20, 10, 5, 2, 1]
> ```
> 3. Quelle est le nom de la structure utilisée pour représenter les pièces ?
> 4. Quel est l'indice de la pièce au montant le plus grand ?

Nous aurons besoin d'une fonction de paramètre `somme_a_rendre` qui renvoie la plus grande pièce possible (celle qu'il faut rendre donc).  
Il faut donc parcourir toutes les pièces jusqu'à en trouver une qui soit inférieure ou égale à la somme à rendre.

> **exercice 5 :**  
> 1. Donnez le code de la fonction suivante :
> ```python
> def piece_a_rendre(somme_a_rendre):
>     # A COMPLETER
> ```
> 2. Ajoutez la documentation.
> 3. Ajoutez les doctests.

Nous pouvons maintenant écrire notre algorithme glouton. Il devra renvoyer un tableau contenant toutes les pièces à rendre.

> **exercice 6 :**  
> 1. Complétez la fonction suivante.
> ```python
> def rendu_monnaie(somme_a_rendre):
>     """
>     Applique l'algorithme glouton de rendu de monnaie.
>     :param somme_a_rendre: (int) la somme à rendre
>     :return: (list) un tableau contenant des pièces à rendre
>     
>     >>> rendu_monnaie(528)
>     [200, 200, 100, 20, 5, 2, 1]
>     >>> rendu_monnaie(154)
>     [100, 50, 2, 2]
>     >>> rendu_monnaie(0)
>     []
>     """
>     resultat = ?
>     while somme_a_rendre != ?:
>         piece = piece_a_rendre(somme_a_rendre)
>         resultat.?(piece)
>         somme_a_rendre = somme_a_rendre - ?
>     return resultat
> ```
> 2. Testez.


### 3. Optimalité


On se pose la question de savoir si cet algorithme est forcément optimal, c'est à dire utilise toujours aussi peu de pièces que possible.

*Exemple : ancien système monétaire britannique*

Ce système utilisait les montants suivants : $`p_0 = 60, p_1 = 30, p_2 = 24, p_3 = 12, p_4=6, p_5 = 2, p_6 = 1`$

> **exercice 7 :**  
> 1. Avec ces montants, peut-on payer n'importe quelle somme entière ?
> 2. Comment payer $`S = 48`$ avec le moins de pièces possibles ?
> 3. Quel est le résultat que l'algorithme glouton fournirait pour $`S = 48`$ ?
> 4. Est-ce optimal ?


## III. Problème du sac à dos

### 1. Enoncé

Vous devez ranger dans un sac des objets de valeurs différentes afin d'emporter avec vous une cargaison aussi précieuse que possible. Hélas, les objets sont plus au moins lourds et le poids maximum que vous pouvez porter dans votre sac sous peine de le déchirer est limité...

Voici, par exemple une liste d'objets à ranger dans un sac pouvant au maximum contenir **20kg**.

| Objets         | TV   | Tableau | Bijoux | Livres | Meubles | Ordinateur |
| -------------- | ---- | ------- | ------ | ------ | ------- | ---------- |
| effectif       | 3    | 5       | 2      | 10     | 2       | 2          |
| valeur         | 500€ | 250€    | 20€    | 10€    | 900€   | 490€       |
| poids unitaire | 10kg | 4kg     | 0,2kg  | 0.5kg  | 30kg    | 7kg        |

Il s'agit à nouveau d'un algorithme glouton. Nous choisissons à chaque étape l'objet qui a la meilleure densité.

> **exercice 8 :**  
> Complétez le tableau ci-dessous :
> 
> | Objets         |  TV  | Tableau | Bijoux | Livres | Meubles | Ordinateur |
> | -------------- | :--: | :-----: | :----: | :----: | :-----: | :--------: |
> | effectif       |  3   |    5    |   2    |   10   |    2    |     2      |
> | valeur         | 500€ |  240€   |  20€   |  10€   |  900€  |    490€    |
> | poids unitaire | 10kg |   4kg   | 0,2kg  | 0.5kg  |  30kg   |    7kg     |
> | densité (€/kg) |      |         |        |        |         |            |

Cette fois-ci, on a un critère en plus : l'effectif ! Ça veut dire qu'on ne peut pas remplir notre sac avec plein de bijoux : il n'y en a que 2.

> **exercice 9 :**  
> 1. Quel est l'objet qui sera mis dans le sac en premier ?
> 2. ... en deuxième ?
> 3. ... en troisième ?
> 4. Donnez le contenu final du sac pour un poids max de 20kg.


### 2. Pour aller plus loin : implémentation

> **exercice 10 :**  
> Expliquez en français l'algorithme qui permettrait de résoudre le problème du sac à dos.

En Python, nous modéliserons les objets en utilisant trois dictionnaires :

```python
VALEUR = {  'TV' : 500,
            'Tableaux' : 240,
            'Bijoux' : 20,
            'Livres' : 10,
            'Meubles' : 900, #2550
            'Ordinateurs' : 490
         }

POIDS = {   'TV' : 10,
            'Tableaux' : 4,
            'Bijoux' : 0.2,
            'Livres' : 0.5,
            'Meubles' : 30,
            'Ordinateurs' : 7
        }

EFFECTIF = {'TV' : 3,
            'Tableaux' : 5,
            'Bijoux' : 2,
            'Livres' : 10,
            'Meubles' : 2,
            'Ordinateurs' : 2
            }

```

> **exercice 11 :**  
> Créez un quatrième dictionnaire `DENSITE` contenant les valeurs appropriées.

> **exercice 12 :**  
> Complétez la fonction `lister_candidats`, de paramètre _poids_max_, le poids maximum autorisé qui renvoie la liste des objets qu'il est possible de mettre dans le sac sans dépasser le poids indiqué.
> ```python
> def lister_candidats(poids_max):
>     '''
>     Donne les objets qu'il est possible de mettre dans le sac sans dépasser le poids.
>     :param poids_max: (float) le poids à ne pas dépasser
>     :return: (list) la liste des candidats possibles
>     
>     >>> lister_candidats(1)
>     ['Bijoux', 'Livres']
>     >>> lister_candidats(9)
>     ['Tableaux', 'Bijoux', 'Livres', 'Ordinateurs']
>     '''
>     # COMPLETER ICI
>     ```

> **exercice 13 :**  
> Complétez la fonction `selectionner_meilleur_candidat`, de paramètre _candidats_, une liste **non vide** d'objets possibles, qui renvoie le meilleur candidat de la liste selon le critère de densité maximale.
> ```python
> def selectionner_meilleur_candidat(candidats):
>     '''
>     Donne le meilleur candidat de la liste selon le critère de densité maximale.
>     :param candidats: (list) une liste de candidats possibles
>     :return: (str) le meilleur candidat
>     
>     >>> selectionner_meilleur_candidat(lister_candidats(20))
>     'Bijoux'
>     '''
>     # COMPLETER ICI
> ```

> **exercice 14 :**  
> Complétez la fonction `remplir_sac`, de paramètre _poids_max_, le poids maximum autorisé, qui renvoie un tableau représentant le sac rempli selon notre critère.
> ```python
> def remplir_sac(poids_max):
>     '''
>     Applique l'algorithme glouton du problème du sac à dos.
>     :param poids_max: (float) le poids max autorisé
>     :return: (list) un tableau représentant le sac rempli
>     '''
>     sac = ?
>     liste_candidats = lister_candidats(poids_max)
>     
>     while liste_candidats != ? : # tant qu'il reste des candidats
>         objet = selectionner_meilleur_candidat(liste_candidats) # on récupère l'objet de meilleure densité
>         
>         if EFFECTIF[objet] == ?: # si cet objet n'est plus disponible
>             liste_candidats.remove(objet) # on le retire de la liste des candidats
>             
>         else: # sinon il est encore disponible
>             sac.?(objet) # donc on l'ajoute dans le sac
>             EFFECTIF[objet] = EFFECTIF[objet] - ? # on en a donc 1 en moins de disponible dans EFFECTIF
>             poids_max = poids_max - ? # on retire le poids le l'objet du poids_max du sac
>             liste_candidats = lister_candidats(poids_max) # et on récupère la nouvelle liste de candidats
>             
>     return sac
> ```

> **exercice 15 :**  
> Ajoutez les doctests pour la fonction `remplir_sac`.


_________________

Inspiré du travail de Mieszczak Christophe : [git](https://framagit.org/tofmzk/informatique_git)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

