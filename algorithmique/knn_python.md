# kNN : iris avec Python

*Rien n'est à recopier dans votre cahier pour cette activité.*


### Lire les données

**Q1)** Téléchargez le fichier [iris.csv](src/iris.csv).

**Q2)** Créez un fichier `iris.py`.

Tout d'abord, il faut importer la base dans un tableau en utilisant une fonction `lire_donnees`.

```python
def lire_donnees(nom_fichier_CSV):
    """
    Lit le fichier csv et construit le tableau des donnees.
    
    :param nom_fichier_CSV: (str)
    :return: (list de dict)
    """
    fichier = open(nom_fichier_CSV, "r")
    lignes = fichier.readlines()
    fichier.close()
    
    donnees = []
    for i in range(1, len(lignes)):
        ligne = lignes[i].split(";")
        
        donnee = {}
        donnee['petal_length'] = float(ligne[0])
        donnee['petal_width'] = float(ligne[1])
        donnee['species'] = ligne[2][:-1]
        
        donnees.append(donnee)

    return donnees
```

**Q3)** Copiez le code précédent dans votre fichier et exécutez le sur `iris.csv`. Qu'obtenez-vous ?


### Affichage des données

Nous allons maintenant représenter graphiquement les fleurs étudiées en prenant :

* la longueur des pétales en abscisse
* la largeur des pétales en ordonnée
* une couleur différente selon la classe de l'individu.

La bibliothèque `pylab` permet de réaliser des représentations graphiques.  
La fonction `representer_donnees` nous permettra d'afficher nos données.

```python
import pylab

def representer_donnees(donnees):
    """
    Représente graphiquement les données.
    
    :param donnees: (list de dict)
    """
    couleur = {'setosa' : 'green',
               'virginica' : 'red',
               'versicolor' : 'blue'}

    pylab.title('Les iris')
    pylab.xlabel('longueur')
    pylab.ylabel('largeur')

    for donnee in donnees:
        pylab.scatter(donnee['petal_length'],
                      donnee['petal_width'],
                      color = couleur[donnee['species']],
                      linewidth = 2.0)

    pylab.text(1, 0.7, 'setosa', color = 'green')
    pylab.text(3, 1.5, 'virginica', color = 'red')
    pylab.text(6, 1.5, 'versicolor', color = 'blue')

    pylab.show()
```

**Q4)** Copiez le code précédent dans votre fichier et exécutez le sur les données récupérées de `iris.csv`. Qu'obtenez-vous ?


### Ajout d'une iris de classe inconnue

En se promenant, on découvre une iris dont on mesure les pétales : 2,5cm sur 0,75cm

**Q5)** Ajoutez à la fonction `representer_donnees` la ligne suivante au dessus de `pylab.show()` :
```pylab.scatter(2.5, 0.75, color = 'black', linewidth = 2.0)```. Testez.


### Recherche des k plus proches voisins

On va devoir regarder à quelle distance l'iris se situe des autres. 

Pour calculer la distance entre deux points de coordonnées respectives $`(x_1, y_1)`$ et $`(x_2, y_2)`$, on applique la formule mathématique : $`d = \sqrt {(x_1 - x_2)^2 + (y_1 - y_2)^2}`$.

**Q6)** Réalisez la fonction `distance` de paramètres _x1_, _y1_, _x2_ et _y2_, quatre flottants, qui renvoie la distance entre les points de coordonnées respectives $`(x_1; y_1)`$ et $`(x_2; y_2)`$.  
*N'oubliez pas d'importer le module `math` pour pouvoir utiliser la fonction racine carrée `sqrt`.*

**Q7)** Ajoutez la fonction suivante, qui calcule la distance entre une fleur inconnue et toutes les données d'apprentissage.

```python
def calculer_distances(x_inconnue, y_inconnue, donnees):
    """
    Renvoie une liste de tuple du genre [(distance1, classe1), ...] où chaque tuple précise la classe de chaque fleur présente dans les données et la distance à la fleur de classe inconnue.

    :param x_inconnue: (float)
    :param y_inconnue: (float)
    :param donnees: (list de dict)
    :return: (list de tuple)
    """
    distances = []
    
    for donnee in donnees:
        d = distance(x_inconnue, y_inconnue, donnee['petal_length'], donnee['petal_width'])
        distances.append((d, donnee['species']))
        
    return distances
```

Conservons maintenant les _k_ plus proches voisins.

**Q8)** Ajoutez la fonction `renvoyer_k_plus_proches` ci-dessous à votre programme. Elle prend en paramètres _k_,  _x_inconnue_, _y_inconnue_ et _donnees_ qui renvoie la liste des classes des _k_ fleurs les plus proches de notre fleur inconnue de coordonnées $`(x_{inconnue}, y_{inconnue})`$.

```python
def renvoyer_k_plus_proches(k, x_inconnue, y_inconnue, donnees):
    """
    Renvoie la liste des tuples correspondant aux k fleurs les plus proches de notre fleur.
    
    :param k: (int)
    :param x_inconnue: (float)
    :param y_inconnue: (float)
    :param donnees: (list de dict)
    :return: (list)
    """
    distances = calculer_distances(x_inconnue, y_inconnue, donnees)
    distances.sort()
    
    k_plus_proches = []
    for i in range(k):
        k_plus_proches.append(distances[i][1])
        
    return k_plus_proches
```

Il reste à faire la classification finale !

**Q9)** Ajoutez enfin la fonction `classifier` de paramètres _k_ ,  _x_inconnue_, _y_inconnue_ et _donnees_ qui renvoie la classe la plus représentée parmi les k plus proche fleurs de notre fleur inconnue de coordonnées ($`x_{inconnue}, y_{inconnue})`$ :

```python
def classifier(k, x_inconnue, y_inconnue, donnees):
    """
    Renvoie la classe de l'inconnue.

    :param k: (int)
    :param x_inconnue: (float)
    :param y_inconnue: (float)
    :param donnees: (list de dict)
    :return: (str)
    """
    effectif = {'setosa' : 0, 'virginica' : 0, 'versicolor' : 0}
    k_plus_proches = renvoyer_k_plus_proches(k, x_inconnue, y_inconnue, donnees)
    
    for fleur in k_plus_proches :
        effectif[fleur] += 1
    for cle in effectif :
        if effectif[cle] == max(effectif.values()) :
            return cle
```

**Q10)** Testez la classification avec k=3, x et y inconnues 2.5 et 0.75. De quelle espèce est notre fleur ?


_________________

Fichier `iris.csv` issu de ["The iris dataset"](https://gist.github.com/curran/a08a1080b88344b0c8a7)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
