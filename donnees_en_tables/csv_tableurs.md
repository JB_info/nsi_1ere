# Traitement de données en tables : fichiers CSV et tableurs


## I. Un peu d'histoire

Au début du XXème siècle, les quantités de données répertoriées sont rapidement devenues très conséquentes au point où l’être humain n’était plus capable de les traiter dans des temps raisonnables. L’idée a donc été de confier ces traitement à des machines spécifiquement développées. Les données ont alors été structurées sur des supports, autour de critères communs, afin que les machines puissent les manipuler. C’est l’apparition des **données structurées**.

De 1930 à 1960, c’est à dire jusqu’à l’apparition des ordinateurs modernes, la mécanographie à cartes perforées a largement été utilisée pour stocker et traiter les données. Voici par exemple une carte perforée à 80 colonnes et une trieuse de carte IBM :

![carte perforee](img/carte_perforee.png)

Regrouper des données de manière organisée permet d'en faciliter l'exploitation et permet de produire de nouvelles informations. Par exemple, regrouper les notes des élèves d'une classe permet d'en extraire la moyenne, la plus haute, la plus basse ...

Pour l’être humain, il est plus facile de représenter les données structurées sous la forme d'un tableau. Sur nos ordinateurs modernes, nous utilisons maintenant très souvent des **tableurs** pour manipuler les tableaux de données.

Le premier logiciel tableur était *VisiCalc*, conçu par Dan Bricklin et Bob Frankston pour Apple en 1979.

Aujourd'hui, les tableurs les plus utilisés sont *Microsoft Excel* (1985) et *LibreOffice calc* (2010). Il existe également des tableurs accessibles en ligne, comme *Google Sheets* (2006) et *Framacalc* (2004).

> **exercice 1 :**  
> Parmi les 4 logiciels tableurs ci-dessus, lesquels sont libres ? lesquels sont propriétaires ?

Pour manipuler de plus grandes données, les professionnels utilisent plutôt des SGBD (système de gestion de bases de données) avec le langage SQL. Vous verrez ça l'année prochaine si vous choisissez de garder NSI. Tout ce qui tourne autour de l'analyse des données de grande taille s'appelle le **Big Data**.

> **exercice 2 :**  
> Par "données de grande taille", on entend minimum Go ou To. Complétez :
> * 1 Ko = 1 kilooctet = $`10^3`$ octets
> * 1 Mo = 1 mégaoctet = $`10^6`$ octets
> * 1 Go = 1 `?` = $`10^?`$ octets
> * 1 To = 1 `?` = $`10^?`$ octets


## II. Vocabulaire des données structurées

Voici un exemple de tableau de données :

![tab voitures](img/voitures.png)

Le vocabulaire utilisé pour les données structurées :
* un tableau de données à deux dimensions s'appelle une **collection** ou une **table**
* une caractéristique en commun (une colonne) s'appelle un **descripteur**
* un ensemble d'informations concernant un même objet (une ligne) s'appelle un **p-uplet**
* la valeur d'un descripteur pour un p-uplet donné (une case) s'appelle une **donnée**
* chaque descripteur prend ses données dans un ensemble appelé **domaine** (ex: int, float, str...)

> **exercice 3 :**  
> Pour la collection de l'image précédente, donnez :
> * les 5 descripteurs
> * un p-uplet
> * deux données correspondant chacune à un descripteur différent
> * le domaine des données de descripteur "Marque"
> * le domaine des données de descripteur "Km"


## III. Le format CSV

En fonction du logiciel tableur utilisé, si vous souhaitez enregistrer les données dans un fichier, le format ne sera pas le même. Vous pouvez trouver des données structurées dans des fichiers aux formats : .xls, .xlsx, .ods, .slk, ... Pas très pratique pour la compatibilité ! En plus, ces fichiers sont souvent très volumineux.

C'est pour ces raisons que le format **CSV** est beaucoup utilisé : il est simple à produire et à lire, et très adapté au partage. "CSV" signifie Comma-Separated Values, c'est-à-dire en français "valeurs séparées par des virgules".

C'est un fichier texte, où, vous l'aurez deviné, toutes les données sont séparées par des virgules "," (ou parfois point-virgules ";"). La première ligne du fichier donne les descripteurs. Les lignes suivantes donnent les p-uplets.

Exemple :
```csv
Marque,Modèle,Énergie,Km,Année
VW,Polo,Essence,95600,2013
Renault,Zoé,Électricité,12300,2017
Peugeot,308,Diesel,56700,2015
```

> **exercice 4 :**  
> Pour créer un fichier CSV, c'est la même chose qu'un fichier texte :
> * ouvrez un éditeur de texte (ex : bloc-notes, gedit...)
> * copiez-collez les 4 lignes de l'exemple ci-dessus
> * enregistrez le fichier avec le nom *voitures.csv*
> * fermez le fichier

> **exercice 5 :**  
> Pour visualiser notre collection de voitures sous forme de table, on peut utiliser un tableur :
> * ouvrez *voitures.csv* avec LibreOfficeCalc. Vous devez obtenir ceci : ![calc csv](img/csv_ouverture.png)
> 
> * veillez à bien mettre "Unicode (UTF-8)" dans le premier champ et à cocher "Virgule" comme séparateur
> * cliquez sur "OK", et vérifiez que tout s'est ouvert correctement
> * ajoutez un p-uplet supplémentaire dans le tableur (avec la voiture que vous voulez)
> * enregistrez le fichier *en faisant attention à garder le bon format*
> * fermez le tableur
>
> Questions : 
> * Rouvrez le fichier csv avec l'éditeur de texte, que remarquez vous ?
> * Rouvrez le fichier csv avec le tableur en sélectionnant autre chose que "Unicode (UTF-8)" à l'ouverture, et expliquez ce qu'il se passe. *(indice : allez voir [le cours sur la représentation des caractères](../representation_donnees/caracteres.md))*

Il existe même un site du gouvernement qui fournit toutes sortes de données, la plupart au format csv : [https://data.gouv.fr](https://data.gouv.fr).


## IV. Traitement avec un tableur

Le traitement des données a plusieurs buts :

* **Visualiser** : afficher les données de plusieurs manières (texte, tableau, graphique...)
* **Rechercher** : rechercher une donnée particulière
* **Filtrer** : sélectionner les objets qui vérifient un critère (conditions : ==, !=, <, >) et combiner les critères (non, et, ou)
* **Trier** : ordonner les données selon un ou plusieurs descripteurs
* **Calculer** : pour produire de nouvelles valeurs
* **Combiner / fusionner** : pour produire de nouvelles collections plus grandes

Normalement, vous avez déjà fait tout cela en SNT à l'aide de tableurs. Nous allons refaire quelques manipulations pour nous entraîner, mais le but en NSI sera de se passer des tableurs et de manipuler les données avec... Python (eh oui, encore).

> **exercice 6 :**  
> * Allez sur [https://data.gouv.fr](https://data.gouv.fr).
> * Trouvez les données « Communes de France – Base des codes postaux » fournies par Mohamed Badaoui et téléchargez le fichier *communes-departements-regions.csv*.
> * Ouvrez ce fichier avec LibreOffice calc.
> 
> Pour chacune des questions suivantes, ne recopiez pas la réponse dans votre cahier. L'important est de réussir à trouver l'information le plus efficacement possible.
> *(indice : la plupart des manipulations nécessitent d'aller dans l'onglet "Données")*
> 
> 1. Quel est le nombre de communes en France d’après ce fichier ?
> 2. Quelles sont les coordonnées GPS de Lambersart ?
> 3. Quel est le nombre de communes dans le département 59 ?
> 4. Quelle est la ville de plus haute latitude ? de plus basse longitude ?
> 5. Quelles sont les communes dont le code commune est supérieur ou égal à 600 ?
> 6. Quelles sont les communes dont le code commune est supérieur ou égal à 600 *et* dont le nom du département commence par un M ?
> 7. Quelles sont les 5 communes dont le code commune est supérieur ou égal à 600 *et* dont le nom du département commence par un M *et* qui ont la plus grande latitude ?

Une fois que vous avez terminé, vous pouvez passer aux [traitements en Python](traitement_python.md).

> **Pour aller plus loin :**  
> Vous pouvez aller chercher des données qui vous intéressent sur [https://data.gouv.fr](https://data.gouv.fr) et vous entraîner aux manipulations de tableurs.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, [S. Ramstein](https://gitlab.com/stephane_ramstein/nsi)
