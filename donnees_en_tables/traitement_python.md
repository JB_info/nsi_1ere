# Traitement de données en tables : traitements avec Python

Les tableurs, c'est bien, mais nous les informaticiens, on aime quand même bien tout programmer. Nous allons donc voir comment manipuler les données en tables avec Python.


## I. Pré-requis

Pour pouvoir réaliser des traitements en Python sur nos tables de données, nous allons avoir besoin de connaître quelques instructions que nous n'avons pas encore vues.

### 1. Passer à la ligne : \n

Nous avons vu les chaînes de caractères en Python : encadrées par des guillemets "", elles peuvent contenir n'importe quel caractère. Mais comment fait-on pour passer à la ligne dans une chaîne ?

Le caractère de saut de ligne est **"\n"**.

> **exercice 1 :**  
> Testez : ``` print("ligne 1\nligne 2") ``` avec Python.  
> Que remarquez-vous ?

### 2. Manipulations de fichiers : open, close, readlines, write

Vous l'avez peut-être vu dans le projet pendu, il est possible de lire un fichier en quelques lignes avec Python.

Pour cela, vous aurez besoin des instructions suivantes :

```python
fichier = open("nom_fichier.extension", mode) # mode = 'r' (lecture) ou 'w' (écriture) ou 'a' (ajout)
```
```python
lignes = fichier.readlines() # lit toutes les lignes (renvoie un tableau)
```
```python
fichier.write("texte") # écrit "texte" dans le fichier
```
```python
fichier.close() # ferme le fichier
```

> **exercice 2 :**
> * Enregistrez un fichier `manipulations.py` *à un endroit où vous allez le retrouver*.
> * Copiez-collez le code suivant et exécutez le :
> ```python
> fichier = open("fichier_manips.txt", "w")
> fichier.write("un petit texte\nsur deux lignes\n")
> fichier.close()
> ```
> * Allez dans votre dossier, vérifiez qu'un fichier est bien apparu et regardez son contenu. Faîtes le lien avec le code.
> * Copiez-collez à la suite (sans enlever le reste) le code suivant et exécutez le :
> ```python
> fichier = open("fichier_manips.txt", "r")
> lignes = fichier.readlines()
> fichier.close()
> print(lignes)
> ```
> * Retrouvez-vous bien ce qu'il faut ? Expliquez.
> * Copiez-collez ensuite (toujours sans enlever le reste) le code suivant et exécutez le :
> ```python
> fichier = open("fichier_manips.txt", "a")
> fichier.write("et maintenant trois\n")
> fichier.close()
> ```
> * Allez dans votre dossier, ouvrez le fichier et regardez son contenu. Expliquez.
> * Remplacez le "a" par un "w" dans le dernier *open*, exécutez le programme et retournez voir votre fichier. Expliquez.
> * Remettez le "a". Ecrivez à la suite un code permettant d'ajouter une quatrième ligne puis de lire toutes les lignes.

### 3. Conversions chaînes/tableaux : split, join

Vous avez vu qu'en lisant les lignes du fichier, on les récupérait sous formes de chaînes de caractères (par exemple "un petit texte\n").  
Ce qui veut dire que pour nos fichiers CSV, nous allons récupérer des chaînes comme ceci : "Renault,Zoé,Électricité,12300,2017\n". Pas très pratique, on ne peut pas récupérer les données toutes seules !

Il nous faut donc une technique pour séparer les données à chaque virgule : c'est le **split**.

> **exercice 3 :**
> Testez :
> ```python
> >>> "Renault,Zoé,Électricité,12300,2017\n".split(",")
> ?
> ```
> Séparez la chaîne "un petit texte\n" selon l'espace :
> ```python
> >>> ?
> ['un', 'petit', 'texte\n']
> ```

Et pour écrire dans le fichier, ce sera l'inverse : il faudra transformer notre tableau en chaîne. Pour cela, on utilise **join**.

> **exercice 4 :**  
> Testez :
> ```python
> >>> " ".join(['un', 'petit', 'texte\n'])
> ?
> ```
> Recollez le tableau ['Renault', 'Zoé', 'Électricité', '12300', '2017\n'] avec des virgules :
> ```python
> >>> ?
> 'Renault,Zoé,Électricité,12300,2017\n'
> ```

### 4. Ne garder qu'une partie d'un tableau / d'une chaîne

Nous avons déjà vu lors des tris qu'il était possible d'enlever les premiers éléments d'un tableau.

> **exercice 5 :**  
> Testez : 
> ```python
> >>> tab = ["a", "z", "e", "r", "t", "y"]
> >>> tab[2:]
> ?
> >>> tab[5:]
> ?
> ```

La notation [x:] permet d'enlever les "x" premières valeurs.

Il est aussi possible d'enlever les "y" *dernières* valeurs, pour cela on écrit [:-y].

> **exercice 6 :**  
> Testez : 
> ```python
> >>> tab = ["a", "z", "e", "r", "t", "y"]
> >>> tab[:-2]
> ?
> >>> tab[:-5]
> ?
> ```

Et pour enlever les "x" premières et les "y" dernières, on fait [x:-y].

> **exercice 7 :**  
> Testez : 
> ```python
> >>> tab = ["a", "z", "e", "r", "t", "y"]
> >>> tab[2:-1]
> ?
> >>> tab[3:-2]
> ?
> ```

*Remarque : n'oubliez pas le - pour enlever les dernières, il est très important !*

> **exercice 8 :**  
> Pour nos tableaux de données, la première ligne du fichier contiendra les descripteurs donc nous devrons l'enlever.
> 1. Donnez l'instruction pour enlever les descripteurs d'un tableau nommé *lignes*.
> 
> Nous devrons aussi enlever le \n à la fin de chaque ligne.
> 
> 2. Expliquez ce que le code suivant permet de faire :
> ```python
> lignes = [ligne[:-1] for ligne in lignes]
> ```

## III. Traitements de données en Python

Nous avons maintenant tous les outils pour effectuer nos traitements de données !

### 1. Récupérer les données

> **exercice 9 :**  
> * Téléchargez le [fichier suivant](src/table.csv).
> * Créez un fichier `traitements.py` *dans le même dossier*.
> * Copiez-y le code suivant et complétez-le :
> ```python
> fichier = open("table.csv", ?) # ouvre le fichier en mode lecture
> lignes = ? # lit les lignes
> fichier.close() # ferme le fichier
> 
> lignes = [ligne[?] for ligne in lignes] # enlève le \n à la fin de chaque ligne
> lignes = [ligne.split(?) for ligne in lignes] # sépare les lignes selon la virgule
> descripteurs = lignes[?] # récupère la ligne des descripteurs
> descripteurs = {descripteurs[i] : i for i in range(5)} # précise à quelle colonne correspondent chaque descripteur
> donnees = lignes[?] # récupère les données sans les descripteurs
> ```
> * Exécutez votre programme.
> * Dans la console, regardez la valeur de `descripteurs` pour vérifier que vous avez les bonnes valeurs. Appelez le professeur pour expliquer ce qui pose problème.
> * Regardez la valeur de `donnees` dans la console. Expliquez pourquoi le domaine n'est pas correct pour certaines valeurs.
> * Ajoutez les lignes suivantes à la fin de votre code précédent :
> ```python
> for donnee in donnees:
>     donnee[0] = int(donnee[0])
> ```
> * Exécutez et regardez à nouveau la valeur de `donnees` dans la console. Expliquez.
> * Faîtes de même pour changer le domaine de l'année de naissance.

Nous voilà donc avec nos belles données, prêtes à être manipulées.

### 2. Rechercher dans la table

Nous allons faire des recherches dans notre table, comme nous l'avons fait avec le tableur.

Pour rechercher quelque chose dans la table, il suffit de parcourir les données une par une et vérifier si le critère est respecté.

Par exemple pour rechercher les p-uplets correspondants à des femmes :

```python
def rechercher_femmes():
    for donnee in donnees:
        if donnee[descripteurs["Sexe"]] == "F":
            print(donnee)
```

> **exercice 10 :**  
> * Copiez la fonction `rechercher_femmes` à la suite de votre code précédent, exécutez et appelez la fonction depuis la console. Obtenez-vous les bons p-uplets ?
> * Écrivez une fonction `rechercher_hommes` sur le même principe. Testez. *(vous devez avoir 5 p-uplets)*

Pour rechercher selon plusieurs critères, il faut utiliser les opérateurs booléens *and*, *or* et *not* dans le *if*.

> **exercice 11 :**  
> * Écrivez une fonction `rechercher_hommes_2005` qui affichent les p-uplets correspondants à des hommes nés en 2005.
> * Testez votre fonction. *(vous devez avoir 3 p-uplets)*

> **exercice 12 :**  
> * Écrivez une fonction `rechercher_inf2005_sup2006` qui affichent les p-uplets correspondants à des personnes nées avant 2005 ou après 2006.
> * Testez votre fonction. *(vous devez avoir 3 p-uplets)*

> **exercice 13 :**  
> * Écrivez une fonction `rechercher_nonF_prenomC` qui affichent les p-uplets correspondants à des personnes qui ne sont pas des femmes et dont le prénom commence par un "C".
> * Testez votre fonction. *(vous devez avoir 2 p-uplets)*

Une des recherches les plus essentielles vise à regarder s'il y a des doublons dans la table (vous verrez l'année prochaine qu'un doublon cause une anomalie dans une base de données).

> **exercice 14 :**  
> Proposez une technique qui permettrait de savoir s'il y a des doublons dans une table (explications en français, n'essayez pas de le programmer).

### 3. Trier la table

Reprenons notre méthode de tri la plus efficace, le tri par insertion : 

```python
def decaler_precedents(tab, indice, x):
    while indice > 0 and tab[indice - 1] > x:
        tab[indice] = tab[indice - 1]
        indice = indice - 1
    return indice

def tri_insertion(tab):
    indice = 0
    for x in tab:
        position_x = decaler_precedents(tab, indice, x)
        tab[position_x] = x
        indice = indice + 1
    return tab
```

Avec notre tableur, nous pouvions choisir selon quelle colonne nous voulions trier. Pour faire pareil ici, nous allons devoir ajouter un paramètre *colonne* qui indique la colonne à regarder pour le tri. Voici donc le code final :

```python
def decaler_precedents(tab, indice, x, colonne):
    while indice > 0 and tab[indice - 1][colonne] > x[colonne]:
        tab[indice] = tab[indice - 1]
        indice = indice - 1
    return indice

def tri_insertion(tab, colonne):
    indice = 0
    for x in tab:
        position_x = decaler_precedents(tab, indice, x, colonne)
        tab[position_x] = x
        indice = indice + 1
    return tab
```

> **exercice 15 :**  
> * Indiquez quelles lignes ont été modifiées.
> * Copiez le code à la suite de votre programme et exécutez le.
> * Testez en appelant `tri_insertion(donnees, descripteurs['Année'])` dans la console.
> * Donnez l'instruction pour avoir les données triées par ordre alphabétique du nom de famille. Testez.

Pour trier dans l'ordre décroissant, la méthode la plus simple consiste à trier dans l'ordre croissant puis à inverser les données.

Voici le code :
```python
def tri_decroissant(tab, colonne):
    tab = tri_insertion(tab, colonne)
    tab.reverse()
    return tab
```

> **exercice 16 :**  
> * Copiez la fonction précédente à la suite de votre code et exécutez le.
> * Testez la fonction dans la console pour trier la colonne "Année" dans l'ordre décroissant.
> * Idem avec les noms de famille.


### 4. Combiner / Fusionner deux tables

On peut également modifier une table de données :
* en ajoutant des lignes = *combiner* 2 tables
* en ajoutant des colonnes = *fusionner* 2 tables

Pour savoir si on va combiner ou fusionner, il faut regarder les descripteurs :
* s'ils sont tous pareils, on combine
* s'il y en a des différents, on fusionne

Par exemple on a la table suivante :
```python
donnees2 = [['Numéro étudiant', 'Prénom', 'Nom', 'Année', 'Sexe'], [11, 'Han', 'Solo', 2003, 'M'], [12, 'Adena', 'El-Amin', 2005, 'F']]
```

Les descripteurs sont identiques donc on peut combiner nos deux tables. Il faut simplement ouvrir le fichier en mode écriture et ajouter les nouvelles lignes à la fin.

> **exercice 17 :**  
> * Copiez les lignes suivantes à la suite de votre programme et complétez les.
> ```python
> donnees2 = [['Numéro étudiant', 'Prénom', 'Nom', 'Année', 'Sexe'], [11, 'Han', 'Solo', 2003, 'M'], [12, 'Adena', 'El-Amin', 2005, 'F']]
> donnees2 = donnees2[?] # récupère les données sans les descripteurs
> 
> for donnee in donnees2:
>     donnee[0] = str(donnee[0]) # transforme le numéro étudiant en chaîne de caractères
>     donnee[?] = str(donnee[?]) # transforme l'année en chaîne de caractères
> 
> donnees2 = [?.join(donnee) for donnee in donnees2] # relie les données par des virgules
> donnees2 = ?.join(donnees2) # relie les p-uplets par des sauts de ligne
> donnees2 = donnees2 + "\n" # ajoute le saut de ligne final
> 
> fichier = open("table.csv", ?, encoding=?) # ouvre le fichier pour y ajouter des données
> fichier.?(donnees2) # écris les données
> fichier.?() # ferme le fichier
> ```
> * Exécutez le code, allez dans votre dossier et ouvrez le fichier avec un tableur pour vérifier que les nouvelles données ont été ajoutée correctement.

Regardons maintenant la table suivante :

```python
donnees3 = [['Numéro étudiant', 'Boursier'], [1, True], [2, False], [3, True], [4, False], [5, True], [6, True], [7, False], [8, False], [9, False], [10, True], [11, False], [12, True]]
```

> **exercice 18 :**  
> * Pourquoi ne pourrait-on pas *combiner* ces données ?

Fusionner consiste simplement à ajouter des nouvelles colonnes pour chaque descripteur supplémentaire. Ici, on ajoute donc une colonne "Boursier", et sur chaque p-uplet déjà existant on ajoute la nouvelle donnée.

> **exercice 19 :**  
> * Donnez les 3 premières lignes (descripteurs compris) qu'on obtiendrait en fusionnant ces 2 tables.
> * Quel problème aurait-on eu s'il y avait par exemple 15 p-uplets dans la table `donnees3` ?
> * Quel problème aurait-on eu si les numéros étudiants de la la table `donnees3` étaient "A", "B", "C", etc. ?

Avant de fusionner (ou combiner d'ailleurs) 2 tables, il faut *toujours* vérifier que les domaines correspondent.


## Pour aller plus loin

> **exercice 20 :**  
> Écrivez une fonction `compte_donnees` qui affiche le nombre de p-uplets présents dans la collection `donnees`.

> **exercice 21 :**  
> Ajoutez les doctests pour chacune de vos fonctions de recherche.

> **exercice 22 :**  
> Écrivez une fonction `compare_donnees` qui renvoie True si le type des données est le même pour `donnees2` que pour `donnees`, False sinon.

> **exercice 23 :**  
> Expliquez en français comment il faudrait faire en Python pour fusionner les `donnees3` avec notre table et les ajouter au fichier csv. Si vous le souhaitez, vous pouvez aussi essayer de le programmer.

Il existe une bibliothèque Python permettant de manipuler les fichiers CSV "plus facilement". Cette bibliothèque s'appelle... csv. *(au moins, c'est facile à retenir...)*

La documentation de cette bibliothèque se trouve [ici](https://docs.python.org/fr/3/library/csv.html). Vous n'avez pas besoin de toutes les fonctions données, alors si vous préférez lire un résumé (avec exemples), allez plutôt [ici](https://pythonforge.com/module-csv-lire-ecrire/).

> **exercice 24 :**  
> Reprenez les exercices précédents en utilisant cette fois la bibliothèque csv.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
