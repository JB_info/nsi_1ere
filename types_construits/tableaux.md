# Types construits : tableaux


Après les tuples, le deuxième type construit que nous allons voir est les **tableaux**.

*REMARQUE IMPORTANTE :* Le **type** des tableaux en Python est **list**. Cela peut porter à confusion car quelques fois on peut vous demander d'"utiliser une liste" dans un sujet. C'est un abus de langage, "utiliser une liste" revient à "utiliser un tableau".

## I. Définition

Un tableau est **itérable** (= on peut le parcourir, comme les tuples ou les chaînes).

Un tableau est **modifiable** (à la différence des tuples).

Dans un tableau, on sépare les éléments par des **virgules** et on entoure l’ensemble de **crochets**.

Voici donc des exemples de tableaux :
```python
>>> tab1 = ["a", "z", "e", "r", "t", "y"]
>>> tab2 = [11, 36, 4, 13, 1, 1974]
```

Un tableau vide :
```python
>>> tab = []
```

Un tableau à 1 élément :
```python
>>> tab = ["nsi"]
```

*Remarque : contrairement aux tuples, un tableau à 1 élément n'a pas de virgule à la fin*

> **exercice 1:**  
> * Créez un tableau `villes` qui contient les villes suivantes (dans cet ordre) : Lille, Dunkerque, Lens, Calais, Arras.
> * Déterminez le type de `villes`.

## II. Utilisation

Pour trouver la taille d'un tableau, c'est comme pour tout : `len`.

> **exercice 2 :**  
> Donnez l'instruction Python pour calculer la taille de votre tableau `villes`.

Pour accéder à un élément dans un tableau, c'est comme les tuples : les **indices de 0 à n-1** (n = taille du tableau). Si on demande un mauvais indice (trop petit ou trop grand), on obtient une exception `IndexError`.

```python
>>> tab = ["a", "z", "e", "r", "t", "y"]
>>> tab[0]
"a"
>>> tab[1]
"z"
>>> tab[5]
"y"
>>> tab[100]
IndexError
```

> **exercice 3 :**  
> * Donnez l'instruction pour récupérer Lens dans `villes`.
> * Donnez l'instruction pour récupérer Lille dans `villes`.
> * Donnez l'instruction pour récupérer la dernière ville.
> * Que se passe-t-il si vous essayez de récupérer la ville à l'indice 11 ?


## III. Opérations

Pour vérifier si une valeur est dans un tableau, on utilise `in`.

```python
>>> tab = ["a", "z", "e", "r", "t", "y"]
>>> "r" in tab
True
>>> "w" in tab
False
```

> **exercice 4 :**  
> * Donnez l'instruction pour vérifier si Boulogne est dans le tableau `villes`.
> * Même question pour Calais.

On peut concaténer des tableaux avec `+`.

> **exercice 5 :**  
> Testez :
> ```python
> >>> t1 = [1, 2]
> >>> t2 = [3, 4, 5]
> >>> t3 = [6]
> >>> t1 + t2 + t3
> ?
> ```

> **exercice 6 :**  
> * Construisez un tableau `villes2` qui contient Auby, Valenciennes et Douai (dans cet ordre).
> * Construisez un tableau `villes_nord` en concaténant `villes` et `villes2`.


## IV. Modifications

Contrairement aux tuples, on peut modifier un tableau !

#### 1. Ajouter une valeur à la fin

On ajoute une valeur à la fin d'un tableau avec la méthode **append** :

```python
>>> t = ["a", "z", "e", "r", "t", "y"]
>>> t.append("u")
>>> t
["a", "z", "e", "r", "t", "y", "u"]
```

À NOTER :  
* append s'utilise comme ceci : `tableau.append(valeur)`
* append ajoute un élément *à la fin* du tableau
* append modifie le tableau mais ne le renvoie pas (pour vérifier que la modification a fonctionné il faut regarder la valeur du tableau à la main)

> **exercice 7 :**  
> Ajoutez Lambersart à votre tableau `villes_nord`.

#### 2. Suppression d'une valeur par son indice

On peut supprimer une valeur dans un tableau avec la méthode **pop**. Pour cela il faut connaître son indice :

```python
>>> t = ["a", "z", "e", "r", "t", "y"]
>>> t.pop(2)
"e"
>>> t
["a", "z", "r", "t", "y"]
```

À NOTER :  
* pop s'utilise comme ceci : `tableau.pop(indice)`
* pop prend en paramètre un *indice* (jamais une valeur)
* pop renvoie la *valeur* supprimée
* pour vérifier que la modification a bien fonctionné il faut regarder la valeur du tableau à la main

> **exercice 8 :**  
> * Supprimez Douai de votre tableau `villes_nord`.
> * Supprimez la première ville.

#### 3. Modifier une valeur par son indice

On peut aussi modifier une valeur déjà existante dans un tableau. On utilise son indice :

```python
>>> t = ["a", "z", "e", "r", "t", "y"]
>>> t[2] = "w"
>>> t
["a", "z", "w", "r", "t", "y"]
```

À NOTER :
* on modifie comme ceci : `tableau[indice] = nouvellevaleur`
* pour vérifier que la modification a bien fonctionné il faut regarder la valeur du tableau à la main

> **exercice 9 :**  
> Changez Lens par Lille dans votre tableau `villes_nord`.

## V. Construction par compréhension

Il est possible de construire un tableau **par compréhension**.

Cela s'écrit ainsi : `tableau = [... for x in range(...)]`

Par exemple pour créer le tableau des entiers de 1 à 10 :
```python
>>> t = [x for x in range(1, 10+1)]
>>> t
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
```
Il faut le lire ainsi : dans le tableau, on met `x` pour chaque `x` qui est dans le range de 1 à 10+1.  
Comme dans le range de 1 à 10+1 il y a 1, puis 2, puis 3, etc., puis 9, puis 10. Alors dans le tableau on met 1, puis 2, puis 3, etc., puis 9, puis 10.

Autre exemple : le tableau des carrés des entiers de 1 à 10.
```python
>>> t = [x**2 for x in range(1, 10+1)]
>>> t
[1, 4, 9, 16, 25, 36, 49, 64, 91, 100]
```
Il faut le lire ainsi : dans le tableau, on met `x**2` (= x puissance 2) pour chaque `x` qui est dans le range de 1 à 10+1. Donc on met le carré de 1, puis le carré de 2, etc., puis le carré de 10.

> **exercice 10 :**  
> Créez trois tableaux par compréhension qui contiennent :
> 1. les entiers entre 0 et 800
> 2. les entiers PAIRS entre 1 et 100
> 3. les multiples de 7 compris entre 7 et 700

## VI. Tableaux de tableaux

Il est possible de créer des tableaux contenant... des tableaux.

Exemple :
```python
>>> t = [[1, 3, 5], [2, 4, 6], [10, 100, 1000]]
```

Pour accéder aux éléments on a donc besoin de deux paires de crochets :
* les premiers pour indiquer l'indice du tableau
* les deuxièmes pour indiquer l'indice de l'élément

Exemple :
```python
>>> t = [[1, 3, 5], [2, 4, 6], [1, 10, 100]]
>>> t[0][0]
1
>>> t[0][1]
3
>>> t[2][1]
10
```

> **exercice 11 :**  
> On utilise le tableau suivant :
> ```python
> >>> t = [[0], [1, 3, 5, 7, 9], [2, 4, 6, 8]]
> ```
> Donnez les instructions pour :
> * récupérer 0
> * récupérer 1
> * récupérer 9
> * récupérer 4
> * vérifier si 5 est dans le deuxième tableau
> * donner la taille du dernier tableau
> * concaténer le premier et le dernier tableau
> * modifier le 8 par un 10
> * ajouter 100 à la fin du premier tableau
> * supprimer 9 du deuxième tableau

## VII. Parcours

On peut parcourir un tableau avec la boucle `for`.

1. Première façon : on parcourt les éléments.

Exemple :
```python
tab = ["a", "z", "e", "r", "t", "y"]
for x in tab:
    print(x)
```
Cela donne l'affichage :
```python
a
z
e
r
t
y
```

2. Deuxième façon : on parcourt les indices.

Exemple :
```python
tab = ["a", "z", "e", "r", "t", "y"]
n = len(tab)
for i in range(n):
    print(tab[i])
```
Cela donne le même affichage :
```python
a
z
e
r
t
y
```

> **exercice 12 :**  
> Écrivez deux fonctions `affiche1` et `affiche2` qui prennent en paramètre un tableau et affichent les éléments du tableau :
> * `affiche1` parcourt les éléments
> * `affiche2` parcourt les indices

> **exercice 13 :**  
> Écrivez une fonction qui prend en paramètre un entier `n` et qui renvoie un tableau de `n` nombres choisis aléatoirement entre 0 et 100 inclus.
> *Indice : utilisez la bibliothèque random.*

Pour parcourir un tableau de tableaux, il faut donc deux boucles `for`.

> **exercice 14 :**  
> Testez :
> ```python
> t = [[1, 3, 5], [2, 4, 6], [1, 10, 100]]
> for tab in t:
>     for val in tab:
>         print(val)
> ```

Pour un affichage plus joli (ligne par ligne) :

> **exercice 15 :**  
> Testez :
> ```python
> t = [[1, 3, 5], [2, 4, 6], [1, 10, 100]]
> for tab in t:
>     for val in tab:
>         print(val, end=" ")
>     print(" ")
> ```
> À votre avis, que fait le end dans le print ?

> **exercice 16 :**  
> Écrivez une fonction qui prend en paramètre un tableau de tableau et l'affiche.
> Les éléments d'un même tableau doivent être sur la même ligne, et les éléments d'une même ligne doivent être séparés par un tiret.


## Pour aller plus loin

> * Testez :
> ```python
> t = [[1, 3, 5], [2, 4, 6], [1, 10, 100]]
> nx = len(t)
> for x in range(nx):
>     ny = len(t[x])
>     for y in range(ny):
>         print(t[x][y])
> ```
> * Modifiez le code pour un affichage ligne par ligne.

> Écrivez une fonction qui prend en paramètre une chaîne de caractères et renvoie un tableau contenant tous ses caractères un par un.

> Écrivez une fonction qui analyse les éléments d'un tableau pour en créer deux nouveaux : l'un contenant les mots de moins de 6 lettres et l'autre ceux de 6 lettres ou plus.
> Exemple d'appel :
> ```python
> >>> separe_six(["aimes", "tu", "manger", "du", "chocolat"])
> (["aimes", "tu", "du"], ["manger", "chocolat"])
> ```

> Écrivez une fonction qui prend en paramètre deux tableaux de même taille et qui renvoie un tableau avec tous les éléments en les alternant.
> Exemple d'appel :
> ```python
> >>> alterne([1, 3, 5, 7], [2, 4, 6, 8])
> [1, 2, 3, 4, 5, 6, 7, 8]
> ```

> Écrivez une fonction qui crée un tableau à deux dimensions de taille 30 par 30 contenant des entiers aléatoires entre 1 et 9999 et le renvoie.
> *Indice : utilisez la bibliothèque random.*

> Si vous avez fini, appelez votre professeur puis vous pourrez aller vous entraîner sur les tableaux sur FRANCE-IOI.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
