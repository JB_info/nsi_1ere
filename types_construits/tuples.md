# Types construits : tuples



Nous n'avons pour l'instant manipulé que les types de base (booléens, entiers, flottants, chaînes de caractères). La plupart des langages de programmation disposent aussi de **types construits** : ce sont des structures de données qui permettent de stocker plusieurs valeurs. Nous allons ici étudier notre premier type construit : les **tuples** (aussi appelés *p-uplets*).



## I. Syntaxe

Un tuple est une structure de données qui permet de stocker un **ensemble fini et ordonné** d'éléments. Cela signifie simplement qu'un tuple permet de stocker un nombre fini de valeurs (on peut calculer la taille du tuple) et que chaque valeur a une place bien précise (il y a un ordre).

### 1. Création

En Python,  un tuple est représenté par une série d'éléments séparés par des virgules et encadrés par des parenthèses. Les éléments peuvent être du même type ou alors de types différents.

Voici quelques exemples de tuples :

```python
>>> tuple_d_entiers = (1, 7, 4, 42)
>>> tuple_de_chaines = ("voici", "un", "tuple")
>>> tuple_heterogene = (2021, "octobre", 7, 14.55)
```

> **exercice 1 :**
>
> Vérifiez le type des 3 variables précédentes.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

Attention, si un tuple ne possède qu'un seul élément, il faut mettre une virgule après cet élément :

```python
>>> tuple_1_element = (7, )
>>> autre_tuple_1_element = ("nsi", )
```

Il existe aussi un tuple particulier, le tuple vide :

```python
>>> tuple_vide = ()
```

> **exercice 2 :**
>
> Créez les tuples contenant les éléments suivants :
>
> * votre prénom, votre nom et votre âge
> * votre année de naissance
> * l'ensemble de vos spécialités
> * rien
>
> ```
> 
> 
> 
> 
> ```

### 2. Accès aux éléments

Chaque élément d'un tuple a une place bien précise. La place d'un élément s'appelle son **indice**. Si un tuple possède `n` éléments, alors les indices vont **de 0 à n-1**. Cela veut dire que le premier élément est à l'indice `0`, le second élément est à l'indice `1`, ...., le dernier élément est à l'indice `n-1`.

Par exemple pour le tuple `("a", "b", "c", "d")` de taille `n = 4` :

* "a" est à l'indice 0
* "b" est à l'indice 1
* "c" est à l'indice 2
* "d" est à l'indice 3 (= n - 1 = 4 - 1)

> **exercice 3 :**
>
> On considère le tuple suivant : `("les", "indices", "vont", "de", 0, "a", "n", "-", 1)`
>
> * quelle est la taille de ce tuple ?
> * quel est l'élément à l'indice 0 ?
> * à quel indice est le dernier élément ?
> * à quel indice est l'élément "vont" ?
> * quel est le type de l'élément à l'indice 4 ?
>
> ```
> 
> 
> 
> 
> 
> ```

Pour **accéder à un élément** dans un tuple, il suffit de préciser son indice entre crochets après le nom du tuple.

```python
>>> mon_tuple = ("a", "b", "c", "d")
>>> mon_tuple[0]
'a'
>>> mon_tuple[1]
'b'
>>> mon_tuple[2]
'c'
>>> mon_tuple[3]
'd'
```

> **exercice 4 :**
>
> Complétez les instructions ci-dessous :
>
> ```python
> >>> tuple_entiers = (7, 4, 42, 11, 2, 36, 21)
> >>> tuple_chaines = ("nsi", "maths", "svt", "physique", "hggsp", "ses")
> >>> tuple_entiers[?]
> 4
> >>> tuple_entiers[5]
> ?
> >>> tuple_chaines[5]
> ?
> >>> tuple_chaines[?]
> 'nsi'
> >>> ?
> 'physique'
> >>> ?
> 21
> ```

> **exercice 5 :**
>
> Exécutez les instructions suivantes :
>
> ```python
> >>> tuple_3_elements = (0, 1, 2)
> >>> tuple_3_elements[3]
> ?
> ```
>
> Comment expliquez-vous cette exception ?
>
> ```
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Il est également possible d'accéder aux éléments à partir de la fin. Pour cela, on utilise des indices négatifs : -1 pour le dernier élément, -2 pour l’avant dernier…
>
> Complétez les instructions suivantes :
>
> ```python
> >>> tuple_entiers = (7, 4, 42, 11, 2, 36, 21)
> >>> tuple_chaines = ("nsi", "maths", "svt", "physique", "hggsp", "ses")
> >>> tuple_entiers[-1]
> ?
> >>> tuple_entiers[-6]
> ?
> >>> tuple_entiers[-?]
> 2
> >>> ?
> 'hggsp'
> >>> ?
> 'nsi'
> ```

### 3. Tuples de tuples

Les tuples que l'on a vu jusqu'ici ne contenaient que des éléments ayant un type de base. Il est également possible de créer des tuples qui contiennent... des tuples :

```python
>>> mon_tuple_de_tuples = (("un", "premier", "tuple"), ("un", "second", "tuple"), ("un", "dernier", "tuple"))
```

Pour accéder aux éléments, on va alors avoir besoin de 2 paires de crochets : la première pour indiquer l'indice du tuple et la seconde pour indiquer l'indice de l'élément dans ce tuple.

```python
>>> mon_tuple_de_tuples = (("un", "premier", "tuple"), ("un", "second", "tuple"), ("un", "dernier", "tuple"))
>>> mon_tuple_de_tuples[0]
('un', 'premier', 'tuple')
>>> mon_tuple_de_tuples[0][1]
'premier'
```

> **exercice 6 :**
>
> Complétez les instructions suivantes :
>
> ```python
> >>> tuple_de_tuples_d_entier = ((2, 5, 2000), (26, 9, 2002), (25, 12, 2017), (14, 7, 2021))
> >>> tuple_de_tuples_d_entier[1]
> ?
> >>> tuple_de_tuples_d_entier[?]
> (14, 7, 2021)
> >>> tuple_de_tuples_d_entier[0][0]
> ?
> >>> tuple_de_tuples_d_entier[2][0]
> ?
> >>> tuple_de_tuples_d_entier[?][0]
> 26
> >>> tuple_de_tuples_d_entier[1][?]
> 2002
> >>> tuple_de_tuples_d_entier[?][?]
> 12
> >>> ?
> 7
> >>> ?
> 25
> ```



## II. Manipulations

Nous allons voir quelques manipulations possibles pour les tuples : calculer la taille d'un tuple, tester si une valeur appartient à un tuple, concaténer deux tuples et dupliquer un tuple.

Il faut savoir qu'un tuple est une structure dite *non mutable*, c'est-à-dire qu'**on ne peut pas modifier un tuple**.

### 1. Taille d'un tuple

Comme vu plus haut, la taille d'un tuple correspond au nombre d'éléments de ce tuple.

Il existe une fonction native de Python pour déterminer la taille d'un tuple : `len`. Comme pour toutes les fonctions Python, pour l'utiliser on écrit le nom de la fonction suivi entre parenthèses de la valeur à tester :

```
>>> mon_tuple = ("a", "b", "c", "d")
>>> len(mon_tuple)
4
```

> **exercice 7 :**
>
> Utilisez `len` pour déterminer la taille des tuples suivants :
>
> * (1, 2, 3)
> * ("nsi", "maths", "svt", "physique", "hggsp", "ses")
> * ("tuple", )
> * ()
> * ((2, 5, 2000), (26, 9, 2002), (25, 12, 2017), (14, 7, 2021))
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 8 :**
>
> Comment obtenir la taille du premier tuple de ((2, 5, 2000), (26, 9, 2002), (25, 12, 2017), (14, 7, 2021)) ?
>
> ```python
> >>> tuple_de_tuples_d_entier = ((2, 5, 2000), (26, 9, 2002), (25, 12, 2017), (14, 7, 2021))
> >>> len(?)
> 3
> ```

### 2. Appartenance

On peut tester l'appartenance d'une valeur à un tuple grâce au mot-clef `in`.

Par exemple, pour tester si l'entier 6 appartient au tuple (1, 3, 5, 7) :

```python
>>> 6 in (1, 3, 5, 7)
False
```

Cette instruction renvoie un booléen : vrai (True) si la valeur appartient au tuple, faux (False) sinon.

> **exercice 9 :**
>
> Complétez les instructions ci-dessous :
>
> ```python
> >>> tuple_entiers = (7, 4, 42, 11, 2, 36, 21)
> >>> tuple_chaines = ("nsi", "maths", "svt", "physique", "hggsp", "ses")
> >>> tuple_de_tuples_d_entier = ((2, 5, 2000), (26, 9, 2002), (25, 12, 2017), (14, 7, 2021))
> >>> 2 in tuple_entiers
> ?
> >>> 0 in tuple_entiers
> ?
> >>> ? in tuple_chaines
> True
> >>> ? in tuple_chaines
> False
> >>> (25, 12, 2017) in tuple_de_tuples_d_entier
> ?
> >>> 25 in tuple_de_tuples_d_entier
> ?
> >>> ? in tuple_de_tuples_d_entier
> True
> >>> 25 in tuple_de_tuples_d_entier[2]
> ?
> >>> ? in tuple_de_tuples_d_entier[0]
> True
> >>> 2002 in tuple_de_tuples_d_entier[?]
> True
> ```

### 3. Opérations

Les deux opérations possibles sur les tuples sont :

* la concaténation : elle se réalise avec l'opérateur `+` et permet de mettre bout à bout 2 tuples.

  > **exercice 10 :**
  >
  > Testez la concaténation sur les exemples suivants :
  >
  > ```python
  > >>> (1, 2, 3) + (4, 5, 6)
  > ?
  > >>> ("a", "b") + (1, 2, 3)
  > ?
  > >>> ("spécialité", ) + ("nsi", )
  > ?
  > >>> ? + (4, )
  > (2, "au carré égal", 4)
  > ```

* la duplication : elle se réalise avec l'opérateur `*` entre un tuple et un entier et permet de dupliquer le tuple.

  > **exercice 11 :**
  >
  > Testez la duplication sur les exemples suivants :
  >
  > ```python
  > >>> (1, 2, 3) * 2
  > ?
  > >>> ("nsi", ) * 5
  > ?
  > >>> (1, 2) * ?
  > (1, 2, 1, 2, 1, 2, 1, 2)
  > >>> ? * 2
  > ("!", "!", "!", "!")
  > ```

### 4. Pour aller plus loin

Si vous avez terminé, vous pouvez faire les exercices ci-dessous.

> **exercice 12 :**
>
> Complétez le programme suivant, qui permet d'ajouter la valeur `ma_valeur` au tuple `mon_tuple` :
>
> ```python
> mon_tuple = (1, 2, 3)
> ma_valeur = 4
> mon_tuple = ???
> ```
>
> Vérifiez dans le shell qu'après exécution du programme, la valeur de la variable `mon_tuple` est bien `(1, 2, 3, 4)`.
>
> ```python
> >>> mon_tuple
> (1, 2, 3, 4)
> ```
>
> Testez votre programme avec le tuple vide et la valeur "nsi" :
>
> ```
> 
> 
> ```
>
> Retenez bien cette méthode qui permet d'ajouter une valeur à un tuple, elle servira à nouveau lors du prochain cours.

> **exercice 13 :**
>
> Affectez à une variable `age` la valeur 16.
>
> ```
> 
> 
> ```
>
> Affectez à une variable `personne` le tuple contenant les valeurs "Peter", "Parker", `age`, "New York" dans cet ordre.
>
> ```
> 
> 
> ```
>
> Quelle instruction permet d'accéder à la première valeur du tuple `personne` ?
>
> ```
> 
> 
> ```
>
> Exécutez l'instruction suivante. Que remarquez-vous ?
>
> ```python
> >>> personne[2]
> ?
> ```
>
> ```
> 
> 
> ```
>
> Modifiez la valeur de la variable `age`, puis exécutez à nouveau l'instruction `personne[2]`. Que remarquez-vous ? Pourquoi ?
>
> ```
> 
> 
> 
> 
> ```

> **exercice 14 :**
>
> Complétez les instructions suivantes :
>
> ```python
> >>> mon_tuple = (1, "a", 2, "b", 3, "c", 4, "d", 5, "e")
> >>> "c" in mon_tuple
> ?
> >>> "5" in mon_tuple
> ?
> >>> mon_tuple[?]
> 2
> >>> mon_tuple[?]
> 4
> >>> ?
> "e"
> >>> ?
> 10
> ```

> **exercice 15 :**
>
> On considère le tuple suivant : `("nsi", "maths", "svt", "physique", "hggsp", "ses")`.
>
> Donnez trois instructions différentes permettant de récupérer le dernier élément du tuple :
>
> ```python
> >>> specialites = ("nsi", "maths", "svt", "physique", "hggsp", "ses")
> >>> ?
> "ses"
> >>> ?
> "ses"
> >>> ?
> "ses"
> ```

> **exercice 16 :**
>
> On travaille avec le tuple suivant :
>
> ```python
> >>> avengers = (("Tony", "Stark", 53), ("Steve", "Rogers", 105), ("Thor", "Odinson", 1500), ("Bruce", "Banner", 53), ("Natasha", "Romanoff", 39), ("Peter", "Parker", 16), ("Peter", "Quill", 43))
> ```
>
> Donnez au moins 2 instructions permettant de récupérer la chaîne de caractères "Peter" :
>
> ```python
> >>> ?
> "Peter"
> ```
>
> Donnez au moins 2 instructions permettant de récupérer l'entier 53 :
>
> ```python
> >>> ?
> 53
> ```
>
> On a oublié Hawkeye... Donnez l'instruction permettant de créer le tuple contenant l'ensemble des Avengers, plus le tuple ("Clint", "Barton", 49).
>
> ```python
> >>> ?
> (("Tony", "Stark", 53), ("Steve", "Rogers", 105), ("Thor", "Odinson", 1500), ("Bruce", "Banner", 53), ("Natasha", "Romanoff", 39), ("Peter", "Parker", 16), ("Peter", "Quill", 43), ("Clint", "Barton", 49))
> ```
>
> Donnez des instructions qui permettent de vérifier que :
>
> * il y a bien un Avengers nommé Bruce Banner qui a 53 ans
>
> * Captain America (Steve Rogers) a bien 105 ans
> * le nom de famille de Natasha est bien Romanoff
> * l'Avengers qui a 1500 ans est bien Thor
>
> ```
> 
> 
> 
> 
> ```

  

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
