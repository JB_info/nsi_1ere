# Types construits : dictionnaires

Les dictionnaires sont les derniers types construits que nous verrons cette année.

## I. Définition

Un dictionnaire ne ressemble pas vraiment aux tuples et aux tableaux car les éléments n'ont pas d'ordre. **Il n'y a donc pas d'indices dans un dictionnaire.**

Un dictionnaire est un ensemble de **valeurs** associées à des **clés**.

* les clés ont un type de base (int, float, bool, str)
* les valeurs ont n'importe quel type

Dans un dictionnaire, on sépare les éléments par des **virgules** et on entoure l’ensemble d'**accolades**. Chaque élément s'écrit **clé : valeur**.

Voici donc un exemple de dictionnaire :
```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
```

Ce dictionnaire contient trois éléments.  
Les clés sont "Nom", "Prénom" et "Age". Les valeurs sont "Potter", "Harry", et 17.  
On dit que la valeur "Potter" «&nbsp;est associée à&nbsp;» la clé "Nom", ou encore que «&nbsp;le couple (clé, valeur)&nbsp;» ("Nom", "Potter") appartient au dictionnaire.

Voici le dictionnaire vide :
```python
>>> dico = {}
```

Les dictionnaires sont de type **dict**.

> **exercice 1 :**  
> * Créez un dictionnaire `dico_hermione` dont les clés sont identiques à celles de `dico_harry` et les valeurs concernent Hermione Granger qui a 16 ans.
> * Déterminez le type de `dico_hermione`.

Un dictionnaire n'a pas d'indices car il n'y pas d'ordre :

```python
>>> {'a' : 1, 'b' : 2} == {'b' : 2, 'a' : 1}
True
```

Les deux dictionnaires sont identiques, alors que les couples (clé, valeur) ne sont pas dans le même ordre !

> **exercice 2 :**  
> Définissez deux dictionnaires identiques dont les clés n'apparaissent pas dans le même ordre. Testez l'égalité.

## II. Utilisation

Pour trouver la taille d'un dictionnaire, c'est comme pour tout : `len`.

> **exercice 3 :**  
> Donnez l'instruction Python pour calculer la taille de votre dictionnaire `dico_hermione`.

Pour accéder à une valeur dans un dictionnaire, on utilise donc plus les indices mais **les clés**. Si on demande une mauvaise clé, on obtient une exception `KeyError`.

```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
>>> dico_harry["Nom"]
"Potter"
>>> dico_harry["Age"]
17
>>> dico_harry["MauvaiseClé"]
KeyError
```

> **exercice 4 :**  
> * Donnez l'instruction pour récupérer le prénom de Hermione.
> * Donnez l'instruction qui va renvoyer 16.
> * Que se passe-t-il si vous essayer d'utiliser la clé "Baguette" ?

Il est possible de récupérer l'ensemble des clés avec la méthode **keys** :
```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
>>> dico_harry.keys()
dict_keys(['Nom', 'Prénom', 'Age'])
```

> **exercice 5 :**  
> Donnez l'instruction pour récupérer les clés de `dico_hermione`.

Il est possible de récupérer l'ensemble des valeurs avec la méthode **values** :
```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
>>> dico_harry.values()
dict_values(['Potter', 'Harry', 17])
```

> **exercice 6 :**  
> Donnez l'instruction pour récupérer les valeurs de `dico_hermione`.

Il est possible de récupérer l'ensemble des couples (clé, valeur) avec la méthode **items** :
```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
>>> dico_harry.items()
dict_items([('Nom', 'Potter'), ('Prénom', 'Harry'), ('Age', 17)])
```

> **exercice 7 :**  
> Donnez l'instruction pour récupérer les couples (clé, valeur) de `dico_hermione`.


## III. Opérations

Pour vérifier si une *clé*, une *valeur* ou un *couple (clé, valeur)* est dans un dictionnaire, on utilise `in`.

```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
>>> "Nom" in dico_harry.keys()
True
>>> "MauvaiseClé" in dico_harry.keys()
False

>>> "Potter" in dico_harry.values()
True
>>> "MauvaiseValeur" in dico_harry.values()
False

>>> ("Nom", "Potter") in dico_harry.items()
True
>>> ("MauvaiseClé", "OuMauvaiseValeur") in dico_harry.items()
False
```

> **exercice 8 :**  
> * Donnez l'instruction pour vérifier si "Prénom" est une clé du dictionnaire `dico_hermione`.
> * Donnez l'instruction pour vérifier si 16 est une valeur de `dico_hermione`.
> * Donnez l'instruction pour vérifier si ("Nom", "Weasley") est un couple (clé, valeur) de `dico_hermione`.


*Remarque : Bonne nouvelle, la concaténation n'existe pas pour les dictionnaires. À votre avis, pourquoi ?*


## IV. Modifications

Un dictionnaire est modifiable.

#### 1. Ajouter une valeur au dictionnaire

Ajouter une valeur veut forcément dire ajouter une nouvelle clé :
```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
>>> dico_harry["Animal"] = "Hedwige"
>>> dico_harry
{"Nom": "Potter", "Prénom": "Harry", "Age": 17, "Animal": "Hedwige"}
```

À NOTER :  
* on ajoute un couple (clé, valeur) ainsi : `dictionnaire[clé] = valeur`
* la clé ne doit pas déjà exister
* l'élément ajouté n'est pas forcément à la fin car il n'y a pas d'ordre dans un dictionnaire (contrairement aux tableaux)
* pour vérifier que la modification a bien fonctionné il faut regarder la valeur du dictionnaire à la main

> **exercice 9 :**  
> * Ajoutez l'animal d'Hermione (Pattenrond).

#### 2. Suppression d'une valeur par sa clé

On peut supprimer une valeur dans un dictionnaire avec la méthode **pop** (comme les tableaux). Pour cela il faut connaître sa clé (et non plus son indice) :

```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17, "Animal": "Hedwige"}
>>> dico_harry.pop("Animal")
"Hedwige"
>>> dico_harry
{"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
```

À NOTER :  
* pop s'utilise ainsi : `dictionnaire.pop(clé)`
* pop prend en paramètre une *clé* (jamais une valeur)
* pop renvoie la *valeur* supprimée
* pour vérifier que la modification a bien fonctionné il faut regarder la valeur du dictionnaire à la main

> **exercice 10 :**  
> Supprimez l'animal d'Hermione.

#### 3. Modifier une valeur par sa clé

On peut aussi modifier une valeur déjà existante dans un dictionnaire. On utilise sa *clé* :

```python
>>> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
>>> dico_harry["Nom"] = "Potter-Weasley"
>>> dico_harry
{"Nom": "Potter-Weasley", "Prénom": "Harry", "Age": 17, "Animal": "Hedwige"}
```

À NOTER :  
*  on modifie ainsi : `dictionnaire[clé] = nouvellevaleur`
*  on modifie une *valeur* (jamais une clé)
*  pour vérifier que la modification a bien fonctionné il faut regarder la valeur du dictionnaire à la main

> **exercice 11 :**  
> C'est l'anniversaire d'Hermione ! Changez son âge pour lui donner 17 ans.

> **exercice 12 :**  
> Que se passe-t-il si on essaye de modifier une clé qui n'existe pas ?  
> Essayez sur un exemple et expliquez.

> **exercice 13 :**  
> Prenons comme exemple les meilleurs buteurs de l'équipe de France :
> ```python
> >>> buts = {}
> >>> buts["Giroud"] = 46
> >>> buts["Benzema"] = 36
> >>> buts["Henry"] = 51
> >>> buts["Griezmann"] = 42
> >>> buts["Mbappé"] = 24
> ```
> 1. Donnez le dictionnaire créé ainsi.
> 2. Donnez l'instruction pour trouver sa taille.
> 3. Donnez l'instruction pour trouver le score de Mbappé.
> 4. Donnez l'instruction pour vérifier si Kanté fait partie des joueurs.
> 5. But de Benzema ! Ajoutez 1 à son score.
> 6. Donnez l'instruction pour récupérer tous les scores.
> 7. Thierry Henry ne fait plus actuellement partie de l'équipe de France... Supprimez son score.
> 9. Ajoutez le score du joueur de votre choix.


## V. Parcours

On peut parcourir un dictionnaire avec la boucle `for`.

1. Meilleure façon : on parcourt les clés.

Exemple :
```python
dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
for cle in dico_harry.keys():
    print(cle, dico_harry[cle])
```
Cela donne l'affichage :
```python
Nom Potter
Prénom Harry
Age 17
```

> **exercice 14 :**  
> Complétez le code suivant pour afficher toutes les valeurs d'un dictionnaire :
> ```python
> dico_harry = {"Nom" : "Potter", "Prénom" : "Harry", "Age" : 17}
> for cle in dico_harry.keys():
>     print(.....)
> ```

2. Autres façons : en théorie, on peut parcourir aussi les valeurs avec dico.values() ou les couples avec dico.items(). On ne le fera pas ici (car dico.keys() est suffisant pour tout faire).

> **exercice 15 :**  
> Écrivez une fonction qui prend en paramètre un dictionnaire et qui affiche ses couples (clé, valeur).

> **exercice 16 :**  
> Écrivez une fonction qui prend en paramètre deux dictionnaires `dic1` et `dic2` et qui renvoie un unique dictionnaire contenant les couples (clé, valeur) de `dic1` et de `dic2`. *(on suppose qu'il n'y a pas de clé en commun)*
> Exemple d'appel :
> ```python
> >>> fusion({'a' : 1, 'b' : 2}, {'c' : 1, 'd' : 2})
> {'a' : 1, 'b' : 2, 'c' : 1, 'd' : 2}
> ```

> **exercice 17 :**  
>  Écrivez une fonction qui prend en paramètre un dictionnaire et renvoie deux tableaux, l'un contenant les clés et l'autre les valeurs.

## Pour aller plus loin

> Écrivez une fonction qui prend en paramètre une chaîne de caractères et renvoie un dictionnaire dont les clés sont les caractères de la chaîne et les valeurs le d'occurrences de chaque caractère. (nombre d'occurrences = nombre de fois où le caractère apparaît)

> Écrivez une fonction qui échange les clés et les valeurs d’un dictionnaire. (on suppose qu'il n'y a pas deux valeurs identiques)

> Écrivez une fonction qui prend en paramètre un dictionnaire dont les valeurs sont toutes des entiers et renvoie la somme de ces valeurs.

> Écrivez une fonction qui renvoie un dictionnaire dont les clés sont les 26 lettres de l'alphabet et les valeurs leur position. (par exemple le couple ("a", 1) doit appartenir au dictionnaire)

> Écrivez une fonction qui prend en paramètre un dictionnaire et renvoie True si toutes les clés et les valeurs d'un dictionnaires sont des chaînes de caractères, False sinon.

> Écrivez une fonction prenant en paramètre deux tableaux `cles` et `valeurs` et renvoie un dictionnaire dont les clés sont dans `cles` et les valeurs sont dans `valeurs`. (on suppose que les tableaux font la même taille)

> Soit les dictionnaires suivants :
> ```python
> magasin_A = {"Pomme" : 10, "Orange" : 15, "Fraise" : 3}
> magasin_B = {"Pomme" : 7, "Orange" : 4, "Fraise" : 8}
> ```
> Écrivez une fonction qui prend en paramètre ces deux dictionnaires, et renvoie un dictionnaire ayant les mêmes clés et dont les valeurs sont la plus grande valeur parmi celle de A ou de B.

> Si vous avez fini, appelez votre professeur puis vous pourrez aller sur FRANCE-IOI.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

