# Projet 2 : Puissance 4


L'objectif est de programmer un jeu de puissance 4 qui permette à deux joueurs de jouer l'un contre l'autre.

## Règles du jeu

Une grille de puissance 4 est une grille de 6 lignes et de 7 colonnes.
Chaque joueur a 21 jetons d'une couleur donnée (rouge et jaune traditionnellement).  
Un joueur a gagné lorsqu'il a aligné de manière verticale, horizontale ou diagonale 4 jetons.  
Lorsqu'un joueur a gagné, la partie s'arrête et il faut afficher le nom du vainqueur.  
Lorsque les deux joueurs n'ont plus de jetons et si aucun des joueurs n'a gagné, la partie est alors déclarée nulle.  

## Objectifs du projet

Pour simplifier, nous représenterons une case vide par un 0, les jetons du premier joueur par un 1 et les jetons du deuxième joueur par un 2.

La grille sera représentée par un tableau de tableau en Python.

Voici la liste des fonctions dont vous allez avoir besoin :

* Créer la grille. Toutes les cases sont vides au départ. Cette fonction doit donc renvoyer `[[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]]`.
* Afficher la grille. Les colonnes seront séparées par le symbole `|`. Le bas de la grille sera symbolisé par une ligne de `-`. Par exemple l'affichage de la grille vide donnera :
	```python
	| 0 | 0 | 0 | 0 | 0 | 0 | 0 |
	| 0 | 0 | 0 | 0 | 0 | 0 | 0 |
	| 0 | 0 | 0 | 0 | 0 | 0 | 0 |
	| 0 | 0 | 0 | 0 | 0 | 0 | 0 |
	| 0 | 0 | 0 | 0 | 0 | 0 | 0 |
	| 0 | 0 | 0 | 0 | 0 | 0 | 0 |
	-----------------------------
	```
* Vérifier s'il reste de la place dans une colonne. Cette fonction renverra un booléen : True s'il reste de la place dans la colonne ou False s'il n'y a plus de place. Une colonne est considérée pleine si la case au sommet n'est pas vide.
* Placer un jeton dans une colonne. Cette fonction doit placer le jeton passé en paramètre (1 ou 2) dans la case vide la plus en bas possible de la colonne indiquée.
* Détecter une ligne gagnante : cette fonction renverra un booléen : True s'il y a une ligne gagnante dans la grille (4 jetons d'affilée) ou False sinon.
* Détecter une colonne gagnante : même principe que la ligne.
* Détecter une diagonale gagnante : même principe que la ligne.
* Effectuer un coup pour un joueur : cette fonction doit demander au joueur concerné la colonne dans laquelle il souhaite placer son jeton. Si la colonne est pleine, on repose la question au joueur jusqu'à ce qu'il choisisse une colonne disponible.
* Lancer une partie : cette fonction doit créer la grille, puis tour à tour effectuer un coup pour le joueur 1 puis le joueur 2, en affichant la nouvelle grille et en vérifiant si l'un des deux a gagné. Elle affichera le nom du vainqueur à la fin (ou éventuellement égalité si les 42 jetons ont été placés).

## Travail à faire

> Créez un fichier Python et copiez-y le code suivant. Toutes les fonctions dont vous aurez besoin sont déjà présentes, il ne vous reste plus qu'à les compléter.
>
> * N'oubliez pas d'écrire des commentaires (avec #...) pour expliquer votre code.
> * Le respect des bonnes pratiques de programmation sera pris en compte.
>  
> *Conseil :* Testez vos fonctions au fur et à mesure de votre avancée.

```python
# PROJET PUISSANCE 4
# Binôme :
# - nom prénom à compléter
# - nom prénom à compléter

def creer_grille():
    """
    Crée la grille (6 lignes, 7 colonnes).
    
    :return: (list) la grille
    """
    grille = ???
    
    for i in range(???):
        ligne = [0 for x in range(???)]
        grille.append(ligne)
        
    return grille

def afficher_grille(grille):
    """
    Affiche la grille.
    
    :param grille: (list) la grille
    """
    for ligne in grille:
        print("| ", end="")
        
        for case in ligne:
            print(???, end=" | ")
        print()
        
    print("-" * ???)
    
def verifie_colonne(grille, numero_colonne):
    """
    Vérifie s'il reste de la place dans la colonne,
	c'est-à-dire si la case du haut est vide.
    
    :param grille: (list) la grille
    :param numero_colonne: (int) la colonne à regarder
    """
    assert 0 <= numero_colonne <= 6
    
    if ??? != 0:
        resultat = False
    else:
        resultat = True
        
    return resultat

def place_jeton(grille, numero_colonne, jeton):
    """
    Place le jeton dans la colonne.
    
    :param grille: (list) la grille
    :param numero_colonne: (int) la colonne où placer le jeton
    :param jeton: (int) le jeton (1 ou 2)
    :return: (list) la grille modifiée
    """
    assert verifie_colonne(grille, numero_colonne) == True
    
    for i in range(5, 0-1, -1):
        if grille[???][???] == 0:
            grille[???][???] = jeton
            return grille

def ligne_gagnante(grille, jeton):
    """
    Indique si une des lignes est gagnante,
    c'est-à-dire s'il y a 4 jetons d'affilée.
    
    :param grille: (list) la grille
    :param jeton: (int) le jeton (1 ou 2)
    :return: (bool) True s'il y a une ligne gagnante, False sinon
    """
    ???

def colonne_gagnante(grille, jeton):
    """
    Indique si une des colonnes est gagnante,
    c'est-à-dire s'il y a 4 jetons d'affilée.
    
    :param grille: (list) la grille
    :param jeton: (int) le jeton (1 ou 2)
    :return: (bool) True s'il y a une colonne gagnante, False sinon
    """
    ???

def diagonale_gagnante(grille, jeton):
    """
    Indique si une des diagonales est gagnante,
    c'est-à-dire s'il y a 4 jetons d'affilée.
    
    :param grille: (list) la grille
    :param jeton: (int) le jeton (1 ou 2)
    :return: (bool) True s'il y a une diagonale gagnante, False sinon
    """
    ???

def effectuer_coup(grille, joueur, jeton):
    """
    Effectue un coup pour le joueur en paramètre.
    
    :param grille: (list) la grille
    :param joueur: (str) le nom du joueur
    :param jeton: (int) le jeton du joueur (1 ou 2)
    :return: (list) la nouvelle grille
    """
    numero_colonne = input(???)
    numero_colonne = int(numero_colonne)
    
    while verifie_colonne(grille, numero_colonne) == False:
        numero_colonne = input(???)
        numero_colonne = int(numero_colonne)
        
    grille = place_jeton(grille, numero_colonne, jeton)
    return grille

def jouer():
    """
    Lance une partie.
    
    :return: (str) le nom du vainqueur (ou égalité)
    """
    joueur1 = input(???)
    joueur2 = input(???)
    dico_jetons = {1 : joueur1, 2 : joueur2}
    
    grille = creer_grille()
    afficher_grille(grille)
    
    for tour in range(???):
        for jeton in dico_jetons.keys():
            joueur = ???
            
            grille = effectuer_coup(grille, joueur, jeton)
            afficher_grille(grille)
            
            if ligne_gagnante(grille, jeton) or ??? or ??? :
                return "Victoire de " + joueur
                
    return "Egalité"
```

> **Pour aller plus loin :**  
> PENSEZ A FAIRE UNE COPIE DE VOTRE PROGRAMME ORIGINAL DANS UN FICHIER DIFFERENT pour conserver votre avancée si la suite ne fonctionne pas.  
> S'il vous reste du temps (ou que vous avez envie de vous amuser un peu à la maison), vous pouvez :
> * gérer le cas où un joueur n'entre pas une bonne valeur pour les colonnes (par exemple si l'utilisateur entre "8" ou "a".)
> * générer un mode contre l'ordinateur
> 	(soit en supposant que l'ordinateur joue au hasard,
> 	soit en « créant » une intelligence artificielle
> 	permettant à l'ordinateur de réfléchir au meilleur coup)
> * créer une joli interface graphique avec Turtle.
> * ...toute autre idée d'amélioration !

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
