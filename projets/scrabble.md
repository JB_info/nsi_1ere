# Projet 3 : scrabble

### Objectif

Si vous ne connaissez pas le scrabble , vous pouvez aller lire les [règles du jeu](https://www.regles-de-jeux.com/regle-du-scrabble/).

Il existe de nombreux sites en ligne qui permettent à une personne de jouer au scrabble contre l'ordinateur, comme par exemple [celui-ci](https://www.clicmouse.fr/scra/).

L'ordinateur contre lequel on joue alors est une "Intelligence Artificielle", terme défini en 1956 par John McCarthy.

L'objectif de ce projet est de programmer une intelligence artificielle pour le scrabble.

### Fichier fourni

Il va d'abord nous falloir le dictionnaire contenant tous les mots qui sont autorisés. Téléchargez donc [ce fichier](src/dictionnaire.txt) *dans un dossier que vous allez retrouver*.


### Travail à faire

* fonction qui récupère le dictionnaire depuis le fichier
* fonction qui trie le dictionnaire
* fonction qui cherche un mot dans le dictionnaire
* fonction qui donne tous les mots possibles à partir d'un tirage de lettres
* fonction qui calcule le score d'un mot
* fonction qui donne le mot au meilleur score s'écrivant avec un tirage de lettres
* fonction qui prend en paramètres une liste de mots déjà placés et un tirage de 7 lettres et qui renvoie le meilleur mot que l'on peut écrire et à quel mot déjà placé le rattacher
* fonction qui prend en paramètre un jeu de lettres et tire au sort celles manquantes pour atteindre 7
* fonction qui crée deux jeux de 7 lettres (un par joueur), puis demande à chaque joueur tour à tour le mot qu'il souhaite jouer (en lui affichant les 3 meilleurs solutions possibles à chaque fois). On réalise 10 tours. N'oubliez pas de tirer à nouveau les lettres manquantes entre chaque tour.


### Améliorations possibles

Si vous avez terminé, vous pouvez :
* créer une vraie grille de scrabble pour y placer les mots
* créer une interface graphique (avec [pygame](https://www.pygame.org/docs/) par exemple)
* ... toute autre idée d'amélioration !
