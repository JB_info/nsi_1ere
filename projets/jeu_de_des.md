# Projet 1 : Jeu de dé

Pour ce premier projet, nous allons créer un jeu de dé aléatoire entre deux joueurs. Nous allons utiliser les constructions élémentaires que nous avons vu jusqu'à présent et nous allons découvrir l'utilisation d'une bibliothèque, *turtle*.

## 1. Utilisation de bibliothèques

Avant de se lancer dans le projet, nous allons rapidement voir comment on utilise une bibliothèque.

Nous avons déjà utilisé quelques fonctions natives de Python, comme *len*, *type*, *pow*, *min*... Ces fonctions font partie de la bibliothèque « standard » de Python et sont toujours disponibles. Vous pouvez trouver la liste des fonctions disponibles sur la [documentation officielle](https://docs.python.org/fr/3/library/functions.html).

Il est également possible d’utiliser d’autres fonctions qui ne sont pas accessibles par défaut. Pour cela il faut charger une **bibliothèque** (aussi appelé *module*). Il en existe des centaines, la liste des modules officiels de Python est disponible [en ligne](https://docs.python.org/fr/3/py-modindex.html). Pour utiliser les fonctions d’une bibliothèque il faut commencer par l'**importer**.

Par exemple, la bibliothèque `math` comporte des fonctions utiles pour les opérations mathématiques. Pour importer cette bibliothèque on utilise le mot-clef **import**, que l'on place au début de notre programme :

```python
>>> import math
```

On peut ensuite utiliser les fonctions de cette bibliothèque. Par exemple, le module `math` propose une fonction `sqrt` qui permet de calculer la racine d'un nombre. Pour l'utiliser, il faut préciser le nom du module, suivi d'un point, suivi d'un appel de fonction classique :

```python
>>> math.sqrt(4)
2.0
```

> **exercice 1 :**
>
> La bibliothèque `math` propose 2 fonctions `floor` et `ceil` qui permettent d'arrondir un nombre flottant à l'inférieur / au supérieur.
>
> Importez le module et testez les deux fonctions sur les nombres 11.36, 2.9 et 7.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

Il existe une deuxième méthode pour importer les fonctions d'une bibliothèque :

```python
>>> from math import sqrt
```

La syntaxe est **from** suivi du nom du module, suivi de **import** et du nom de la (des) fonction(s) à importer. Si on importe les fonctions ainsi, on peut effectuer des appels sans avoir à repréciser le nom de la bibliothèque :

```python
>>> sqrt(4)
2.0
```

On peut importer plusieurs fonctions en séparant leurs noms par une virgule :

```python
>>> from math import sqrt, cos, sin
```

On peut également importer toutes les fonctions d'un module avec le signe ***** :

```python
>>> from math import *
```

> **exercice 2 :**
>
> Reprenez l'exercice 1 avec cette deuxième méthode.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

Bonne pratiques :

* Généralement, on utilise la première méthode lorsqu'on a besoin d'un usage ponctuel de la bibliothèque (une ou deux fois dans tout le programme) et la deuxième méthode lorsqu'on a besoin d'un usage régulier de la bibliothèque (plus de trois fois dans le programme).
* Si on importe beaucoup de bibliothèques dans un même programme, on utilise plutôt la première méthode.

* On évite d'importer tout le module avec \* si on n'a besoin que d'une ou deux fonctions !

Certaines bibliothèques ne contiennent pas uniquement des fonctions, mais également des **constantes**. Les constantes sont des variables dont la valeur de change jamais.

Par exemple, la bibliothèque `math` propose une constante pour représenter le nombre $`\pi`$ :

```python
>>> import math
>>> math.pi
3.141592653589793
```

> **exercice 3 :**
>
> Importez puis regardez la valeur des constantes `e` et `inf`.
>
> ```
> 
> 
> 
> 
> 
> ```

Pour notre projet, nous allons avoir besoin de 2 bibliothèques :

* *random* ([documentation ici](https://docs.python.org/fr/3/library/random.html#module-random))
* turtle ([documentation ici](https://docs.python.org/fr/3/library/turtle.html#module-turtle))

Pour la bibliothèque *random*, ce sera à vous de lire la documentation des fonctions pour trouver celle(s) dont vous avez besoin. Cette bibliothèque propose des fonctions pour faire des tirages aléatoires.

Le module turtle permet de faire des dessins simples assez facilement. La bibliothèque *turtle* possède énormément de fonctions, voilà donc un résumé des fonctions dont vous pourriez avoir besoin :

|                nom de la fonction                |                           objectif                           |                          paramètres                          | résultat |
| :----------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :------: |
|           `turtle.forward`(*distance*)           | Avance la tortue de la *distance* spécifiée, dans la direction où elle se dirige. |         *distance* -- un nombre (entier ou flottant)         |   None   |
|          `turtle.backward`(*distance*)           | Déplace la tortue de *distance* vers l'arrière (dans le sens opposé à celui vers lequel elle pointe). Ne change pas le cap de la tortue. |         *distance* -- un nombre (entier ou flottant)         |   None   |
|             `turtle.right`(*angle*)              | Tourne la tortue à droite de *angle* unités (les unités sont par défaut des degrés). |          *angle* -- un nombre (entier ou flottant)           |   None   |
|              `turtle.left`(*angle*)              | Tourne la tortue à gauche d'une valeur de *angle* unités (les unités sont par défaut des degrés). |          *angle* -- un nombre (entier ou flottant)           |   None   |
|               `turtle.position`()                |       Renvoie la position actuelle de la tortue (x,y).       |                              /                               |  tuple   |
|                `turtle.heading`()                |                 Renvoie le cap de la tortue.                 |                              /                               |  float   |
|             `turtle.goto`(*x*, *y*)              | Déplace la tortue vers une position absolue. Si le stylo est en bas, trace une ligne. Ne change pas l'orientation de la tortue. |              *x* -- un nombre, *y* -- un nombre              |   None   |
|                 `turtle.home`()                  | Déplace la tortue à l'origine — coordonnées (0,0) — et l'oriente à son cap initial. |                              /                               |   None   |
|            `turtle.circle`(*radius*)             |             Dessine un cercle de rayon *radius*.             |                    *radius* -- un nombre                     |   None   |
|                 `turtle.undo`()                  | Annule la ou les dernières (si répété) actions de la tortue. |                              /                               |   None   |
|             `turtle.speed`(*speed*)              | Règle la vitesse de la tortue à une valeur entière comprise entre 1 et 10 inclus. | *speed* -- un nombre entier compris dans l’intervalle entre 1 et 10 inclus (1 lent -> 10 rapide) |   None   |
|                 `turtle.down`()                  |   Baisse la pointe du stylo — dessine quand il se déplace.   |                              /                               |   None   |
|                  `turtle.up`()                   | Lève la pointe du stylo  — pas de dessin quand il se déplace. |                              /                               |   None   |
|                `turtle.isdown`()                 | Renvoie `True` si la pointe du stylo est en bas et `False` si elle est en haut. |                              /                               |   bool   |
|            `turtle.pencolor`(*color*)            |                  Règle la couleur du stylo.                  | *color* -- une chaîne de spécification de couleur, telle que `"red"`, `"yellow"`... |   None   |
|           `turtle.fillcolor`(*color*)            |               Règle la couleur de remplissage.               | *color* -- une chaîne de spécification de couleur, telle que `"red"`, `"yellow"`... |   None   |
|              `turtle.begin_fill`()               |    À appeler juste avant de dessiner une forme à remplir.    |                              /                               |   None   |
|               `turtle.end_fill`()                | Remplit la forme dessinée après le dernier appel à `begin_fill()`. |                              /                               |   None   |
|                 `turtle.clear`()                 | Supprime les dessins de la tortue de l'écran. Ne déplace pas la tortue. |                              /                               |   None   |
| `turtle.write`(*texte*, *move*, *align*, *font*) | Écrit du texte - La représentation de la chaîne *texte* - à la position actuelle de la tortue conformément à *align* ("*left*", "*center*" ou "*right*") et en police donnée. Si *move* est `True`, le stylo est déplacé vers le coin inférieur droit du texte. *Exemple : write("mon texte à écrire", False, "left", ("Arial", 24 ,"normal"))* | *texte* -- texte à écrire sur l'écran turtle / *move* -- `True` ou `False` / *align* -- l'une des chaînes de caractères suivantes : "left", "center" ou "right" / *font* -- triplet (nom de police, taille de police, type de police) |   None   |

Je vous conseille avant de commencer le projet d'importer *turtle* dans le shell et de tester quelques fonctions.

## 2. Le projet

Nous allons créer un jeu de dé aléatoire entre deux joueurs.

À chaque tour de jeu, chacun des deux joueurs lance un dé à 6 faces. Celui qui a le plus gros score remporte la manche et gagne 1 point.  Le joueur qui remporte la partie est celui qui arrive à avoir 2 points de plus que son adversaire.

Voici un exemple de partie entre deux joueurs "j1" et "j2" :

* Manche 1 : j1 fait un 3 et j2 fait un 5 $`\rightarrow`$ j2 remporte 1 point (score : 0 / 1)
* Manche 2 : j1 fait un 4 et j2 fait un 3 $`\rightarrow`$ j1 remporte 1 point (score : 1 / 1)
* Manche 3 : j1 fait un 5 et j2 fait un 1 $`\rightarrow`$ j1 remporte 1 point (score : 2 / 1)
* Manche 4 : j1 fait un 4 et j2 fait un 4 $`\rightarrow`$ égalité (score : 2 / 1)
* Manche 5 : j1 fait un 6 et j2 fait un 1 $`\rightarrow`$ j1 remporte 1 point (score : 3 / 1)
* j1 a deux points de plus que j2, la partie est terminée : victoire de j1

Vous pouvez choisir l'affichage que vous préférez pour votre jeu (les dessins des points des dés peuvent être carrés ou ronds, vous pouvez choisir les couleurs, le texte à afficher...). Voici un exemple possible d'affichage pour la partie aléatoire décrite ci-dessus :

![exemple de partie](img/exemple_partie_de.png)

Dernière chose à savoir avant de se lancer : 

Il est possible de laisser des **commentaires** dans un programme Python, c'est à dire du code qui va être ignoré par l'ordinateur. Les commentaires peuvent servir à expliquer une partie compliquée d'un programme, ou à mettre des indications dans le code.

En Python, on insère un commentaire avec le caractère dièse `#`. Par exemple :

```python
if nombre % 2 == 0:
    resultat = True # le nombre est pair
else :
    resultat = False # le nombre est impair
```

N'hésitez pas à utiliser des commentaires dans votre projet pour expliquer votre raisonnement, ou pour expliquer ce que vous cherchiez à faire à un endroit du programme.

> **Travail à faire :**
>
> Voilà les différents éléments du projet qui seront évalués :
>
> * dessin d'un carré
> * dessin des 6 faces existantes
> * optimisation du dessin des faces
> * tirage aléatoire d'une face
> * affichage aléatoire de 2 faces
> * affichage du résultat d'une manche
> * comptage des scores
> * détection de fin d'une partie
> * bon choix des fonctions à écrire
> * documentation (et éventuelles préconditions / postconditions) des fonctions
> * code propre, bonnes pratiques respectées, commentaires éventuels clairs



> **Pour aller plus loin :**
>
> Si vous avez terminé le projet, vous pouvez travailler sur une des améliorations suivantes. *Pensez à travailler sur un fichier différent pour conserver votre programme à rendre propre.*
>
> * améliorer l'affichage d'une partie (affichage des scores par exemple)
>
> * changer le système de comptage des points pour le jeu (au lieu de remporter 1 point, le joueur remporte le score de son dé par exemple)
>
> * créer un jeu similaire mais avec 3 joueurs
>
> * créer un jeu de "zanzibar"
>
>   Règles : Le premier joueur lance 3 dés. S'il fait un 1, il marque 100 points, s'il fait un 6 il marque 60 points, les autres faces du dé indiquent le nombre de points correspondant (2= 2 points, 3= 3 points, 4= 4 points, 5= 5 points).
>
>   Le gagnant est celui qui a le plus de points au bout de 3 tours. Vous choisissez le nombre de joueurs.
>
> * créer un jeu de "suites"
>
>   Règles : Le premier joueur lance 6 dés. L'ordre des dés n'est pas important.
>   S'il fait 1.2.3.4.5.6. il marque 25 points
>   S'il fait 1.2.3.4.5. et un autre il marque 20 points
>   S'il fait 1.2.3.4. et deux autres il marque 15 points
>   S'il fait 1.2.3. et trois autres il marque 10 points
>   S'il fait 1.2. et quatre autres il marque 5 points
>
>   Puis c'est au deuxième joueur de tenter de faire une suite et de marquer ses points.
>
>   Attention, si un joueur fait trois 1, il repart à zéro.
>   Le gagnant est celui qui atteint 100 points.



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*