# Projet 2 : Filtres pour images


## Présentation

L'objectif est de créer des filtres pour des images comme les filtres Instagram.

Nous utiliserons la bibliothéque Python PIL (Python Image Library), qui permet d'accéder simplement aux pixels d'une image :
```python
from PIL import Image
```

![pixels](https://upload.wikimedia.org/wikipedia/commons/2/2b/Pixel-example.png)

Une image se décompose en une multitude de points appelés pixels.
Chaque pixel possède sa propre couleur. Cette couleur résulte de la combinaison des trois couleurs primaires : rouge, vert et bleu (Red, Green et Blue : RGB). Ces niveaux peuvent en général prendre
toutes les valeurs entre 0 et 255. On représente ainsi la couleur d'un pixel par trois nombres correspondant aux niveaux de rouge, vert et bleu.  
Par exemple pour un pixel vert on aura (0, 255, 0). Vous pouvez aller sur [ce site](https://htmlcolorcodes.com/fr) pour voir le code RGB de toutes les couleurs affichables sur un écran.

La documentation de PIL est disponible à [cette adresse](https://pillow.readthedocs.io/en/stable/reference/Image.html).

Pour vous faciliter la prise en main de cette bibliothéque, voici les commandes les plus utiles :

| Commande                               | Description                                                                                     |
|----------------------------------------|-------------------------------------------------------------------------------------------------|
| image = Image.open("photo.jpg")        | Ouvre le fichier "photo.jpg" et le stocke dans la variable image.                               |
| image = Image.new("RGB", (larg, haut)) | Crée une nouvelle image de largeur larg et de hauteur haut et la stocke dans la variable image. |
| copie = image.copy()                           | Retourne une copie de l’image. Utile avant de transformer une image.                    |
| image.save("photo.jpg")                | Sauvegarde l’objet image dans le fichier "photo.jpg".                                           |
| image.show()                           | Affiche l’image dans une fenêtre.                                                               |
| image.width                            | Retourne la largeur de l’image en pixels.                                                       |
| image.height                           | Retourne la hauteur de l’image en pixels.                                                       |
| image.getpixel((x, y))                 | Retourne la couleur du pixel (x, y) de image.                                                   |
| image.putpixel((x, y),(r, g, b))       | Modifie la couleur du pixel (x, y) de image en (r, g, b).                                       |

## Travail à faire

Vous allez devoir créer différents filtres.

> Téléchargez l'image suivante : [maison.jpg](img/maison.jpg)

C'est sur cette image que vous testerez vos filtres. Vous pourrez *à la fin* du projet tester sur d'autres images personnelles.

> Créez un fichier Python et copiez-y le code suivant.

```python
# PROJET FILTRES
# Binôme :
# - nom prénom à compléter
# - nom prénom à compléter

def augmente_luminosite(nom_image):
    original = Image.open(nom_image)
    largeur = original.width
    hauteur = original.height
    image = Image.new("RGB", (largeur, hauteur))
    
    for x in range(largeur):
        for y in range(hauteur):
            (r, g, b) = original.getpixel((x, y))
            image.putpixel((x, y), (r+100, g+100, b+100))
    
    return image
```

> Testez la fonction sur l'image de la maison. Il faudra donc utiliser les lignes suivantes dans la console :
> ```python
> >>> image = augmente_luminosite("maison.jpg")
> >>> image.show()
> ```
> Indiquez avec des commentaires (#...) sur chaque ligne ce que fait la fonction `augmente_luminosite`.
> Ajoutez aussi la documentation de la fonction.

En vous appuyant sur le code de la fonction ci-dessus, vous allez ajouter des fonctions appliquant différents filtres à une image (une fonction par filtre, dans le même fichier).

> Créez un premier filtre qui ne garde que la composante **rouge** des pixels d'une image. C'est-à-dire qu'il faut pour chaque pixel mettre le vert et le bleu à zéro et ne pas toucher à la composante rouge.  
> Vous écrirez ce filtre dans une fonction nommée `filtre_rouge(nom_image)` qui prend en paramètre le nom d'une image et qui renvoie la nouvelle image.  
> Pour testez votre fonction, il faudra donc utiliser les lignes suivantes dans la console :
> ```python
> >>> image = filtre_rouge("maison.jpg")
> >>> image.show()
> ```

> Créez deux autres filtres, un **vert** et un **bleu** sur le même principe que le filtre rouge.

> Créez une fonction **interface** qui propose de choisir le filtre souhaité et d'enregistrez l'image dans un nouveau fichier. Voici le début pour vous guider :

```python
def interface():
    nom_image = input("Nom de l'image à utiliser ?")
    
    print("Voici les filtres disponibles :")
    print("1 - rouge")
    print("2 - vert")
    print("3 - bleu")
    filtre = input("Quel filtre voulez-vous appliquer ? ")
    
    if filtre == "1":
        image = filtre_rouge(nom_image)
        
    # COMPLETER ICI AVEC LES AUTRES FILTRES
    
    print("Le filtre est appliqué.")
    print("Vous pouvez :")
    print("1 - sauvegarder l'image")
    print("2 - ouvrir l'image")
    sauvegarde = input("Que souhaitez-vous faire ? ")
    
    if sauvegarde == "1":
        # DEMANDER A L'UTILISATEUR LE NOM DU FICHIER POUR LA SAUVEGARDE
        # PUIS SAUVEGARDER L'IMAGE
    elif sauvegarde == "2":
        # AFFICHER L'IMAGE
    else:
        # A VOTRE AVIS, COMMENT COMPLETER ICI ?
```

Maintenant que vous avez la base du programme, vous allez créer d'autres filtres. Pour chacun des filtres suivants, écrivez une fonction similaire à celles déjà faites puis *ajoutez le filtre dans l'interface*.

> Créez un filtre **gris** qui transforme une image en nuances de gris. Pour cela, chaque composante d'un pixel prend la moyenne des trois composantes.

> Créez un filtre **noir et blanc** qui met chaque pixel soit en blanc soit en noir en fonction de son niveau de gris. C'est-à-dire que pour chaque pixel, il faut calculer la moyenne des trois composantes. Si la moyenne est inférieure à 128 alors le pixel devient noir, si c'est supérieur, il devient blanc.

> Créez un filtre **miroir** qui retourne l'image comme si on la voyait dans un miroir.

> Créez un filtre **color** qui prend en paramètre une des trois couleurs primaires et un nombre entier k. Il ajoute alors ce nombre à la couleur correspondante de chaque pixel et enlève k/2 aux deux autres couleurs (il n’y a pas besoin de faire attention aux limites 0 et 255, PIL s’en charge automatiquement).

> Créez un filtre **color512** qui limite à 8 le nombre de valeurs pour chaque composante de façon à donner un effet « vieux jeu vidéo » à l'image. Par exemple si la composante est entre 0 et 31 alors on la remplace par 0, entre 32 et 63 on la remplace par 32...

Le rendu pourra parfois être meilleur avec une autre image que vous trouverez sur internet.

N'oubliez pas la documentation de vos fonctions.  
Les bonnes pratiques de programmation seront prises en compte.

> **Pour aller plus loin :**  
> PENSEZ A FAIRE UNE COPIE DE VOTRE PROGRAMME ORIGINAL DANS UN FICHIER DIFFERENT pour conserver votre avancée si la suite ne fonctionne pas.  
> S'il vous reste du temps (ou que vous avez envie de vous amuser un peu à la maison), vous pouvez créer plus de filtres (pensez aussi à les ajouter à l'interface) :
> * Un filtre **color256** qui limite à 4 le nombre de valeurs pour chaque composante (similaire à color512).
> * Un filtre **pixel3** qui parcourt l’image par bloc de 3 pixels de large et remplace les couleurs des 9 pixels par une même couleur qui est la moyenne des 9 couleurs. Cela devra faire l’effet d’une image pixelisée.
> * Un filtre **pixel10** qui fait la même chose que le filtre pixel3 mais avec des blocs de 10 pixels de large.
> * Un filtre **swap** qui prend en paramètre deux couleurs primaires et échange les valeurs de ces composantes pour chaque pixel.
> * ...toute autre idée de filtre !
---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
