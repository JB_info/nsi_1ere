# PROJET PENDU

# NOM 1:
# PRENOM 1:
# NOM 2:
# PRENOM 2:

# ------------------------------------------------------------------------

# bibliothèques utiles (ne pas y toucher)
import random
from turtle import *
from time import sleep
from math import sqrt
import itertools

# ------------------------------------------------------------------------

# VOUS AUREZ BESOIN DE TOUTES CES POSITIONS ET TAILLES POUR LE DESSIN :
# (ne pas modifier cette partie)

# taille du crayon
crayon = 5
# tailles pour le mot
taille_lettre = 24
taille_espace = taille_lettre // 2
# position (x, y) en bas à gauche du mot
x_mot_bg = -300
y_mot_bg = +50
# position (x, y) en bas à gauche du pendu
x_pendu_bg = -300
y_pendu_bg = -250
# tailles de la potence
bas = 200
corde = 40
gauche = bas + corde
haut = gauche // 2
# tracer le triangle en haut à gauche de la potence
x_triangle = x_pendu_bg
y_triangle = y_pendu_bg + bas
taille_triangle = sqrt(2) * corde
angle_triangle = 45
# tailles des traits du pendu
rayon_tete = 30
taille_bras_jambe = sqrt(2) * rayon_tete
taille_corps = rayon_tete * 2
# position pour le cercle de la tête
x_tete = x_pendu_bg + haut
y_tete = y_pendu_bg + bas - 2 * rayon_tete
# position pour le trait du corps
x_corps = x_tete
y_corps = y_tete
# position pour les bras
x_bras = x_tete
y_bras = y_tete - 10
# position pour les jambes
x_jambe = x_tete
y_jambe = y_tete - 2 * rayon_tete
# position pour le visage
rayon_oeil = 4
x_oeil_gauche = x_tete - 10
y_oeil_gauche = y_triangle - 25
x_oeil_droit = x_tete + 10
y_oeil_droit = y_oeil_gauche
rayon_sourire = -rayon_tete
taille_sourire = 2 * rayon_tete
x_sourire = x_oeil_gauche - 5
y_sourire = y_oeil_gauche - 20
# position pour le mode triche
x_triche = 0
y_triche = -rayon_tete


# ------------------------------------------------------------------------

# POUR CHAQUE FONCTION, AIDEZ VOUS DE LA DOCUMENTATION
# POUR ECRIRE SON CODE

def mots_dictionnaire():
    """
    FONCTION COMPLETE NE PAS Y TOUCHER !
    Fournit tous les mots du dictionnaire dans un tableau.
    
    :return: (str)
    """
    fichier = open("dictionnaire.txt", "r", encoding="utf8")
    lignes = fichier.readlines()
    fichier.close()
    return [mot.rstrip("\n") for mot in lignes]


def mot_aleatoire_dictionnaire(dictionnaire):
    """
    FONCTION COMPLETE NE PAS Y TOUCHER !
    Donne un mot aléatoire dans le dictionnaire.
    
    :param dictionnaire: (list)
    :return: (str)
    """
    return random.choice(dictionnaire)


def creer_masque(mot):
    """
    Crée un masque pour le mot,
    c'est-à-dire une chaîne de caractères
    où toutes les lettres sont remplacées par un tiret.    
    Par exemple un masque pour "TEST" est "----".
    
    :param mot: (str)
    :return: (str)
    """
    # COMPLETER ICI
    

def initialise_affichage(mot):
    """
    * Cache la tortue (FAIT)
    * Initialise la taille du crayon (FAIT)
    * Se déplace à la bonne position (FAIT) puis dessine autant de traits que de lettres dans le mot
    * Se déplace à la bonne position puis dessine la potence
    
    :param mot: (str)
    """
    hideturtle()
    pensize(crayon)
    
    up()
    goto(x_mot_bg, y_mot_bg)
    down()
    
    # COMPLETER ICI


def demande_lettre():
    """
    FONCTION COMPLETE NE PAS Y TOUCHER !
    
    * demande à l'utilisateur d'entrer une lettre (ou espace pour le mode triche)
    * si la lettre est None, ferme Turtle puis quitter
    * si l'utilisateur a saisi plus qu'une lettre ou a entré un caractère non valide, repose la question
    * transforme la lettre en majuscule 
    * renvoie la lettre
    
    :return: (str)
    """
    lettres_valides = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
    lettre = textinput("CHOIX DE LA LETTRE", "Entrez une lettre :") # ouvre une mini-fenêtre pour saisir la lettre
    if lettre == None: # si l'utilisateur ferme la fenêtre
        bye()
        exit()
    elif len(lettre) != 1 or lettre[0] not in lettres_valides:
        lettre = demande_lettre() # redemande la lettre si mauvaise saisie
    return lettre.upper() # .upper() permet de passer en majuscule


def modifier_masque(mot, masque, lettre):
    """
    Modifie le masque pour y faire apparaître toutes les lettres.
    Par exemple le masque "T--T-R" du mot "TESTER" avec la lettre "E" est modifié en "TE-TER".
    
    :param mot: (str)
    :param masque: (str)
    :param lettre: (str)
    :return: (str)
    """
    # COMPLETER ICI
    
    
def ajouter_lettre(mot, lettre):
    """
    Ajoute la lettre à la fenêtre Turtle au dessus des _ correspondants dans le mot.
    Par exemple pour le mot "TESTER" et la lettre "E",
    il faut écrire dans Turtle les deux "E"
    au dessus du deuxième et du cinquième trait.
    
    * se déplace au dessus du 1er trait (FAIT)
    * pour chaque lettre du mot
    * si cette lettre correspond à celle cherchée, écrire la lettre à la position actuelle
    * se déplacer vers la droite à la position suivante
    
    Pour écrire en turtle : write(lettre, False, "left", ("Arial", 24 ,"normal"))
    
    :param mot: (str)
    :param lettre: (str)
    """
    up()
    goto(x_mot_bg + 3, y_mot_bg + 3) # se positionne juste au dessus des tirets
    down()
    
    # COMPLETER ICI
    

def ajouter_trait(nb_erreurs):
    """
    Ajoute un trait supplémentaire au pendu
    en fonction du nombre d'erreurs.
    
    -> pour dessiner le sourire, se déplacer à la bonne position puis utiliser :
    * left(-rayon_sourire)
    * circle(rayon_sourire, taille_sourire)
    
    :param nb_erreurs: (int)
    """
    if nb_erreurs == 1:
        up()
        goto(x_tete, y_tete)
        down()
        circle(rayon_tete)
        
    elif nb_erreurs == 2:
        up()
        goto(x_corps, y_corps)
        down()
        # COMPLETER ICI
        
    # COMPLETER ICI
        

def mots_dictionnaires_tries(dictionnaire):
    """
    Renvoie les mots du dictionnaire triés dans l'ordre alphabétique.
    
    * Vous devez utiliser une méthode de tri vue en cours.
    * Il y beaucoup de mots, il faut donc utiliser la méthode la plus efficace.
    
    :param dictionnaire: (list)
    :return: (list)
    """
    # COMPLETER ICI
    
    
def mot_valide(mot, dictionnaire_trie):
    """
    Renvoie True si le mot est valide (c'est-à-dire s'il existe dans le dictionnaire), False sinon.
    
    * Vous devez utiliser une méthode de recherche vue en cours.
    * Il y beaucoup de mots, il faut donc utiliser la méthode la plus efficace.
    
    :param mot: (str)
    :param dictionnaire_trie: (list)
    :return: (bool)
    """
    # COMPLETER ICI
    

def combinaisons_possibles(masque):
    """
    FONCTION COMPLETE NE PAS Y TOUCHER !
    Renvoie toutes les combinaisons de lettres possibles pour compléter le masque.
    On ne vérifie pas que ce sont des mots corrects.
    Par exemple pour "T--", les combinaisons sont "TAA", "TAB", "TAC", ..., "TBA", "TBB", ...
    
    :param masque: (str)
    :return: (list)
    """
    resultat = [list(i) for i in itertools.product("ABCDEFGHIJKLMNOPQRSTUVWXYZ", repeat = masque.count("-"))] # crée les combinaisons pour les tirets
    for i in range(len(masque)):
        [mot.insert(i, masque[i]) for mot in resultat if masque[i] != "-"] # insère les lettres déjà existantes entre les tirets   
    return ["".join(mot) for mot in resultat] # transforme les tableaux en chaînes pour reconstituer les mots


def affiche_triche(mots):
    """
    FONCTION COMPLETE NE PAS Y TOUCHER !
    Affiche les mots du mode triche sur le côté.
    S'il y en a beaucoup, seuls les 10 premiers sont affichés.
    
    :param mots: (str)
    """
    # efface la triche précédente
    up()
    goto(x_triche - 50, y_triche + 50)
    down()
    pencolor("white")
    fillcolor("white")
    begin_fill()
    for _ in range(4):
        forward(300)
        right(90)
    end_fill()
    
    # écris les mots en jaune en bas à droite
    right(90)
    up()
    goto(x_triche, y_triche)
    down()
    pencolor("yellow")
    for i in range(min(10, len(mots))):
        write(mots[i], False, "left", ("Arial", 12 ,"normal"))
        up()
        forward(20)
        down()
        
    # retourne à l'état initial
    pencolor("black")
    setheading(0)
    
    
def mode_triche(masque, dictionnaire):
    """
    Affiche les mots du dictionnaire qu'il est possible d'écrire avec ce masque.
    
    * trouve tous les combinaisons possibles (FAIT)
    * crée un tableau de mots valides vide (FAIT)
    * pour chaque combinaison
        * si c'est un mot valide, l'ajouter au tableau de mots valides
    * afficher le tableau (FAIT)
    
    :param masque: (str)
    :param dictionnaire: (list)
    """
    combinaisons = combinaisons_possibles(masque)
    mots_valides = []
    
    # COMPLETER ICI
    
    affiche_triche(mots_valides)


def jouer():
    """
    Lance une partie de pendu.
    
    * Récupère le dictionnaire (FAIT)
    * Choisit un mot aléatoire (FAIT)
    * Crée le masque (FAIT)
    * Initialise l'affichage (FAIT)
    * tant que le nombre d'erreur < 7 et que le masque est différent du mot :
        * demander la lettre
        * si la lettre est l'espace (" ")
            * appeler le mode triche
        * si la lettre est dans le mot :
            * modifier le masque
            * afficher la lettre
        * sinon
            * ajouter un trait au pendu correspondant au nouveau nombre d'erreurs
    * attendre 3 secondes (sleep(3))
    * si le masque est égal au mot, afficher un message de félicitations
    * sinon, afficher un message de défaite précisant quel était le mot à trouver
    
    Exemple de message de fin :
    pencolor("green")
    write("GAGNÉ ! Bien joué, le mot était « " + mot + " ».", False, "left", ("Arial", 24 ,"normal"))
    """
    dictionnaire = mots_dictionnaire()
    mot = mot_aleatoire_dictionnaire(dictionnaire)
    masque = creer_masque(mot)
    initialise_affichage(mot)
    
    # A COMPLETER


# ------------------------------------------------------------------------

# LANCE LE JEU A L'INFINI (ne pas y toucher)

while True:
    clear()
    jouer()
    sleep(5)
    pencolor("black")
        