# Projet final : Morpion sur une page Web


![morpion](img/morpion.png)

#### 1

L'objectif est de créer un jeu de morpion (ou 'tic tac toe') sur une page Web.

Le code HTML devra donc contenir un tableau de taille 3x3.

Le CSS devra mettre en forme le tableau.

Le JS devra gérer les événements :

* un clic sur une case ajoute une croix bleue à l'intérieur
* un double clic sur une case ajoute un rond rouge à l'intérieur

#### 2

Quand c'est fait, il faudra alors ajouter un bouton pour effacer le contenu du tableau et recommencer une nouvelle partie.

#### 3

Vous pouvez maintenant améliorer un peu le visuel.

Par exemple :

* la case change de couleur quand on la survole (mais seulement si elle est vide)
* faire des belles bordures au tableau
* centrer le jeu sur la page
* ajouter un petit titre
* ajouter un paragraphe qui explique comment on doit cliquer
* ... toute autre idée !

#### 4

Si vous avez le temps, vous pourrez aussi faire en sorte que le JS détecte quand il y a trois croix ou ronds alignés et affiche un message d'alerte (ex : "Victoire des bleus").

Il faudra alors aussi gérer le cas de l'égalité (quand le tableau est plein).

#### 5

Bravo ! Vous pouvez maintenant jouer 😄


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
