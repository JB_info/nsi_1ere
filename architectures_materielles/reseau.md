# Réseau

Un **réseau** informatique est un **ensemble d'équipements reliés entre eux et qui peuvent s'échanger des informations.**

> **exercice 1 :**  
> Citez quelques équipements qui peuvent faire partie d'un réseau informatique (objets connectés...).

Dès les années 50, les ordinateurs ont été mis en réseau pour échanger des informations, mais de façon très liée aux constructeurs d’ordinateurs ou aux opérateurs téléphoniques. Les réseaux généraux indépendants des constructeurs sont nés aux États-Unis avec ArpaNet (1970) et en France avec Cyclades (1971). Cet effort a culminé avec internet, né en 1983.

Certains réseaux sont limités à une salle, voire un bâtiment. D'autres font la taille d'une ville ou d'un quartier, et d'autres encore ont une étendue nationale ou mondiale :

- Réseaux locaux :

  Les réseaux les plus petits contiennent entre 2 et 100 appareils, qui sont reliés avec des câbles ou une connexion sans fil.  C'est le cas des réseaux personnels et de ceux internes aux entreprises par exemple.

  On les appelle **PAN** (*Personnal Area Network*) lorsqu'ils recouvrent un très petit espace (technologie Bluetooth par exemple) ou plus généralement **LAN** (*Local Area Network*) lorsqu'ils contiennent des équipements dans un rayon de 2 à 100 mètres.

- Réseaux interconnectés :

  On les appelle **MAN** (*Metropolitan Area Network*) lorsqu'ils ont la taille d'une ville. Les **WAN** (*Wide Area Network*) permettent de relier entre eux des réseaux locaux dans des villes différentes.

- Réseaux mondiaux :

  **Internet** (*interconnection of networks*) est une interconnexion de réseaux à l'échelle mondiale. Environ 47 000 réseaux de grande taille sont interconnectés pour former internet. Les informations qui transitent sur internet passent par des câbles qui traversent les océans.

> **exercice 2 :**  
> À quel type de réseau correspond le réseau du lycée ?



## I. Transmission de données dans un réseau

> **exercice 3 :**  
> Recherchez rapidement le nombre actuel d'internautes dans le monde.

Mais comment est-il possible de faire communiquer autant d'objets connectés ? Étant donné le nombre de connexions, de types de matériels, le nombre de pays qui utilisent internet, il va falloir s’entendre afin que l’information soit reconnue quel que soit l'équipement qu’elle va croiser.

Imaginez une réunion avec des personnes parlant chacune une langue différente, on pourrait par exemple communiquer en utilisant une table de symboles commune et compréhensible par tout le monde. Se mettre d’accord et établir une règle c’est ce que l’on appelle en informatique établir un protocole.

Un **protocole** est donc un **ensemble de règles qui régissent la transmission d’informations.**

Chaque protocole a un rôle différent, et ils sont **encapsulés** les uns dans les autres. À chaque étape d'encapsulation on associe ce que l'on appelle une couche :

- les protocoles applicatifs HTTP, FTP, SMTP, DHCP, DNS... sont associés à la couche **Application**
- les protocoles TCP et UDP sont associés à la couche **Transport**
- le protocole IP est associé à la couche **Internet**
- les trames Ethernet ou Wifi sont associées à la couche **Accès réseau**

On présente souvent ces différentes couches avec un schéma, où la couche du dessous encapsule la couche du dessus : 	

![Modèle TCP/IP](img/modele_tcp_ip.png)

On nomme ce système de couches **modèle de couches TCP/IP** car il repose principalement sur les protocoles TCP et IP.

Ce modèle de couches TCP/IP est basé sur le modèle de couches OSI (*Open Systems Interconnection*) qui date de 1970. Ce modèle est principalement théorique et a permis de poser les bases des communications réseau. Il y a des correspondances entre le modèle théorique OSI et le modèle TCP/IP.

> **exercice 4 :**  
> Recherchez sur internet le nom des couches du modèle OSI pour compléter ce schéma :
>
> ![Modèle OSI](img/osi.png)



## II. Protocoles de communication

Nous allons maintenant voir le rôle de chacun des protocoles de ces différentes couches du modèle TCP/IP.



### 1. Protocoles de la couche Application

La couche application est encapsulée par la couche transport (protocole TCP principalement) qui permet de mettre en forme les données à envoyer :

![TCP - donnees](img/tcp_donnees.png)

Ces données encapsulées par TCP correspondent aux requêtes (et aux réponses à ces requêtes) réalisées par les protocoles de la couche application.

Ceux que vous utilisez sûrement le plus souvent sont :
* HTTP (*HyperText Transfert Protocol*) qui est le protocole de communication du web
* FTP (*File Transfert Protocol*) qui permet d'envoyer des fichiers sur un réseau
* SMTP (*Simple Mail Transfert Protocol*) qui permet d'envoyer des emails
* DHCP (*Dynamic Host Configuration Protocol*) qui permet d'attribuer une adresse IP à un appareil
* DNS (*Domain Name Server*) qui permet d'avoir la correspondance entre une adresse IP et une adresse symbolique
* et bien d'autres encore...

On dit que tous ces protocoles (HTTP, FTP, SMTP, DNS...) sont des **protocoles applicatifs**.

Au final, avec le protocole TCP qui encapsule les requêtes d'un protocole applicatif, les paquets de données correspondent par exemple à cela :

![HTTP](img/http.png) ![FTP](img/ftp.png)



> **exercice 5 :**  
> Regardez la vidéo suivante : [ports](videos/ports.mp4)
> 
> ([source](https://www.youtube.com/watch?v=YSl6bordSh8))
>
> Qu'est-ce qu'un port ? À quoi servent-ils ?

### 2. Un exemple de protocole : DNS

Nous allons étudier plus en détail un des protocoles applicatifs : le protocole DNS (*Domain Name Server*).

Ce protocole permet d'avoir la correspondance entre une adresse numérique (= adresse IP) et une adresse symbolique (= nom de domaine).

###### a. Adresse IP

L'adresse IP est une **adresse numérique permettant d'identifier les appareils connectés à un réseau**. Il en existe actuellement deux versions, IPv4 et IPv6.

Une adresse IPv4 est constituée de 4 nombres d'1 octet chacun, séparés par le signe point.

> **exercice 6 :**  
> Ouvrez la console windows (tapez "cmd" dans la barre de recherche), et utilisez la commande **ipconfig**.  
> Quelle est votre adresse IPv4 ?

> **exercice 7 :**  
> Parmi les adresses suivantes, lesquelles sont des adresses IPv4 valides ?
>
> * 192.168.23.242
> * 127.3.87.256
> * 1.2.3.4
> * 10.64.0.42

Une adresse IPv6 est constituée de 8 nombres de 2 octets chacun, séparés par le signe deux-points (par exemple 2A00 : 1450 : 4007 : 080A : 0000 : 0000 : 200E : 000F).

Avec la version IPv4, c'est en théorie $`2^{4 \times 8} = 2^{32} = 4 294 967 296`$ adresses disponibles (en réalité certaines sont réservées), ce qui n'est plus suffisant aujourd'hui : du coup les adresses IPv6 sont de plus en plus utilisées.

###### b. Nom de domaine

Connaissez-vous par cœur l'adresse IP des sites web que vous consultez tous les jours ? Si je vous dis "J'ai passé une commande sur 52.95.120.39." Savez-vous de quoi je parle ? Et si j'avais dit "J'ai passé une commande sur Amazon." ?

Vous avez en effet vu en SNT que ce n'est pas évident de retenir une adresse numérique. C'est pour cela que l'on connaît surtout des adresses symboliques, aussi appelées **noms de domaines**, comme par exemple *python.org* ou *google.com*.

###### c. DNS

Il existe de nombreuses manières d'utiliser le protocole DNS pour rechercher l'adresse IP correspondant à un nom de domaine et inversement, nous allons en voir 2 :

* avec Python :

  > **exercice 8 :**  
  > Tapez les commandes suivantes dans le shell :
  >
  > ```python
  > >>> import socket
  > >>> socket.gethostbyname("python.org")
  > ?
  > >>> socket.gethostbyaddr("8.8.8.8")
  > ?
  > ```
  >
  > Que trouvez-vous ?


* dans une fenêtre de commandes :

  > **exercice 9 :**  
  > Tapez les commandes suivantes :
  >
  > ```bash
  > > nslookup python.org
  > ?
  > > nslookup 8.8.8.8
  > ?
  > ```
  >
  > Que trouvez-vous ?

> **exercice 10 :**  
> Avec les 2 méthodes, cherchez l'adresse IP de *wikipedia.fr*.

> **Pour aller plus loin :**  
> Il existe aussi des sites internet de résolution de noms de domaines,
> comme par exemple [whois](https://www.whois.com/whois).


### 2. Protocoles de la couche Transport

#### a. TCP

Le principe du protocole TCP (*Transfert Control Protocol*), est similaire à l'expédition d'un colis par la poste. Admettons que vous ayez commandé un meuble en kit à l'entreprise Dupont en Suède. Dupont ne peut pas envoyer un énorme carton avec le meuble entier, alors il décide de l'envoyer pièce par pièce en empaquetant chaque pièce soigneusement.

Ensuite, Dupont amène tous les paquets à la poste de son quartier. Celle-ci va les transporter jusqu'à la poste centrale qui dirigera certains paquets vers l'aéroport pour un transport aérien, d'autres vers une gare pour un transport ferroviaire, d'autres vers un port pour un transport fluvial... On peut imaginer toutes les possibilités qui vont amener les paquets jusqu'à vous.

Lorsque vous recevez les colis, comme Dupont a joint la notice de montage à chaque paquet, vous pouvez vérifier s'il ne manque pas une pièce et éventuellement faire une réclamation.

*C'est le même principe pour le réseau informatique !*

Un utilisateur `A` veut envoyer le message "Salut, comment ça va ?" à un utilisateur `B`.

Le message est décomposé en paquets qui contiennent chacun un bloc de données (le nombre de paquets dépend de la taille du message) : c'est le rôle du protocole TCP.

![TCP - découpe en paquets](img/tcp.png)

Ensuite, chaque paquet va transiter par différents points dans le réseau jusqu'à atteindre le destinataire. Chaque paquet peut avoir une route différente.

À l'arrivée, le protocole TCP permet de reconstituer le message envoyé à partir de tous les paquets reçus, et éventuellement de renvoyer d'éventuels paquets perdus :

![TCP/IP simulation](img/tcp_ip.png)

#### b. Protocoles de récupération de paquets

Vous savez désormais que les informations transmises dans les réseaux sont découpées en paquets. Même si la fiabilité des réseaux a beaucoup progressé, il peut arriver qu'un paquet se perde ou soit endommagé pendant la transmission. Des protocoles doivent donc être mis en œuvre pour savoir si un paquet s'est perdu et le récupérer si c'est le cas.

Comme tout à l'heure, un utilisateur `A` veut envoyer un message `M` à un utilisateur `B`. Avec le protocole TCP, le message `M` est découpé en sous-messages  $`M0`$,  $`M1`$,  $`M2`$, ...

Même si `A` envoie les sous-messages dans l'ordre, on ne sait pas combien de temps vont mettre ces sous-messages pour arriver, ni même s'ils ne vont pas être détruits en route.

![BA 1](img/BA_1.png)

Le sous-message M0 est arrivé après le M1, le message M2 n'est jamais arrivé... Au final, le message reçu par `B` ne ressemble plus du tout au message envoyé par `A`. 

Pour y remédier, `B` va envoyer un accusé de réception (appelé $`ACK`$ pour *acknowledgement*) pour dire à `A` qu'il vient bien de recevoir son sous-message. Ce signal `ACK` permettra à `A` de renvoyer un message qu'il considérera comme perdu :

![BA 2](img/BA_2.png)

Mais ça ne marche pas toujours :

![BA 3](img/BA_3.png)


> **exercice 11 :**  
> Dans la situation de l'image précédente, l'accusé de réception de M1 a mis trop de temps à arriver. Du coup, `A` a cru qu'il s'était perdu et a renvoyé M1.  
> Quelles sont les conséquences sur le message reçu par `B` ?


La solution au problème de l'exercice 11 proposée au début du cours avec l'envoi du message "Salut, comment ça va ?" était de numéroter les messages pour que `B` puisse les remettre dans l'ordre à l'arrivée.  
C'est ce que réalise le protocole TCP : c'est très efficace, mais trop cher (imaginez la place que ça prend dans le réseau de numéroter le message numéro 465331562...).

Nous allons étudier une autre solution : **le protocole de bit alterné**.

Pour éviter les doublons dans le message que reçoit `B`, `A` va rajouter à chacun de ses sous-messages un bit en plus appelé `FLAG` (*drapeau*).

Au départ, ce `FLAG` vaut 0.

Quand `B` reçoit un `FLAG`, il renvoie un `ACK` égal au `FLAG` reçu.

`A` va attendre ce `ACK` contenant le même bit que son dernier `FLAG` envoyé :

- tant que `A` ne l'aura pas reçu, il continuera à envoyer le *même sous-message, avec le même `FLAG`*.
- dès qu'il l'a reçu, il peut envoyer un nouveau sous-message en *alternant le bit de son dernier `FLAG`* (d'où le nom de *protocole de bit alterné*).

`B`, de son côté, va contrôler la validité de ce qu'il reçoit : il ne gardera que *les sous-messages dont le `FLAG` est différent du précédent*. C'est cette méthode qui lui permettra d'ignorer les doublons.

> **exercice 12 :**  
> Étudiez les trois cas suivants. Ce bit alterné a-t-il permis de résoudre les problèmes de doublons vus dans l'exercice 11 ?
>
> * Cas 1 : un `FLAG` est perdu :
>
> ![BA 4](img/BA_4.png)
>
> * Cas 2 : un $`ACK`$ est perdu
>
> ![BA 5](img/BA_5.png)
>
> * Cas 3 : un `FLAG` est en retard :
>
> ![BA 6](img/BA_6.png)
>

> **exercice 13 :**  
> Que se passerait-il si dans la situation 3, `M1` était arrivé juste après `M2` ?

Dans certaines situations (comme pour l'exercice 13), le protocole de bit alterné ne permet pas d'éviter les doublons, c'est pour cela que ce protocole est aujourd'hui remplacé par des protocoles plus performants.


#### c. UDP

Le protocole UDP (*User Datagram Protocol*) ressemble beaucoup au protocole TCP. La grande différence entre UDP et TCP est que le protocole UDP ne gère pas les accusés de réception. Les échanges de données avec UDP sont donc moins fiables qu'avec TCP (un paquet "perdu" est définitivement "perdu" et ne sera pas renvoyé) mais beaucoup plus rapides (puisqu'il n'y a pas d'accusé de réception à transmettre).

UDP est donc très souvent utilisé pour les échanges de données qui doivent être rapides, mais où la perte d'un paquet de données de temps en temps n'est pas un gros problème. C'est le cas par exemple lorsque vous regardez une vidéo en streaming ou quand vous jouez à des jeux vidéos en ligne.



### 3. Protocole de la couche Internet

Le protocole de la couche Internet est le protocole IP (*Internet Protocol*). Il encapsule les données issues du protocole TCP (ou UDP) :

![encapsulation](img/tcp_ip_encapsulation.png)

Son rôle est d'ajouter l'adresse IP de l'expéditeur et celle du destinataire à chaque paquet de données créé par le protocole TCP. L'adresse IP est une adresse unique attribuée à chaque objet connecté qui, comme l'adresse postale sur une lettre, permet d'identifier de manière unique le destinataire et l'expéditeur :

![lettre](img/lettre.png)

Pour reprendre l'exemple de l'envoi du message "Salut, comment ça va ?" de A à B, le protocole TCP avait découpé et numéroté ce message en trois paquets, et le protocole IP va donc ajouter à chacun de ces paquets l'adresse IP de A et celle de B :

![IP - ajoute adresses](img/ip.png)

C'est grâce à l'adresse IP de B (destinataire) que les paquets arrivent au bon endroit et c'est grâce à l'adresse IP de A (expéditeur) que les accusés de réceptions arrivent au bon endroit.

### 4. Protocoles de la couche Accès réseau

Nous avons vu avec les protocoles TCP et IP le processus d'encapsulation des données : IP encapsule TCP (qui lui-même encapsule un protocole applicatif). Les paquets IP ne peuvent en réalité pas transiter sur un réseau tels quels, ils vont eux aussi être encapsulés avant de pouvoir voyager sur le réseau. L'encapsulation des paquets IP produit ce que l'on appelle une **trame**. Les deux types de trame les plus utilisées sont Wifi et Ethernet.

Vous savez que le paquet IP contient les adresses IP de l'émetteur et du récepteur :

![Trames - adresses IP](img/trame_1.png)

Le paquet IP étant encapsulé par la trame Ethernet/Wifi, les adresses IP ne sont plus directement disponibles (il faut désencapsuler le paquet IP pour pouvoir lire ces adresses IP), nous allons donc trouver un autre type d'adresse qui permet d'identifier l'émetteur et le récepteur : l'**adresse MAC** (*Media Access Control*).

![Trames - adresses MAC](img/trame_2.png)

Une adresse MAC est codée sur 6 octets (généralement en hexadécimal) séparés par le signe deux-points.

Chaque ordinateur relié à un réseau doit posséder une **carte réseau**, et chaque carte réseau possède sa propre adresse MAC. Il n'existe pas dans le monde 2 cartes réseau qui possèdent la même adresse MAC.

> **exercice 14 :**  
> Ouvrez la console windows, et tapez la commande **ipconfig /all**.
>
> L'adresse MAC est l'adresse figurant dans le champ "Adresse physique".
> Quelle est l'adresse MAC correspondant à l'adresse IP que vous aviez trouvée à l'exercice 6 ?

> **exercice 15 :**  
> Parmi les adresses suivantes, lesquelles sont des adresses MAC valides ?
>
> * 8D:A9:D5:67:E6:F3
> * FF:A9:00:67:E6:F3
> * 8H:A9:D5:67:E6:F3

Au moment de l'encapsulation d'un paquet IP, l'ordinateur émetteur va utiliser un protocole nommé **ARP** (*Address Resolution Protocol*) qui va permettre de déterminer l'adresse MAC de l'ordinateur destination. Pour cela il effectue une requête *broadcast* (requête destinée à tous les ordinateurs du réseau) du type : "J'aimerais connaitre l'adresse MAC de l'ordinateur ayant pour IP XXX.XXX.XXX.XXX". Une fois qu'il a obtenu une réponse à cette requête ARP, l'ordinateur émetteur encapsule le paquet IP dans une trame Ethernet et envoie cette trame sur le réseau.



## III. Tester la communication avec votre voisin et avec un serveur à l'extérieur du réseau du lycée

###### a. Avec votre voisin

Pour communiquer avec votre voisin, vous avez besoin de son adresse IP. Il suffit ensuite de faire un **ping** sur cette adresse. La commande *ping* permet d'envoyer des paquets de données d'une machine A vers une machine B pour tester la connexion.

> **exercice 16 :**  
> Demandez à votre voisin son adresse IP (voir exercice 6).  
> Dans la console, tapez ensuite la commande *ping XXX.XXX.XXX.XXX* (en remplaçant les X par l'adresse IP de votre voisin).  
> Quels renseignements obtenez-vous ?


Nous avons vu que lors de l'envoi de paquets vers votre voisin (destinataire), votre ordinateur (émetteur) utilise un protocole nommé ARP qui va permettre de déterminer l'adresse MAC de l'ordinateur destination (votre voisin donc). Cette correspondance $`adresse IP \leftrightarrow adresse MAC`$ est stockée dans une table que l'on peut consulter avec la commande **arp -a**.

> **exercice 17 :**  
> Dans la console, tapez la commande *arp -a*.  
> Quelle est l'adresse MAC de votre voisin ?



###### b. Avec un serveur à l'extérieur du réseau du lycée

Pour communiquer avec un serveur extérieur au réseau du lycée (*python.org* par exemple), vous n'avez pas besoin de connaitre son adresse IP, le nom de domaine suffit.

> **exercice 18 :**  
> Dans une fenêtre de commandes, tapez la commande *ping python.org*.  
> Quels renseignements obtenez-vous ?

Il est également possible de tracer l'itinéraire suivi par un paquet entre l'émetteur et le récepteur, un peu comme l'itinéraire de touristes qui transitent par des escales lors d'un voyage en avion. Pour cela il faut utiliser la commande **tracert** (pour *Trace RouTe*).

> **exercice 19 :**  
> Dans une fenêtre de commandes, tapez la commande *tracert python.org*.  
> Qu'obtenez-vous ?

> **Pour aller plus loin :**  
> Vous pouvez même localiser des adresses IP !  
> Allez sur le site https://trouver-ip.com/index.php et listez toutes les villes par lesquelles est passé votre paquet de l'exercice 19.


## IV. Architecture d'un réseau

Vous savez désormais comment les données sont transmises au sein d'un réseau. Il reste cependant une question : comment, physiquement, sont reliés tous les objets connectés ?

Nous allons étudier l'architecture matérielle des réseaux à l'aide du logiciel *Filius*.

Filius dispose de 3 modes :

![Filius : 3 modes](img/modes_filius.png)

### 1. Créer un réseau

Il est possible de faire communiquer deux ordinateurs en les reliant par un simple câble.

> **exercice 20 :**  
> Ouvrez Filius. Passez en mode *conception* (cliquez sur le marteau).
>
> Placez un ordinateur (sur Filius les *Ordinateurs* et les *Portables* n'ont aucune différence, vous pouvez utiliser ce que vous voulez).
>
> Nous allons configurer cet ordinateur : double-cliquez dessus (ou bien clic droit puis *Configurer*). Renommez le *Ordinateur A*, et mettez son adresse IP à *192.168.2.1*. Notez que vous pouvez voir son adresse MAC aussi, mais que vous ne pouvez pas la changer (car une adresse MAC est liée directement à la carte réseau de l'ordinateur). Nous verrons plus tard ce que signifient les autres champs de configuration.
>
> Nous allons vérifier la configuration de notre ordinateur : passez en mode *simulation* (cliquez sur le triangle vert).
>
> Cliquez sur *Ordinateur A*. Une fenêtre s'ouvre, cliquez sur *Installation des logiciels*. Installez la ligne de commande (cliquez sur *Ligne de commande*, puis la flèche verte vers la gauche et enfin *Appliquer les modifications*).
>
> Cliquez sur la ligne de commande pour l'ouvrir.
>
> Vérifiez l'adresse IP de *Ordinateur A*. Quelle commande avez-vous utiliser ?
>
> Repassez en mode *conception*.
>
> Placez un second ordinateur nommé *Ordinateur B* d'adresse IP *192.168.2.2*.
>
> Reliez maintenant les deux ordinateurs avec un câble (cliquez sur *Cable*, puis sur *Ordinateur A*, puis sur *Ordinateur B*).
>
> ![Filius : 2 ordinateurs](img/filius_1.png)
>
> Nous allons vérifier que les ordinateurs sont bien en réseau : passez à nouveau en mode *simulation*.
>
> Vérifiez que *Ordinateur A* peut bien envoyer des paquets à *Ordinateur B*.
>
> Quelle commande avez-vous utilisé ?
>
> Toujours en ligne de commande, trouvez l'adresse MAC de *Ordinateur B*. Quelle commande avez-vous utilisé ?
>
> Vous pouvez enregistrer votre réseau dans un fichier *reseau.fls*. Nous le complèterons par la suite.

Dans la plupart des cas, le câble reliant les 2 ordinateurs est un câble Ethernet. Ce type de câble possède à ses 2 extrémités des prises RJ45 :

![Câble Ethernet](img/cable_ethernet.png)

Pour relier deux ordinateurs, on branche ce câble sur la prise RJ45 femelle de l'ordinateur qui est connectée à la carte réseau Ethernet :

![carte réseau ethernet](img/carte_reseau.png)

Nous allons maintenant essayer de connecter un troisième ordinateur à notre réseau.

> **exercice 21 :**  
> Ajoutez un ordinateur nommé *Ordinateur C* d'adresse IP *192.168.2.3* à votre réseau.
>
> Essayez de relier l'*Ordinateur C* à l'*Ordinateur A* par un câble. Que se passe-t-il ?

Pour relier un plus grand nombre d'ordinateurs, il est nécessaire d'utiliser un **switch** (aussi appelé commutateur réseau). Un switch est constitué de plusieurs prises RJ45.

![Switch](img/switch.png)

Chaque ordinateur doit être relié au switch par l'intermédiaire d'un câble Ethernet.

> **exercice 22 :**  
> Supprimez le câble reliant *Ordinateur A* et *Ordinateur B* (clic droit sur un ordinateur puis *Supprimer le cable*).  
> Placez un *switch*, puis reliez les 3 ordinateurs au switch avec des câbles.
>
> ![Filius : premier switch](img/filius_2.png)
>
> En mode *simulation*, vérifiez que les trois ordinateurs sont bien en réseau. Quelle(s) commande(s) avez-vous utilisée(s) ?
>
> N'oubliez pas de sauvegarder votre fichier.



Les switchs ayant un nombre de prises RJ45 limité, il peut être nécessaire d'utiliser plusieurs switchs dans un même réseau. Par exemple si on ne dispose que de switchs ayant 4 prises RJ45 et que l'on souhaite connecter 5 ordinateurs, on va devoir connecter 3 ordinateurs à un premier switch (comme pour l'exercice 22), puis brancher un câble dans la dernière prise de ce switch pour le relier à un second switch qui lui même sera relié aux 2 autres ordinateurs.

> **exercice 23 :**  
> Créez le réseau suivant (l'adresse IP de *Ordinateur D* est *192.168.2.4* et celle de *Ordinateur E* est *192.168.2.5*) :
>
> ![Filius : 2 switchs](img/filius_3.png)
>
> En mode *simulation*, vérifiez que les 5 ordinateurs sont bien en réseau.
>
> N'oubliez pas de sauvegarder votre fichier.

> **Pour aller plus loin :**  
> En admettant que vous n'ayez plus d'autres switchs disponibles et que les deux que vous avez n'ont que 4 prises RJ45, combien d'ordinateurs pouvez-vous encore ajouter au réseau de l'exercice 23 ?


### 2. Masques de réseau

Vous avez sans doute remarqué que dans le réseau que vous avez créé dans la partie précédente, les adresses IP des 5 ordinateurs commençaient de la même manière, seul le dernier octet était différent :

| Ordinateur A | Ordinateur B | Ordinateur C | Ordinateur D | Ordinateur E |
| :----------: | :----------: | :----------: | :----------: | :----------: |
| 192.168.2.1  | 192.168.2.2  | 192.168.2.3  | 192.168.2.4  | 192.168.2.5  |

Cela est dû au fait qu'une partie de l’adresse IP permet d’identifier le réseau auquel appartient la machine et l’autre partie de l’adresse IP permet d’identifier la machine sur ce réseau.

Dans notre exemple, ce sont les 3 premiers octets qui permettent d'identifier le réseau et le dernier octet qui permet d'identifier l'ordinateur. L'adresse réseau est donc *192.168.2.0*.

Toutes les machines appartenant au même réseau doivent posséder la même adresse réseau, sinon elles ne peuvent pas communiquer ensemble, même si elles sont bien physiquement reliées.

> **exercice 24 :**  
> Changez l'adresse IP de l'*Ordinateur B* à *192.168.3.2*, puis en mode *simulation*, faites un ping sur cette adresse depuis l'*Ordinateur A*. Que se passe t-il ? Pourquoi ?
>
> Avant de passer à la suite, remettez l'adresse IP de *Ordinateur B* à *192.168.2.2*.

Ce ne sont pas forcément les trois premiers octets de l'adresse IP qui désignent l'adresse réseau, cela dépend du **masque réseau** choisi.

Une adresse IP est de la forme *a.b.c.d* :

* Si le masque réseau est 255.0.0.0, c'est le premier octet qui identifie le réseau : l'adresse réseau est a.0.0.0. On peut aussi noter le masque comme ceci : a.b.c.d/8 (ce sont les 8 premiers bits qui désignent le réseau).
* Si le masque réseau est 255.255.0.0, ce sont les deux premiers octets qui identifient le réseau : l'adresse réseau est a.b.0.0. On peut aussi noter le masque comme ceci : a.b.c.d/16 (ce sont les 16 premiers bits qui désignent le réseau).
* Si le masque réseau est 255.255.255.0, ce sont les trois premiers octets qui identifient le réseau : l'adresse réseau est a.b.c.0. On peut aussi noter le masque comme ceci : a.b.c.d/24 (ce sont les 24 premiers bits qui désignent le réseau).

> **exercice 25 :**  
> Quel était donc le masque utilisé dans notre réseau de la partie précédente ?
>
> Vous pouvez vérifier sur Filius en double-cliquant sur un des ordinateurs du réseau, il y a un champ de configuration nommé *Masque*.

> **exercice 26 :**
>
> Déterminez les adresses réseaux à partir des adresses IP suivantes :
>
> * 192.168.2.18/16
> * 44.55.66.77/24
> * 8.8.8.8/8

> **exercice 27 :**  
> 2 ordinateurs A et B sont connectés à un switch. Dans quels cas A et B peuvent-ils s'envoyer des données ?
>
> |    IP de A     |     IP de B      |
> | :------------: | :--------------: |
> | 192.168.0.1/24 | 192.168.0.200/24 |
>
> |    IP de A    |    IP de B    |
> | :-----------: | :-----------: |
> | 147.0.55.1/16 | 147.1.55.2/16 |
>
> |    IP de A     |    IP de B     |
> | :------------: | :------------: |
> | 192.168.0.1/16 | 192.168.1.2/16 |

Certaines adresses IP ne sont pas disponibles dans un réseau :

* L'adresse réseau : par exemple 192.168.2.0/24 ne peut pas être attribuée à un ordinateur.
* L'adresse dont tous les octets de la partie machine sont 255 : par exemple 192.168.2.255/24 ne peut pas être attribuée à un ordinateur (c'est l'adresse de *broadcast* qui permet d'envoyer des données vers toutes les machines du réseau).

> **Pour aller plus loin :**  
> On peut trouver d'autres masques de réseaux, comme par exemple a.b.c.d/10 ou a.b.c.d/27 : ce qu'il y a après le "/" correspond au nombre de bits qui désignent le réseau... 


### 3. Connecter deux réseaux

> **exercice 28 :**  
> Reprenez votre réseau de l'exercice 23.
>
> Renommez le switch de droite (celui connecté aux ordinateurs D et E) en *Reseau 1* (double-clic sur le switch).
>
> Dans le même fichier, créez maintenant un second réseau comme ceci :
>
> ![Filius : reseau 2](img/filius_4.png)
>
> L'adresse IP de *Ordinateur X* est *192.168.200.1* et celle de *Ordinateur Y* est *192.168.200.2*.
>
> Les ordinateurs X et Y peuvent-ils appartenir au même réseau que A, B, C, D et E ? Pourquoi ?

Nous avons donc besoin d'un nouvel équipement qui puisse interconnecter deux réseaux entre eux : c'est le **routeur**.

>**exercice 29 :**  
> Placez un routeur à 2 interfaces (le nombre d'interfaces correspond au nombre de réseaux à connecter).
>
> Reliez par un câble le routeur au switch nommé *Reseau 1*, puis par un autre câble au switch nommé *Reseau 2*.
>
> ![Filius - Routeur](img/filius_5.png)
>
> Il faut configurer le routeur (double-clic dessus). Cochez la case "Routage automatique". Changez l'adresse IP de l'onglet qui indique la connexion avec le réseau 1 à 192.168.2.254 et celle de l'onglet qui indique la connexion avec le réseau 2 à 192.168.200.254.
>
> Essayez maintenant de faire un ping depuis l'ordinateur A (192.168.2.1) vers l'ordinateur X (192.168.200.1). Que se passe-t-il ?

Le message envoyé par la commande ping de l'ordinateur A a besoin de quitter le réseau 1 pour atteindre l'ordinateur X du réseau 2. Il faut donc indiquer à l'ordinateur A qu'il va devoir passer par le routeur : on fait cela avec les **passerelles**.

> **exercice 30 :**  
> Double-cliquez sur *Ordinateur A*. L'ordinateur A a accès au routeur par l'adresse du réseau 1 qu'on a configurée pour le routeur, c'est-à-dire 192.168.2.254. C'est cette adresse qu'il faut mettre comme passerelle.
>
> Double-cliquez sur *Ordinateur X*. L'ordinateur X a quant à lui accès au routeur par l'adresse du réseau 2 qu'on a configurée pour le routeur, c'est-à-dire 192.168.200.254. C'est cette adresse qu'il faut mettre comme passerelle.
>
> Testez à nouveau de faire un ping depuis l'ordinateur A (192.168.2.1) vers l'ordinateur X (192.168.200.1). Que se passe-t-il cette fois ?
>
> Vérifiez par où passent les paquets que vous envoyez de *Ordinateur A* à *Ordinateur X*. Quelle commande utilisez-vous ? Qu'obtenez-vous ?
>
> Configurez de la même manière les passerelles des ordinateurs B, C, D, E et Y puis vérifiez que tous sont bien en réseau.

> **Pour aller plus loin :**  
> Si vous avez terminé, vous pouvez par exemple créer et configurer le réseau suivant :
>
> ![Filius - Autre réseau](img/filius_6.png)





---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, [*pixees*](https://pixees.fr/informatiquelycee/), [*diderot*](http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/co/_Module_NSI.html)
