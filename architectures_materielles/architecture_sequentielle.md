# Modèle d'architecture séquentielle



## I. Composants d'un ordinateur

### 1. Entraînement à l'oral

**Consigne :**

Vous allez travailler en binôme sur l'un des composants d'un ordinateur. L'objectif est de réaliser un exposé de *5 minutes* (temps de parole également réparti). Vous n'avez pas le droit à un support de présentation (pas de diaporama, pas d'affiche...). Vous pouvez éventuellement avoir une feuille avec des notes, mais ces notes doivent être composées de *mots-clefs uniquement* (pas de phrases ni de texte).

**Ce que doit contenir l'exposé :**

- histoire du composant (date de création + éventuelles évolutions depuis)
- description physique (aspect, taille, poids, matériaux...)
- définition / principe / composition (qu'est-ce que ce composant ? comment fonctionne t-il ? + éventuellement ce dont il est composé)
- fonctions (à quoi le composant sert-il ?)
- lien avec les autres composants (à quoi est-il relié ? comment y est-il relié ? pourquoi ?)
- exemples de ce composant (modèles + concepteurs)
- toute autre information que vous jugez utile

**Sujets :**

- la carte mère *Félix - Nouredine - Paul Li.*
- le processeur (CPU) *Eliot - Thomas - Paul Le.*
- la mémoire vive (RAM) *Thibaud - Flavio*
- la mémoire de masse (disques durs...) *Esteban - Benoît - Matteo*
- la carte graphique (GPU) *Amine - Ayoub*
- la carte son *Lilou - Ines*
- la carte réseau *Léo - Remi*
- les entrées/sorties (I/O + périphériques externes) *Zachary - Nathan*
- les bus *Solenn - Clemence*

### 2. Bilan

>  **exercice 1 :**
>
> Replacez les différents composants présentés lors des exposés sur l'image suivante :
>
> [![composants ordinateur](img/ordinateur_composants.jpg)]()
>
> |    Composant    | Numéro |
> | :-------------: | :----: |
> |  Alimentation   |        |
> |   Processeur    |        |
> |   Ventilation   |        |
> |   Carte mère    |        |
> | Carte graphique |        |
> |    Carte son    |        |
> |  Carte réseau   |        |
> | Lecteur CD/DVD  |        |
> |   Disque dur    |        |
> |       RAM       |        |
> |       Bus       |        |



C'est la carte mère qui supporte tous ces composants :

![composants ordinateur](img/carte_mere_composants.jpg)



Voilà donc ce qu’il se passe dans votre ordinateur lorsque vous demandez à un logiciel de s’ouvrir :

![ouverture application matériel](img/ouverture_app_materiel.jpg)

> **exercice 2 :**
>
> Complétez :
>
> 1. Vous ouvrez un logiciel à l’aide de la .................................
> 2. Le ................................. demande au disque dur de lire les données du logiciel
> 3. Le ................................. renvoie les données au processeur
> 4. Le processeur transmet ces données dans la .................................
> 5. Le processeur envoie les données produites à la .................................
> 6. La carte graphique va convertir les données en une image transmise à votre .................................

> **Pour aller plus loin :**
>
> Si vous souhaitez en découvrir plus sur les composants d'un ordinateur, et notamment quel composant choisir pour votre ordinateur, vous pouvez consulter [ce site](https://www.inmac-wstore.com/guides-achat-composants/cp37557.htm).



## II. Architecture de Von Neumann

À l'étape 5 de l'exercice 2 nous avons déterminé que le processeur envoyait les *données produites* à la carte graphique. mais quelles sont ces "données produites" ? Pour le comprendre, il faut rentrer un peu plus dans les détails de la composition du processeur.

### 1. Principe

Comme vous avez pu le voir, les programmes (données des logiciels) et les données classiques sont tous deux stockés dans la même mémoire vive : il n'y a pas une mémoire pour les instructions et une mémoire différente pour les données. Cette idée de **considérer les programmes comme des données** vient de John Von Neumann en 1945, et elle est encore utilisée aujourd'hui !

> **exercice 3 :**
>
> John Von Neumann (1903 - 1957) est un mathématicien, informaticien, chimiste et physicien américano-hongrois.
>
> En vous aidant de [ce site](https://www.futura-sciences.com/sciences/personnalites/matiere-john-von-neumann-256/), listez ses principaux domaines de recherche :
>
> ```
> 
> 
> 
> 
> 
> 
> ```

À partir de cette idée, John Von Neumann construit un modèle d'architecture qui servira pour la conception du premier ordinateur entièrement électronique (EDVAC - 1949). Ce modèle, baptisé **modèle d'architecture de Von Neumann** en son honneur, peut être représenté par le schéma ci-dessous :

![archi von neumann](img/CPU.png)

L'architecture de Von Neumann se compose donc :

* de la mémoire (la RAM) qui contient **données et programmes**
* des entrées/sorties qui permettent de communiquer avec "le monde extérieur" (clavier, souris, écran, carte graphique...)
* des bus (les flèches du schéma)
* d'un processeur, lui-même constitué :
  * d'une **unité de commande** (ou unité de contrôle) qui gère l'exécution des *instructions machines*
  * d'une **unité arithmétique et logique** qui effectue les *opérations* de base
  * de **registres** (dont l'accumulateur) qui permettent de stocker les résultats intermédiaires lors des calculs

Nous allons voir en détail le fonctionnement de l'*UAL* et de l'*UC*.

> **Pour aller plus loin :**
>
> Pour plus d'informations sur la naissance du modèle d'architecture de Von Neumann, vous pouvez lire [cet article](https://interstices.info/le-modele-darchitecture-de-von-neumann/).

### 2. UAL

L'Unité Arithmétique et Logique (UAL) est la partie du CPU chargée d'**effectuer les opérations de base**.

Les opérations de base s'effectuent en binaire. Pour rappel, on travaille avec le binaire car les ordinateurs sont composés de **transistors** (inventés fin 1947 par John Bardeen, William Shockley et Walter Brattain). On obtient un 1 si le transistor laisse passer le courant, un 0 sinon.

![transistor](img/transistor.jpg)

Les transistors sont aujourd'hui regroupés dans des **circuits intégrés** (inventés en 1958 par Jack Kilby).

![circuit intégré](img/circuit_integre.jpg)

Les transistors sont à la base des **circuits logiques**. Il existe 2 types de circuits : les circuits combinatoires et les circuits séquentiels. Ceux au programme de NSI sont les **circuits combinatoires**.

Ces circuits réalisent des **expressions booléennes**. Nous avons déjà vu les expressions booléennes : on utilisait les booléens de Python *True* et *False* et les opérateurs *not*, *and* et *or*.

Dans l'ordinateur, on remplace :

* *True* par **1**
* *False* par **0**
* *not* par le circuit suivant : ![not](img/not.png)
* *and* par le circuit suivant : ![and](img/and.png)
* *or* par le circuit suivant : ![or](img/or.png)

Pour le *not*, on avait la table de vérité suivante :

|   x   | not x |
| :---: | :---: |
| False | True  |
| True  | False |

Pour le circuit du *not*, on a donc désormais la table suivante :

| E (entrée) | S (sortie) |
| :--------: | :--------: |
|     0      |     1      |
|     1      |     0      |

> **exercice 4 :**
>
> Complétez la table de vérité du circuit combinatoire du *and* :
>
> |  E1  |  E2  |  S   |
> | :--: | :--: | :--: |
> |  0   |  0   |      |
> |  0   |  1   |      |
> |  1   |  0   |      |
> |  1   |  1   |      |

> **exercice 5 :**
>
> Complétez la table de vérité du circuit combinatoire du *or* :
>
> |  E1  |  E2  |  S   |
> | :--: | :--: | :--: |
> |  0   |  0   |      |
> |  0   |  1   |      |
> |  1   |  0   |      |
> |  1   |  1   |      |

Il est possible de combiner ces circuits pour représenter de plus grandes expressions booléennes : ![ex circuit](img/circuit.png)

> **exercice 6 :**
>
> Pour trouver l'expression booléenne de S, il suffit de combiner tous les circuits logiques dans l'ordre.
>
> Par exemple en bas du circuit précédent nous avons l'entrée C suivie d'un *not* donc *not C* et l'entrée D suivie d'un *not* donc *not D*. Puis les deux se rejoignent sur un *and* donc nous obtenons *(not C) and (not D)*.
>
> Donnez l'expression booléenne de S du schéma ci-dessus.
>
> ```
> 
> 
> 
> ```

> **exercice 7 :**
>
> Nous allons dessiner le circuit combinatoire permettant de réaliser l'une des opérations proposées par l'UAL : un **additionneur**. Pour simplifier le problème, nous allons nous limiter à l'addition de deux nombres binaires de 2 bits chacun.
>
> *Question 1*
>
> Nous allons d'abord réaliser le circuit de l'addition de deux nombres sur 1 bit.
>
> Combien de bits faut-il pour écrire le résultat d'une telle addition ?
>
> ```
> 
> 
> ```
>
> *Question 2*
>
> Nous additionnons `A` (1 bit) et `B` (1 bit). Le bit de gauche du résultat (la retenue) sera `R` et le bit de droite (la somme) sera `S`. Complétez la table de vérité suivante :
>
> |  A   |  B   |  R   |  S   |
> | :--: | :--: | :--: | :--: |
> |  0   |  0   |      |      |
> |  0   |  1   |      |      |
> |  1   |  0   |      |      |
> |  1   |  1   |      |      |
>
> *Question 3*
>
> À quelle opérateur correspond la table de vérité du `R` ?
>
> ```
> 
> 
> ```
>
> Dessinez le circuit correspondant (`A` et `B` en entrées et `R` en sortie) :
>
> ```
> 
> 
> 
> ```
>
> *Question 4*
>
> À quelle expression peut correspondre la table de vérité du `S` ?
>
> ```
> 
> 
> ```
>
> On peut simplifier cette expression en utilisant un nouvel opérateur : le *ou exclusif*. Voici son circuit : ![xor](img/xor.png)
>
> *Question 5*
>
> Complétez enfin le circuit suivant, qui prend `A` et `B` en entrées et donne `R` et `S` en sorties :
>
> ![demi additionneur](img/demi_additionneur.png)
>
> *Question 6*
>
> On peut désormais passer à l'addition de deux nombres sur 2 bits.
>
> Réalisez les trois additions suivantes :
>
> `A = 01` et `B = 10`
>
> `A = 01` et `B = 01`
>
> `A = 11` et `B = 11`
>
> Combien de bits faut-il prévoir pour le résultat ?
>
> ```
> 
> 
> 
> 
> 
> ```
>
> *Question 7*
>
> Pour additionner les bits de droite de `A` et `B` : c'est simple, on utilise le circuit de la question 5.
>
> Pour additionner les bits de gauche, combien notre circuit va-t-il avoir d'entrées ?
>
> ```
> 
> 
> 
> 
> ```
>
> *Question 8*
>
> On appelle $`A_1`$ le bit de gauche de `A` et $`A_2`$ le bit de droite. On a donc $`A = A_1A_2`$. Idem pour `B` : $`B = B_1B_2`$.
>
> Nous additionnons $`A_1`$ + $`B_1`$ + `R` (bits de gauche plus la retenue). Le résultat s'écrit sur 2 bits : le bit de gauche du résultat sera $`S_1`$ et le bit de droite sera $`S_2`$. Complétez la table de vérité suivante :
>
> | $`A_1`$ | $`B_1`$ |  R   | $`S_1`$ | $`S_2`$ |
> | :-----: | :-----: | :--: | :-----: | :-----: |
> |    0    |    0    |  0   |         |         |
> |    0    |    0    |  1   |         |         |
> |    0    |    1    |  0   |         |         |
> |    0    |    1    |  1   |         |         |
> |    1    |    0    |  0   |         |         |
> |    1    |    0    |  1   |         |         |
> |    1    |    1    |  0   |         |         |
> |    1    |    1    |  1   |         |         |
>
> 
>
> Voici le circuit qui correspond à la table de vérité que vous venez de compléter :
>
> ![additionneur](img/additionneur.png)
>
> Pour simplifier vous pouvez le dessiner comme ceci sur votre cahier : ![additionneur simplifié](img/additionneur2.png)
>
> *Question 9*
>
> Dessinez maintenant le circuit entier, qui prend en entrées $`A_1, A_2, B_1, B_2`$ et renvoie en sorties $`S, S_1, S_2`$ les trois bits du résultat de l'addition. Vous devez utiliser les circuits des questions 5 et 8.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Dessinez le circuit de l'addition de 2 nombres binaires de 3 bits.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> `A` et `B` sont les entrées, `C` et `D` sont les sorties. Dessinez le circuit de la table de vérité suivante :
>
> |  A   |  B   |  C   |  D   |
> | :--: | :--: | :--: | :--: |
> |  0   |  0   |  1   |  1   |
> |  0   |  1   |  1   |  0   |
> |  1   |  0   |  1   |  0   |
> |  1   |  1   |  0   |  1   |
>
> ```
> 
> 
> 
> 
> 
> 
> ```

### 3. UC

L'Unité de Commande (UC) est la partie du CPU qui gère l'exécution des **instructions machines**. Mais qu'est-ce qu'une instruction machine ?

Comme vous le savez déjà, un ordinateur exécute des programmes qui sont des suites d'instructions. Le CPU est incapable d'exécuter directement des programmes écrits, par exemple, en Python. En effet, comme tous les autres constituants d'un ordinateur, le CPU gère uniquement le binaire, les instructions exécutées au niveau du CPU sont donc codées en binaire.  L'ensemble des instructions exécutables directement par le microprocesseur constitue ce que l'on appelle le **langage machine**.

Une instruction machine est une chaîne binaire composée principalement de 2 parties :

- le champ **code opération** qui indique au processeur le type de traitement à réaliser. Par exemple le code "00100110" donne l'ordre au CPU d'effectuer une multiplication.
- le(s) champ(s) **opérandes** indique la nature des données sur lesquelles l'opération désignée par le "code opération" doit être effectuée. Par exemple #11 #36 pour multiplier les valeurs 11 et 36. 

Il existe 3 types d'opérandes :

* une valeur immédiate : l'opération est effectuée directement sur la valeur donnée dans l'opérande (ex : #11, #36)

* un registre du CPU : l'opération est effectuée sur la valeur située dans un des registres (ex : R0, R1, R2)

* une donnée située en mémoire vive : l'opération est effectuée sur la valeur située en mémoire vive à l'adresse donnée (ex : @79, @26)

> **exercice 8 :**
>
> Pour chacune des valeurs suivantes, dites s'il s'agit d'une valeur immédiate, d'un registre ou d'une adresse en RAM :
>
> * R1
> * #1
> * @1

Un programme en langage machine est donc une suite très très longue de 1 et de 0, ce qui vous en conviendrez est quelque peu rébarbatif à programmer. Pour pallier cette difficulté, les informaticiens ont remplacé les codes binaires par des symboles mnémoniques  (plus facile à retenir qu'une suite de "1" et de "0"). Programmer avec ces instructions s'appelle programmer **en assembleur**. Voici les principales opérations :

| Opération |    Opérandes     |                        Signification                         |
| :-------: | :--------------: | :----------------------------------------------------------: |
|    LDR    |   R*x* @*add*    | Place la valeur stockée à l'adresse mémoire @*add* dans le registre R*x*. |
|    STR    |   R*x* @*add*    | Place la valeur stockée dans le registre R*x* en mémoire vive à l'adresse @*add*. |
|    ADD    | R*x* *op1* *op2* | Additionne *op1* et *op2*, place le résultat dans le registre R*x*. *op1* et *op2* peuvent être des valeurs immédiates # ou des registres R. |
|    SUB    | R*x* *op1* *op2* | Soustrait *op2* de *op1*, place le résultat dans le registre R*x*. *op1* et *op2* peuvent être des valeurs immédiates # ou des registres R. |
|    MOV    |    R*x* *op*     | Place *op* dans le registre R*x*. *op* peut être une valeur immédiate # ou un registre R. |
|     B     |      @*add*      | Rupture de séquence : la prochaine instruction à exécuter se trouve à l'adresse mémoire @*add*. |
|    CMP    |   *op1* *op2*    | Compare la valeur *op1* à la valeur *op2*. *op1* et *op2* peuvent être des valeurs immédiates # ou des registres R. Cette instruction doit être suivie d'un BEQ, BNE, BGT ou BLT. |
|    BEQ    |      @*add*      | Suit un CMP. La prochaine instruction à exécuter se situe à l'adresse mémoire @*add* si *op1* est égale à *op2*. |
|    BNE    |      @*add*      | Suit un CMP. La prochaine instruction à exécuter se situe à l'adresse mémoire @*add* si *op1* n'est pas égale à *op2*. |
|    BGT    |      @*add*      | Suit un CMP. La prochaine instruction à exécuter se situe à l'adresse mémoire @*add* si *op1* est supérieure à *op2*. |
|    BLT    |      @*add*      | Suit un CMP. La prochaine instruction à exécuter se situe à l'adresse mémoire @*add* si *op1* est inférieure à *op2*. |
|   HALT    |                  |               Arrête l'exécution du programme                |

> **exercice 9 :**
>
> Expliquez ce que font chacune des instructions suivantes :
>
> * LDR R1, @78
>
> * ADD R0, R1, #42
>
> * CMP R4, #18
>
>   BGT @22
>
> * STR R0, @15
>
> * B @102
>
> ```
> 
> 
> 
> 
> 
> ```

Voici un exemple de programme en assembleur :

```assembly
MOV R1, #2
MOV R2, #3
ADD R0, R1, R2
STR R0, @150
HALT
```

> **exercice 10 :**
>
> Que fait le programme précédent ?
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 11 :**
>
> Modifiez le programme précédent pour soustraire 11 de 36 et placer le résultat à l'adresse @230 en mémoire vive.
>
> ```
> 
> 
> 
> 
> 
> ```

Pour rappel, l'architecture de Von Neumann range les programmes dans la même mémoire que les données. On va supposer que les instructions de nos programmes sont rangées à partir de l'adresse @1.

Par exemple pour le programme ci-dessus :

```assembly
MOV R1, #2			est rangée à l'adresse @1
MOV R2, #3			est rangée à l'adresse @2
ADD R0, R1, R2		est rangée à l'adresse @3
STR R0, @150		est rangée à l'adresse @4
HALT				est rangée à l'adresse @5
```

Ceci nous permet d'effectuer les ruptures de séquence (instructions B, BEQ, BNE, BGT, BLT). Par exemple `B @3` signifie que la prochaine exécution à exécuter est `ADD R0, R1, R2`.

> **exercice 12 :**
>
> Que fait le programme suivant ?
>
> ```assembly
> MOV R1, #8
> MOV R2, #5
> CMP R1, R2
> BGT @6
> B @8
> STR R1, @150
> HALT
> STR R2, @150
> HALT
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```
>
> Que fait ce programme ci (on a inversé 8 et 5) ?
>
> ```assembly
> MOV R1, #5
> MOV R2, #8
> CMP R1, R2
> BGT @6
> B @8
> STR R1, @150
> HALT
> STR R2, @150
> HALT
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 13 :**
>
> Notre programme précédent permettait donc de ranger à l'adresse @150 la plus grande des deux valeurs.
>
> Modifiez ce programme pour ranger à l'adresse @151 la plus petite des deux valeurs :
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Combinez vos programmes assembleur des exercices 12 et 13 pour stocker à la fois la plus petite valeur à l'adresse @151 et la plus grande valeur à l'adresse @150.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Que fait le programme suivant ?
>
> ```assembly
> MOV R0, #6
> STR R0, @100
> MOV R0, #8
> STR R0, @200
> LDR R0, @100
> CMP R0, #10
> BNE @9
> B @12
> ADD R0, R0, #1
> STR R0, @100
> B @5
> LDR R1, @100
> LDD R2, @200
> SUB R0, R1, R2
> STR R0, @200
> HALT
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Sauriez-vous transformer les programmes en assembleur précédents en code Python ?
>
> ```
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Sauriez-vous transformer le programme Python suivant en assembleur ?
>
> ```python
> x = 0
> while x < 3:
>     x = x + 1
> ```
>
> ```assembly
> 
> 
> 
> 
> 
> 
> ```

### 4. Évolution des performances

Pendant des années, pour augmenter les performances des ordinateurs, les constructeurs utilisaient principalement deux stratégie :

- La réduction de la taille des transistors, grâce aux progrès technologiques, a permis d'augmenter le nombre de ces composants sur le microprocesseur.

  - En 1975, Gordon Moore énonce une prévision basée sur l'évolution du nombre de transistors sur une puce : ce nombre est sensé doubler chaque année. Le constructeurs font en sorte de s'aligner sur ce qu'on appelle la **[loi de Moore](https://fr.wikipedia.org/wiki/Loi_de_Moore)** pendant très longtemps.
  - Aujourd'hui on atteint une limite physique : la taille des transistors est de l'ordre de quelques atomes, réduire davantage n'est plus vraiment possible car la miniaturisation est telle que les fabricants commencent à buter sur des effets quantiques. Par conséquent, augmenter leur nombre devient très difficile.

  ![loi de Moore](img/courbe_transistors.png)

- L'augmentation de la fréquence d'horloge des microprocesseurs est également un facteur de performance : elle est liée à sa capacité d'exécuter un nombre plus ou moins important d'instructions machines par seconde. Plus la fréquence d'horloge du CPU est élevée, plus ce CPU est capable d'exécuter un grand nombre d'instructions machines par seconde. Cependant :

  - L'augmentation des fréquences entraine une augmentation de la consommation électrique : en doublant la fréquence, on multiplie sa consommation par 8. Cela est un important problème à une époque où on cherche à augmenter l'autonomie de matériel embarquant des processeurs (téléphones, objets connectés...)
  - L'augmentation des fréquences entraine également une forte augmentation de la température et donc implique de recourir à un refroidissement bruyant et consommant lui aussi de l'énergie !
  
  ![Fréquence d'horloge](img/courbe_frequence.png)

Pour poursuivre la course à la performance il faut donc trouvez d'autres solutions : les microprocesseurs actuels embarquent plusieurs **cœurs**, c'est à dire plusieurs processeurs dans le processeur.

- Aujourd'hui (en 2020) on trouve sur le marché des CPU possédant jusqu'à 18 cœurs ! Même les smartphones possèdent des microprocesseurs multicœurs : le Snapdragon 845 possède 8 cœurs.
- Le gain semble évident : en doublant le nombre de cœurs, on calcule deux fois plus vite. En réalité, le gain n'est obtenu que pour les applications spécifiquement programmées pour utiliser cette architecture et travailler **en parallèle** , c'est à dire en effectuant plusieurs tâches simultanément en exploitant simultanément plusieurs cœurs. *C'est donc la qualité des algorithmes qui, en faisant travailler les cœurs en équipe, feront la différence !*

L'architecture de Von Neumann ressemble donc aujourd'hui plutôt à ça :

![von neumann multi-coeur](img/CPU_ajd.png)

> **exercice 14 :**
>
> Cherchez combien de cœurs possède votre ordinateur.
>
> *Indice : Appuyez sur CTRL + MAJ + Échap, onglet Performance.*
>
> ```
> 
> 
> ```

> **Pour aller plus loin :**
>
> Pour découvrir le processus de fabrication d'un circuit intégré, vous pouvez regarder [cette vidéo](https://www.youtube.com/watch?v=NFr-WyytNfo).

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, [Belwatech](https://sites.google.com/site/belwatech2016/), [Interstices](https://interstices.info/), [Pixees](https://pixees.fr/informatiquelycee/)
