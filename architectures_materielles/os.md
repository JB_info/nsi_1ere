# Systèmes d'exploitation

## I. Présentation

Nous allons voir ce qu'est un système d'exploitation (aussi appelé **OS** pour Operating System).

### 1. Qu'est-ce qu'un OS ?

Regardez la vidéo suivante puis répondez aux questions :

[Qu'est-ce qu'un os ?](videos/os.mp4)

(Source : [Introduction aux systèmes d'exploitation | Rémi Sharrock](https://www.youtube.com/watch?v=Y-KyODibJcw))

> **exercice 1 :**  
> 1. Quel est le rôle principal d’un système d’exploitation ?
> 2. Donnez les 5 systèmes d’exploitation les plus importants.
> 3. Quelles sont les six grandes fonctions d’un système d’exploitation ?
> 4. Complétez le schéma ci-dessous qui situe le système d’exploitation dans un ordinateur : ![os](img/os_simplifie.png)


### 2. Logiciel libre

Regardez la vidéo suivante puis répondez aux questions :

[Qu'est-ce qu'un logiciel libre ?](videos/libre.mp4)

(Source : [Le Logiciel Libre – Man #2](https://www.youtube.com/watch?v=CteBUrSW0l4))

> **exercice 2 :**  
> 1. Qui est le créateur du mouvement du logiciel libre ?
> 2. Quelles sont les quatre libertés d’un logiciel libre ?
> 3. À quoi faut-il avoir accès pour pouvoir exercer ces libertés ?
> 4. Quel est le contraire d’un logiciel libre ? 

### 3. Système d'exploitation libre

Il existe de nombreux systèmes d’exploitation libres mais le plus répandu est **Linux**.

*Remarque : Par abus de langage, on appelle Linux tout système d’exploitation basé sur le noyaux Linux. En réalité, il faut dire GNU/Linux : Linux pour le noyau et GNU pour la base de logiciels.*

Regardez la vidéo suivante puis répondez aux questions :

[Qu'est-ce qu'une distribution Linux ?](videos/distribution.mp4)

(Source : [3 minutes pour comprendre: Les distributions Linux](https://www.youtube.com/watch?v=jxStDDGDstQ))

> **exercice 3 :**  
> 1. Où peut-on trouver le code source du noyau Linux ?
> 2. Qui est le créateur (et développeur principal) de Linux ?
> 3. Qu'est-ce qu'une distribution ?
> 4. Donnez les trois distributions Linux les plus utilisées.

## II. Console Linux

Voici donc un schéma récapitulatif de fonctionnement d'un système d'exploitation :

![os](img/os.jpg)

Vous utilisez toujours les applications via une interface graphique.

Il est temps maintenant d'apprendre à se servir de la **console** (ou **shell** ou encore **terminal**).

La console est un écran qui fournit une **ligne de commande**.

Pour découvrir les commandes qui existent, vous allez jouer à TERMINUS :

[DM TERMINUS](dm_terminus.pdf)

## III. Système de fichiers

### 1. Stockage

Nous avons vu que l'architecture de Von Neumann permet à la mémoire de contenir à la fois les données et les programmes. La quantité de données à stocker devient vite importante, trop pour être conservées dans les registres des microprocesseurs ou la mémoire vive (RAM). La nécessité de conserver ces données dans le temps impose la recherche d'un système de stockage permanent et fiable : cartes perforées, disquettes, disques durs, CD et DVD, clefs USB, cloud ...

Voici l'évolution des **supports de stockage de masse** :

![cartes perforée - wikipédia - domaine puplic](https://upload.wikimedia.org/wikipedia/commons/8/87/IBM_card_storage.NARA.jpg)

_Archivage de cartons de cartes perforées archivés au service du [NARA](https://fr.wikipedia.org/wiki/National_Archives_and_Records_Administration) en 1959. Chaque carton peut contenir 2 000 cartes (d'une ligne de 80 colonnes chacune)._ 


![disquettes- wikipédia - CCSA](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Diskettes.jpg/439px-Diskettes.jpg)



_Les disquettes ont régné sur le stockage transportable de 1967 (80 ko par disquette...) au début des années 2000, date d'entrée sur scène des premières clefs USB._


![cles USB](https://www.darty.com/darty-et-vous/sites/default/files/thumbnails/image/cle-usb-standard.jpg)


_Les premières clés USB datent de 1999, mais commercialisées seulement au début des années 2000.


![ancien dd  -wikipdia GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/IBM_old_hdd_mod.jpg/714px-IBM_old_hdd_mod.jpg)



_Le premier disque dur a fait son apparition en 1956 (commercialisation autour de 1960 au prix de 10000$ le mégaoctet...) et n'a jamais cessé d'évoluer même si, aujourd'hui, les disques SSD occupent une place grandissante sur le marché et tendent à le détrôner._


![CD wikipédia GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/CD-R_Back.jpg/621px-CD-R_Back.jpg)



_Inventé par Philipps en 1987, le CD-ROM offrait la capacité de 500 disquettes, fonctionnait avec un système optique et ne donnait pas d'accès en écriture, du moins dans un premier temps. Ses successeurs DVD puis Blu-Ray font encore, un peu, de résistance face aux autres systèmes de stockages._



![le cloud wikipedia CC SA](https://upload.wikimedia.org/wikipedia/commons/9/93/Nuage33.png)



_Stocker sur le cloud (le nuage) consiste à utiliser des serveurs informatiques distants par l'intermédiaire d'un réseau, généralement Internet, pour stocker des données ou les exploiter. La fiabilité est très supérieure aux stockages locaux, permet des accès de différents endroits par différentes personnes. La capacité de stockage est énorme, tout autant que la consommation d'énergie_.

> **exercice 4 :**  
> Quel support de stockage utilisez-vous encore aujourd'hui ?

Une unité de stockage utilise un moyen **physique** pour coder des suites de 0 et de 1. Ce peut être fait de façon magnétique ([disque dur](https://fr.wikipedia.org/wiki/Disque_dur)), par un système de piégeage d'électrons ([mémoire flash, clé USB](https://fr.wikipedia.org/wiki/M%C3%A9moire_flash)), en traitant une surface réfléchissante ou non  ([CD, DVD...](https://fr.wikipedia.org/wiki/DVD)), ... 

Il faut toutefois organiser cette série de 0 et 1 afin de savoir à quoi elle correspond. S'agit-il :

* de texte ?
* d'une image ?
* d'un son ?
* du code d'un logiciel ?

Pour cela, le système d'exploitation utilise un **système de fichiers**.

### 2. Fichiers & répertoires

Le système de fichiers (en abrégé `FS` pour `File System`) utilisé par un OS lui permet de savoir exactement où est stocké un ensemble de données destiné à coder un objet précis. Cet ensemble de données est ce qu'on appelle un _fichier_. Évidemment, le `FS` gère tous les fichiers présent sur l'espace de stockage.

Il existe plusieurs types de systèmes de fichiers : [FAT](https://fr.wikipedia.org/wiki/File_Allocation_Table#Structure), [NTFS](https://fr.wikipedia.org/wiki/NTFS_(Microsoft)), [ext](https://fr.wikipedia.org/wiki/Extended_file_system), etc.

> **exercice 5 :**  
> Il est possible de trouver le type de système de fichiers d'un support de stockage. Ouvrez le gestionnaire de fichier, cliquez droit sur le support souhaité puis cliquez sur "Propriétés".

Le fichier possède un nom qui peut aujourd'hui utiliser quasiment l'ensemble des caractères du répertoire Unicode, mais certains caractères spécifiques ayant un sens pour le système d'exploitation peuvent être interdits ou déconseillés. Cela peut être le cas des accents, des espaces ou de certains caractères comme `\` ou `/`.

Dans les environnements graphique, le type du fichier apparaît sous la forme d'un suffixe comme `.txt` pour un fichier texte ou `.jpg` pour une image au format `jpeg`. Il détermine la façon dont le système d'exploitation va interpréter les données du fichier. Par exemple, l'octet _01000001_ peut être interprété comme un 'A' s'il s'agit d'un fichier texte, ou comme la première ligne d'une image, etc.

Chaque fichier possède un unique **i-node**. Il s'agit d'une structure de donnée qui contient les **métadonnées** du fichier telles que :

* son type (texte, image, son...)
* son emplacement (la position de ses données sur le disque)
* sa taille (en ko)
* son propriétaire
* ses droits d'accès
* ...

> **exercice 6 :**  
> Cliquez droit sur un des fichiers présents sur votre ordinateur, puis cliquez sur "Propriétés". Pouvez-vous repérer toutes ces métadonnées ?

L'entassement de tous les fichiers sans organisation entraînerait une grande difficulté à les gérer pour l'utilisateur, un peu comme si les pages de nombreux cahiers étaient toutes rangées sans reliures dans un même tiroir. Il faut donc un système de rangement.

![arborescence - wikipédia- domaine public](https://upload.wikimedia.org/wikipedia/commons/d/db/AHEADHierarchy.JPG)

**Pour l'utilisateur**, un système de fichiers est vu comme une arborescence : les fichiers sont regroupés dans des **répertoires** (ou "dossiers").

Ces répertoires contiennent soit des fichiers, soit d'autres répertoires.

Il y a un unique répertoire **racine** (noté `/` sous Linux), le seul qui n'a pas de parent mais seulement d’éventuels fils, ses sous-répertoires.  
Tous les autres répertoires ont un unique parent et, éventuellement plusieurs répertoires fils.

On accède au fichier par _un chemin d'accès_ :

* Il peut être **absolu** s'il démarre de la racine. Il n'y a qu'un seul chemin absolu puisqu'il n'y a qu'une seule racine.

* Il peut être **relatif** s'il démarre d'un autre répertoire. Il peut y avoir autant de chemins relatifs que de répertoires.

Par exemple le fichier _X.java_ aurait pour chemin d'accès absolu : `/A/Code/X.java`. Il commence par `/` car on part de la racine et les répertoires sont séparés par des `/` également.


> **exercice 7 :**  
> Donnez les chemins absolus de R.drc et Z.htm.


Si on se place dans le répertoire `A`, le chemin relatif vers _X.java_ serait : `Code/X.java`.

> **exercice 8 :**  
> Si on se place dans A, quel est le chemin relatif vers R.drc ? et vers Z.htm ?


Le père d'un répertoire est atteignable par par le chemin `..`.

Par exemple depuis le répertoire Code, le chemin relatif vers R.drc est `../R.drc`.


> **exercice 9 :**  
> Quel est le chemin relatif vers X.java depuis le répertoire Htm ?


**Du point de vue du système de fichiers**, un répertoire est en réalité traité comme un fichier dont le contenu est la liste des fichiers à l'intérieur. Un répertoire a donc lui aussi un i-node et les mêmes métadonnées qu'un fichier comme le nom, la `taille`, la `date`, les `droits d'accès`, et les divers autres attributs. 

Lorsqu'on _ouvre_ un répertoire, on lui demande d'afficher les `i-nodes` des fichiers qu'il référence.


### 3. Droits et permissions

Par défaut, lorsqu'un utilisateur crée un fichier ou un dossier, il en  devient propriétaire. Le propriétaire est celui qui possède les droits sur le fichier. Il peut également allouer ou retirer ces droits aux autres utilisateurs.

Il y a 3 catégories d'utilisateurs d'un fichier :

* l'**utilisateur propriétaire** du fichier : user (**u**)
* le **groupe propriétaire** : group user (**g**)
* les **autres utilisateurs** : others (**o**)

Les permissions accordées à chaque utilisateur ou groupe d'utilisateurs sont de 3 types :

* la **lecture** : Read (**r**)
* l'**écriture** : Write (**w**)
* l’**exécution** : eXecute (**x**)

Les droits sont alors affichés par une série de 9 caractères :

![droits](img/droits.png)

Le symbole `-` signifie que le droit n'est pas accordé.

Par exemple, `rwxrw-r--` signifie :
* l'utilisateur possède les droits de lecture, écriture et exécution
* le groupe possède les droits de lecture et écriture
* les autres possèdent le droit de lecture

> **exercice 10 :**  
> Que signifient :
> * `rw-r--r--` ?
> * `rwxr-x--x` ?

Selon que l'on considère un fichier ou un répertoire, ces 3 droits n'ont pas exactement le même sens.

Pour un fichier :
* lecture (noté r) : droit d'afficher le contenu du fichier
* écriture (noté w) : droit de modifier le contenu du fichier
* exécution (noté x) : droit d'exécuter le fichier s'il s'agit d'un fichier exécutable (script, programme)

Pour un répertoire :
* lecture (noté r) : droit de lister les fichiers et sous-répertoires qu'il contient
* écriture (noté w) : droit de créer ou supprimer des fichiers ou sous-répertoires
* exécution (noté x) : droit d'entrer dans le répertoire et d'accéder aux fichiers qu'il contient

Pour savoir s'il s'agit d'un fichier ou répertoire, on place avant les droits :
* **d** s'il s'agit d'un répertoire (**d**irectory)
* **-** s'il s'agit d'un fichier

Par exemple `drwxrw-r--` ou `-rwxrw-r--`.

> **exercice 11 :**  
> * Expliquez ce que peut faire l'utilisateur pour `-rw-rw-r--`.
> * Expliquez ce que peuvent faire les autres pour `dr-xr--r-x`.


### 4. Lien avec les commandes Linux

Le système de fichier est intégralement manipulable depuis la console.

Par exemple pour modifier les droits de lecture, écriture et exécution, on utilise la commande **chmod**.

Elle s'utilise suivie des lettres `u, g, o` puis d'un symbole `+, -, =` puis des lettres `r, w, x`, puis du nom de fichier/répertoire.

Par exemple `chmod ug+x a.txt` signifie qu'on ajoute à l'utilisateur et au groupe le droit d'exécution sur le fichier a.txt. Autre exemple, `chmod o=r Documents` signifie que les autres utilisateurs ont uniquement le droit de lecture sur le répertoire Documents.

> **exercice 12 :**  
> Les droits sont actuellement `drwxrw-rw-` sur le répertoire NSI.
> * Donnez deux commandes permettant de retirer le droit d'écriture au groupe et aux autres utilisateurs.
> * Donnez deux commandes permettant d'ajouter le droit d'exécution au groupe.

Il existe de nombreuses autres commandes permettant de manipuler un système de fichier : vous les avez découvertes en jouant à Terminus !

Voici celles que vous devez connaître :

* pwd (Print Working Directory)
* ls (LiSt), ou ls -l pour afficher les droits en plus
* cd (Change Directory)
* mv (MoVe)
* cp (CoPy)
* touch (créer un fichier)
* mkdir (MaKe DIRectory)
* rm (ReMove)
* rmdir (ReMove DIRectory)
* cat (lire un fichier)
* chmod (CHange MODe)

Si vous avez oublié ce que signifie une commande, vous pouvez utiliser `man`. Pour fermer la console, on utilise `exit`.

Les commandes ne sont utilisables que si l'utilisateur possède les droits appropriés. Il existe cependant un moyen de réaliser n'importe quel commande : utiliser `sudo`. En mode `sudo`, l'utilisateur peut tout faire... à condition de connaître le mot de passe !

> **Pour aller plus loin :**  
> Les ordinateurs du lycée sont sous Windows, les commandes sont donc différentes.  
> Pour vous amuser un peu, vous pouvez utiliser ce simulateur en ligne : [simulateur linux](https://www.masswerk.at/jsuix/index.html). Cliquez sur "open terminal" (en haut), puis entrez votre prénom et appuyer sur entrée. Vous arrivez sur le répertoire racine (/) qui est vide.  
> À vous de créer des répertoires, des fichiers, supprimer des fichiers, changer les droits, vous déplacer en utilisant des chemins absolus ou relatifs...  
> Essayez d'utiliser un maximum de commandes !


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, [C. Mieszczak](https://framagit.org/tofmzk/informatique_git)
