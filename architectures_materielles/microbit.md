# Activité carte micro:bit

**Pour toute cette partie, vous n'êtes pas obligés de recopier votre code dans votre cahier.  
Pensez cependant à copier-coller votre code dans un éditeur de texte (bloc-note, LibreOffice...) au cas où vous en auriez besoin par la suite.**

RAPPEL : Vous devez être à cette adresse :[https://create.withcode.uk/](https://create.withcode.uk/) ; avoir supprimé le code déjà existant et l'avoir remplacé par :
```python
from microbit import *
```
En exécutant le programme à l'aide de la flèche verte (en bas à droite), vous devez avoir quelque chose comme ça :

![simulteur micro:bit](img/simulateur.png)
 

## Utilisons les actionneurs

### Affichage

Il y a plusieurs façons d'afficher des choses sur les 25 LEDS de la cartes en utilisant l'objet `display` qui possède plusieurs méthodes :

* la méthode `clear` eteint toute les LEDs.
  ```python
  display.clear()
  ```
* la méthode `scroll` permet de faire défiler du texte, c'est à dire une chaine de caractères.  
  Testez :
  ```python
  display.scroll('Hello World !')
  ```
* la méthode `sleep` permet de faire une pause (temps en millisecondes).  
  Testez :
  ```python
  display.scroll('A')
  sleep(2000)
  display.scroll('B')
  ```

> Réalisez une fonction `compte_a_rebours` qui prend en paramètre un nombre entier _depart_ et qui affiche toutes les 1 seconde le décompte de la valeur de départ jusqu'à 0.

* la méthode `show` permet d'afficher une image. 

	* Certaines sont déjà définies. Vous en trouverez la liste [ici](https://microbit-micropython.readthedocs.io/en/latest/tutorials/images.html).  
		Testez :
		```python
		display.show(Image.HAPPY)
		```
	* Vous pouvez également définir vos images vous même. Chaque LED pouvant être allumée avec une intensité allant de 0 (éteinte) à 9 (intensité maximale). Voici un joli bateau :
		```python
		def afficher_image():
		    image =  Image("05050:"
		                   "05050:"
		                   "05050:"
		                   "99999:"
		                   "09990")
			display.show(image)
		afficher_image()
		```

> Réalisez une fonction `lancer_de` qui simule le lancé d'un dé en affichant le résultat sur les 25 LEDs de la carte (il faut afficher la **face** du dé, pas juste le numéro).

* On peut même créer des animations. Testez ce code pour couler notre bateau :
	```python
	def animer_images():
	    bateau1 = Image( "05050:"
	                     "05050:"
	                     "05050:"
	                     "99999:"
	                     "09990")
	    bateau2 = Image( "00000:"
	                     "05050:"
	                     "05050:"
	                     "05050:"
	                     "99999")
	    bateau3 = Image( "00000:"
	                     "00000:"
	                     "05050:"
	                     "05050:"
	                     "05050")
	    bateau4 = Image( "00000:"
	                     "00000:"
	                     "00000:"
	                     "05050:"
	                     "05050")
	    bateau5 = Image( "00000:"
	                     "00000:"
	                     "00000:"
	                     "00000:"
	                     "05050")
	    bateau6 = Image( "00000:"
	                     "00000:"
	                     "00000:"
	                     "00000:"
	                     "00000")
	    bateaux = [bateau1, bateau2, bateau3, bateau4, bateau5, bateau6]
	    display.show(bateaux, delay = 400)
	animer_images()
	```

* La méthode `display.set_pixel(x, y, intensite)` permet de fixer, entre 0 (éteinte) et 9 (intensité maximale) la luminosité de la LED de coordonnées (x, y). Les LEDs sont repérées de la façon suivante :

  ![micro:bit - wikipédia](https://upload.wikimedia.org/wikipedia/commons/c/cb/Micro_bit_position_des_DEL.png)

Ainsi, pour faire clignoter dix fois la deuxième LED, le code est le suivant. Testez le :

```python
def faire_clignoter(nombre_de_fois, delai):
    for i in range(nombre_de_fois) :
        display.set_pixel(1, 0, 9)
        sleep(delai)
        display.set_pixel(1, 0, 0)
        sleep(delai)
faire_clignoter(10, 100)
```

> Réalisez une fonction `tourner`, de paramètre _delai_, un entier représentant le délai entre chaque déplacement en millisecondes, qui allume une LED à la fois de façon à donner l'impression que la lumière allumée fait le tour de l'écran.

> Réalisez une fonction `deplacement_aleatoire`, de paramètre _delai_, un entier représentant le délai entre chaque déplacement en millisecondes, qui affiche une LED se déplaçant aléatoirement.

## Utilisons les capteurs

### Boutons

Nous disposons de deux boutons : à gauche le bouton _a_ et, à droite, le bouton _b_ modélisé par deux objets en Python, `button_a`et `button_b`.

Pour tester si on appuie sur un de ces boutons, on utilise la méthode `is_pressed()` qui renvoie _True_ ou _False_.

Testez le code ci-dessous :
```python
def tester_boutons():
    while True :
        if button_a.is_pressed() :
            display.show(Image.HAPPY)
        elif button_b.is_pressed():
             display.show(Image.SAD)
        else :
            display.clear()             
tester_boutons()
```

> Reprenez votre fonction `lance_de`. Modifiez le code de façon à ce que presser sur le bouton _a_ lance un nouveau dé.

> * Réalisez une fonction qui prend en paramètre un nombre x et allume la LED de coordonnées (x, 4).  
> * Réalisez une fonction `deplace_a_gauche` de paramètre _x_ qui :
> 	* si _x_ est strictement supérieur à 0, renvoie x - 1
> 	* sinon renvoie x
> * Réaliser une fonction `deplace_a_droite` de paramètre _x_ qui:
> 	* si _x_ est strictement inférieur à 5 renvoie x + 1
> 	* sinon renvoie x
> * Faites en sorte qu'une pression : 
> 	* sur le bouton _a_ décale, si possible, la LED allumée d'un cran vers la gauche
> 	* sur le bouton _b_ décale, si possible, la LED allumée d'un cran vers la droite.

### Capteur de lumière

Une LEDs peu fonctionner soit comme affichage, soit comme capteur : en les _inversant_ l'écran va devenir un point d'entrée et un capteur de lumière basique, permettant de détecter la luminosité ambiante. Si on utilise les LEDs comme capteurs, on ne peut s'en servir comme affichage. C'est l'un ou l'autre...

Pas d'exemple ici, on ne peut pas se servir des LEDs comme capteur de lumière depuis le simulateur.

### Capteur de température

Ce capteur permet à la carte micro:bit de mesurer la température actuelle de l’appareil, en degrés Celsius.

Testez :
``` Python
print(temperature())
```

*Vous pouvez changer la température en allant dans l'onglet Thermometer au dessus de la carte*.

> Réaliser une fonction `afficher_temperature` qui affiche la température via les LEDs.

### Accéléromètre

La carte micro:bit possède un accéléromètre qui mesure les déplacement selon 3 axes :

- _X_ de gauche à droite
- _Y_ d'avant en arrière
- _Z_ de haut en bas

Vous pouvez aller dans l'onglet Accelerometer pour simuler un mouvement.

Les méthode `get_x`, `get_y` et `get_z` de `accelerometre` permettent de récupérer ces valeurs.Testez les trois fonctions ci-dessous :
```python
def tester_x():
    while True :
        reading = accelerometer.get_x()
        if reading > 20:
            display.show("D") # droite
        elif reading < -20:
            display.show("G") # gauche
        else:
            display.show("-")

def tester_y():
    while True :
        reading = accelerometer.get_y()
        if reading > 20:
            display.show("a") # arriere
        elif reading < -20:
            display.show("A") # avant
        else:
            display.show("-")

def tester_z():
    while True :
        reading = accelerometer.get_z()
        if reading > 20:
            display.show("H") # haut
        elif reading < -20:
            display.show("B") # bas
        else:
            display.show("-")

tester_x()
```

### Boussole

La boussole détecte le champ magnétique de la Terre, permettant de savoir quelle direction la carte indique. 

* La boussole doit être étalonnée avant de pouvoir être utilisée. Testons un peu cela grâce au programme ci-dessous :
	```python
	compass.calibrate()
	```
* On peut savoir si le compas est bien calibré en utilisation la méthode `is_calibrated()` :
	```python
	print(compass.is_calibrated())
	```
* Nous pouvons maintenant récupérer le _cap_ grâce à la méthode `headling()` de `compass` qui renvoie un entier entre 0 et 359 :
	* 0 pour une direction vers le Nord
	* 90 pour une direction vers l'Est
	* 180 pour le Sud
	* 270 pour l'Ouest

	![rose des vents](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Compass_Card_B%2BW.svg/600px-Compass_Card_B%2BW.svg.png)


Testez (vous pouvez aller dans l'onglet Compass pour changer la direction):
```python
def renvoyer_cap() :
    compass.calibrate()
    while True :
        print(compass.heading())
        sleep(1000)
renvoyer_cap()
```

> Réalisez une fonction qui affiche sur les LEDs la direction actuelle (N pour nord, S pour Sud, E pour Est ou O pour Ouest).

### Les broches

Il y a 25 connecteurs externes sur la tranche du _micro:bit_, que l'on nomme _broches_ ou _pins_ en anglais. on peut programmer des moteurs, des LEDs, un haut-parleur ou tout autre composant électrique à l'aide de ces broches. Il est donc possible d'ajouter l'actionneur de son choix en reliant cet actionneur, avec des pinces crocodiles par exemple, à une broche et à la terre (GND) pour fermer le circuit.

![les broches](https://microbit-micropython.readthedocs.io/en/latest/_images/pin0-gnd.png)

On peut utiliser les broches en sortie, comme actionneurs donc, vers un haut-parleur par exemple. Mais on peut également les utiliser en entrée avec un capteur supplémentaire.

Cette possibilité démultiplie les options de la carte. Cependant, l'objectif n'est pas ici d'exploiter la micro:bit au maximum et nous ne nous étendrons pas là dessus. 

Nous n'allons pas développer cette partie d'avantage ici (impossible à réaliser avec le simulateur). Si ça vous intéresse, [allez-ici](https://archive.microbit.org/fr/guide/hardware/pins/).

> **Pour aller plus loin :**
> Si vous avez fini, vous pouvez vous amuser un peu avec la carte !

---

Inspiré du travail de [Christophe Mieszczak](https://framagit.org/tofmzk/informatique_git/)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)



