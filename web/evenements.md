# Web : évènements et interaction avec l'utilisateur

Il ne nous reste plus qu'une chose à découvrir sur le Web : comment utiliser le **JavaScript pour gérer l'interactivité** de la page.

## I. Intégrer du JavaScript

Aujourd'hui un développeur web ne peut pas faire l'impasse sur le JavaScript.

Notre but ici n'est pas d'apprendre un nouveau langage de programmation, mais juste d'étudier quelques exemples d'utilisation du JavaScript pour ajouter des interactions entre l'utilisateur et la page Web.

Avant d'entrer dans le vif du sujet, un petit rappel historique : JavaScript a été créé en seulement dix jours par Brendan Eich en 1995. Malgré son nom, *JavaScript* n'a rien à voir avec le langage *Java*.

Pour être exécuté, le JavaScript doit être intégré à la page HTML. Nous aurons donc maintenant trois fichiers :
* un **.html** avec le contenu
* un **.css** avec le style
* un **.js** avec l'interaction ("js" = JavaScript)

Il faut ensuite ajouter la ligne suivante dans le HTML pour relier le JavaScript à la page :
```html
<script src="script.js" defer></script>
```

Cette balise est à ajouter dans le `head` juste après le lien vers le fichier CSS.

Cela donne donc ceci :

```html
<!DOCTYPE html>

<html lang="fr">

    <head>
        <meta charset="utf-8">
        <title>Page avec JS</title>
        <link rel="stylesheet" href="style.css">
	    <script src="script.js" defer></script>
    </head>

    <body>
        ...
    </body>
    

</html>
```

**1)** Créez un répertoire `evenements`.

**2)** Ajoutez dans ce répertoire 3 fichiers : `page.html`, `style.css` et `script.js`.

**3)** Laissez le CSS et le JS vide pour l'instant. Dans le HTML, copiez-collez le code précédent.

**4)** Remplacez les "..." du body par la balise suivante :

```html
<p> Un petit paragraphe pas très utile. </p>
```


## II. Un premier événement

Une fois le JavaScript relié au HTML, on peut écrire le code.

Un **évènement** est une action réalisée par l'utilisateur sur la page : clic de souris, survol de quelque chose sur la page, appui sur une touche du clavier, etc.

Il est possible d'ajouter des "écouteurs d'évènements" sur des éléments de la page HTML. Leur rôle est d'attendre qu'il se passe quelque chose, et à ce moment là une action sera réalisée. Par exemple : si l'utilise clique sur un paragraphe, alors sa couleur change.

**5)** Ouvrez le fichier `script.js` avec un éditeur de texte (le bloc-notes de Windows par exemple).

Il faut d'abord sélectionner l'élément sur laquelle l'action sera réalisée.

On utilise pour cela la fonction `querySelector` en lui passant en paramètre le nom de la balise à sélectionner dans la page.

**6)** Ajoutez la ligne suivante au fichier JS :

```javascript
let p = document.querySelector("p");
```

On sélectionne donc la balise `<p>` du HTML.

*REMARQUE :* une instruction en JS finit toujours par un point-virgule.

Ensuite, il faut ajoute l'écouteur avec la fonction `addEventListener`.

**7)** Ajoutez la ligne suivante au fichier JS :

```javascript
p.addEventListener("click", frouge);
```

Cela signifie que lorsque l'utilisateur réalise un "clic" sur le paragraphe, alors la fonction `frouge` est appelée.

Il ne reste plus qu'à définir la fonction `frouge` :

**8)** Ajoutez les lignes suivantes au fichier JS :

```javascript
function frouge() {
    this.style.color = "red";
}
```

Une fonction s'écrit en JS avec le mot-clef "function".

Le "this" permet de récupérer l'élément qui a été cliqué (donc le paragraphe ici).

Le .style.color = "red" permet de changer la couleur du paragraphe en rouge (comme dans le CSS).

**9)** Enregistrez le JS puis ouvrez le HTML dans Firefox. Essayez de cliquer sur le paragraphe. Que se passe-t-il ?


## III. Plein d'événements

Il existe beaucoup de types d'événements que l'on peut écouter.

La liste complète est disponible [ici](https://developer.mozilla.org/fr/docs/Web/Events). Voici les plus utilisés : 

| Événement |                Signification                |
|:---------:|:-------------------------------------------:|
|   click   |                clic de souris               |
|  dblclick |            double clic de souris            |
|  keydown  |             appui sur une touche            |
|   keyup   |              relâche une touche             |
|  keypress |          touche maintenue enfoncée          |
| mouseover |            survol avec la souris            |
|  mouseout |       arrêt d'un survol avec la souris      |
|   change  |      modification du contenu d'un input     |
|   submit  | appui sur le bouton d'envoi d'un formulaire |

**10)** Recommencez les manipulations précédentes pour déclencher le changement de couleur du paragraphe lorsqu'on réalise un double-clic dessus.

**11)** Recommencez avec un événement "mouseover", testez, puis faites de même avec un "mouseout". Voyez-vous la différence ?

Il est possible d'ajoutez 2 événements différents sur un même élément.

**12)** Remplacez le code JS par ceci :

```javascript
let p = document.querySelector("p");
p.addEventListener("click", frouge);
p.addEventListener("dblclick", fbleu);

function frouge() {
    this.style.color = "red";
}

function fbleu() {
    this.style.color = "blue";
}
```

**13)** Testez. Que se passe-t-il ?

**14)** Modifiez le JS pour qu'au survol, le paragraphe devienne rouge, et quand on enlève la souris il redevienne noir.


## IV. Modifier un élément

Pour l'instant, nous avons toujours changé la couleur d'écriture. Mais il est possible de faire plein d'autre choses !

On peut modifier tous les *styles* comme avec le CSS en utilisant la notation ".style".

**15)** Modifiez la fonction "frouge" comme ceci :

```javascript
function frouge() {
    this.style.background = "red";
}
```

**16)** Testez. Que se passe-t-il ?

**17)** Modifiez le JS pour que :
* un clic change la couleur de l'arrière plan
* un double clic change la couleur du texte
* un survol change la taille du texte
* un arrêt de survol passe le texte en italique

Il est aussi possible de modifier le *contenu* de l'élément.

Pour cela il faut utiliser la notation ".innerHTML".

**18)** Remplacer votre code JS par ceci :

```javascript
let p = document.querySelector("p");
p.addEventListener("click", transforme);

function transforme() {
    this.innerHTML = "Je vois que tu as fais un clic.";
}
```

**19)** Testez. Que se passe-t-il ?

**20)** Faîtes en sorte qu'au survol de la souris, le texte soit modifié, puis à l'arrêt du survol le texte d'origine réapparait.


## V. Sélectionner divers éléments

On peut ajouter des événements sur tous les éléments du HTML si on le veut.

Il suffit de les sélectionner avec "querySelector" dans le JS.

**21)** Ajoutez un deuxième paragraphe à la suite du premier dans le HTML. Par exemple :

```html
<body>
    <p> Un petit paragraphe pas très utile. </p>
    <p> Un 2eme petit paragraphe pas très utile non plus. </p>
</body>
```

**22)** Remettez le code JS suivant :

```javascript
let p = document.querySelector("p");
p.addEventListener("click", frouge);

function frouge() {
    this.style.color = "red";
}
```

**23)** Testez sur le premier paragraphe, puis sur le deuxième. Que se passe-t-il ?

Lorsqu'il y a plusieurs balises identiques, seule la première est sélectionnée. Pour ajouter des événements sur les éléments que l'on veut, il faut leur ajouter un **ID**.

**24)** Modifiez le HTML comme ceci :

```html
<body>
    <p id="p1"> Un petit paragraphe pas très utile. </p>
    <p id="p2"> Un 2eme petit paragraphe pas très utile non plus. </p>
</body>
```

**25)** Modifiez le JS comme ceci :

```javascript
let p1 = document.querySelector("#p1");
p1.addEventListener("click", frouge);

let p2 = document.querySelector("#p2");
p2.addEventListener("click", frouge);

function frouge() {
    this.style.color = "red";
}
```

On sélectionne les éléments par leur ID en ajoutant un "#" devant.

**26)** Testez ! A-t-on résolu le problème ?

**27)** Modifiez le HTML pour qu'il y ait un titre (`<h1>`) et 2 paragraphes. Ajoutez des événements pour :

* un survol du titre le passe en rouge, il redevient noir quand on enlève la souris
* un clic sur l'un des paragraphes met son fond en jaune, un double clic le repasse en blanc


## VI. Un clic sur l'un modifie l'autre

Il est aussi de modifier un autre élément que celui qui a déclenché l'événement.

**28)** Modifiez la page HTML comme ceci :
```html
<body>
  	<p id="p1"> Un petit paragraphe pas très utile. </p>
    <p id="p2"> Un 2eme petit paragraphe pas très utile non plus. </p>
</body>
```

**29)** Modifiez le JS comme ceci :

```javascript
let p1 = document.querySelector("#p1");
p1.addEventListener("click", frouge);

function frouge() {
    let p2 = document.querySelector("#p2");
    p2.style.color = "red";
}
```

Un clic sur le premier paragraphe modifie le deuxième, car on a remplacé le "this" par une sélection de "#p2".

**30)** Testez !

Souvent, on trouve ce genre de modification quand il y a un bouton sur la page.

**31)** Modifier le HTML ainsi :

```html
<body>
    <p> Un petit paragraphe pas très utile. </p>
    <button> Changer la couleur </button>
</body>
```

**32)** Modifiez le JS ainsi :

```javascript
let bouton = document.querySelector("button");
bouton.addEventListener("click", frouge);

function frouge() {
    let p = document.querySelector("p");
    p.style.color = "red";
}
```

**33)** Testez !

**34)** Modifiez la page pour qu'elle contienne un titre, un paragraphe et un bouton appelé "Mode Sombre". Modifiez le JS pour qu'un clic sur le bouton passe le fond de la page (`<body>`) en noir, le titre en jaune, et le paragraphe en blanc.

## VII. Petit jeu

Et voilà, vous savez maintenant tout faire !

Vous avez mérité de vous amuser un peu.

**35)** Téléchargez le [répertoire suivant](src/cibles) (3 fichiers).

**36)** Lancez la page HTML dans Firefox. C'est un petit jeu où le but est de faire disparaître toutes les cibles du plateau. Mais attention, certaines cibles s'enlèvent avec un clic, d'autres un double clic, d'autres un survol... Pour trouver comment faire disparaître les cibles et gagner, vous allez devoir regarder le code JS !

Tous ces événements JavaScript sont ceux qui sont activés quand vous jouez en ligne (et qu'il n'y a pas besoin d'envoyer des données au serveur).


## Pour aller plus loin

**37)** Si vous avez terminé, vous pouvez reprendre votre DM HTML sur le langage de programmation et y ajouter un bouton "Mode sombre" qui transforme le fond en noir et les écritures en couleurs claires.

*Si vous n'avez pas votre DM avec vous, appelez-moi ! Je peux vous le renvoyer.*

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
