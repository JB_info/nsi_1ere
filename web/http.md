# Web

Selon une étude (publiée par Hootsuite et We are social en 2020), *89%* de la population française a accès à l'Internet et passent en moyenne *5h08* par jour à l'utiliser.

![](img/etude1.jpg)
![](img/etude2.jpg)

Vous savez tous ouvrir un navigateur et vous rendre sur une page, grâce à votre ordinateur, votre téléphone ou votre tablette... Mais savez-vous comment fonctionne le Web ?

## I. Qu'est-ce que le Web ?

On confond souvent Internet et le Web (ou « toile » en français). Mais où est la différence ?

En bref, **le Web est un ensemble d’informations**, tandis qu’**Internet est le réseau informatique qui permet de les transporter**.

Pour prendre une analogie avec le réseau postal : si l'Internet est le facteur, le Web est le courrier.

![](img/internet-vs-web.jpg)

Deux choses si différentes qu’elles n’ont pas été créées en même temps : ARPAnet, c’est à dire l’ancêtre d’Internet, a vu le jour en 1967, alors que le Web, permettant de naviguer entre des pages, date de 1989.

> **exercice 1 :**  
> Du coup, quelle est la phrase correcte ici :
> * J'ai fait une recherche sur Internet.
> * J'ai fait une recherche sur le Web.

Le terme **Web** est une contraction de **World Wide Web**.

> **exercice 2 :**  
> Alors, que signifie le www dans une URL ?

Concrètement, le Web est constitué de données stockées sur des serveurs Web. Ces données sont appelées des **pages Web**.

Une page Web est un fichier HTML. Ce fichier peut contenir des liens vers d'autres fichiers, par exemple :
* des images
* des feuilles de style (CSS)
* des scripts pour l'animation de la page (Javascript)
* ...

Le fichier HTML peut aussi contenir des liens vers d'autres pages Web. Tous ces liens sont appelés **liens hypertextes**. Les liens forment une sorte de toile qui relie toutes les pages Web.

> **exercice 3 :**  
> Voyez-vous donc pourquoi le Web est parfois appelé la « toile » ?

Anecdote : la toute première page Web jamais créée (le 13 novembre 1990) est encore disponible ! Vous pouvez aller la consulter ici : [http://info.cern.ch/hypertext/WWW/TheProject.html](http://info.cern.ch/hypertext/WWW/TheProject.html)

> **exercice 4 :**  
> Allez sur la première page Web créée et indiquez le nombre de liens hypertextes qu'elle contient.

Pour accéder à une page Web, on utilise des **URL** (*U*niform *R*essource *L*ocator).

Une URL ressemble à ceci :

[http://www.example.com/repertoire/sousrep/index.html](http://www.example.com/repertoire/sousrep/index.html)

Nous y retrouvons :
* le protocole réseau utilisé : `http`
* le nom de domaine = nom du serveur Web qu'il faut contacter : `www.example.com`
* le chemin où trouver la page sur le serveur : `/repertoire/sousrep/`
* le nom de la page Web : `ìndex.html`

> **exercice 5 :**  
> * Rappelez à quelle couche réseau appartient le protocole HTTP.
> * Quel protocole utilise votre navigateur pour trouver l'adresse IP à laquelle correspond le nom de domaine `www.example.com` ?
> * Le chemin `/repertoire/sousrep/` est-il absolu ou relatif ?

Vous saisissez ces URL dans un navigateur. Le **navigateur est un logiciel qui permet l'affichage des pages Web**.

Il existe de nombreux navigateurs même s’il y en a qui dominent le marché.

> **exercice 6 :**  
> Donnez le nom des navigateurs appartenant aux sociétés suivantes :
> * Google :
> * Mozilla : 
> * Apple : 
> * Microsoft :


## II. HTTP 

### 1. Interaction client/serveur

C'est donc le protocole HTTP qui s'occupe de transporter les pages Web du serveur jusqu'à votre ordinateur quand vous entrez une URL dans votre navigateur.

Mais concrètement, comment ça se passe tout ça ?

HTTP permet le transfert de pages Web entre deux machines :
* le client (= navigateur) qui demande les pages
* le serveur (= machine qui stocke une page Web) qui envoie la page demandée

On dit que le **client effectue une requête HTTP** et que le **serveur envoie une réponse à la requête** : c'est l'interaction client/serveur.

> **exercice 7 :**  
> Sur le schéma suivant, précisez à quelle couleur de flèche correspondent les "Requêtes" et à quelle couleur correspondent les "Réponses".
> 
> ![](img/client_serveur.png)


Le client peut être n'importe quel ordinateur équipé d'un navigateur.  
Le serveur peut aussi être n'importe quel ordinateur (même le votre !).  
Aujourd'hui, les serveurs sont des ordinateurs conçus exprès pour ça (avec beaucoup de mémoire et de processeurs). Ils sont souvent situés dans des datacenters :

![](img/datacenter.jpg)

HTTP permet au client et au serveur de créer et d'interpréter les requêtes et les réponses.

Il existe un autre protocole qui fait le même boulot : HTTPS. La différence ?
* HTTP = HyperText Transfer Protocol
* HTTPS = HyperText Transfer Protocol Secure

HTTPS est sécurisé (les données ne peuvent pas être lues par un hacker). Cette année, nous ne verrons que HTTP. Pour le chapitre sur la cybersécurité, RDV en Terminale !

### 2. Requêtes et réponses

À quoi ressemble la requête HTTP d'un client ?

Elle est constituée de : 
* un en-tête de requête
* un corps de requête

L'entête de requête précise :
* la méthode utilisée : GET / POST / ...
* la page Web (avec son chemin) demandée
* le protocole utilisé (HTTP et sa version)
* d'autres détails sur le client (navigateur, système d'exploitation, langue...)

La méthode sert à indiquer au serveur ce que vous voulez réaliser. La **méthode pour récupérer une page Web est GET**.

Le corps de requête est toujours vide avec la méthode GET.

> **exercice 8 :**  
> * Allez à l'adresse [http://info.cern.ch/hypertext/WWW/TheProject.html](http://info.cern.ch/hypertext/WWW/TheProject.html).
> * Cliquez droit puis "Inspecter".
> * Allez dans l'onglet "Réseau" :
> ![](img/inspecter.png)
> * Cliquez sur "Rechargez" pour faire apparaître les requêtes et les réponses.
> * Cliquez sur la première ligne (Fichier TheProject.html)
> * Allez voir les informations qui apparaissent dans "En-tête de la requête" (il faut cliquer sur texte brut sur la droite) :
> ![](img/requete.png)
> * Retrouvez-vous bien :
> 	- la méthode ?
> 	- la page Web demandée ?
> 	- le protocole ?
> 	- les autres infos (nom du navigateur par exemple) ?
> * Allez dans l'onglet "Requête" et vérifiez que le corps est bien vide.

La réponse est constituée de :
* un entête de réponse
* un corps de réponse

L'entête de réponse précise :
* le code de statut et sa signification : 200 OK / 404 Not Found / 301 Moved Permanently / ...
* le protocole utilisé (HTTP et sa version)
* d'autres informations sur le serveur (type de server, date de mise à jour...)

Le code de statut indique le résultat de la requête :
* 200 si tout va bien
* 404 si la page Web n'est pas trouvée
* 301 si la page Web a été déplacée sur le serveur (redirection)

Si le code de statut est 200, le corps de réponse contient le code HTML de la page demandée.

> **exercice 9 :**  
> * Effectuez les mêmes manipulations que précédemment afin d'accéder cette fois à l'entête de réponse.
> * Retrouvez-vous bien :
> 	- le code de statut ?
> 	- le protocole ?
> 	- d'autres infos (type du serveur par exemple) ?
> * Allez dans l'onglet "Réponse" et cochez "Brut". Retrouvez-vous bien le code HTML de la page ?
> ![](img/reponse.png)


> **exercice 10 :**
> * Ouvrez un nouvel onglet de votre navigateur.
> * Cliquez droit, "Inspecter" puis onglet "Réseau".
> * Copiez-coller l'adresse suivante dans la barre de recherche en haut et appuyez sur Entrée : https://fr.wikipedia.org.
> * Quel est le code de statut de la toute première réponse ?
> * Regardez l'URL où vous êtes maintenant. Que s'est-il passé ?

> **exercice 11 :**
> * Ouvrez un nouvel onglet de votre navigateur.
> * Cliquez droit, "Inspecter" puis onglet "Réseau".
> * Copiez-coller l'adresse suivante dans la barre de recherche en haut et appuyez sur Entrée : https://enthdf.fr/faussepage.html.
> * Quel est le code de statut de la toute première réponse ? Expliquez.

Vous avez du remarquer que même si vous ne demandez qu'une page Web, il y a toujours plusieurs requêtes et réponses dans l'onglet "Réseau".

En effet, lorsque le client (navigateur) fait une requête, il ne demande que le code HTML. Si ce code est associé à une feuille de style, le navigateur doit alors faire une deuxième requête au serveur pour récupérer le code CSS. Et une troisième requête s'il y a de l'interaction pour récupérer le code JavaScript. Puis une quatrième s'il y a une image dans la page. Et une cinquième si... vous avez compris. Une requête par fichier !

> **exercice 12 :**  
> En allant à l'adresse [https://www.google.com/](https://www.google.com/), repérez au moins :
> * une requête pour le contenu (fichier HTML)
> * une requête pour le style (fichier CSS)
> * une requête pour l'interaction (fichier JavaScript)
> * une requête pour une image (fichier PNG, JPG, SVG...)


### 3. GET ou POST ?

Nous avons vu que la méthode GET permettait de récupérer une page Web. Mais quelque fois, utiliser cette méthode peut être dangereux...

 > **exercice 13 :**  
 > * Allez sur Wikipédia : [https://fr.wikipedia.org/](https://fr.wikipedia.org/)
 > * Dans la barre de recherche de Wikipédia, tapez "html css" et appuyez sur Entrée.
 > * Le serveur vous a renvoyé la page avec la méthode GET. Regardez l'URL où vous êtes maintenant. Retrouvez-vous les mots que vous venez de taper ?
 > * Quel serait le problème si on utilisait GET pour, par exemple, afficher une page Web après avoir demandé à l'utilisateur de saisir son identifiant et son mot de passe ?

Avec la méthode GET, les données entrées apparaissent directement dans l'URL :

[http://www.example.com/repertoire/sousrep/index.html?champ1=valeur1&champ2=valeur2](http://www.example.com/repertoire/sousrep/index.html?champ1=valeur1&champ2=valeur2)

Tout ce qu'il y a après le "?" correspond aux données rentrées par l'utilisateur. Par exemple, si une page Web utilisait GET pour afficher une page après qu'un utilisateur ait entré son identifiant et son mot de passe, cela donnerait : `?id=jbenouwt&password=fauxmdp`

Pour les données sensibles, il faut donc utiliser une autre méthode : **POST**.

Avec POST, les données n'apparaissent plus dans l'URL, elles sont envoyées au serveur **dans le corps de requête** (qui était vide avec GET).

> **exercice 14 :**  
> * Allez à l'adresse [https://liris-ktbs01.insa-lyon.fr:8000/blogephem/](https://liris-ktbs01.insa-lyon.fr:8000/blogephem/).
> * Cliquez droit, "Inspecter", onglet "Réseau".
> * Aller tout en bas de la page, cliquez sur "Créer un nouvel article".
> * Saisissez le titre et le contenu que vous voulez (⚠️ ATTENTION PAS DE DONNÉES PERSONNELLES CE SERVEUR NE M'APPARTIENT PAS ⚠️).
> * Cliquez sur Envoyer et :
> 	- Dans "Entête de la requête", vérifiez que la méthode utilisée est bien POST.
> 	- Vérifiez que vos données n'apparaissent plus dans l'URL.
> 	- Dans l'onglet "Requête", vérifiez que vos données sont bien dans le corps de requête.

POST sécurise un peu l'envoi de données au serveur car elles n'apparaissent plus dans l'URL mais un utilisateur plus expérimenté (comme vous) peut quand même les retrouver dans l'onglet Réseau.

Le seul moyen de sécuriser totalement les données est d'utiliser HTTPS au lieu d'HTTP.

> **exercice 15 :**  
> L'[ENT](https://enthdf.fr/) utilise HTTPS, donc votre mot de passe est caché. Par contre, [France IOI](http://www.france-ioi.org/) utilise HTTP, donc tout est visible.  
> En pensant à ouvrir l'onglet Réseau avant, connectez vous à l'ENT et vérifiez la requête POST ; puis connectez-vous à France IOI et vérifiez la requête POST.


## III. Formulaires

Vous savez maintenant que pour envoyer des données au serveur, le client doit utiliser une requête POST. Mais comment fait-on pour créer une page Web qui permet d'envoyer des données ?

On utilise un **formulaire**. Un formulaire contient :
* des champs de saisie
* un bouton de validation

Un formulaire est **toujours lié à une méthode** (GET ou POST).

Les formulaires s'intègrent directement au code HTML.

Une fois que les données du formulaire parviennent au serveur, c'est un autre langage de programmation qui intervient : [le PHP](https://www.php.net/manual/fr/intro-whatis.php). PHP n'est pas au programme de NSI, il faudra attendre le post-bac si ça vous intéresse (ou vous former vous-même, il y a de très bons tutoriels sur *le Web*).

Pour apprendre à faire des formulaires, allez faire [l'activité suivante](./formulaires.md).

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, datacenter de Microsoft
