# Web : formulaires

*Pas besoin de recopier les exercices suivants dans votre cahier.*

## Un premier formulaire

1. Copiez le code suivant dans un ficher que vous nommerez `form1.html` :

```html
<!DOCTYPE html>

<html lang="fr">

  <head>
    <meta charset="utf-8"/>
    <title>Form 1</title>
  </head>

  <body>
    <h1>Un petit formulaire en HTML</h1>
    <!-- Le formulaire ci-dessous -->

    <form action="" method="GET">

      <label> Entrez votre nom : </label>
      <input type="text" name="nom">
    	
      <br>
    	<button type="submit">Envoyer les données</button>
    </form>

  </body>

</html>
```

2. Ouvrez le fichier avec un navigateur, remplissez le formulaire, envoyez les données et observez l'URL.
3. Modifiez le formulaire HTML pour utiliser la méthode POST et recommencez les manipulations de la question 2.

Ici aucun serveur Web ne récupère les données (donc rien ne se passe, et on met action="" dans la balise `form`).


## Les zones de saisie

Les zones de saisie sont toutes réalisées avec des balises `<input type="..." name="...">`.

Il en existe différentes sortes :

* type="text"
* type="number"
* type="password"
* type="email"
* type="color"
* type="checkbox"
* type="radio"

Pour une aide sur comment utiliser tous ces types d'input, vous pouvez aller voir [ici](https://www.w3schools.com/html/html_form_input_types.asp).

Tous les input doivent obligatoirement avoir un attribut name="..." pour que le serveur puisse récupérer les valeurs.

4. À vous de jouer : créez un fichier `form2.html` avec un formulaire contenant des input de tous les types de la liste précédente.

5. Quelle méthode avez-vous utilisé, GET ou POST ? Pourquoi ?

Si vous avez terminé, vous pouvez vous amuser à créer des petits formulaires (il y a plein d'autres types d'input sur [ce site](https://www.w3schools.com/html/html_form_input_types.asp)).

## DM Facultatif

**A RENDRE POUR LE MERCREDI 25/05/2022 à 23h59 dernier délai.**

L'objectif est de réaliser un [portrait chinois](https://www.noubliepasdecrire.com/blog/article/50-choses-demander-correspondants-portrait-chinois/74) avec un formulaire HTML.

Un portrait chinois est constitué de questions de type « Si j'étais un ...., je serais .... ».

Exemple : 
* Si j'étais un plat, je serais ...
* Si j'étais une couleur, je serais le ...
* Si j'étais une saison, je serais ...
* etc.

Dans votre formulaire HTML, les champs à compléter ... seront des zones de saisie.

**TRAVAIL À FAIRE :**

* Une page Web bien structurée contenant au moins un titre, un paragraphe explicatif (qui indique à l'utilisateur ce qu'il doit faire), un formulaire, et un texte final avec votre nom/prénom.
* Le formulaire doit contenir au moins 10 questions de portrait chinois construits à l'aide de zones de saisie de 5 types différents (ex : une zone de texte, une zone de couleur, etc.) et le bouton de validation.
* La page Web doit être propre et bien construite. Utilisez des `<br>` pour sauter des lignes et réalisez quelques règles de style CSS.

**BAREME :**

* Structure de la page Web : 0.5
* Titre + paragraphe + nom/prénom : 1
* Structure du formulaire avec bouton : 1.5
* 5 types de zones de saisie + 10 questions : 5
* Style : 2


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
