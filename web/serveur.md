# Web : créer son propre serveur

*Pas besoin de recopier les exercices suivants dans votre cahier.*

## I. Un mini serveur

1. Créez un répertoire nommé `mini_serveur`.
2. Dans ce répertoire, créer un fichier Python `serveur.py`.
3. Copiez-collez le code Python suivant dans `serveur.py` :

```python
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def accueil():
    return render_template('accueil.html')

app.run()
```

N'exécutez pas tout de suite ce code.

4. Dans le répertoire `mini_serveur`, créez un répertoire `templates`.
5. Dans le répertoire `templates`, créez un fichier `accueil.html` contenant le code suivant :

```html
<!DOCTYPE html>

<html lang="fr">

  <head>
    <meta charset="utf-8"/>
    <title>Accueil</title>
  </head>

  <body>
    <h1>Une petite page d'accueil</h1>
    
	<p> Bonjour ! </p>
	
  </body>

</html>
```

6. Retournez dans le fichier python `serveur.py` et exécutez le.
7. Allez dans un navigateur et entrez `localhost:5000/` dans la barre d'adresse de votre navigateur.

Bravo, vous venez de mettre en route votre tout premier serveur !

*Quelques explications :*

* Flask est une bibliothèque qui permet de gérer un serveur directement depuis Python, mais les développeurs utilisent plus souvent le langage PHP.
* "localhost:5000/" signifie que le serveur est local, c'est-à-dire qu'il n'existe que sur votre propre ordinateur. Pour mettre votre page web sur internet et que tout le monde puisse y accéder, il faudrait acheter un nom de domaine ([ce site](https://www.ionos.fr/digitalguide/domaines/conseils-sur-le-domaine/comment-acheter-un-nom-de-domaine/) explique comment faire)
* le @app.route('/') du fichier Python donne le chemin à saisir dans l'URL
* render_template('accueil.html') permet de retrouver la page Web à ouvrir dans le répertoire templates

8. Allez dans l'inspecteur de réseau (clic droit, Inspecter, onglet Réseau). Cliquez sur la requête de la page Web (la première de la liste normalement). Retrouvez dans les en-têtes les informations suivantes :

* le langage utilisé par le serveur (python)
* le nom du navigateur que vous utilisez (mozilla firefox)
* le protocole (http)
* le type de requête (get)
* le code de réponse (200)

9. Toujours dans l'inspecteur réseau, allez dans l'onglet "Réponse" et vérifiez que le code correspond bien à ce que vous avez copié dans `accueil.html`.

10. Dans le répertoire `templates`, créez une deuxième page web `ma_page.html` qui contient ce que vous voulez.

11. Dans votre fichier Python `serveur.py`, ajoutez les lignes suivantes *au-dessus de app.run()* :

```python
@app.route('/pageperso')
def ma_page():
    return render_template('ma_page.html')
```

12. Exécutez le programme Python puis dans le navigateur allez cette fois à l'adresse `localhost:5000/pageperso`.

Bravo, vous venez de créer votre premier serveur à plusieurs pages !

13. Ajoutez une troisième page que vous voulez dans templates, et un nouveau chemin d'accès dans le code Python. Testez.

## II. Un serveur interactif

L'intérêt des serveurs Web (PHP ou Python comme nous) est de proposer du contenu personnalisé au client.

14. Créez un nouveau répertoire `serveur_interactif`.

15. Ajoutez-y un nouveau fichier python `serveur.py` qui contient le code suivant :

```python
from flask import Flask, render_template, request
import datetime

app = Flask(__name__)

@app.route('/heure')
def horloge():
    date = datetime.datetime.now()
    h = date.hour
    m = date.minute
    s = date.second
    return render_template('horloge.html', heure = h, minute = m, seconde = s)

app.run()
```

N'exécutez pas tout de suite.

Le "datetime" est une bibliothèque Python qui récupère l'heure actuelle.

16. Créez un répertoire templates et ajoutez dedans un fichier `horloge.html` qui contient ceci :

```html
<!DOCTYPE html>

<html lang="fr">

  <head>
    <meta charset="utf-8"/>
    <title>Horloge</title>
  </head>

  <body>
    <h1>Une petite page qui donne l'heure</h1>
    
	<p> Bonjour ! </p>
	<p> Il est {{heure}} h, {{minute}} minutes et {{seconde}} secondes.</p>
	
  </body>

</html>
```

17. Retournez dans `seveur.py`, exécutez le et allez à l'adresse `localhost:5000/heure`.

Bravo, vous venez de créer votre premier serveur personnalisé au client !

*Quelques explications :*
* les données entre doubles-accolades `{{ ... }}` sont des paramètres qui sont personnalisés pour chaque client
* les paramètres sont à écrire dans le render_template (ex : `render_template('horloge.html', heure = h, minute = m, seconde = s`)

18. Créez une page qui indique la date du jour (pour récupérer la date en Python il faut utiliser date.year, date.month, date.day) lorsqu'on se rend à l'adresse `localhost:5000/date`.


## III. Gestion des formulaires

19. Créez un nouveau répertoire `serveur_formulaires` avec dedans un répertoire `templates` et un fichier `serveur.py`

20. Dans templates, ajoutez un fichier `formulaire_post.html` contenant ceci :

```html
<!DOCTYPE html>

<html lang="fr">

  <head>
    <meta charset="utf-8"/>
    <title>Formulaire</title>
  </head>

  <body>
    <h1>Un petit formulaire</h1>
    
    <form action="/resultatpost" method="POST">
        <label>Nom : </label>
		<input type="text" name="nom"/>
		
        <label>Prénom : </label>
		<input type="text" name="prenom"/>
		
        <br>
    	<button type="submit">Envoyer les données</button>
    </form>
	
  </body>

</html>
```

21. Dans templates, ajoutez aussi un fichier `reponse.html` qui contient ceci : 

```html
<!DOCTYPE html>

<html lang="fr">

  <head>
    <meta charset="utf-8"/>
    <title>Réponse</title>
  </head>

  <body>
    <p>Bonjour {{prenom}} {{nom}}, j'espère que tu vas bien.</p>
  </body>

</html>
```

22. Et pour finir, voici le contenu de `serveur.py` :

```python
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/formpost')
def formulaire():
    return render_template('formulaire_post.html')

@app.route('/resultatpost', methods = ['POST'])
def reponse():
  reponses = request.form
  n = reponses['nom']
  p = reponses['prenom']
  return render_template("reponse.html", nom = n, prenom = p)

app.run()
```

23. Lancez le serveur, allez à l'adresse `localhost:5000/formpost`, remplissez et envoyez le formulaire.

Bravo, vous avez créé votre premier serveur capable de gérer les données des clients !

*Quelques explications :*

* le `action="/resultatpost"` de la balise form indique le chemin à suivre quand le formulaire est envoyé
* le `method="POST"` de la balise form doit être repris dans le @app.route pour que le serveur sache quel type de requête attendre (`@app.route('/resultatpost', methods = ['POST'])`)

24. Ajoutez dans le dossier templates un fichier `formulaire_get.html` qui contient le même code que `formulaire_post.html` mais en changeant la méthode POST par GET et l'action /resultatpost par /resultatget dans la balise "form".

25. Ajoutez dans `serveur.py` le code suivant *au dessus de app.run()* :

```python
@app.route('/formget')
def formulaire():
    return render_template('formulaire_get.html')

@app.route('/resultatget', methods = ['GET'])
def reponse():
  reponses = request.args
  n = reponses['nom']
  p = reponses['prenom']
  return render_template("reponse.html", nom = n, prenom = p)
```

26. Relancez le serveur, allez à l'adresse `localhost:5000/formget`, remplissez et envoyez le formulaire.

27. En ouvrant l'onglet réseau, recommencez les manipulations des questions 23 et 24 pour voir la différence entre les requêtes et les URL.

28. En choisissant la bonne méthode entre GET et POST, créez dans un nouveau répertoire un serveur qui possède un formulaire d'inscription (nom, prénom, identifiant, adresse mail, numéro de téléphone et mot de passe) et renvoie l'utilisateur vers une page avec un petit mot de confirmation (ex : "Bienvenue Jean Dupont, vos contacts j.dupont@xxx.fr et 0612345678 ont bien été enregistrés. Pour vous connecter la prochaine fois, il faudra saisir votre identifiant superjeandupont et votre mot de passe jeandupont09.")

## Serveur sur les langages de programmation

29. Téléchargez le [répertoire suivant](src/serveur_langages) qui contient le fichier `serveur.py` et un répertoire `templates`.
 
Dans templates, il y a des fichiers html présentant divers langages de programmation (ce sont vos DMs !) et un fichier formulaire.html.

30. Lancez le serveur et allez à l'adresse `localhost:5000/`. Remplissez le formulaire et envoyez le. Qu'obtenez-vous ?

31. Complétez les fichiers `formulaire.html` et `serveur.py` pour y ajouter tous les langages disponibles. Testez.

32. Si vous avez terminé, vous pouvez faire le CSS pour le formulaire par exemple !

---

II. inspiré du travail de [D. Roche](https://github.com/dav74)

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
