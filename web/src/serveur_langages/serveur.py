from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def formulaire():
    return render_template('formulaire.html')

@app.route('/resultat', methods = ['POST'])
def resultat():
    reponses = request.form
    if reponses['langage'] == 'c++':
        return render_template('c++.html')
    elif reponses['langage'] == 'c#':
        return render_template('c#.html')
    # A COMPLETER

if __name__ == '__main__':
    app.run()