// lance le jeu au clic du bouton
let bouton = document.querySelector("button");
bouton.addEventListener('click', demarre);

function demarre() {
    //permet d'empécher de cliquer sur le bouton une fois le jeu démarré
    this.removeEventListener('click', demarre);

    let plateau = document.querySelector("#plateau");
    let nb_cibles = document.querySelector("input").value;
    
    // boucle for pour créer les cibles aléatoirement
    for (let i=0; i<nb_cibles; i++) {

        // crée une cible
        let cible = document.createElement("div");
        cible.className = "cible"

        // place la cible à un endroit au hasard sur le plateau
        cible.style.top = Math.random()*370+"px";
        cible.style.left = Math.random()*370+"px";

        // tire au sort une couleur
        coul = Math.floor(Math.random()*4);

        // événements pour retirer les cibles
        if (coul == 0) {
            cible.style.background = "red";
            cible.addEventListener('click', enlevecible);
        }
        else if (coul == 1) {
            cible.style.background = "blue";
            cible.addEventListener('dblclick', enlevecible);
        }
        else if (coul == 2) {
            cible.style.background = "violet";
            cible.addEventListener('mouseover', enlevecible);
        }
        else {
            cible.style.background = "green";
            cible.addEventListener('mouseout', enlevecible);
        }

        // ajoute la cible au plateau
        plateau.appendChild(cible);

    };

};

// fonction pour enlever une cible
function enlevecible() {
    // retire la cible
    let plateau = document.querySelector("#plateau");
    plateau.removeChild(this);

    // s'il n'y a plus de cible
    if (plateau.innerHTML == '') {
        // affiche un message de félicitations
        alert("Gagné ! Bravo :)")
        // permet de relancer le jeu
        let bouton = document.querySelector("button");
        bouton.addEventListener('click', demarre);
    }
};
