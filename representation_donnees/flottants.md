# Représentation des données : nombres réels



## I. Les nombres flottants

### 1. Ensemble des réels  

* L'ensemble des entiers naturels ($`\mathbb N`$) est l'ensemble des entiers positifs (ex: 0, 1, 5...).
* L'ensemble des entiers relatifs ($`\mathbb Z`$) est l'ensemble des entiers positifs et négatifs (ex: -5, 0, 3...).
* L'ensemble des rationnels ($`\mathbb Q`$) est l'ensemble des nombres que l'on peut écrire comme un quotient de 2 entiers relatifs (ex: -5/1, 5/2, 1/3...).
* L'ensemble des irrationnels ($`\mathbb Q'`$) est l'ensemble des nombres que l'on ne peut pas exprimer comme un quotient de 2 entiers relatifs (ex: $`\pi`$, $`\sqrt{2}`$...).
* L'ensemble des réels ($`\mathbb R`$) est l'ensemble des rationnels et irrationnels.

Vous avez déjà vu comment sont représentés les entiers relatifs au sein d'un ordinateur. Nous allons maintenant voir comment sont représentés les nombres réels.

### 2. Type float en Python  

Dans les machines, on utilise **les nombres à virgule flottante**. Les nombres sont alors appelés des *flottants* (*float* en anglais).

Python encode donc les nombres soit comme des entiers (type `int`), soit comme des réels (type `float`).

> **exercice 1 :**
>
> Vérifiez dans le shell le type des nombres suivants : 1, 1.0, 1/3, 4/2, $`\pi`$, $`\sqrt{2}`$, et $`\sqrt{4}`$.  
> *Indice : la bibliothèque `math` propose la constante `pi`, et la fonction `sqrt` pour la racine carrée.*
>
> ```
> 
> 
> ```



## II. Représentation binaire des flottants

### 1. Passage de la base 10 à la base 2  

Prenons par exemple le nombre `12,25`.

* Il faut déjà représenter la partie entière : 12.  
  C'est un entier relatif, vous savez déjà comment ils sont représentés !  
  Rappel : $`12 = 1100_{2}`$ (divisions par 2 et on remonte les restes)

* Il faut ensuite représenter la partie décimale : 0,25.

  - *en base 10 :* $`0,25 = 0,2 + 0,05 = 2 \times 10^{-1} + 5 \times 10^{-2}`$.  
    Cela vient de la multiplication par 10 :
    
    + $`0,25 \times 10 = 2,5 = 2 + 0,5`$ : la partie entière est **2**, la partie décimale restante est `0,5`
    
    + $`0,5 \times 10 = 5,0 = 5`$ : la partie entière est **5**, il n'y a plus de partie décimale à explorer.
    
    Au final, la partie décimale de notre nombre s'écrit donc $`25_{10}`$.
  
  - *en base 2 :* c'est le même principe, mais en multipliant par 2 :

    + $`0.25 \times 2 = 0,5 = 0 + 0,5`$ : la partie entière est **0**, la partie décimale restante est `0,5`

    + $`0.5 \times 2 = 1,0 = 1`$ : la partie entière est **1**, il n'y a plus de partie décimale à explorer.
  
    Au final, la partie décimale de notre nombre s'écrit donc $`01_{2}`$.
    
    En effet, $`0,01_{2} = 0 \times 2^{-1} + 1 \times 2^{-2} = 0 \times 0,5 + 1 \times 0,25 = 0,25`$.

La représentation binaire de $`12,25_{10}`$ est donc $`1100,01_{2}`$.

> **exercice 2 :**
>
> Trouvez la représentation binaire des nombres suivants : 2,125 et 5,375.
>
> ```
> 
> 
> 
> 
> 
> ```

### 2. Passage de la base 2 à la base 10  

Retrouver la représentation en base 10 en partant de la représentation binaire d'un flottant ne pose aucun problème.

Prenons par exemple $`100,01_{2}`$ :

* Pour la partie entière $`100_{2}`$, vous savez déjà le faire : $`1 \times 2^{2} + 0 \times 2^{1} + 0 \times 2^{0} = 4 + 0 + 0 = 4`$.
* Pour la partie décimale $`,01_{2}`$, c'est le même principe mais avec les puissances négatives : $`0 \times 2^{-1} + 1 \times 2^{-2} = 0 + 0,25 = 0,25`$.

Finalement, on a donc $`100,01_{2} = 4,25_{10}`$.

> **exercice 3 :**
>
> Trouvez la représentation en base 10 des flottants en base 2 suivants : 1101,101 et 11,11.
>
> ```
> 
> 
> 
> 
> 
> ```

### 3. Une représentation approximative

> **exercice 4 :**
>
> Dans le shell, tapez la commande `0.1 + 0.2 == 0.3`. Que trouvez-vous ?  
> Pour comprendre ce qu'il se passe, cherchez l'écriture binaire de 0,1.  
> Que constatez-vous ?
> **Appelez votre professeur pour faire le point.**
>
> ```
> 
> 
> 
> 
> ```

Nous venons de voir qu'il n'est pas possible de représenter 0,1 de façon exacte en base 2 : on obtient alors une **approximation** du nombre. C'est le cas pour la plupart des flottants.

**Le test d'égalité (le '`==`' en Python) entre 2 flottants n'a donc *aucun sens*.**

*Un exemple frappant : En 1991, pendant la guerre du Golfe, un anti-missile Patriot manque l'interception d'un missile Scud. L'erreur était due au fait que la position de l'anti-missile était calculée en multiples de 0,1 seconde. Le manque de précision peut vous sembler négligeable, mais après 100 heures de vol, on arrive à une erreur de 0,34 seconde, et pendant ce temps le missile a parcouru près de 600 mètres... Bilan : 28 morts !*

Pour comparer deux flottants, on va devoir les comparer de façon approximative en ne regardant qu'un certain nombre de chiffres après la virgule.

> **exercice 5 :**
>
> Ecrivez une fonction `comparaison_flottants(flottant1, flottant2, precision)` qui prend en paramètre deux nombres flottants et la précision souhaitée (le nombre de chiffres après la virgule qu'on va regarder).  
> Cette fonction renverra `True` si $`| flottant1 - flottant2 | < 10^{-precision}`$ et `False` sinon.  
> Vous pourrez utilisez la fonction `abs` pour la valeur absolue.
>
> Testez votre fonction :
> ```python
> >>> comparaison_flottants(0.1+0.2, 0.3, 16)
> True
> ```



## III. L'écriture à virgule flottante

Il devient vite compliqué d'utiliser la méthode vue au-dessus pour les très petits ou les très grands nombres. Pour y remédier on utilise la notation scientifique (la virgule va "flotter" à gauche ou à droite, d'où le nom d'*écriture à virgule flottante*).

Cette façon de représenter les flottants a été définie en 1985 par la norme IEEE-754, aujourd'hui la plus utilisée pour la représentation des nombres à virgule flottante.

### 1. Notation scientifique

- En base 10, la notation scientifique permet d'écrire un nombre décimal sous forme $`s \times m \times 10^{n}`$, avec :
  * s le *signe* (-1 ou +1)
  * m la *mantisse* (nombre décimal appartenant à $`[1,10[`$)
  * n l'*exposant* (entier relatif)
  
  Exemples :  
  $`123456,789  = +1 \times 1,23456789 \times 10^{5}`$ (on décale la virgule de 5 rangs vers la gauche)  
  $`0,0000312 = +1 \times 3,12 \times 10^{-5}`$ (on décale la virgule de 5 rangs vers la droite)

- En base 2, la notation scientifique fonctionne de la même façon mais avec 2 :  $`s \times m \times 2^{n}`$, avec :
  * s le *signe* (-1 ou +1)
  * m la *mantisse* (nombre décimal appartenant à $`[1,2[`$)
  * n l'*exposant* (entier relatif)
  
  Exemples :  
  $`1100,01_{2} = +1 \times 1,10001_{2} \times 2^{3}`$ (on décale la virgule de 3 rangs vers la gauche)  
  $`0,00001101_{2} = +1 \times 1,101_{2} \times 2^{-5}`$  (on décale la virgule de 5 rangs vers la droite)



Pour trouver l'écriture flottante en base 2 d'un nombre décimal, il faut donc :

1. Trouver la représentation binaire du nombre (comme dans l'exercice 2)
2. Transformer cette représentation en notation scientifique.

Pour `12,25`, cela donne :

1. $`12,25_{10} = 1100,01_{2}`$
2. $`1100,01_{2} = +1 \times 1,10001_{2} \times 2^{3}`$ .

> **exercice 6 :**
>
> Donnez l'écriture à virgule flottante en base 2 des nombres suivants : 5,375 et 13,625
>
> ```
> 
> 
> 
> 
> ```

### 2. Formats de la norme IEEE-754

Il existe deux formats principaux pour la norme IEEE-754 :

* *simple précision* sur 32 bits
  - 1 bit pour le signe $`s`$ (0 pour positif, 1 pour négatif)
  - 8 bits pour gérer l’exposant $`n`$ (les 8 bits ne représentent pas $`n`$ directement, ils représentent $`n + 127`$)
  - 23 bits pour gérer la mantisse $`m`$ (les 23 bits ne représentent pas $`m`$ directement, ils représentent $`m - 1`$)
  
* *double précision* sur 64 bits (pour les curieux, plus d'infos ici : [https://fr.wikipedia.org/wiki/IEEE_754#Format_double_pr%C3%A9cision_(64_bits)](https://fr.wikipedia.org/wiki/IEEE_754#Format_double_pr%C3%A9cision_(64_bits)))

  

**Exemple :**  
Cherchons quel nombre est représenté par `01000001010001000000000000000000` avec la norme IEEE-754 sur 32 bits :

1. Repérons le bit de signe, les 8 bits de l'exposant et les 23 bits de la mantisse :

| signe (1 bit) | exposant (8 bits) |   mantisse (23 bits)    |
| :-----------: | :---------------: | :---------------------: |
|       0       |     10000010      | 10001000000000000000000 |

2. Cherchons le signe $`s`$ : $`0 \Rightarrow positif`$
3. Cherchons l'exposant $`n`$ :
   * $`10000010_{2} = 2^{7} + 2^{1} = 130`$
   * $`10000010_{2}`$ représente $`n + 127`$ donc $`n = 130 - 127 = 3`$
4. Cherchons la mantisse $`m`$:
   * $`10001000000000000000000_{2} = 2^{-1} + 2^{-5} = 0,53125`$
   * $`10001000000000000000000_{2}`$ représente $`m - 1`$ donc $`m = 0,53125 + 1 = 1,53125`$
5. Il reste à calculer notre nombre final : $`s \times m \times 2^{n} = +1 \times 1,53125 \times 2^{3} = 12,25`$.

> **exercice 7 :**
>
> Déterminez quel nombre est représenté par 10111110001000000000000000000000 avec la norme IEEE-754.
>
> ```
> 
> 
> 
> 
> 
> ```



## IV. Pour aller plus loin : conversions avec Python

S'il vous reste du temps, vous pouvez faire les exercices suivants :

> **exercice 8 :**
>
> Ecrivez une fonction `decimal_en_binaire(x)` qui prend en paramètre la partie décimale d'un nombre en base 10 ($`0 \leq x < 1`$) et qui renvoie une chaîne de caractères correspondant à la représentation binaire de $`x`$.  
> Vous pourrez utiliser la fonction `floor` de la bibliothèque `math` pour récupérer la partie entière.
>
> ```python
> >>> decimal_en_binaire(0.25)
> '01'
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 9 :**
>
> Vérifiez les résultats obtenus à l'exercice 2 avec votre fonction `decimal_en_binaire`.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

> **exercice 10 :**
>
> Ecrivez une fonction `binaire_en_decimal(s)` qui prend en paramètre une chaîne de caractères représentant la partie décimale d'un nombre en base 2 et qui renvoie un flottant correspondant à la représentation décimale de $`s`$.
>
> ```python
> >>> binaire_en_decimal('01')
> 0.25
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

>**exercice 11 :**
>
>Vérifiez les résultats obtenus à l'exercice 3 avec votre fonction `binaire_en_decimal`.
>
>```
>
>
>
>
>
>
>```

> **exercice 12 :**
>
> Ecrivez une fonction de conversion d'un nombre flottant de la base 10 à la base 2 (conversion simple sans la norme IEEE-754). Vous pourrez utiliser votre fonction de l'exercice 8.
>
> ```python
> >>> flottant_10_vers_2(12.25)
> '1100,01'
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
