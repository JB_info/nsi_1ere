# Représentation des données : nombres entiers négatifs



Nous avons vu lors de la séance précédente comment sont représentés les nombres entiers positifs en machine. Nous allons voir aujourd'hui comment sont représentés les nombres entiers négatifs.

L'ensemble des nombres entiers (positifs et négatifs) correspond à l'ensemble des **nombres relatifs**, noté $`\mathbb{Z}`$ en mathématiques. En Python, les entiers relatifs correspondent au type *int*.



## I. Opérations en binaire

Pour trouver la représentation des entiers négatifs, on va avoir besoin d'effectuer quelques opérations de base. Nous allons donc commencer par voir comment fonctionnent les opérations que vous connaissez depuis la primaire (addition, multiplication) sur des entiers écrits en base 2.

### 1. Addition

L'addition de deux nombres binaires fonctionne selon le même principe que l'addition de deux nombres décimaux.

Pour additionner deux nombres en base 10, vous additionnez les chiffres un par un en partant de la droite :

![21 + 46](img/21_46.png)

Lorsque l'addition de deux chiffres est supérieure ou à égale à 10, c'est-à-dire qu'elle s'écrit sur plus d'un chiffre, vous reportez une retenue vers la gauche :

![27+46](img/27_46.png)

L'addition de deux nombres binaires fonctionnent exactement pareil (avec l'addition de chiffres en base 2). On additionne les chiffres en partant de la droite :

![01+10](img/01_10.png)

On peut vérifier que le résultat est correct en convertissant l'addition en base 10 :

* $`01_2 = 1_{10}`$
* $`10_2 = 2_{10}`$
* $`11_2 = 3_{10}`$
* 1 + 2 = 3 : tout va bien !

Lorsque l'addition de deux chiffres binaires est supérieure ou à égale à $`2 = 10_2`$, c'est-à-dire qu'elle s'écrit sur plus d'un chiffre, vous reportez une retenue vers la gauche :

![01+01](img/01_01.png)

À nouveau, on peut vérifier en convertissant l'addition en base 10 :

* $`01_2 = 1_{10}`$
* $`10_2 = 2_{10}`$
* 1 + 1 = 2 : tout va bien !

> **exercice 1 :**
>
> Réalisez les additions binaires suivantes :
>
> * $`1001_2 + 0010_2`$
> * $`10_2 + 11_2`$
> * $`0101101_2 + 0011001_2`$
>
> ```
> 
> 
> 
> 
> 
> 
> ```
>
> Convertissez les nombres en base 10 afin de vérifier vos résultats.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

Vous avez dû remarquer que parfois, le résultat de l'addition de deux nombres binaires s'écrit sur plus de bits que les deux nombres. Par exemple l'addition des deux nombres sur 2 bits `10` et `10` donne un résultat sur 3 bits : `100`.

![10+10](img/10_10.png)

Cela peut parfois poser problème. En effet, les entiers sont généralement tous stockés sur le même nombre de bits (en ajoutant des 0 à gauche pour les nombres plus petits). Les tailles des entiers les plus courantes sont 8, 16, 32 ou 64 bits (c'est-à-dire 1, 2, 4 ou 8 octets).

Si l'addition de deux entiers dépasse la taille du stockage de ces entiers, on obtient un **dépassement de capacité**. Les chiffres "en trop" à gauche ne sont alors pas pris en compte.

Par exemple avec l'addition binaire suivante de 2 nombres sur 8 bits :

![dépassement addition](img/depassement.png)

On obtient un dépassement, le 1 n'est pas pris en compte donc le résultat de l'addition binaire $`10000000_2 + 10101010_2`$ est $`00101010_2`$. On peut vérifier en convertissant en décimal que ce n'est pas correct :

* $`10000000_2 = 128_{10}`$
* $`10101010_2 = 170_{10}`$
* $`00101010_2 = 42_{10}`$
* $`128 + 170 \neq 42`$ : le dépassement de capacité a rendu l'addition incorrecte

Il est donc très **important de prévoir le nombre de bits nécessaires à l'écriture** des entiers et de leur addition.

> **exercice 2 :**
>
> Quel est le plus grand nombre entier (en base 10) que l'on peut écrire sur 1 octet ? sur 2 octets ? sur `n` octets ?
>
> ```
> 
> 
> 
> ```

> **exercice 3 :**
>
> Pour chacune des additions suivantes, donnez le nombre d'octets nécessaires à l'écriture en base 2 des deux entiers ainsi que du résultat de l'addition. Précisez aussi s'il y a ou non dépassement de capacité.
>
> * $`36_{10} + 42_{10}`$
> * $`222_{10} + 101_{10}`$
> * $`65500_{10} + 35_{10}`$
>
> ```
> 
> 
> 
> 
> 
> 
> ```

### 2. Multiplication

La multiplication de deux nombres binaires fonctionne également comme la multiplication de deux nombres décimaux.

![17 x 24](img/17_24.png)

Pour multiplier 17 par 24 :

* on multiplie 17 par 4 :
  * 4 x 7 = 28, on note 8 on retient 2
  * 4 x 1 = 4, plus la retenue 4+2 = 6, on note 6
* on multiplie 17 par 2 :
  * on place un 0 sur une nouvelle ligne
  * 2 x 7 = 14, on note 4 on retient 1
  * 2 x 1 = 2, plus la retenue 2+1 = 3, on note 3
* on réalise l'addition des deux résultats : 68 + 340 = 408

Les multiplications en binaire sont identiques :

![011 x 101](img/011_101.png)

Pour multiplier 011 par 101 :

* on multiplie 011 par 1 :

  * 1 x 1 = 1
  * 1 x 1 = 1
  * 1 x 0 = 0

* on multiplie 011 par 0 :

  * on place un 0 sur une nouvelle ligne
  * 0 x 1 = 0
  * 0 x 1 = 0
  * 0 x 0 = 0

* on multiplie 011 par 1 :

  * on place deux 0 sur une nouvelle ligne

  * 1 x 1 = 1
  * 1 x 1 = 1
  * 1 x 0 = 0

* on réalise l'addition des trois résultats : 011 + 0000 + 01100 = 01111

Comme pour l'addition, on peut vérifier notre résultat en convertissant nos nombres binaires en base 10 :

* $`011_2 = 3_{10}`$
* $`101_2 = 5_{10}`$
* $`01111_2 = 15_{10}`$
* $`3 \times 5 = 15`$ : tout va bien !

> **exercice 4 :**
>
> Réalisez les multiplications binaires suivantes :
>
> * $`1001_2 \times 0010_2`$
> * $`10_2 \times 11_2`$
> * $`100101_2 \times 1101_2`$
>
> ```
> 
> 
> 
> 
> 
> ```
>
> Convertissez les nombres en base 10 afin de vérifier vos résultats.
>
> ```
> 
> 
> 
> 
> 
> ```

Tout comme pour l'addition, la multiplication de deux nombres binaires peut provoquer un dépassement de capacité.

> **exercice 5 :**
>
> Pour chacune des multiplications suivantes, donnez le nombre d'octets nécessaires à l'écriture en base 2 des deux entiers ainsi que du résultat de la multiplication. Précisez aussi s'il y a ou non dépassement de capacité.
>
> * $`11_{10} \times 9_{10}`$
> * $`10_{10} \times 42_{10}`$
> * $`5_{10} \times 13107_{10}`$
>
> ```
> 
> 
> 
> 
> 
> 
> ```



## II. Représentation des entiers négatifs

Maintenant que nous avons les outils pour, regardons comment sont représentés les entiers négatifs en machine.

### 1. Une mauvaise idée

La première idée à laquelle on pourrait penser pour représenter les entiers relatifs serait d'avoir, pour un nombre possédant `n` bits :

* `1` bit pour représenter le signe
* `n-1` bit pour représenter la valeur absolue du nombre

Le bit de signe serait alors le bit de poids fort (le bit tout à gauche). Le bit de poids fort serait à 0 dans le cas d'un nombre positif et à 1 dans le cas d'un nombre négatif.

Par exemple : on représente l'entier `5` sur 8 bits par $`00000101_2`$, du coup `-5` serait représenté par $`10000101_2`$.

Cette technique pose cependant plusieurs problèmes :

* il existe 2 représentations pour zéro : un "zéro positif" ($`00000000_2`$) et un "zéro négatif" ($`10000000_2`$)

* l'addition ne donne pas toujours un résultat correct :

  > **exercice 6 :**
  >
  > Effectuez l'addition de 5 ( $`00000101_2`$) et -5 ($`10000101_2`$). Trouvez-vous 0 ?
  >
  > ```
  > 
  > 
  > 
  > 
  > 
  > ```

Cette technique était donc une mauvaise idée, nous allons devoir utiliser une autre méthode pour représenter les entiers relatifs : le **complément à 2**.

### 2. Le complément à 2

Tout d'abord, il faut décider sur combien de bits on représente les entiers (généralement 8, 16, 32 ou 64). Le bit de poids fort déterminera bien toujours le signe de l'entier : 0 pour un positif et 1 pour un négatif.

Le complément à 2 fonctionne ainsi :

* Si l'entier est positif, on le représente comme vous savez déjà le faire. Il faut cependant toujours conserver un bit de poids fort à 0.

* Si l'entier est négatif, il y deux méthodes pour trouver sa représentation :

  * *Méthode 1* : On calcule le complément directement avec la formule $`2^n - |x|`$, où `n` est la taille de la représentation (par exemple 8 si l'on travaille sur 8 bits) et `|x|` est la valeur absolue du nombre.

    Par exemple avec `-5` sur 8 bits : $`2^n - |x| = 2^8 - |-5| = 256 - 5 = 251`$. Et comme $`251_{10} = 11111011_2`$, on en déduit que $`-5_{10} = 11111011_2`$.

  * *Méthode 2* :

    * on trouve la représentation de la valeur absolue de l'entier
    * on inverse les 0 et les 1 sur la série de bits obtenue : c'est le complément à 1.
    * on ajoute 1 (sans tenir compte du dépassement) pour obtenir la représentation en complément à 2 sur le nombre de bits choisi.

    Par exemple avec `-5` sur 8 bits :

    * $`5_{10} = 00000101_2`$
    * on inverse les bits : $`11111010_2`$
    * on ajoute 1 :  $`11111010_2 + 1_2 = 11111011_2`$

La deuxième méthode est généralement la plus utilisée (plus simple à retenir et à calculer), mais vous pouvez choisir celle que vous préférez.

> **exercice 7 :**
>
> Donnez les représentations binaires en complément à 2 des entiers relatifs suivants sur 8 bits :
>
> * 4
> * 127
> * -4
> * -42
> * -128
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Reprenez l'exercice 7 avec l'autre méthode que celle que vous avez utilisée, puis vérifiez vos résultats.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

Pour convertir du binaire au décimal, facile : ce sont les mêmes méthodes !

Si le bit de poids fort est 0, le nombre est positif, on convertit comme d'habitude. Si le bit de poids fort est 1, le nombre est négatif, on utilise une des deux méthodes.

Par exemple, reprenons $`11111011_2`$ sur 8 bits :

* *Méthode 1* : $`11111011_2 = 251_{10}`$, on calcule $`2^n - |x| = 2^8 - |251| = 256 - 251 = 5`$. Et comme le bit de poids fort était négatif, on en déduit que $`11111011_2 = -5_{10}`$.
* *Méthode 2* :
  * on inverse les bits : $`00000100_2`$
  * on ajoute 1 : $`00000100_2 + 1_2 = 00000101_2`$
  * on convertit : $`00000101_2 = 5_{10}`$, et comme le bit de poids fort était négatif, on retrouve bien -5

> **exercice 8 :**
>
> Donnez les entiers relatifs correspondants à ces représentations binaires en complément à 2 :
>
> * $`01010101_2`$
> * $`01111111_2`$
> * $`11111101_2`$
> * $`11111110_2`$
> * $`10000000_2`$
> * $`10000001_2`$
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Reprenez l'exercice 8 avec l'autre méthode que celle que vous avez utilisée, puis vérifiez vos résultats.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

Le plus petit entier qu'il est possible de représenter sur 8 bits est $`10000000`$ (soit -128). Le plus grand est $`01111111`$ (soit 127). 

Plus généralement, nous pouvons dire que pour une représentation sur `n` bits, il sera possible de représenter les entiers relatifs compris entre $`-2^{n-1}`$ et $`+2^{n-1} - 1`$.

![cercle](img/cercle.png)

> **exercice 9 :**
>
> Quels sont les plus petit et plus grand entiers représentables sur 4 bits ? sur 16 bits ?
>
> ```
> 
> 
> 
> 
> 
> 
> ```

> **exercice 10 :**
>
> Donnez le nombre d'octets nécessaires à la représentation des nombres suivants :
>
> * 42
> * -42
> * 127
> * -127
> * 128
> * -128
>
> ```
> 
> 
> 
> ```

### 3. Pour aller plus loin

> **exercice 11 :**
>
> Avec le complément à 2 sur 8 bits :
>
> - Donnez la représentation de 0.
> - Donnez la représentation de -0.
>
> A-t-on bien résolu le problème des zéros de notre première mauvaise idée de représentation ?
>
> ```
> 
> 
> 
> 
> 
> 
> ```

> **exercice 12 :**
>
> Avec le complément à 2 sur 8 bits :
>
> - Donnez la représentation de 36.
> - Donnez la représentation de -36.
> - Effectuez l'addition binaire de 36 et -36.
>
> A-t-on bien résolu le problème des additions de notre première mauvaise idée de représentation ?
>
> ```
> 
> 
> 
> 
> 
> 
> ```

> **exercice 13 :**
>
> Avec le complément à 2 sur 8 bits :
>
> * Donnez la représentation de 25.
> * Donnez la représentation de -25.
> * Donnez la représentation de -(-25). Que remarquez-vous ?
>
> ```
> 
> 
> 
> 
> 
> 
> ```



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, [eduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g)
