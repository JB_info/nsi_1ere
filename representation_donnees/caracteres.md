# Représentation des données : caractères

Vous savez qu'un ordinateur est uniquement capable de traiter des données binaires. Mais alors comment sont codés les textes ?

## I. Premiers codages

C'est l'**ISO** (organisation internationale de normalisation) qui établit les normes de codage.

### 1. ASCII

La première norme pour coder les caractères informatiques a été créée en 1963 et s’appelle donc **ASCII** (American Standard Code for Information Interchange). Elle associe un nombre sur **7 bits** (de 0 à 127) aux lettres de l’alphabet, aux chiffres, aux symboles mathématiques et de ponctuation.

Voici une table recensant tous les caractères ASCII :

![ascii](img/ascii.png)

Lorsqu'on donne donne un code ASCII, on utilise généralement son codage hexadécimal.

> **exercice 1 :**
> À quel texte correspond les octets codés en ASCII suivants :  
> `4A 27 75 74 69 6C 69 73 65 20 6C 65 20 41 53 43 49 49 2E`

> **exercice 2 :**
> Donnez le code ASCII (en hexadécimal) du texte suivant :  
> `J'aime la NSI !`

> **exercice 3 :**
> Quel problème remarquez-vous si je vous demande le code hexadécimal du texte suivant ?  
> `Ça ne va déjà plus.`


### 2. ISO 8859

Comme constaté dans l'exercice précédent, le ASCII est insuffisant car il manque de nombreux caractères (lettres accentuées par exemple).

Pour y remédier, l'ISO propose la norme **ISO 8859** qui utilise cette fois 8 bits (de 0 à 255 donc). Les caractères de 0 à 127 sont les mêmes que le ASCII (pour assurer la compatibilité). Les caractères de 128 à 255 sont spécifiques (lettres accentuées par exemple).

Cela reste insuffisant pour représenter tous les caractères utilisés, donc la norme définit plusieurs tables notées ISO 8859-*n* avec *n* le numéro de la table. Les caractères de 128 à 255 changent selon la table pour correspondre aux besoins d'une zone géographique.

Voilà les 16 tables définies par la norme :

| Code ISO | Zone                                |
|:----------|:-------------------------------------|
| 8859-1   | europe occidentale                  |
| 8859-2   | europe centrale ou de l’est         |
| 8859-3   | europe du sud                       |
| 8859-4   | europe du nord                      |
| 8859-5   | cyrillique                          |
| 8859-6   | arabe                               |
| 8859-7   | grec                                |
| 8859-8   | hébreu                              |
| 8859-9   | turc et kurde                       |
| 8859-10  | nordique                            |
| 8859-11  | thaï                                |
| 8859-12  | devanāgarī (abandonné)              |
| 8859-13  | balte                               |
| 8859-14  | celtique                            |
| 8859-15  | révision du 8859-1 (ajout de €, œ…) |
| 8859-16  | europe du sud-est                   |

> **exercice 4 :**
> Quelle table utilisera une personne originaire de :
> 1. Lambersart ?
> 2. Athènes ?
> 3. Tunis ?

Cette norme pose notamment 2 problèmes majeurs :

* Il manque encore de nombreux caractères, pour toutes les langues qui n'utilisent pas l'alphabet "latin" (le notre) : le chinois, le japonais, le coréen...
* Si une personne souhaite communiquer avec quelqu'un qui habite dans une zone différente, elle devra à chaque fois modifier la configuration de son logiciel de traitement de texte. Sinon, elle verra des caractères bizarres comme Ã© ou � : ![mauvais encodage](img/mauvais_encodage.png)


## II. Codage universel

### 1. Unicode

Pour éviter ces problèmes, une norme **universelle** voit le jour en 1991 : **Unicode**.

Unicode est une table qui regroupe tous les caractères existants au monde. En 2021 on comptait 144697 caractères.

Chaque caractère est associé à un nombre unique appelé **point de code**. Les points de code 0 à 255 correspondent au code ISO 8859-1 (à nouveau pour assurer la compatibilité).  
On écrit un point de code sous la forme U+*xxxx* ou *xxxx* est exprimé en hexadécimal. Ainsi U+0041 le code du `A` (voir tableau ASCII).

> **exercice 5 :**
> Quels sont les points de code des caractères `n` et `?` ?

Il existe plusieurs formats de codage associés à Unicode : UTF-8, UTF-16, UTF-32. Le plus utilisé est UTF-8 (95% des sites WEB en 2020 sont en UTF-8).

### 2. UTF-8

Pour encoder les caractères Unicode, UTF-8 utilise un nombre variable d'octets : les caractères "classiques" (les plus souvent utilisés) sont codés sur 1 octet, alors que les caractères "moins classiques" sont codés sur un plus grand nombre d'octets (4 maximum).

Le principe de fonctionnement est résumé dans le tableau suivant :

![utf 8](img/utf8.png)

Prenons par exemple le caractère `A` :

1. Son point de code est `U+0041` (trouvé dans la table ASCII).
2. $`41_{16} = 1000001_2`$ (on convertit le point de code en binaire).
3. `U+0041` est compris entre `U+0000` et `U+007F` (première ligne du tableau), donc il s'écrit sur 1 octet.
4. On voit dans le tableau que l'octet s'écrit `0xxxxxxx`, on remplace les *x* par le nombre binaire trouvé au point 2. On trouve donc `010000001`. *Si jamais il y a trop de x par rapport à notre nombre binaire, on ajoute des 0 devant.*

> **exercice 6 :**
> En utilisant la norme UTF-8, donnez le codage du caractère :
> * `b`
> * `€` (point de code U+20AC)


## III. En Python

*Rappel : les caractères et chaînes de caractères Python sont entourées de guillemets "..." ou d'apostrophes '...'.*

### 1. Point de code

Il existe une fonction Python qui prend en paramètre un caractère et renvoie son point de code (en décimal) : **ord**.

> **exercice 7 :**
> Testez la fonction *ord* sur les exemples suivants. Vérifiez le résultat dans la table ASCII.
> ```python
> >>> ord('A')
> ?
> >>> ord('n')
> ?
> >>> ord(' ')
> ?
> ```

Il existe aussi une fonction qui prend en paramètre un point de code (en décimal) et renvoie le caractère correspondant : **chr**.

> **exercice 8 :**
> Testez la fonction *chr* sur les exemples suivants. Vérifiez le résultat dans la table ASCII.
> ```python
> >>> chr(97)
> ?
> >>> chr(35)
> ?
> ```

### 2. Manipulations de chaînes de caractères

* Concaténation (+)

> **exercice 9 :**
> Essayez l'exemple suivant :
> ```python
> >>> str1 = "The "
> >>> str2 = "Walking "
> >>> str3 = "Dead"
> >>> str4 = str1 + str2 + str3
> >>> str4
> ?
> ```

* Longueur (len)

> **exercice 10 :**
> Essayez l'exemple suivant :
> ```python
> >>> len("nsi")
> ?
> >>> chaine = "1 2 3"
> >>> len(chaine)
> ?

* Accès à un caractère

> **exercice 11 :**
> Essayez l'exemple suivant :
> ```python
>>> chaine = "informatique"
> >>> chaine[0]
> ?
> >>> chaine[3]
> ?
> >>> chaine[11]
> ?
> >>> chaine[20]
> ?
> ```

### 3. Quelques algorithmes

Vous n'êtes pas obligés de documenter vos fonctions ni de donner les assertions. Seul le code de la fonction est demandé.

> **exercice 12 :**
> Complétez la fonction suivante, qui prend en paramètre une chaîne de caractères et renvoie le tuple des points de code de tous ses caractères :
> ```python
> def ord_chaine(chaine):
>     resultat = ()
>     for caractere in chaine:
>         point_de_code = ..........
>         resultat = resultat + (point_de_code, )
>     return resultat
> ```
> Testez votre fonction sur une chaîne de votre choix.

> **exercice 13 :**
> Complétez la fonction suivante, qui prend en paramètre un tuple de points de code et renvoie la chaîne correspondante :
> ```python
> def chr_chaine(tuple_de_code):
>     resultat = ""
>     for point_de_code in tuple_de_code:
>         caractere = ..........
>         resultat = resultat + caractere
>     return resultat
> ```

> **exercice 14 :**
> Ecrivez une fonction qui prend en paramètre une chaîne et renvoie True si la chaîne contient un "e", False sinon.

> **exercice 15 :**
> Ecrivez une fonction qui prend en paramètre une chaîne et renvoie le nombre d'espaces de la chaîne.

> **exercice 16 :**
> Ecrivez une fonction qui prend en paramètre une chaîne et renvoie le nombre de "a" (majuscules + minuscules) de la chaine.

> **exercice 17 :**
> Ecrivez une fonction qui prend en paramètre une chaîne et la renvoie dans le sens inverse.

> **exercice 18 :**
> Ecrivez une fonction qui prend en paramètre une chaîne et renvoie True si la chaîne est un palindrome, False sinon.

> **exercice 19 :**
> Ecrivez une fonction qui prend en paramètre une chaîne et renvoie True si la chaîne contient un "l" suivi d'un "a", False sinon.

> **exercice 20 :**
> Ecrivez une fonction qui prend en paramètre une chaîne `s` et renvoie deux éléments :
> * une chaine contenant les trois premiers caractères de `s`
> * une chaine contenant le reste des caractères de `s`

> **exercice 21 :**
> Ecrivez une fonction qui prend en paramètre une chaîne `s` et renvoie une autre chaîne contenant 1 caractère sur 2 de `s`.

> **Pour aller plus loin :**
> * Il existe une [vidéo](https://www.youtube.com/watch?v=o8XvMlCoOac) qui fait défiler tous les caractères Unicode. Regardez combien de temps elle dure !
> * Allez sur France IOI et entraînez-vous sur le niveau 2, section 3 (Chaînes de caractères).
> * Documentez vos fonctions 12 à 21 et ajoutez les assertions.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, [Wikipedia](https://fr.wikipedia.org/wiki/Fichier:ASCII-Table.svg)
