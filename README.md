# COURS DE PREMIÈRE NSI

![logo NSI](nsi_logo.png)

Le programme : [BO](https://cache.media.eduscol.education.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)

Progression de l'année :
1. [Histoire](histoire/histoire.md)
2. [Découverte de Python](programmation/decouverte.md)
3. [Types de base, variables, affectation](programmation/affectation.md)
4. [Instructions conditionnelles](programmation/conditionnelles.md)
5. [Tuples](types_construits/tuples.md)
6. [Boucles bornées et non bornées](programmation/boucles.md)
7. [Représentation en machine des entiers positifs](representation_donnees/positifs.md)
8. [Représentation en machine des entiers relatifs](representation_donnees/negatifs.md)
9. [Architecture](architectures_materielles/architecture_sequentielle.md)
10. [Fonctions](programmation/fonctions.md)
11. Projet 1 : [Jeu de dés](projets/jeu_de_des.md)
12. [Représentation en machine des flottants](representation_donnees/flottants.md)
13. [Représentation en machine des caractères](representation_donnees/caracteres.md)
14. [IHM](architectures_materielles/ihm.md)
15. [Tableaux](types_construits/tableaux.md)
16. [Dictionnaires](types_construits/dictionnaires.md)
17. [Systèmes d'exploitation](architectures_materielles/os.md)
18. Projet 2 : [Filtres pour images](projets/filtres_images.md) - [Puissance 4](projets/puissance4.md)
19. [Réseau](architectures_materielles/reseau.md)
20. [Tests](programmation/tests.md)
21. [Algorithmes sur les tableaux](algorithmique/tableaux.md)
22. [Dichotomie](algorithmique/dichotomie.md)
23. Tris : [par sélection](algorithmique/tris/selection.md) puis [par insertion](algorithmique/tris/insertion.md) (puis éventuellement [comparaison des 2](algorithmique/tris/comparaison.md))
24. Projet 3 : [Pendu](projets/pendu.md)
25. [DM : Découverte d'autres langages + rappels HTML](programmation/diversite_langages.md)
26. Traitement de données en tables : [Le format CSV](donnees_en_tables/csv_tableurs.md) puis [Traitement en Python](donnees_en_tables/traitement_python.md)
27. [Algorithmes gloutons](algorithmique/gloutons.md)
28. K plus proches voisins : [kNN](algorithmique/knn.md)
29. Principe du Web : [Cours](web/http.md) puis [Création de formulaires](web/formulaires.md) puis [Créer son serveur web](web/serveur.md)
30. [Evènements sur le Web](web/evenements.md)
31. Projet de fin d'année : [Morpion sur le Web](projets/morpion_web.md)

Projets supplémentaires : [Jeu de NIM sur micro:bit](projets/nim.md) ou [Triche au scrabble](projets/scrabble.md) ou [Bataille navale](projets/touche_coule.md) ou [Morpion](projets/morpion.md)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*

