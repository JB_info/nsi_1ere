# Découverte : HTML & autres langages de programmation

## I. Rappels sur une page Web

Une page Web se construit avec plusieurs langages :
* **HTML pour le contenu**
* **CSS pour le style**
* **JavaScript pour l'interactivité**

Le **HTML** (HyperText Langage Markup) est un langage de description qui permet de construire le contenu d'une page web. Sa dernière version, HTML5, date de 2007 mais les premières traces de ce langage remonte à la création du Web par Tim Berners-Lee en 1989.

Si vous ne vous souvenez plus de vos cours de SNT, vous pourrez par exemple aller voir [le site du MDN](https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web/HTML_basics) qui résume comment se construit le code HTML d'une page Web.

Le **CSS** (Cascading Style Sheets) est un langage de description permettant de modifier le style, l'apparence d'une page Web. La première version, CSS1, date de décembre 1996.

Si vous ne vous souvenez plus de vos cours de SNT, vous pourrez par exemple aller voir [le site du MDN](https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web/CSS_basics) qui résume comment se construit le code CSS d'une page Web.

Le **JavaScript** (ou "JS") est un langage de programmation qui ajoute de l'interactivité à votre site web (par exemple : jeux, réponses quand on clique sur un bouton ou des données entrées dans des formulaires, composition dynamique, animations). Il a été créé en 1995 par Brendan Eich. Nous verrons comment on l'utilise en toute fin d'année.

Il est possible de consulter les codes HTML et CSS d'une page Web directement depuis un navigateur.  
Depuis Firefox par exemple, cliquez droit puis "Inspecter" :
* le code HTML est dans l'onglet "Inspecteur"
* le code CSS est dans l'onglet "Éditeur de style"

Pour créer soi-même une page Web, on peut utiliser n'importe quel éditeur de texte :
* [Notepad++](https://notepad-plus-plus.org/)
* [Geany](https://www.geany.org/)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Atom](https://atom.io/)
* [Sublime Text](https://www.sublimetext.com/)
* ...

*Remarque : ne codez jamais directement dans le navigateur, car ça n'enregistre pas automatiquement votre travail !*


## II. Découverte d'autres langages

Vous connaissez donc les trois langages du Web, et vous êtes (presque) des experts du langage Python. Mais il en existe bien d'autres !

Il n’existe pas de langage de programmation « universel ». En effet, si l’on peut, en général, tout programmer avec n’importe quel langage, certains langages seront plus adaptés à la programmation de telle ou telle tâche. Le langage Python, par exemple, est très utilisé pour la mise en place rapide de programmes. Cependant, dès lors que l’on a besoin de performances plus élevées, ce n’est plus le langage le plus adapté.

Voici quelques autres langages qui ont marqué l'histoire de l'informatique, en plus de Python (1991) :
* Fortran (1954)
* Algol (1958)
* Basic (1964)
* Lisp (1958)
* Prolog (1972)
* Cobol (1959)
* Pascal (1970)
* C (1972)
* C++ (1985)
* C# (2001)
* Perl (1987)
* JavaScript (1995)
* PHP (1994)
* Ruby (1995)
* Ada (1980)
* Java (1995)
* Haskell (1990)
* OCaml (1996)

Il existe même un langage qui sert à convertir un algorithme dans le langage de programmation souhaité, [Flowgorithm](http://www.flowgorithm.org/) !

## III. Devoir Maison

L'objectif du DM est de **créer une page Web qui donne des informations sur un langage de programmation**.

Le code **HTML doit obligatoirement contenir** minimum :
* un titre
* un paragraphe contenant au moins un mot en gras et un autre en italique
* un lien
* une image
* une liste
* un tableau

C'est **à vous de choisir le langage** de programmation dont vous souhaitez parler, parmi ceux de la liste plus haut (⚠️ pas Python ni HTML ni CSS).  
Vous pouvez y mettre toutes les informations que vous jugez utile, l'objectif étant de réaliser un résumé clair du langage (histoire, utilisation, syntaxe, avantages/inconvénients, comparaison avec Python, etc.).

Vous serez évalués :
* /10 sur le code HTML en lui-même ;
* /10 sur les informations fournies concernant le langage ;
* le CSS est en bonus (plus c'est joli plus vous aurez de points ☺).

Le DM est à déposer via l'application *Casier* sur l'ENT pour le **vendredi 29 avril**.

À vos marques, prêts, codez !

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

