# Constructions élémentaires : fonctions



Lors des séances précédentes, nous avons eu l'occasion d'utiliser quelques fonctions (*len*, *type*...). Nous allons voir aujourd'hui ce qu'est une fonction, comment on les utilise et comment on les crée.



## I. Fonctions natives de Python

Nous allons commencer par étudier plus en détail comment marchent les fonctions natives de Python.

### 1. Principe

Avant de regarder comment on utilise une fonction, il faut déjà savoir ce qu'est une fonction.

Une fonction est une construction qui prend en entrée des données, réalise un **algorithme** et renvoie en sortie d'autres données.

![fonction schéma](img/fonction.png)

Le mot "algorithme" vient du nom d'un mathématicien du IXème siècle, [Al-Khwârizmî](https://fr.wikipedia.org/wiki/Al-Khw%C3%A2rizm%C3%AE). Un algorithme n'est rien de plus qu'une séquence d'instructions. Tous les petits programmes que l'on a écrit étaient donc des algorithmes.

Les fonctions en informatique sont très similaires aux fonctions en mathématiques. Par exemple la fonction mathématique `f(x) = 2x + 3` correspond en informatique à la fonction qui :

* prend en entrée une donnée : *x*
* réalise un algorithme : *multiplier x par 2 puis ajouter 3*
* renvoie en sortie une donnée : *le résultat de l'opération 2x + 3*

> **exercice 1 :**
>
> À quelles fonctions en informatique correspondent les fonctions mathématiques suivantes ?
>
> * $`f(x) = \frac{x - 7}{x + 1}`$
> * $`f(x, y) = x^2 - y^2`$
>
> ```
> 
> 
> 
> 
> 
> 
> ```

Les données en entrée d'une fonction s'appellent les **paramètres** de la fonction (ou *arguments*). Les données en sortie s'appelle le **résultat** de la fonction. On dit qu'une fonction **renvoie** un résultat.

Une fonction peut posséder zéro, un ou plusieurs paramètres. Par contre, elle ne peut renvoyer qu'un seul résultat.

Revenons par exemple sur la fonction native de Python *type*. Une fonction native d'un langage de programmation est une fonction qui existe déjà, qui est fournie par le langage et que l'on peut donc utiliser. La fonction *type* prend en *paramètre* une valeur quelconque et *renvoie* le type de cette valeur.

> **exercice 2 :**
>
> Donnez le(s) paramètre(s) et le résultat des fonctions natives de Python suivantes :
>
> * str
> * int
> * float
>
> ```
> 
> 
> 
> ```



### 2. Appel de fonction

Pour utiliser les fonctions en Python, on effectue un **appel** de fonction. Un appel se réalise en écrivant le nom de la fonction suivi entre parenthèses de la(des) valeur(s) sur laquelle(lesquelles) on veut exécuter la fonction. *(c'est ce que vous faîtes depuis la première séance)*

Un appel de fonction renvoie toujours une valeur.

Un appel de fonction se déroule donc ainsi :

* le(s) paramètre(s) de la fonction est(sont) la(les) valeur(s) indiquée(s) entre parenthèses
* l'algorithme de la fonction est exécuté
* le résultat de l'appel est la valeur renvoyée par la fonction

Par exemple, pour connaître le type de la valeur `2`, il faut réaliser l'appel suivant :

```python
>>> type(2)
<class 'int'>
```

L'appel se déroule ainsi :

* le paramètre de la fonction est la valeur `2`
* l'algorithme de la fonction est exécuté
* le résultat de l'appel est la valeur `<class 'int'>`

La valeur renvoyée par la fonction peut toujours être réutilisée. On peut par exemple l'affecter à une variable, l'utiliser dans une opération...

```python
>>> cinq = int("5")
>>> cinq
5
>>> int("5") + int("7")
12
```

Une fonction peut posséder zéro, un ou plusieurs paramètres. Voici quelques exemples de fonctions natives de Python :

* qui acceptent 0 paramètre :

  > **exercice 3 :**
  >
  > La fonction native *tuple* peut être appelée avec zéro paramètre. Que renvoie l'appel suivant ?
  >
  > ```python
  > >>> tuple()
  > ?
  > ```

* qui acceptent 1 paramètre :

  > **exercice 4 :**
  >
  > On a déjà eu l'occasion d'utiliser la fonction *len* sur les tuples. Cette fonction renvoie la taille de la valeur passée en paramètre. En réalité, la fonction *len* ne s'utilise pas que sur les tuples mais sur n'importe quel itérable. On peut donc appeler *len* sur un tuple, mais aussi sur une chaîne de caractère, sur un intervalle...
  >
  > Appelez la fonction *len* pour trouver la taille des valeurs suivantes :
  >
  > * (0, 1, 2, 3)
  > * "nsi"
  > * "informatique"
  > * ""
  >
  > ```python
  > 
  > 
  > 
  > 
  > 
  > 
  > ```

* qui acceptent 2 paramètres :

  > **exercice 5 :**
  >
  > La fonction *pow* permet de calculer un nombre `p` à la puissance `n ` : $`p^n`$. Elle prend deux paramètres : le nombre `p` puis le nombre `n`. Lorsqu'on appelle une fonction avec plusieurs paramètres, on les sépare par des virgules. **L'ordre des paramètres est crucial.**
  >
  > Calculez à la main puis vérifiez avec Python le résultat des appels suivants :
  >
  > ```python
  > >>> pow(2, 3)
  > ?
  > >>> pow(10, 2)
  > ?
  > >>> pow(2, 10)
  > ?
  > >>> pow(1, 8)
  > ?
  > >>> pow(8, 1)
  > ?
  > ```

* qui acceptent un nombre variable de paramètres :

  * > **exercice 6 :**
    >
    > Vous connaissez déjà une fonction qui prend un nombre variable de paramètres : la fonction *range* qui sert à créer des intervalles.
    >
    > Combien de paramètres peut prendre la fonction range ?
    >
    > ```
    > 
    > 
    > ```

  * > **exercice 7 :**
    >
    > Il existe aussi des fonctions qui peuvent prendre un nombre illimité de paramètres. Par exemple, la fonction *min* renvoie le minimum de tous les nombres passés en paramètres. Exécutez par exemple les appels suivants :
    >
    > ```python
    > >>> min(1, 2)
    > ?
    > >>> min(4, 8, 3)
    > ?
    > >>> min(4, 8, 2, 27, 18)
    > ?
    > >>> min(4.2, 3.6, 1.1, 26.9, 2.5, 13.1, 25.8, 12.6, 18.5)
    > ?
    > ```
    >
    > La fonction *min* peut aussi être appelée avec un seul paramètre : un itérable. Elle renvoie alors le minimum des valeurs de l'itérable. Exécutez par exemple les appels suivants :
    >
    > ```python
    > >>> min((7, 2, 11, 36, 5))
    > ?
    > >>> min((1, -4, 2.3))
    > ?
    > ```

Il est aussi possible de combiner plusieurs appels de fonction.

> **exercice 8 :**
>
> Essayez de deviner puis vérifiez avec Python le résultat des appels suivants :
>
> ```python
> >>> len(tuple())
> ?
> >>> len(str(42))
> ?
> >>> min(int("3"), int("7"))
> ?
> >>> min(len("informatique"), len("python"))
> ?
> >>> min(pow(2, int("3")), len("nsi"))
> ?
> >>> len(str(3 * min(int("42"), 5)))
> ?
> >>> min(range(2, 10, 3))
> ?
> ```

Il existe aussi en Python une fonction, *help*, qui permet de consulter la **documentation** d'une fonction. La documentation est une petite description de ce que fait la fonction et comment l'utiliser.

> **exercice 9 :**
>
> Regardez par exemple la documentation de la fonction *len*. Quelles informations trouvez vous ?
>
> ```python
> >>> help(len)
> ?
> ```
>
> ```
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Regardez la documentation de la fonction *max*.
>
> ```
> 
> 
> 
> ```
>
> Calculez à la main la valeur de chacune des variables du programme suivant :
>
> ```python
> res1 = len('Quelques mots')
> res2 = len("Autre exemple !")
> res3 = len("-7.2")
> res4 = len('21')
> res5 = str(21.1)
> res6 = len(res5)
> res7 = max(12, 8, 12.3, 4)
> res8 = min(12, 8, 12.3, 4)
> res9 = max(len("compare"), len("  compare"), len(" compare"))
> resA = min(len("9"), len("1.1"), len("10"), len("-3"))
> resB = max(len("9"), len("len('1.1')"), len("11"))
> resC = min(res9, resA, resB)
> resD = max(res9, resA, resB)
> ```
>
> Vous pouvez vérifier vos réponses en exécutant le programme et en regardant la valeur des variables dans le shell.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```



## II. Définition de fonctions

### 1. Syntaxe

Vous savez désormais utiliser les fonctions natives de Python, mais bien souvent nous avons besoin de fonctions qui n'existent pas encore : il faut alors les créer soi-même. Créer une fonction s'appelle **définir une fonction**.

Reprenons un de nos tous premiers algorithmes :

```python
nombre1 = 4
nombre2 = 7
if nombre1 < nombre2:
    resultat = "inferieur"
elif nombre1 == nombre2:
    resultat = "egal"
else:
    resultat = "superieur"
```

Souvenez-vous, pour connaître le résultat de cet algorithme, il fallait enregistrer le programme, l'exécuter puis regarder la valeur de la variable `resultat` dans le shell. Et si on voulait connaître le résultat de l'algorithme avec des valeurs différentes, il fallait tout recommencer... pas très pratique ! Définir une fonction pour cet algorithme va nous permettre de simplifier tout ça : 

* les données en entrée (paramètres) de notre fonction sont `nombre1` et `nombre2`
* l'algorithme de la fonction est notre instruction conditionnelle, on ne change rien
* les données en sortie (résultat) de notre fonction est la variable `resultat`

En Python, la syntaxe pour définir notre fonction va donner ceci :

```python
def comparaison_nombres(nombre1, nombre2):
    if nombre1 < nombre2:
        resultat = "inferieur"
    elif nombre1 == nombre2:
        resultat = "egal"
    else:
        resultat = "superieur"
    return resultat
```

Il y a plusieurs chose à remarquer :

* le mot-clef pour définir une fonction est **def**
* *def* est suivi du nom de la fonction à définir (ici `comparaison_nombres`), le nom d'une fonction doit respecter les mêmes règles que pour les identificateurs de variables
* le nom de la fonction est suivi d'une paire de parenthèses qui contient les noms des paramètres (s'il n'y a pas de paramètres, on laisse les parenthèses vides)
* la première ligne de la définition de la fonction termine par le signe deux-points **:**
* l'algorithme de la fonction est repéré par une **indentation** supplémentaire, c'est l'indentation qui marque le début/la fin de la fonction
* le mot-clef **return** permet de renvoyer le résultat de la fonction (ici la variable `resultat`)

```python
def nom_de_la_fonction(parametre1, parametre2, ...):
  	algorithme de la fonction
	return resultat_a_renvoyer
```

Une fois la fonction définie, il suffit d'exécuter le programme une seule fois et on peut l'utiliser facilement dans le shell via des appels comme pour les fonctions natives de Python.

Par exemple pour tester l'algorithme avec les nombres 4 et 7, on réalise l'appel suivant :

```python
>>> comparaison_nombres(4, 7)
"inferieur"
```

Les paramètres de la fonction sont 4 et 7, l'algorithme de notre fonction est donc exécuté avec `nombre1` qui vaut 4 et `nombre2` qui vaut 7. À la fin de l'algorithme de la fonction, la variable `resultat` vaut "inferieur". Cette valeur est renvoyée dans la fonction avec le *return* donc on la récupère lors de notre appel.

> **exercice 10 :**
>
> Enregistrez la fonction `comparaison_nombres` dans un programme et exécutez le. Appelez ensuite la fonction avec les valeurs suivantes :
>
> * 3 et 11
> * 11 et 3
> * 42 et 42
>
> ```
> 
> 
> 
> ```

> **exercice 11 :**
>
> Complétez la définition de la fonction suivante, qui renvoie le carré du nombre passé en paramètre :
>
> ```python
> def carre(???):
>     resultat = ???
>     return ???
> ```
>
> Testez votre fonction avec les nombres 1, 2, 5.0 et -10.
>
> ```
> 
> 
> 
> 
> ```

> **exercice 12 :**
>
> Définissez une fonction `addition` qui renvoie la somme de ses trois paramètres.
>
> ```python
> 
> 
> 
> ```
>
> Utilisez votre fonction pour trouvez les résultats des additions suivantes :
>
> * 4 + 9 + 1
> * 2 + (-8) + 3
> * -4 + 11 + (-1)
> * -7 + 7 + 0
>
> ```
> 
> 
> 
> 
> ```

Il est tout à fait possible qu'une fonction n'ait aucun paramètre. Par exemple la fonction suivante n'a aucun paramètre et renvoie la chaîne de caractères vide :

```python
def chaine_vide():
    resultat = ""
    return resultat
```

On peut même l'écrire sans variable intermédiaire, directement comme ceci :

```python
def chaine_vide():
    return ""
```

On l'appelle de la même manière que les fonctions natives de Python qui ne prennent aucun paramètre :

```python
>>> chaine_vide()
""
```

> **exercice 13 :**
>
> Définissez une fonction qui renvoie le tuple vide, puis appelez votre fonction.
>
> ```python
> 
> 
> 
> 
> ```

Il est possible que dans l'algorithme d'une fonction, on ait besoin d'appeler une autre fonction.

Par exemple pour définir la fonction qui renvoie le tuple vide de l'exercice 13, on pourrait faire appel à la fonction native de Python *tuple* :

```python
def tuple_vide():
    resultat = tuple()
    return resultat
```

> **exercice 14 :**
>
> Reprenez l'exercice 11 en faisant appel à la fonction native de Python *pow*.
>
> ```
> 
> 
> 
> ```

Il peut arriver d'avoir besoin de renvoyer plusieurs résultats. Mais une fonction renvoie toujours **un seul résultat**. Pour renvoyer plusieurs valeurs, on va alors renvoyer un tuple qui contient toutes les valeurs à renvoyer.

Par exemple, on souhaite définir une fonction qui prend en paramètre un entier `n` et renvoie tous les nombres de 0 à n inclus. On va donc mettre tous ces nombres dans un tuple :

```python
def nombres_0_a_n(n):
    resultat = ()
    for entier in range(n+1):
        resultat = resultat + (entier, )
    return resultat
```

> **exercice 15 :**
>
> Définissez une fonction `calculs` qui prend en paramètre deux entiers et renvoie les résultats des addition, soustraction, multiplication et division de ces deux entiers.
>
> ```python
> 
> 
> 
> 
> ```
>
> Testez votre fonction sur les exemples suivants :
>
> ```python
> >>> calculs(6, 2)
> (8, 4, 12, 3)
> >>> calculs(36, 6)
> (42, 30, 216, 6)
> >>> calculs(0, 1)
> (1, -1, 0, 0)
> ```

Il peut aussi arriver de n'avoir besoin de renvoyer aucun résultat. Il y a alors deux possibilités :

* `return None` : *None* symbolise l'absence de valeur, on peut donc renvoyer None si on n'a aucune valeur à renvoyer
* ne pas mettre de `return` : par défaut, si on ne met pas de *return* dans une fonction, c'est None qui est renvoyé

Il existe aussi des fonctions particulières appelées **prédicats**. Un prédicat est une fonction qui renvoie un booléen (True ou False).

Par exemple, la fonction suivante est un prédicat qui teste si le nombre passé en paramètre est nul :

```python
def est_nul(nombre):
    if nombre == 0:
		resultat = True
	else:
		resultat = False
	return resultat
```

> **exercice 16 :**
>
> Définissez un prédicat qui indique si un nombre passé en paramètre est pair.
>
> ```python
> 
> 
> 
> 
> 
> 
> ```

### 2. Portée des variables

Les variables définies à l’**intérieur** d’une fonction sont dites **locales**. Cela veut dire qu’elles n’existent qu’à l’intérieur de la fonction. On ne peut pas consulter leurs valeurs à l'extérieur de la fonction.

> **exercice 17 :**
>
> Exécutez le programme suivant :
>
> ```python
> def fonction_test():
>     variable_test = 2
>     return variable_test
> ```
>
> Dans le shell, exécutez ensuite les instructions suivantes :
>
> ```python
> >>> fonction_test()
> ?
> >>> variable_test
> ?
> ```
>
> Comment expliquez vous ce résultat ?
>
> ```
> 
> 
> ```

Les variables définies à l’**extérieur** d’une fonction sont dites **globales**. Elle sont visibles dans une fonction mais ne peuvent pas être modifiées par la fonction pour éviter qu’une fonction ne modifie par erreur une variable.

> **exercice 18 :**
>
> Exécutez le programme suivant :
>
> ```python
> variable_globale = 1
> 
> def test_changement_valeur():
>     variable_globale = 2
>     return variable_globale
> ```
>
> Dans le shell, exécutez ensuite les instructions suivantes :
>
> ```python
> >>> variable_globale
> ?
> >>> test_changement_valeur()
> ?
> >>> variable_globale
> ?
> ```
>
> Comment expliquez vous ce résultat ?
>
> ```
> 
> 
> ```

Donner la **portée** d'une variable consiste à dire où elle est définie, c'est à dire si elle est globale au programme ou locale à une fonction.



### 3. Pour aller plus loin

Si vous avez terminé, vous pouvez faire les exercices suivants.

> **exercice 19 :**
>
> Définissez une fonction qui renvoie le cube d'un nombre passé en paramètre.
>
> ```
> 
> 
> 
> ```
>
> Testez votre fonction avec 1, 2, -5 et 11.36.
>
> ```
> 
> 
> 
> ```
>
> *(vous devez trouver 1, 8, -125 et 1466.0034..)*

> **exercice 20 :**
>
> Définissez une fonction qui renvoie un tuple contenant les résultats de toutes les tables de multiplication de 1 à 9 : les résultats de 1 fois tous les nombres de 1 à 9, puis les résultats de 2 fois tous les nombres de 1 à 9, etc.
>
> ```
> 
> 
> 
> 
> ```

> **exercice 21 :**
>
> Définissez une fonction qui renvoie le nombre d'espaces d'une chaîne de caractères passée en paramètre.
>
> ```
> 
> 
> 
> 
> ```

> **exercice 22 :**
>
> Définissez une fonction `mon_len` qui renvoie la taille de l'itérable (chaine ou tuple) passé en paramètre.
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 23 :**
>
> Définissez une fonction qui prend en paramètre un entier `n` et permet de déterminer à partir de quel entier le produit des premiers entiers `1 x 2 x 3 x ...` dépasse `n`. Votre fonction renverra l'entier ainsi que le produit atteint.
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 24 :**
>
> Définissez une fonction qui renvoie la valeur absolue d'un nombre passé en paramètre.
>
> Vous écrirez deux versions de votre fonction :
>
> * la première qui utilise la fonction native de Python *abs*
> * la seconde avec une instruction conditionnelle
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 25 :**
>
> Définissez une fonction qui renvoie le nombre médian des trois entiers distincts passés en paramètres. Le nombre médian est le nombre "du milieu" : par exemple avec 36, 4 et 11, le nombre médian est 11.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 26 :**
>
> Définissez un prédicat qui indique si un nombre passé en paramètre est premier.
>
> *Rappel : Un entier `p` est dit premier s'il est supérieur ou égal à 2 et si aucun des entiers compris entre 2 et `p - 1` inclus ne divise `p` .*
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 27 :**
>
> Définissez un prédicat qui vérifie qu'un tuple correspond bien à de l'ADN, c'est-à-dire qu'il ne contient aucune autre chaîne de caractères que les quatre bases A, C, G et T.
>
> Par exemple ('A', 'C', 'T', 'A', 'G', 'A', 'A', 'T') est valide, par contre ('T', 'A', 'G', 'U', 'C') ne l'est pas.
>
> ```
> 
> 
> 
> 
> 
> 
> ```



## III. Spécification

Une fonction permet d’organiser un programme.  Elle est une suite d’instructions qui sert à faire des actions précises. Le but des fonctions est de simplifier un programme pour le rendre plus lisible, pour ne pas avoir à retaper le même code plusieurs fois dans un même programme, pour éviter les erreurs. Avant de commencer à écrire l'algorithme d'une fonction, il faut toujours réfléchir sur sa **spécification**.

La spécification d'une fonction décrit ce que fait la fonction et dans quelles conditions elle le fait. Une spécification contient :

- la description de l’objectif, de ce que fait la fonction
- la description des attendus sur les paramètres (types et contraintes particulières)
- la description de la valeur renvoyée (type et particularités)

Reprenons par exemple notre toute première fonction :

```python
def comparaison_nombres(nombre1, nombre2):
    if nombre1 < nombre2:
        resultat = "inferieur"
    elif nombre1 == nombre2:
        resultat = "egal"
    else:
        resultat = "superieur"
    return resultat
```

Voici un exemple de spécification pour cette fonction :

```
La fonction `comparaison_nombres` permet de comparer deux nombres et d'indiquer si le premier nombre est inférieur, égal ou supérieur au second.
La fonction prend toujours deux paramètres, tous deux des nombres entiers, relatifs ou réels (type int ou float) qui doivent être comparables entre eux.
La fonction renvoie une chaîne de caractère qui représente le résultat de la comparaison.
La fonction peut renvoyer un résultat incorrect si les deux nombres passés en paramètres ne sont pas comparables avec l'opérateur ==, comme deux flottants par exemple.
```

Dans tout projet en informatique, on commence d'abord par réfléchir sur la spécification d'une fonction, et après seulement on peut commencer à écrire son algorithme.

Pour pouvoir utiliser une fonction, l'utilisateur a besoin de connaître un bref résumé de la spécification de cette fonction. Chaque fonction doit donc posséder une **documentation**.

Comme vu plus haut, la documentation des fonctions natives de Python peut être consultée grâce à la fonction *help*. Nous allons maintenant apprendre à écrire notre propre documentation pour nos fonctions.

Voilà la fonction `comparaison_nombre` documentée :

```python
def comparaison_nombres(nombre1, nombre2):
    '''
    Indique si le premier nombre est inférieur, égal ou supérieur au second.
    
    :param nombre1: (int ou float) le premier nombre
    :param nombre2: (int ou float) le second nombre
    :return: (str) une chaîne qui représente le résultat de la comparaison
    '''
    if nombre1 < nombre2:
        resultat = "inferieur"
    elif nombre1 == nombre2:
        resultat = "egal"
    else:
        resultat = "superieur"
    return resultat
```

Pour documenter une fonction, on insère une **docstring** entre la première ligne de la fonction et son algorithme. Une docstring s'écrit ainsi :

* elle est encadrée par trois apostrophes **'''** (ou bien par trois guillemets **"""**)
* la première ligne donne une brève description de ce que fait la fonction
* pour chaque paramètre, on crée une ligne qui contient **:param nom_du_parametre: (type du paramètre)** suivi d'une brève description du paramètre
* pour le résultat on crée une ligne qui contient **:return: (type du résultat)** suivi d'une brève description du résultat

Cela donne :

```python
def nom_de_la_fonction(parametre1, parametre2, ...):
	'''
	Brève description de ce que fait la fonction.
	
	:param parametre1: (type de parametre1) brève description de parametre1
	:param parametre2: (type de parametre2) brève description de parametre2
	... idem avec les autres paramètres ...
	:return: (type du résultat) brève description du résultat
	'''
  	algorithme de la fonction
	return resultat_a_renvoyer
```

On peut ensuite consulter notre documentation comme pour les fonctions natives, avec *help* :

````python
>>> help(comparaison_nombres)
Help on function comparaison_nombres in module __main__:

comparaison_nombres(nombre1, nombre2)
    Indique si le premier nombre est inférieur, égal ou supérieur au second.
    
    :param nombre1: (int ou float) le premier nombre
    :param nombre2: (int ou float) le second nombre
    :return: (str) une chaîne qui représente le résultat de la comparaison
````

> **exercice 28 :**
>
> Documentez votre fonction de l'exercice 12.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

Quelque fois, les paramètres peuvent avoir des contraintes particulières. On les appelle des **préconditions** Regardez par exemple la fonction suivante :

```python
def division_entiere(dividende, diviseur):
    '''
    Calcule le quotient de la divison du dividende par le diviseur.
    
    :param dividende: (int) le dividende
    :param diviseur: (int) le diviseur
    :return: (int) le quotient de la division
    '''
    quotient = dividende // diviseur
    return quotient
```

Cette fonction ne peut pas être appelée avec un diviseur égal à 0, sinon elle déclenche une *ZeroDivisionError*.

```python
>>> division_entiere(4, 0)
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "C:\Users\Justine\Desktop\test.py", line 68, in division_entiere
    quotient = dividende // diviseur
ZeroDivisionError: integer division or modulo by zero
```

Il faut donc ajouter à notre fonction une ligne pour s'assurer que l'utilisateur n'appelle pas notre fonction avec le diviseur 0, sinon tout notre programme peut planter !

Pour cela, on utilise une **assertion**. Pour notre exemple, cela s'écrit ainsi :

```python
def division_entiere(dividende, diviseur):
    '''
    Calcule le quotient de la divison du dividende par le diviseur.
    
    :param dividende: (int) le dividende
    :param diviseur: (int) le diviseur
    :return: (int) le quotient de la division
    '''
    assert diviseur != 0, "le diviseur ne doit pas être égal à 0"
    
    quotient = dividende // diviseur
    return quotient
```

L'assertion est placée avant l'algorithme de la fonction. Elle s'écrit ainsi :

* le mot-clef pour l'assertion est **assert**
* *assert* est suivi d'une expression booléenne (ici `diviseur != 0`)
* l'assertion est suivie d'une virgule **,** et d'une chaîne de caractères qui décrit la condition que le paramètre doit vérifier

L'expression booléenne est évaluée, et :

* si elle est vraie, tout va bien : l'algorithme de la fonction est exécuté
* si elle est fausse, la condition n'est pas respectée : une erreur est déclenchée

Retestons l'appel avec le diviseur 0 :

```python
>>> division_entiere(4, 0)
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
  File "C:\Users\Justine\Desktop\test.py", line 68, in division_entiere
    assert diviseur != 0, "le diviseur ne doit pas être égal à 0"
AssertionError: le diviseur ne doit pas être égal à 0
```

> **exercice 29 :**
>
> Documentez et créez l'(les) assertion(s) nécessaire(s) pour votre fonction de l'exercice 15.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> ```

Une fonction peut aussi posséder des contraintes particulières sur son résultat. On les appelle des **postconditions**. Par exemple, la fonction native *len* calcule la longueur d'un itérable. Une postcondition pour cette fonction serait donc que le résultat est toujours positif ou nul (une longueur ne peut pas être négative). On vérifie les postconditions avec des assertions également. On les place juste avant de renvoyer le résultat, c'est-à-dire avant le *return*.

Par exemple la fonction suivante calcule l'aire d'un carré dont la longueur du côté est passée en paramètre. La précondition est que la longueur du côté doit être positive et la postcondition que l'aire doit aussi être positive. Cela donne :

```python
def aire_carre(longueur):
    """
    Calcule l'aire d'un carré.
    
    :param longueur: (int ou float) la longueur du côté du carré
    :return: (int ou float) l'aire du carré
    """
    assert longueur >= 0, "la longueur du côté doit être positive"
    
    resultat = longueur**2
    
    assert resultat >= 0, "l'aire doit être positive"
    
    return resultat
```

> **exercice 30 :**
>
> Définissez, documentez et vérifiez les préconditions et postconditions d'une fonction qui calcule l'aire d'un rectangle dont les longueurs des côtés sont passées en paramètres.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

À partir de maintenant, dès qu'on vous demande d'écrire une fonction, cela signifie la définir, la documenter et vérifier les préconditions et postconditions.

> **Pour aller plus loin :**
>
> Un jour est composé de 24 heures, une heure de 60 minutes, et une minute de 60 secondes.
>
> En vous appuyant sur l'exemple suivant :
>
> ```python
> def minutes_en_secondes(minutes):
>     """
>     Calcule le nombre de secondes correspondant au nombre de minutes donné.
>     
>     :param minutes: (int) le nombre de minutes à convertir
>     :return: (int) le nombre de secondes
>     """
>     assert minutes >= 0, "le nombre de minutes doit être positif"
>     
>     secondes = minutes * 60
>     
>     assert secondes >= 0, "le nombre de secondes doit être positif"
>     
>     return secondes
> ```
>
> Écrivez deux autres fonctions :
>
> * `heures_en_minutes` de conversion d’heures en minutes
>
> * `jours_en_heures` de conversion de jours en heures
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```
>
> Écrivez ensuite une fonction de conversion en secondes d’une durée exprimée en jours, heures, minutes et secondes. Cette fonction nommée `en_secondes` acceptera donc quatre paramètres : un nombre de jour, un nombre d’heures, un nombre de minutes, et un nombre de secondes. Vous utiliserez les fonctions précédemment définies.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> Reprenez vos fonctions des exercices 19 à 27 : documentez les et ajoutez les assertions nécessaires.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*
