# Constructions élémentaires : affectation

L'objectif de cette séance est d'étudier l'une des instructions élémentaires des langages de programmation : l'affectation.

L'affectation est une instruction qui permet d'associer une **valeur** à une **variable**. Avant de voir comment cela fonctionne, nous allons regarder les différentes valeurs qui existent en Python.



## I. Valeurs : types de base

En Python, comme dans la plupart des langages de programmation, les valeurs ont un **type**. Par exemple, `2` est du type *int* qui sert à représenter tous les entiers. À chaque type correspond un ensemble d'**opérations** qui permettent de manipuler les valeurs de ce type. Par exemple, l'addition, réalisable avec l'opérateur `+`​ est l'une des opérations du type *int*.

Pour consulter le type d'une valeur, il existe une fonction native de Python appelée `type`​. Nous verrons lors d'une prochaine séance ce qu'est une fonction native, pour l'instant il faut juste savoir comment les utiliser : on écrit le nom de la fonction, suivi entre parenthèses de la valeur à tester. Par exemple, pour vérifier que le type de la valeur 2 est bien *int*, on exécute l'instruction suivante :

```python
>>> type(2)
<class 'int'>
```

L'interpréteur affiche alors `<class 'int'>`​ : la valeur 2 est donc bien du type *int*.

*Remarque : en Python, les types correspondent à des classes, c'est pour cela que la fonction type indique "class" int. Les classes font parties de la programmation objet au programme de Terminale.*

> **exercice 1 :**
>
> Donnez le type des valeurs suivantes :
>
> * 42
> * -8
> * 3.14
> * 1/3
> * -5.2
> * "a"
> * "nsi"
> * True
> * False
>
> ```
> 
> 
> 
> 
> 
> 
> ```

On divise généralement les types en 2 catégories : les types de base et les types construits. Nous allons pour le moment uniquement étudier les types de base : *int*, *float*, *str* et *bool*.

Il existe également une valeur particulière en Python :

```python
None
```

**None** est une valeur qui permet de représenter l'absence de valeur.

> **exercice 2 :**
>
> Quel est le type de la valeur None ?
>
> ```
> 
> 
> ```

Nous allons maintenant étudier les types de base et leurs opérations.

### 1. Entiers *int*

Le type *int* permet de représenter l'ensemble des entiers (positifs et négatifs). Il correspond à l'ensemble $`\mathbb Z`$ en mathématiques.

Voici l'ensemble des opérations possibles sur les entiers :

* addition : elle se réalise avec l'opérateur `+`​

  > **exercice 3 :**
  >
  > Testez l'addition sur les exemples suivants :
  >
  > ```python
  > >>> 1 + 2
  > ?
  > >>> 57 + 8
  > ?
  > >>> -9 + 4
  > ?
  > >>> -9 + (-6)
  > ?
  > >>> 7 + (-3)
  > ?
  > ```

* soustraction : elle se réalise avec l'opérateur `-`​

  > **exercice 4 :**
  >
  > Testez la soustraction sur les exemples suivants :
  >
  > ```python
  > >>> 2 - 1
  > ?
  > >>> 5 - 7
  > ?
  > >>> -3 - 4
  > ?
  > >>> -2 - (-5)
  > ?
  > >>> 8 - (-7)
  > ?
  > ```

* multiplication : elle se réalise avec l'opérateur `*`​

  > **exercice 5 :**
  >
  > Testez la multiplication sur les exemples suivants :
  >
  > ```python
  > >>> 2 * 5
  > ?
  > >>> 3 * (-6)
  > ?
  > >>> -3 * 4
  > ?
  > >>> -5 * (-8)
  > ?
  > >>> 0 * 4
  > ?
  > ```

* division : elle se réalise avec l'opérateur `/`​

  > **exercice 6 :**
  >
  > Testez la division sur l'exemple suivant :
  >
  > ```python
  > >>> 10 / 2
  > ?
  > ```
  >
  > Quel est le type du résultat ?
  >
  > ````
  > 
  > 
  > ````
  >
  > Testez aussi la division entière sur les exemples suivants :
  > ```python
  > >>> 10 / 3
  > ?
  > >>>  5 / 1
  > ?
  > >>> 41 / 5
  > ?
  > >>> 20 / 3
  > ?
  > ```

  L'opérateur `/` donne un résultat de type *float* et non plus *int*. Bien souvent, ce n'est pas très pratique car on ne souhaite manipuler que des entiers. C'est pour cela qu'il existe un autre opérateur de division : le `//`​.

* division entière : elle se réalise avec l'opérateur `//`

  Elle permet de récupérer le quotient de la division euclidienne : ![division euclidienne](img/division.png)

  > **exercice 7 :**
  >
  > Testez la division entière sur l'exemple suivant :
  >
  > ```python
  > >>> 10 // 2
  > ?
  > ```
  >
  > Quel est le type du résultat ?
  >
  > ```
  > 
  > 
  > ```
  >
  > Testez aussi la division entière sur les exemples suivants :
  >
  > ```python
  > >>> 10 // 3
  > ?
  > >>> 5 // 1
  > ?
  > >>> 41 // 5
  > ?
  > >>> 20 // 3
  > ?
  > ```

* reste de la division entière (aussi appelée *modulo*) : elle se réalise avec l'opérateur `%`​

  Elle permet de récupérer le reste de la division euclidienne.

  > **exercice 8 :**
  >
  > Testez le modulo sur les exemples suivants :
  >
  > ```python
  > >>> 10 % 2
  > ?
  > >>> 10 % 3
  > ?
  > >>> 5 % 1
  > ?
  > >>> 41 % 5
  > ?
  > >>> 20 % 3
  > ?
  > ```
  
* puissance : elle se réalise avec l'opérateur `**`​

  > **exercice 9 :**
  >
  > Testez la puissance sur les exemples suivants :
  >
  > ```python
  > >>> 2 ** 3
  > ?
  > >>> 2 ** 1
  > ?
  > >>> 1 ** 10
  > ?
  > >>> 8 ** 50
  > ?
  > ```

> **exercice 10 :**
>
> Complétez le tableau récapitulatif suivant :
>
> |     opération      | opérateur | type du résultat |
> | :----------------: | :-------: | :--------------: |
> |      addition      |           |                  |
> |    soustraction    |           |                  |
> |   multiplication   |           |                  |
> | division flottante |           |                  |
> |  division entière  |           |                  |
> |       modulo       |           |                  |
> |     puissance      |           |                  |



###  2. Réels *float*

Le type *float* permet de représenter l'ensemble des nombres réels (appelés nombres flottants en informatique). Il correspond à l'ensemble $`\mathbb R`$ en mathématiques.

> **exercice 11 :**
>
> En testant les opérateurs sur différents exemples de nombres flottants, complétez le tableau suivant :
>
> | opération | opérateur | type du résultat |
> | :-------: | :-------: | :--------------: |
> |           |    `+`​    |                  |
> |           |    `-`    |                  |
> |           |    `*`    |                  |
> |           |    `/`    |                  |
> |           |   `//`    |                  |
> |           |    `%`    |                  |
> |           |   `**`    |                  |

### 3. Chaînes de caractères *str*

Le type *str* permet de représenter les chaînes de caractères. Une chaîne de caractères est encadrée par des guillemets `"..."` ou éventuellement par des apostrophes `'...'`​.

```python
>>> type("nsi")
<class 'str'>
>>> type('nsi')
<class 'str'>
```

Une chaîne de caractères peut contenir n'importe quel caractère : lettre, chiffre, espace, ponctuation... Pour mettre un guillemet dans la chaîne, on l'encadre par des apostrophes et inversement :

```python
>>> type("J'aime la NSI")
<class 'str'>
>>> type('Il a dit : "bonjour".')
<class 'str'>
```

Il existe 2 caractères spéciaux que l'on peut trouver dans une chaîne de caractères :

* `\n` symbolise le retour à la ligne
* `\t` symbolise la tabulation

Il existe 2 opérations principales sur les chaînes de caractères :

* concaténation : elle se réalise avec l'opérateur `+` et permet de "coller" deux chaînes de caractères ensemble

  > **exercice 12 :**
  >
  > Testez la concaténation sur les exemples suivants :
  >
  > ```python
  > >>> "a" + "b"
  > ?
  > >>> "bonjour" + ' !'
  > ?
  > >>> "1" + "4"
  > ?
  > >>> "1" + "ere"
  > ?
  > ```

* duplication : elle se réalise avec l'opérateur `*`​ entre une chaîne de caractère et un *entier* et permet de dupliquer la chaîne de caractères

  > **exercice 13 :**
  >
  > Testez la duplication sur les exemples suivants :
  >
  > ```python
  > >>> "A" * 4
  > ?
  > >>> "nsi" * 1
  > ?
  > >>> "bonjour" * 10
  > ?
  > ```

  > **exercice 14 :**
  >
  > Essayez de dupliquer une chaîne de caractères par $`0`$. Que trouvez-vous ?
  >
  > ```
  > 
  > 
  > ```
  >
  > Cette chaîne particulière est appelée chaîne de caractères vide.

Nous reviendrons sur les chaînes de caractères plus tard dans l'année.

### 4. Booléens *bool*

Le dernier type de base est les booléens *bool*. Ce type ne contient que 2 valeurs :

* `True` = vrai
* `False`​ = faux

*Remarque : True et False s'écrivent toujours avec une majuscule en début de mot.*

Nous verrons les opérations sur les booléens lors de la prochaine séance sur les [instructions conditionnelles](./conditionnelles.md).



## II. Variables

### 1. Affectation

Une variable est un emplacement mémoire dans lequel on stocke une donnée.

En Python, pour déclarer une variable, on lui donne un nom et on lui **affecte** une valeur en utilisant le symbole `=`. L'exemple suivant permet d'affecter la valeur `2` à la variable de nom `nombre` :

```python
>>> nombre = 2
```

On peut ensuite retrouver la valeur de la variable en entrant simplement son nom :

```python
>>> nombre
2
```

On peut également regarder le type d'une variable : en Python, le type de la variable correspond au type de la valeur qui lui est affectée.

```python
>>> type(nombre)
<class 'int'>
```

Il est ensuite possible d'utiliser cette variable dans des opérations comme vu plus haut :

```python
>>> nombre + 1
3
>>> nombre * 5
10
```

> **exercice 15 :**
>
> Donnez l'instruction permettant d'affecter votre prénom à une variable nommée `prenom`​ :
>
> ```
> 
> 
> ```
>
> Donnez les instructions permettant de vérifier le type de la variable `prenom`​ et d'afficher sa valeur :
>
> ```
> 
> 
> 
> 
> ```
>
> Donnez l'instruction permettant de dupliquer votre prénom 3 fois :
>
> ```python
> 
> 
> ```

Le nom d'une variable est appelé **identificateur**. En Python, les identificateurs peuvent contenir des lettres minuscules et majuscules, des chiffres et des underscores ($`\_`$). Ils ne peuvent pas commencer par un chiffre.

Dès qu'un programme se complexifie, il peut utiliser de nombreuses variables. Pour garder le code compréhensible, il y a quelques règles de bonnes pratiques :

- l'identificateur s'écrit toujours en minuscule, et on utilise un underscore s'il est en plusieurs parties (exemple : `nombre_de_joueurs`​)
- on donne aux variables un nom significatif (exemple : l'identificateur de la variable pour le nombre de joueurs sera `nombre_de_joueurs` et non pas `n`)

*Remarque : n'ayez pas peur d'utiliser des identificateurs longs : en appuyant sur la touche tabulation, l'identificateur se complète automatiquement.*

> **exercice 16 :**
>
> Donnez l'instruction permettant d'affecter la valeur "Bonjour " à une variable nommée `chaine_bonjour` :
>
> ```
> 
> 
> ```
>
> Donnez l'instruction permettant d'affecter la valeur "!" à une variable dont vous choisirez l'identificateur :
>
> ```
> 
> 
> ```
>
> Donnez l'instruction permettant d'obtenir la chaîne de caractères "Bonjour !!!!!" en utilisant les opérations de concaténation et de duplication :
>
> ```
> 
> 
> ```

### 2. Changement de type

> **exercice 17 :**
>
> Réalisez les trois affectations des valeurs suivantes, en choisissant à chaque fois un identificateur significatif :
>
> * la chaîne de caractères "J'ai "
> * l'entier correspondant à votre âge
> * la chaîne de caractères " ans."
>
> ```
> 
> 
> 
> ```
>
> Vérifiez le type de vos trois variables. Vous devez avoir deux *str* et un *int*.
>
> ```
> 
> 
> 
> ```
>
> Essayer de concaténer les trois variables afin d'afficher votre âge. Qu'obtenez-vous ?
>
> ```
> 
> 
> 
> 
> ```

Félicitations, vous venez de rencontrer votre première **exception** ! Une exception est une erreur qui est déclenchée lors de l'exécution d'un programme.

La dernière ligne du message d'erreur indique ce qu'il s'est passé. Ici, l'exception déclenchée est **TypeError** qui indique un problème avec le type des valeurs utilisées lors de l'opération. Cela signifie que l'opération de concaténation ne fonctionne qu'entre deux chaînes de caractères de type *str*, elle ne fonctionne pas entre une chaîne *str* et un entier *int*.

Il va donc falloir **convertir** la variable contenant votre âge du type *int* au type *str*.

Il existe une fonction native en Python qui permet de faire cette conversion : elle s'appelle `str` *(comme le type, facile à retenir !)*. Comme pour la fonction `type`​, pour l'utiliser il suffit de préciser le nom de la fonction suivie entre parenthèses de la valeur à convertir. Par exemple :

```python
>>> nombre_deux = 2
>>> nombre_deux
2
>>> type(nombre_deux)
<class 'int'>
>>> chaine_deux = str(nombre_deux)
>>> chaine_deux
'2'
>>> type(chaine_deux)
<class 'str'>
```

>  **exercice 18 :**
>
> Utilisez la fonction `str`​ pour convertir la variable contenant votre âge en une chaîne de caractères, puis concaténer les 3 variables afin d'afficher votre âge.
>
> ```
> 
> 
> 
> 
> ```

Il existe plusieurs fonctions de conversion pour les types de base :

* `str`​ pour convertir une valeur en chaîne de caractères
* `int`​ pour convertir une valeur en entier
* `float`​ pour convertir une valeur en flottant
* `bool`​ pour convertir une valeur en booléen

> **exercice 19 :**
>
> Testez les fonctions de conversion sur les exemples suivants :
>
> ```python
> >>> str(142)
> ?
> >>> str(3.14)
> ?
> >>> str(-1.2)
> ?
> >>> int("5")
> ?
> >>> int("142")
> ?
> >>> int(3.14)
> ?
> >>> int(-1.2)
> ?
> >>> float(5)
> ?
> >>> float("5")
> ?
> >>> float("3.14")
> ?
> >>> float("-1.2")
> ?
> ```

> **exercice 20 :**
>
> Un élève de première NSI a obtenu trois notes au premier trimestre : 17, 9 et 11. Il souhaite calculer sa moyenne, l'arrondir puis l'afficher dans une belle phrase : "Au premier trimestre, ma moyenne de NSI est de 12".
>
> Donnez les instructions (affectations et conversions) qu'il doit utiliser :
>
> ```
> 
> 
> 
> 
> 
> 
> ```

### 3. Séquence

Pour l'instant, nous avons exécuté toutes nos instructions dans le shell. Nous allons maintenant écrire notre premier petit programme.

> **exercice 21 :**
>
> Dans l'éditeur (onglet `<untitled>`​, recopiez l'affectation suivante :
>
> ```python
> cinq = 5
> ```
>
> Enregistrez ce programme dans un fichier, puis exécutez le.
>
> *(rappel : pour exécuter un programme, on appuie sur `F5`​ ou on clique sur la flèche verte)*
>
> Une fois que le programme est exécuté, on peut l'utiliser dans le shell. Tentez de regarder la valeur de la variable `cinq`​ :
>
> ```python
> >>> cinq
> ?
> ```

Pour exécuter plusieurs instructions dans un même programme, on met chacune des instructions sur une ligne différente. Une suite d'instructions s'appelle une **séquence**. Lorsqu'on exécute un programme, chaque instruction de la séquence est interprétée **une par une dans l'ordre**.

Par exemple, regardez le programme suivant :

```python
nombre1 = 1
nombre2 = 2
nombre3 = nombre1 + nombre2
```

Lors de l'exécution de ce programme, la première ligne est d'abord interprétée : on affecte à la variable `nombre1` la valeur 1. Ensuite la seconde ligne est interprétée : on affecte à la variable `nombre2` la valeur 2. Enfin, la troisième ligne est interprétée : on affecte à la variable `nombre3` la somme des valeurs des variables `nombre1` et `nombre2`.

> **exercice 22 :**
>
> Recopiez et exécutez le programme précédent. Vérifiez ensuite les valeurs des 3 variables dans le shell.
>
> ```
> 
> 
> 
> ```

Il est possible dans une séquence d'affecter une nouvelle valeur à une variable déjà existante. On perd alors la valeur précédente.

> **exercice 23 :**
>
> Exécutez le programme suivant, puis vérifiez la valeur de la variable `mon_nombre`​ dans le shell.
>
> ```python
> mon_nombre = 1
> mon_nombre = 2
> ```
>
> ```
> 
> 
> ```

Donner les valeurs de chaque variable à chaque étape de l'exécution d'un programme s'appelle donner **l'état de la mémoire**. On représente souvent l'état de la mémoire dans un tableau.

Par exemple, voilà l'état de la mémoire à chaque étape de l'exécution du programme de l'exercice 23 :

|                 | mon_nombre |
| :-------------: | :--------: |
| avant exécution | pas défini |
|     ligne 1     |     1      |
|     ligne 2     |     2      |
| après exécution |     2      |

> **exercice 24 :**
>
> Donnez l'état de la mémoire à chaque étape de l'exécution des programmes suivants :
>
> ```python
> a = 10
> b = a
> a = 12 
> b = b + 1
> ```
>
> |                 |  a   |  b   |
> | :-------------: | :--: | :--: |
> | avant exécution |      |      |
> |     ligne 1     |      |      |
> |     ligne 2     |      |      |
> |     ligne 3     |      |      |
> |     ligne 4     |      |      |
> | après exécution |      |      |
>
> ```python
> x = 1
> y = 2
> z = y
> y = x
> x = z
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

## III. Pour aller plus loin

Si vous avez terminé, vous pouvez faire les exercices suivants :

> **exercice 25 :**
>
> Affectez aux identificateurs `a`, `b`, `c` et `d`​ respectivement les valeurs 1, 2, 3 et 4.
>
> Dans le même programme, affectez ensuite à 4 variables différentes les valeurs suivantes :
>
> * $`(a + b)^2`$
> * $`a^2 + 2ab + b^2`$
> * $`\frac{a + b}{c + d}`$
> * $`\frac{1}{a + 1}`$
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> ```
>
> Vérifiez ensuite dans le shell les valeurs de ces variables.
>
> ```
> 
> 
> 
> 
> ```
>
> *(vous devez trouver 9, 9, 0.42857.. et 0.5)*

> **exercice 26 :**
>
> Affectez aux identificateurs `nombre1` et `nombre2`​ respectivement les valeurs 1 et 2.
>
> Ecrivez ensuite une séquence permettant d'échanger les valeurs des deux variables (sans réutiliser 1 et 2, uniquement avec les variables).
>
> ```
> 
> 
> 
> 
> 
> ```
>
> *(indice : vous pouvez utiliser une troisième variable intermédiaire)*
>
> Vérifiez que votre programme fonctionne toujours en changeant les valeurs initiales des variables.

> **exercice 27 :**
>
> Donner l'état de la mémoire à chaque étape de l'exécution de votre programme de l'exercice 26.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 28 :**
>
> Donnez la séquence permettant d'effectuer les calculs suivants sur les entiers.
>
> - soit un nombre
> - lui ajouter 3
> - multiplier par 2
> - ajouter le nombre initial
> - soustraire 12
> - diviser par 3
> - ajouter 2
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```
>
> Testez votre programme avec plusieurs valeurs du nombre initial. Qu'obtenez-vous ?
>
> ```
> 
> 
> ```

> **exercice 29 :**
>
> Donnez une instruction calculant le chiffre des unités de 2019. *(vous devez trouver 9)*
>
> ```
> 
> 
> ```
>
> Donnez une instruction calculant le nombre de dizaines de 2019. *(vous devez trouver 201)*
>
> ```
> 
> 
> ```
>
> Donnez une instruction calculant le chiffre des dizaines de 2019. *(vous devez trouver 1)*
>
> ```
> 
> 
> ```

> **exercice 30 :**
>
> Soit les quatre instructions suivantes :
>
> ```
> chaine1 = "oh "
> chaine2 = chaine2 + "!"
> chaine2 = coeff * chaine1
> coeff = 3
> ```
>
> Remettez ces instructions dans l’ordre pour que suite à l'exécution de la séquence, la valeur associée à la variable `chaine2`​ soit "oh oh oh !".
>
> ```
> 
> 
> 
> 
> ```
>
> Même chose avec les six instructions suivantes (vous devez utiliser toutes les instructions) :
>
> ```
> chaine1 = "oh "
> chaine1 = "oh !"
> chaine2 = chaine2 + "!"
> coeff2 = coeff - 1
> chaine2 = coeff * chaine1
> coeff = 3
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> ```



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*

Source des exercices III : [FIL](https://www.fil.univ-lille1.fr/portail/)