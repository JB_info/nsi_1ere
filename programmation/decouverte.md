# Découverte : le langage de programmation Python



## I. Environnement de travail

### 1. Un IDE pour Python

Le principal langage de programmation au programme de NSI est Python. Il a été créé en 1991 par Guido van Rossum. Pour le télécharger sur votre ordinateur personnel, c'est ici : [https://www.python.org/downloads/](https://www.python.org/downloads/).

Python est un langage de programmation dit **de haut niveau**. Cela signifie que le code Python est très éloigné de ce qu'il se passe réellement dans la machine. Cela nous permet de nous concentrer sur le déroulement du programme en lui-même. Nous aurons cependant également l'occasion dans l'année d'étudier ce qu'il se passe en machine.

Pour programmer en Python, nous allons utiliser un IDE (*Integrated Development Environment* = environnement de développement intégré), logiciel qui propose des outils facilitant l'écriture de nos programmes. Il existe de nombreux IDE pour le langage Python, nous utiliserons Thonny. Pour le télécharger sur votre ordinateur personnel, c'est ici : [https://thonny.org/](https://thonny.org/).

Lorsque l'on lance Thonny, une fenêtre comme ceci apparaît :

![thonny démarrage](img/thonny.png)

La fenêtre est séparée en 2 onglets principaux :

* l'éditeur (onglet `<untitled>`).
* l'interpréteur (onglet `Console`​ ou `Shell`​ ).

### 2. Le shell

Le shell est l'onglet dans lequel l’utilisateur interagit avec l’**interpréteur** Python. L'interpréteur Python est un programme qui transforme le code Python en bytecode, et c'est ce bytecode qui est ensuite exécuté par la machine.

On trouve dans le shell une **invite de commande** : les 3 chevrons `>>>`​. Cette invite de commande attend des **instructions**.

Vous avez sans doute déjà vu quelques instructions en SNT, nous allons étudier les instructions élémentaires de Python dans les prochaines semaines.

### 3. L'éditeur

L'éditeur est l'onglet dans lequel on écrit les programmes.

Avant d'exécuter un programme, on doit tout d'abord l'enregistrer dans un fichier. Ensuite, 2 moyens possibles pour l'exécuter :

* appuyer sur la touche `F5`​
* cliquer sur le bouton flèche verte ![exécuter programme](img/executer.png)

Une fois qu'un programme a été exécuté, on peut ensuite l'utiliser dans le shell.



## II. Repérer les constructions élémentaires

Avant de se lancer dans la programmation, regardons un peu à quoi ressemble un programme Python.

Par exemple, le programme suivant contient toutes les constructions de base de Python dont vous allez avoir besoin cette année :

```python
def fibonacci(rang):
    if rang == 0 or rang == 1:
        return 1
    u0 = 1
    u1 = 1
    resultat = 2
    for rang_intermediaire in range(3, rang+1):
        u0 = u1
        u1 = resultat
        resultat = u0 + u1
    return resultat

def rang_fibonacci(nombre_a_atteindre):
    rang_a_tester = 2
    while fibonacci(rang_a_tester) < nombre_a_atteindre:
        rang_a_tester = rang_a_tester + 1
    return rang_a_tester
```

> **exercice 1 :**
>
> Essayez de lister les différentes structures que vous pouvez trouver dans le programme ci-dessus.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```

Nous allons étudier plus en détail toutes ces constructions élémentaires. Il est important de savoir que **ces constructions permettent d'écrire absolument tous les programmes Python**.



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*