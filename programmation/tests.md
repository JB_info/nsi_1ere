# Mise au point de programmes : tests


Il est impératif de toujours vérifier que nos fonctions sont correctes.

Les utilisateurs de nos programmes s'attendent à ce qu'ils renvoient, sans erreurs, les résultats attendus.

Pour cela nous allons écrire des **doctests**.


## I. Syntaxe des doctests

Les doctests s'écrivent à la fin de la documentation d'une fonction.

Ils s'écrivent ainsi :

```python
"""
D'abord la documentation.

>>> test d'appel de la fonction
résultat attendu
"""
```

Un exemple :
```python
def somme(entier1, entier2):
    """
    Calcule la somme de deux entiers.
    :param entier1: (int) le premier entier
    :param entier2: (int) le premier entier
    :return: (int) la somme

    >>> somme(2, 5)
    7
    """
    resultat = entier1 + entier2
    return resultat
```

Le doctest indique ici que la somme de 2 et 5 devrait renvoyer 7.

Il faut ensuite ajouter ces lignes *à la fin du fichier* :

```python
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
```

Lorsqu'on exécute notre programme, ceci s'affiche désormais :

```python
Trying:
    somme(2, 5)
Expecting:
    7
ok
1 passed and 0 failed.
Test passed.
```

> **exercice 1 :**  
> Copiez la fonction *somme* puis les 3 lignes pour exécuter les doctests dans un fichier. Exécutez le programme.  
> Retrouvez-vous le message comme quoi le test est passé ?

Si jamais on a fait une faute dans notre programme, le message affiché l'indique.

Essayons par exemple de remplacer le + par un - dans notre fonction :
```python
def somme(entier1, entier2):
    """
    Calcule la somme de deux entiers.
    :param entier1: (int) le premier entier
    :param entier2: (int) le premier entier
    :return: (int) la somme

    >>> somme(2, 5)
    7
    """
    resultat = entier1 - entier2
    return resultat
```

Le message affiché est maintenant ceci :

```python
Trying:
    somme(2, 5)
Expecting:
    7
**********************************************************************
File "somme_exemple.py", line 8, in __main__.somme
Failed example:
    somme(2, 5)
Expected:
    7
Got:
    -3
**********************************************************************
0 passed and 1 failed.
***Test Failed*** 1 failures.
```

Il indique en effet qu'il teste somme(2, 5) et qu'il s'attendait à 7 mais qu'il a obtenu -3 : le test échoue donc.

> **exercice 2 :**  
> Modifiez la fonction somme en changeant le + par un -. Exécutez le programme.  
> Retrouvez-vous le message comme quoi le test a échoué ?


## II. Faire suffisamment de tests

Faire un seul doctest ne suffit pas.

L'objectif est de **tester tous les cas possibles**.

Plus on fait de tests, plus il y a de chances que notre programme soit correct.

Par exemple, pour la fonction *somme*, il faudrait ajouter ces tests ci :
```python
def somme(entier1, entier2):
    """
    Calcule la somme de deux entiers.
    :param entier1: (int) le premier entier
    :param entier2: (int) le premier entier
    :return: (int) la somme

    >>> somme(2, 5)
    7
    >>> somme(2, -5)
    -3
    >>> somme(-2, 5)
    3
    >>> somme(-2, -5)
    -7
    >>> somme(0, 0)
    0
    """
    resultat = entier1 + entier2
    return resultat
```

De cette manière, on a testé :
* avec deux nombres positifs
* avec un nombre positif puis un négatif
* avec un nombre négatif puis un positif
* avec deux nombres négatifs
* avec le cas particulier du zéro

On peut estimer que l'on a testé suffisamment de cas possibles.

> **exercice 3 :**  
> Copiez les nouveaux doctests dans votre fichier et exécutez le.  
> Quel message est maintenant affiché dans la console ?

Le plus délicat est de **bien choisir ses tests** : il est évidemment impossible de tester toutes les valeurs qui existent, donc il faut bien choisir ses valeurs pour vérifier le plus de cas de figure possibles.

Il faut faire attention aux cas particuliers notamment, comme :
* le 0
* le tableau vide []
* la chaîne vide ""
* ...

> **exercice 4 :**  
> Quels tests faudrait-il faire pour une fonction `soustraction` qui doit soustraire 2 entiers ?


## III. Les tests ne sont cependant pas une vraie preuve

Il ne faut cependant par penser que les tests sont une preuve certaine que le programme fonctionne.

Il se peut très bien que le développeur ait "oublié" un cas particulier.

La mise en place des doctests permet tout de même de diminuer grandement le risque d'erreurs.


## IV. Méthodologie pour écrire un programme

Voici donc une méthodologie qui permet de mettre au point un programme.

### Etape 1

Première Etape : à la fin du programme, en bas du fichier, on ajoute systématiquement les lignes de codes suivantes afin que les tests s'exécutent automatiquement quand on appuie sur le bouton pour exécuter :

```python
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
```

Vous ne devez pas connaître ces lignes par cœur, mais conservez les quelque part : il faudra prendre l'habitude de **toujours les copier en bas de vos programmes**.

### Etape 2

Deuxième étape : on **trouve l'idée** que l'on veut mettre en place.

Par exemple, pour le programme "calculer la somme de deux entiers", l'idée serait : j'additionne les deux nombres puis je renvoie le résultat.

### Etape 3

Troisième étape : on **identifie les structures** et instructions Python dont on va avoir besoin.

Par exemple, pour le programme "calculer la somme de deux entiers", on va avoir besoin uniquement de l'opérateur + pour l'addition.


### Etape 4

Quatrième étape : on écrit le **code** de notre fonction.

Par exemple, pour le programme "calculer la somme de deux entiers", on écrit :

```python
def somme(entier1, entier2):
    resultat = entier1 + entier2
    return resultat
```

### Etape 5

Cinquième étape : on ajoute la **documentation**.

Par exemple, pour le programme "calculer la somme de deux entiers", on écrit :
```python
"""
Calcule la somme de deux entiers.
:param entier1: (int) le premier entier
:param entier2: (int) le premier entier
:return: (int) la somme
"""
```

### Etape 6

Sixième étape : on ajoute suffisamment de doctests :

* un test pour le cas général
* un test pour chaque particulier

Par exemple, pour le programme "calculer la somme de deux entiers", on écrit :

```python
>>> somme(2, 5)
7
>>> somme(2, -5)
-3
>>> somme(-2, 5)
3
>>> somme(-2, -5)
-7
>>> somme(0, 0)
0
```

### Etape 7

Dernière étape : on exécute !

Si tous les tests passent, c'est fini : bravo !

Si 1 ou plusieurs tests ne passent pas, il faut relire votre programme pour essayer de le corriger.

## Exercices

> **exercice 5 :**  
> En détaillant étape par étape votre raisonnement, écrivez une fonction qui fait la multiplication de deux entiers.

> **exercice 6 :**  
> En détaillant étape par étape votre raisonnement, écrivez une fonction qui affiche les valeurs d'un tableau.  
> *Le tableau peut contenir des entiers ou des caractères (ou rien !).*

> **Pour aller plus loin :**  
> Reprenez les fonctions du [cours sur les chaînes de caractères](https://framagit.org/JB_info/nsi_1ere/-/blob/main/representation_donnees/caracteres.md) et ajoutez-y les doctests.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

