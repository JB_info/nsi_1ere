# Constructions élémentaires : boucles bornées et non bornées



Dans certaines situations, on peut vouloir exécuter les mêmes instructions sur plusieurs valeurs différentes. Pour faire cela, ce n'est pas très pratique de copier le même programme plusieurs fois : on utilise plutôt des **boucles**. Une boucle permet de **répéter une séquence**.

Il existe deux types de boucles :

* les **boucles bornées (*for*)** si on connaît le nombre de répétition à effectuer ;
* les **boucles non bornées (*while*)** si on ne connaît pas le nombre de répétition.



## I. Boucles bornées : *for*

Les boucles bornées permettent de répéter une séquence sur une série de valeurs. On les utilise quand on connaît le nombre de répétitions à effectuer, c'est-à-dire quand on connaît toutes les valeurs sur lesquelles on veut exécuter notre séquence.

### 1. Itérables

Une série de valeurs que l'on peut parcourir s'appelle un **itérable**.

On va alors exécuter une séquence d'instructions successivement sur chacune des valeurs de l'itérable. L'exécution de la séquence sur une valeur de l'itérable s'appelle une **itération** (ou *tour de boucle*).

Vous connaissez pour l'instant deux itérables :

* les chaînes de caractères *str* : les itérations se réalisent sur chacun des caractères de la chaîne,
* les tuples *tuple* : les itérations se réalisent sur chacun des éléments du tuple.

Par exemple pour la chaîne `"nsi"`:

* la première itération de la séquence se réalise sur le caractère `"n"`
* la seconde itération de la séquence se réalise sur le caractère `"s"`
* la troisième itération de la séquence se réalise sur le caractère `"i"`.

Autre exemple avec le tuple `(7, 1, 4, 36)` :

* la première itération de la séquence se réalise sur l'élément `7`
* la seconde itération de la séquence se réalise sur l'élément `1`
* la troisième itération de la séquence se réalise sur l'élément `4`
* la quatrième itération de la séquence se réalise sur l'élément `36`

Nous verrons d'autres itérables plus tard dans l'année.

> **exercice 1 :**
>
> Pour chacun des itérables suivants, donnez la série de valeurs successives sur lesquelles vont se réaliser les itérations :
>
> * "itérable"
> * "une p'tite chaîne !"
> * (1, 2, 4, 8, 16)
> * ("un", "petit", "tuple")
> * "z"
> * ""
> * (42, )
> * ()
>
> ```
> 
> 
> 
> 
> ```

On peut bien utiliser une boucle bornée sur un itérable car on connaît le nombre de répétitions (= nombre d'itérations = nombre de tours de boucle) à effectuer :

* le nombre de répétitions correspond au nombre de caractères pour les chaînes
* le nombre de répétitions correspond au nombre d'éléments pour les tuples

> **exercice 2 :**
>
> Pour chacun des itérables suivants, donnez le nombre d'itérations qu'une boucle bornée va effectuer :
>
> * "informatique"
> * "j'aime la nsi !"
> * (0, 1, 2, 3, 4)
> * ("un", second", "petit", "tuple")
> * ""
> * ()
>
> ```
> 
> 
> 
> ```

### 2. For

Comme dit plus haut, une boucle bornée permet de réaliser une séquence d'instructions sur chaque valeur d'un itérable.

```
début du programme
pour chaque valeur de l'itérable
	séquence à réaliser
fin pour
suite du programme
```

Voilà comment on l'écrit en Python :

* le mot-clef pour cette instruction est **for** (=pour en anglais)
* le signe deux-points **:** marque le début de la séquence à réaliser
* la séquence à réaliser est repérée par une **indentation** supplémentaire

Cela donne :

```python
début du programme
for variable_de_boucle in iterable:
    séquence à réaliser
suite du programme
```

La variable `variable_de_boucle` prend automatiquement comme valeur successivement chaque valeur de l'itérable que l'on parcourt (comme vu à l'exercice 1).

Pour sortir de la boucle bornée, il suffit de continuer à écrire notre programme en revenant à une indentation normale.

Regardez cette première boucle toute simple : on va juste ajouter chaque valeur de l'itérable à un tuple nommé `resultat`.

```python
iterable = "nsi"
resultat = ()
for caractere in iterable:
    resultat = resultat + (caractere, )
```

Notre itérable est la chaîne de caractères "nsi".  Cela signifie que :

* la première itération de la boucle se réalise sur le caractère `"n"`
* la seconde itération de la boucle se réalise sur le caractère `"s"`
* la troisième itération de la boucle se réalise sur le caractère `"i"`.

A chaque itération de la boucle, on ajoute le caractère concerné au tuple `resultat`. Cela signifie que :

* la première itération de la boucle ajoute le caractère `"n"` au tuple `resultat` : `resultat` vaut `("n", )`
* la seconde itération de la boucle ajoute le caractère `"s"` au tuple `resultat` : `resultat` vaut `("n", "s")`
* la troisième itération de la boucle ajoute le caractère `"i"` au tuple `resultat` : `resultat` vaut `("n", "s", "i")`.

Vérifions la valeur finale du tuple `resultat` :

```python
>>> resultat
('n', 's', 'i')
```

On peut résumer cela simplement avec un tableau qui décrit l'état de la mémoire :

|                     | iterable | caractere  |    resultat     |
| :-----------------: | :------: | :--------: | :-------------: |
|   avant la boucle   |  "nsi"   | non défini |       ()        |
| première itération  |  "nsi"   |    "n"     |     ("n", )     |
|  seconde itération  |  "nsi"   |    "s"     |   ("n", "s")    |
| troisième itération |  "nsi"   |    "i"     | ("n", "s", "i") |
|   après la boucle   |  "nsi"   |    "i"     | ("n", "s", "i") |

> **exercice 3 :**
>
> Donnez l'évolution de l'état de la mémoire pour la boucle du programme suivant :
>
> ```python
> chaine = "j'aime la nsi !"
> resultat = ()
> for caractere in chaine:
>        resultat = resultat + (caractere, )
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 4 :**
>
> Complétez le programme suivant, qui parcourt l'itérable `"12345"` pour créer le tuple d'entiers `(1, 2, 3, 4, 5)`.
>
> *Rappel : vous pouvez convertir une chaine en entier avec la fonction `int(...)`*
>
> ```python
> chaine_de_chiffres = "12345"
> tuple_d_entiers = ()
> for ??? in ??? :
>        tuple_d_entiers = tuple_d_entiers + ???
> ```

> **exercice 5 :**
>
> Ecrivez un programme qui crée la copie d'une chaîne de caractère à l'aide d'une boucle bornée :
>
> ```python
> chaine_initiale = ???
> chaine_copiee = ???
> for ...
> 
> ```
>
> Testez votre programme en prenant comme chaîne initiale "Nsi !" :
>
> ```python
> >>> chaine_copiee
> 'Nsi !'
> ```
>
> Décrivez l'état de la mémoire à chaque itération de votre boucle :
>
> ```
> 
> 
> 
> 
> 
> 
> ```

### 3. Range

Pour l'instant, on sait parcourir uniquement des itérables déjà existants (chaînes de caractères, tuples). Parfois, on souhaite répéter une séquence d'instructions un certain nombre de fois sans que ce soit lié à un itérable existant. Par exemple, on peut vouloir faire une opération 5 fois. On utilise alors un **intervalle**.

Un intervalle est une série d’entiers. Par exemple, la série des entiers entre 3 et 7 (3, 4, 5, 6, 7) forme un intervalle.

La fonction de Python `range` permet de créer des intervalles. Il y a trois manières de l'utiliser :

* `range(fin)` crée l'intervalle qui va de `0` inclus à `fin` exclus. Par exemple `range(7)` crée la série d'entiers 0, 1, 2, 3, 4, 5, 6.

  > **exercice 6 :**
  >
  > Donnez la série d'entiers correspondant à chacun des intervalles suivants :
  >
  > * `range(5)`
  > * `range(1)`
  > * `range(0)`
  > * `range(-5)`

* `range(debut, fin)` crée l'intervalle qui va de `debut` inclus à `fin` exclus. Par exemple `range(3, 7)` crée la série d'entiers 3, 4, 5, 6.

  > **exercice 7 :**
  >
  > Donnez la série d'entiers correspondant à chacun des intervalles suivants :
  >
  > * `range(1, 6)`
  > * `range(0, 4)`
  > * `range(-3, 2)`
  > * `range(3, -2)`

* `range(debut, fin, pas)` crée l'intervalle qui va de `debut` inclus à `fin` exclus avec un pas de `pas`. Par exemple `range(1, 9, 2)` crée la série d'entiers 1, 3, 5, 7.

  > **exercice 8 :**
  >
  > Donnez la série d'entiers correspondant à chacun des intervalles suivants :
  >
  > * `range(1, 12, 3)`
  > * `range(0, 5, 1)`
  > * `range(-3, 2, 2)`
  > * `range(10, 2, -1)`

Un intervalle est un itérable. Il sera donc possible de parcourir la série des entiers d'un intervalle avec une boucle *for*.

Regardez le programme suivant par exemple, qui ajoute chaque entier de l'intervalle au tuple `resultat` :

```python
resultat = ()
for entier in range(5):
    resultat = resultat + (entier, )
```

Voilà l'évolution de l'état de la mémoire pour cette boucle :

|                     |   entier   |    resultat     |
| :-----------------: | :--------: | :-------------: |
|   avant la boucle   | pas défini |       ()        |
| première itération  |     0      |      (0, )      |
|  seconde itération  |     1      |     (0, 1)      |
| troisième itération |     2      |    (0, 1, 2)    |
| quatrième itération |     3      |  (0, 1, 2, 3)   |
| cinquième itération |     4      | (0, 1, 2, 3, 4) |
|   après la boucle   |     4      | (0, 1, 2, 3, 4) |

> **exercice 9 :**
>
> Utilisez le programme précédent pour vérifier vos réponses des exercices 6, 7 et 8.
>
> ```
> 
> 
> ```

> **exercice 10 :**
>
> Complétez le programme suivant, qui initialise un nombre `n` et calcule le carré des nombres de 0 à n inclus.
>
> ```python
> n = ???
> carres = ()
> for ??? in range(???):
>     carres = carres + (???, )
> ```
>
> Testez votre programme avec `n = 10` :
>
> ```python
> >>> carres
> (0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100)
> ```

> **exercice 11 :**
>
> Ecrivez un programme qui initialise un entier `n` et calcule la factorielle de `n` (noté `n!` en mathématiques). Pour rappel, factorielle n est égal au produit des entiers de 1 à n : $`n! = 1 \times 2 \times 3 \times ... \times n`$.
>
> ```
> 
> 
> 
> 
> ```
>
> Testez votre programme avec les entiers 5, 10 et 1.
>
> *(vous devez obtenir 120, 3628800 et 1)*

> **exercice 12 :**
>
> Ecrivez un programme qui initialise
>
> * un nombre `premier_indice`
> * un nombre `dernier_indice`
> * un tuple `tuple_initial` de taille supérieure ou égale à `dernier_indice`
>
> puis à l'aide d'une boucle et d'un intervalle, créez un tuple `resultat` qui ne contient que les éléments du tuple `tuple_initial` dont les indices sont entre `premier_indice` et `dernier_indice` inclus.
>
> Par exemple pour `premier_indice = 1`, `dernier_indice = 4` et `tuple_initial = ("a", "b", "c", "d", "e", "f", "g")`, vous devez obtenir :
>
> ```python
> >>> resultat
> ("b", "c", "d", "e")
> ```
>
> *Rappel : pour accéder à l'élément qui est à l'indice ind d'un tuple tup, on écrit tup[ind]*.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

Il est également possible d'imbriquer plusieurs boucles bornées. C'est à nouveau l'indentation qui détermine le début/la fin de la séquence d'instructions associée à la boucle.

> **exercice 13 :**
>
> La comète Tchouri est passée dans notre système solaire pour la dernière fois en 2015. Elle a une période de révolution de 7 ans, ce qui signifie qu'elle repassera dans notre système solaire en 2022, 2029… La comète Encke qui est passée pour la dernière fois en 2017, a une période de révolution de 3 ans. On cherche à déterminer en quelle année, au plus tôt, les deux comètes  Tchouri et Encke traverseront simultanément notre système solaire.
>
> On appelle `a` le nombre de révolutions de la comète Tchouri et `b` le nombre de révolutions de la comète Encke. Donnez l'équation que doivent vérifier `a` et `b` pour que les deux comètes  traversent le système solaire la même année.
>
> ```
> 
> 
> ```
>
> Complétez le programme ci-dessous pour répondre à la question posée :
>
> ```python
> for a in range(10):
> 	for b in range(10):
>     	if ???:
>         	annee_rencontre = ???
> ```
>
> Exécutez le programme et déduisez-en la prochaine année de rencontre de ces deux comètes dans le système solaire.
>
> ```
> 
> 
> ```
>
> Ce programme teste avec a = 0 et b = 0, puis a = 0 et b = 1, puis a = 0 et b = 2, ...... puis a = 0 et b = 9, puis a = 1 et b = 0, puis a = 1 et b = 1, ...

> **exercice 14 :**
>
> Complétez le programme suivant pour créer un tuple qui contient tous les couples (a, b) avec a < 3 et b < 5.
>
> ```python
> resultat = ()
> for a in range(???):
> 	for b in range(???):
> 		resultat = resultat + ((a, b), )
> ```
>
> Vous devez trouver :
>
> ```python
> >>> resultat
> ((0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2, 0), (2, 1), (2, 2), (2, 3), (2, 4))
> ```
>
> Complétez le tableau d'évolution de l'état de la mémoire ci-dessous (la boucle "externe" correspond au "for a ..." et la boucle "interne" au "for b ...") :
>
> |                                         |                                          |     a      |     b      |                           resultat                           |
> | :-------------------------------------: | :--------------------------------------: | :--------: | :--------: | :----------------------------------------------------------: |
> |            avant les boucles            |                                          | pas défini | pas défini |                              ()                              |
> | première itération de la boucle externe | première itération de la boucle interne  |     0      |     0      |                          ((0, 0), )                          |
> |                                         |  seconde itération de la boucle interne  |     0      |     1      |                        ((0,0), (0,1))                        |
> |                                         | troisième itération de la boucle interne |     ?      |     ?      |                              ?                               |
> |                                         | quatrième itération de la boucle interne |     ?      |     ?      |                              ?                               |
> |                                         | cinquième itération de la boucle interne |     ?      |     ?      |                              ?                               |
> | seconde itération de la boucle externe  | première itération de la boucle interne  |     ?      |     ?      |                              ?                               |
> |                                         |  seconde itération de la boucle interne  |     ?      |     ?      |                              ?                               |
> |                                         |                    ?                     |     ?      |     ?      |                              ?                               |
> |                                         |                    ?                     |     ?      |     ?      |                              ?                               |
> |                                         |                    ?                     |     ?      |     ?      |                              ?                               |
> |                    ?                    |                    ?                     |     ?      |     ?      |                              ?                               |
> |                                         |                    ?                     |     ?      |     ?      |                              ?                               |
> |                                         |                    ?                     |     ?      |     ?      |                              ?                               |
> |                                         |                    ?                     |     ?      |     ?      |                              ?                               |
> |                                         |                    ?                     |     ?      |     ?      |                              ?                               |
> |            après les boucles            |                                          |     2      |     4      | ((0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2, 0), (2, 1), (2, 2), (2, 3), (2, 4)) |
>
> 

> **exercice 15 :**
>
> Ecrivez un programme qui crée un tuple qui contient les résultats de toutes les tables de multiplication de 1 à 9 : les résultats de 1 fois tous les nombres de 1 à 9, puis les résultats de 2 fois tous les nombres de 1 à 9, etc.
>
> ```
> 
> 
> 
> 
> 
> ```
>
> Vous devez trouver :
>
> ```python
> >>> resultat
> (1, 2, 3, 4, 5, 6, 7, 8, 9, 2, 4, 6, 8, 10, 12, 14, 16, 18, 3, 6, 9, 12, 15, 18, 21, 24, 27, 4, 8, 12, 16, 20, 24, 28, 32, 36, 5, 10, 15, 20, 25, 30, 35, 40, 45, 6, 12, 18, 24, 30, 36, 42, 48, 54, 7, 14, 21, 28, 35, 42, 49, 56, 63, 8, 16, 24, 32, 40, 48, 56, 64, 72, 9, 18, 27, 36, 45, 54, 63, 72, 81)
> ```

### 4. Pour aller plus loin

S'il vous reste du temps, vous pouvez vous entraîner sur les exercices ci-dessous.

> **exercice 16 :**
>
> Ecrivez un programme qui initialise un nombre entier `n` et calcule la somme des entiers de 1 à `n` inclus.
>
> ```
> 
> 
> 
> 
> ```
>
> Testez votre programme avec les nombres 3, 9 et 1.
>
> *(vous devez trouver 6, 45 et 1)*

> **exercice 17 :**
>
> Ecrivez un programme qui initialise un nombre entier `n` et calcule la somme des entiers impairs de 1 à `n` inclus.
>
> ```
> 
> 
> 
> ```
>
> Testez votre programme avec les nombres 3, 9 et 1.
>
> *(vous devez trouver 4, 25 et 1)*

> **exercice 18 :**
>
> Ecrivez un programme qui initialise deux nombres entiers `n` et `p` et calcule $`n^p`$ en utilisant uniquement l'opérateur de multiplication.
>
> ```
> 
> 
> ```
>
> Testez votre programme avec les nombres n=2 et p=5, n=1 et p=20, n=8 et p=1.
>
> *(vous devez trouver 32, 1 et 8)*

> **exercice 19 :**
>
> Ecrivez un programme qui initialise un nombre `n` et crée un tuple contenant les entiers pairs de 0 inclus à `n` exclus.
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 20 :**
>
> Ecrivez un programme qui compte le nombre de caractères d'une chaîne.
>
> ```
> 
> 
> 
> 
> ```

> **exercice 21 :**
>
> Ecrivez un programme qui compte le nombre d'espaces d'une chaîne de caractères.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

> **exercice 22 :**
>
> Ecrivez un programme qui vérifie que tous les caractères d'une chaîne sont bien des minuscules.
>
> *indice : vous pouvez par exemple utiliser le test d'appartenance d'un caractère au tuple ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z').*
>
> ```
> 
> 
> 
> 
> 
> 
> ```

> **exercice 23 :**
>
> Ecrivez un programme qui renverse une chaîne de caractères. Par exemple avec la chaîne initiale "informatique" vous devez créer une chaine "euqitamrofni".
>
> ```
> 
> 
> 
> 
> 
> ```
>
> Ecrivez ensuite un programme qui détermine si une chaîne est un palindrome. Un palindrome est un mot qui se lit dans les deux sens, comme "elle", kayak" ou "mon nom".
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 24 :**
>
> Ecrivez un programme qui crée un tuple contenant des chaines de caractères représentant les tables de multiplication de 1 à 9.
>
> Le résultat ressemblera par exemple à ceci :
>
> ```python
> >>> resultat
> ('1 x 1 = 1', '1 x 2 = 2', '1 x 3 = 3', '1 x 4 = 4', '1 x 5 = 5', '1 x 6 = 6', '1 x 7 = 7', '1 x 8 = 8', '1 x 9 = 9', '2 x 1 = 2', '2 x 2 = 4', '2 x 3 = 6', '2 x 4 = 8', '2 x 5 = 10', '2 x 6 = 12', '2 x 7 = 14', '2 x 8 = 16', '2 x 9 = 18', '3 x 1 = 3', '3 x 2 = 6', '3 x 3 = 9', '3 x 4 = 12', '3 x 5 = 15', '3 x 6 = 18', '3 x 7 = 21', '3 x 8 = 24', '3 x 9 = 27', '4 x 1 = 4', '4 x 2 = 8', '4 x 3 = 12', '4 x 4 = 16', '4 x 5 = 20', '4 x 6 = 24', '4 x 7 = 28', '4 x 8 = 32', '4 x 9 = 36', '5 x 1 = 5', '5 x 2 = 10', '5 x 3 = 15', '5 x 4 = 20', '5 x 5 = 25', '5 x 6 = 30', '5 x 7 = 35', '5 x 8 = 40', '5 x 9 = 45', '6 x 1 = 6', '6 x 2 = 12', '6 x 3 = 18', '6 x 4 = 24', '6 x 5 = 30', '6 x 6 = 36', '6 x 7 = 42', '6 x 8 = 48', '6 x 9 = 54', '7 x 1 = 7', '7 x 2 = 14', '7 x 3 = 21', '7 x 4 = 28', '7 x 5 = 35', '7 x 6 = 42', '7 x 7 = 49', '7 x 8 = 56', '7 x 9 = 63', '8 x 1 = 8', '8 x 2 = 16', '8 x 3 = 24', '8 x 4 = 32', '8 x 5 = 40', '8 x 6 = 48', '8 x 7 = 56', '8 x 8 = 64', '8 x 9 = 72', '9 x 1 = 9', '9 x 2 = 18', '9 x 3 = 27', '9 x 4 = 36', '9 x 5 = 45', '9 x 6 = 54', '9 x 7 = 63', '9 x 8 = 72', '9 x 9 = 81')
> ```
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 25 :**
>
> Ecrivez un programme qui initialise un tuple de chaînes de caractères et compte le nombre total de caractères des chaînes du tuple.
>
> Par exemple avec le tuple ("j'aime", "la", "nsi", "!") vous devez trouver 12.
>
> ```
> 
> 
> 
> 
> 
> ```



## II. Boucles non bornées : *while*

Pour répéter une séquence d’instructions, on peut utiliser une boucle *for* si l’on connait à l’avance le nombre de répétitions à effectuer. On peut aussi vouloir répéter une séquence d’instructions **tant que** une certaine condition est – ou n’est pas – vérifiée : le nombre de répétitions n’est alors pas connu à l’avance.

```python
début du programme
tant que condition
	séquence à réaliser
fin tant que
suite du programme
```

### 1. While

Voilà comment on écrit une boucle non bornée en Python :

* le mot-clef pour cette instruction est **while** (=tant que en anglais)

* la condition s'exprime sous la forme d'une expression booléenne

* la condition se termine par le signe deux-points **:** qui marque le début de la séquence à réaliser

* la séquence à réaliser est repérée par une **indentation** supplémentaire

Cela donne :

```python
début du programme
while condition:
    séquence à réaliser
suite du programme
```

L'expression booléenne de la condition est évaluée, et si elle est vraie, la séquence indentée est réalisée. Puis l'expression booléenne de la condition est à nouveau évaluée, et si elle vraie, la séquence est à nouveau réalisée. Ainsi de suite, jusqu'à ce que l'expression booléenne soit fausse : on sort alors de la boucle.

![while schéma](img/while.png)

> **exercice 26 :**
>
> Le programme suivant permet de déterminer à partir de quel entier la somme des premiers entiers `1 + 2 + 3 + ...` dépasse 100.
>
> ```python
> entier = 0
> somme = 0
> while somme < 100:
>     entier = entier + 1
>     somme = somme + entier
> ```
>
> Quelle est la condition qui est évaluée à chaque itération de la boucle ?
>
> ```
> 
> 
> ```
>
> A quelle condition la boucle va-t-elle s'arrêter ?
>
> ```
> 
> 
> ```
>
> Executez le programme. A partir de quel entier la somme dépasse-t-elle 100 ? Quelle est cette somme ?
>
> ```
> 
> 
> ```
>
> Complétez le tableau suivant d'évolution de l'état de la mémoire :
>
> |                     | entier | somme |   condition    |
> | :-----------------: | :----: | :---: | :------------: |
> |   avant la boucle   |   0    |   0   | True (0 < 100) |
> | première itération  |   1    |   1   |      True      |
> |  seconde itération  |   2    |   3   |      True      |
> | troisième itération |   3    |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |                     |        |       |                |
> |   après la boucle   |        |       |                |

> **exercice 27 :**
>
> Donnez le tableau d'évolution de l'état de la mémoire pour le programme suivant :
>
> ```python
> mon_tuple = (42, 7, 11, 4, 36)
> indice = 0
> while indice < len(mon_tuple):
>     nombre = mon_tuple[indice]
>     indice = indice + 1
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 28 :**
>
> On cherche à écrire un programme qui détermine si un nombre `nombre_recherche` est présent dans un tuple `mon_tuple`. On pourrait utiliser une boucle *for* pour parcourir tout le tuple, mais c'est un peu bête : une fois le nombre trouvé, il est inutile de continuer à parcourir le reste du tuple. On va donc utiliser un *while*.
>
> On commence à l'indice 0, et à chaque itération on augmente cet indice pour parcourir tout le tuple (comme à l'exercice 27).
>
> On entre dans la boucle tant que :
>
> * l'indice est inférieur à la taille du tuple
> * et le nombre situé à l'indice à regarder dans le tuple est différent du nombre recherché
>
> Complétez le programme suivant :
> ```python
> nombre_recherche = 7
> mon_tuple = (42, 7, 11, 4, 36)
> indice_a_regarder = ???
> while ??? and ???:
> 	indice_a_regarder = ???
> ```
>
> Executez ce programme. Regardez ensuite la valeur de indice_a_regarder. Que signifie cette valeur ?
>
> ```
> 
> 
> 
> ```
>
> Combien d'itérations de la boucle ont été effectuées ?
>
> ```
> 
> 
> ```
>
> Exécutez à nouveau le programme mais avec `nombre_recherche = 3`. Quelle est la valeur de indice_a_regarder ? Que signifie cette valeur ?
>
> ```
> 
> 
> 
> ```

> **exercice 29 :**
>
> Ecrivez un programme qui vérifie que chaque valeur d'un tuple `mon_tuple` est bien de type *int*.
>
> ```
> 
> 
> 
> 
> 
> ```
>
> Exécutez votre programme avec les tuples suivants :
>
> * (42, 7, 11, 4, 36)
> * (42, 7, "11", 4, 36)
> * ("a", "b")
> * (42, 7, 11, 4, 36.1)
> * ()

### 2. Boucles infinies

Le nombre d’itérations d’une boucle non bornée n’est pas connu.

Il peut parfois arriver que la condition de la boucle *while* soit toujours vraie. On parle de **boucle infinie**. L’exécution du programme ne termine alors jamais.

Regardons par exemple le programme suivant :

```python
i = 1 
while i > 0 :
    i = i + 1
```

Le nombre `i` est au départ supérieur à 0 : la condition est vraie. Puis à chaque itération, on augmente `i` de 1 : `i` est donc de plus en plus grand et toujours supérieur à 0 : la condition est toujours vraie. Comme la condition ne sera jamais fausse, le programme ne va jamais s'arrêter : c'est une boucle infinie.

> **exercice 30 :**
>
> Pour chacun des programmes suivants, déterminez si la boucle est infinie ou non.
>
> ```python
> nombre = 5
> while nombre != 0:
>  nombre = nombre + 1
> ```
>
> ```python
> nombre = -5
> while nombre != 0:
>  nombre = nombre + 1
> ```
>
> ```python
> nombre = 0
> while nombre != 0:
>  nombre = nombre + 1
> ```
>
> ```python
> indice = 0
> mon_tuple = (42, 7, 11, 4, 36)
> while indice < len(tuple):
>  nombre = mon_tuple[indice]
> ```
>
> ```python
> indice = 0
> mon_tuple = (42, 7, 11, 4, 36)
> while mon_tuple[indice] != 3:
>  indice = indice + 1
> ```
>
> ```
> 
> 
> 
> 
> 
> ```

A chaque fois qu'on utilise une boucle non bornée dans un programme, il va alors falloir vérifier que la boucle se termine bien. On utilise pour cela un **variant de boucle**.

Un variant de boucle est un nombre entier qui décroit strictement et garantit la sortie de la boucle lorsqu'il atteint 0.

Regardez par exemple le programme suivant :

```python
i = 1 
while i > 0 :
    i = i - 1
```

Le variant de boucle est `i` : à chaque itération, il décroit strictement et finit par atteindre 0. La boucle va donc se terminer.

Par contre, regardez à nouveau le programme suivant :

```python
i = 1 
while i > 0 :
    i = i + 1
```

On ne peut pas trouver de variant de boucle donc la boucle est infinie.

> **exercice 31 :**
>
> QCM : Pour chacun des programmes suivants, déterminez le bon variant de boucle parmi les propositions données.
>
> ```python
> nombre = 10
> while nombre > 0:
>     nombre = nombre // 2
> ```
>
> * nombre
> * nombre // 2
> * 10 - nombre
> * 0
>
> ```python
> entier = 0
> somme = 0
> while somme < 100:
>     entier = entier + 1
>     somme = somme + entier
> ```
>
> * entier
> * somme
> * 100 - somme
> * somme + entier
>
> ```python
> mon_tuple = (42, 7, 11, 4, 36)
> indice = 0
> while indice < len(mon_tuple):
>     nombre = mon_tuple[indice]
>     indice = indice + 1
> ```
>
> * indice
> * nombre
> * len(mon_tuple)
> * len(mon_tuple) - indice

Comme pour les boucles *for*, il est possible d'imbriquer plusieurs boucles *while*. On évite généralement de le faire car cela pose souvent des problèmes de boucles infinies.

### 3. Pour aller plus loin

S'il vous reste du temps, vous pouvez vous entraîner sur les exercices ci-dessous.

> **exercice 32 :**
>
> Ecrivez un programme qui permet de déterminer à partir de quel entier le produit des premiers entiers `1 x 2 x 3 x ...` dépasse 5000.
>
> ```
> 
> 
> 
> 
> 
> ```
>
> *(vous devez trouver 7)*

> **exercice 33 :**
>
> Ecrivez un programme qui permet de jouer au "ni oui ni non" : initialisez un tuple de chaînes de caractères et vérifiez si une des chaînes du tuple est "oui" ou "non".
>
> Par exemple le tuple ("absolument", "c'est correct", "je crois", "hors de question", "il me semble", "c'est sur !") est gagnant au "ni oui ni non". Par contre le tuple ("peut-être", "impossible", "sans aucun doute", "oui", "jamais") est perdant.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

> **exercice 34 :**
>
> Les cheveux de Elsa mesurent actuellement 21 centimètres. Elle souhaite les couper quand ils atteindront 50 centimètres. Chaque jour la longueur de ses cheveux augmente d’un pour cent (leur longueur est multipliée par 1.01).
>
> Ecrivez un programme qui calcule dans combien de jours Elsa devra couper ses cheveux.
>
> ```
> 
> 
> 
> 
> ```
>
> *(vous devez trouver 88)*

> **exercice 35 :**
>
> La période radioactive d’un isotope est le temps nécessaire à la désintégration de la moitié des noyaux d’un fragment de cet isotope.
>
> On cherche à calculer le nombre de périodes radioactives nécessaire à la disparition d’une partie des noyaux d’un isotope. Par exemple, pour faire disparaitre les trois quarts des noyaux d’un isotope donné, il faut attendre deux périodes radioactives : au bout d’une période, il en reste la moitié, et au bout de deux périodes il en reste un quart. Les trois quarts ont donc disparu.
>
> Ecrivez un programme qui initialise un flottant représentant la part de noyaux d’un isotope que l’on veut voir disparaître et qui calcule le nombre de périodes radioactive nécessaires à cette disparition.
>
> ```
> 
> 
> 
> 
> 
> ```
>
> Testez votre programme avec 3/4 et 0.99. *(vous devez trouver 2 et 7)*

> **exercice 36 :**
>
> Ecrivez un programme qui vérifie qu'un tuple correspond bien à de l'ADN, c'est-à-dire qu'il ne contient aucune autre chaîne de caractères que les quatre bases A, C, G et T.
>
> Par exemple ('A', 'C', 'T', 'A', 'G', 'A', 'A', 'T') est valide, par contre ('T', 'A', 'G', 'U', 'C') ne l'est pas.
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 37 :**
>
> Un entier `p` est dit premier s'il est supérieur ou égal à 2 et si aucun des entiers compris entre 2 et `p - 1` inclus ne divise `p` .
>
> Ecrivez un programme qui vérifie si un entier `p` est premier.
>
> ```
> 
> 
> 
> 
> 
> ```



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*

Source de l'exercice 13 : [kxs](https://kxs.fr/cours/)

