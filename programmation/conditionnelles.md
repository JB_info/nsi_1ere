# Constructions élémentaires : instructions conditionnelles

Maintenant que vous maîtrisez l'affectation, nous allons voir une seconde construction élémentaire, qui elle s'écrit sur plusieurs lignes : l'instruction conditionnelle.

L'instruction conditionnelle permet de **réaliser une séquence si une certaine condition est vérifiée**. Pour vérifier une condition, on va avoir besoin de nouveaux opérateurs pour nos types de base : les opérateurs de comparaisons.



## I. Opérateurs de comparaisons

Les opérateurs de comparaison, comme leur nom l’indique, permettent de comparer deux valeurs. Le résultat de la comparaison est un booléen. Pour rappel, le type booléen possède 2 valeurs : `True` (vrai) et `False` (faux).

Par exemple, on peut tester si la valeur 2 est inférieure à la valeur 7 avec l'opérateur de comparaison `<` comme ceci :

```python
>>> 2 < 7
True
```

Le booléen renvoyé est `True`, donc c'est vrai : 2 est inférieur à 7.

Voici tous les opérateurs de comparaison qui existent en Python :

| opérateur |    comparaison    |
| :-------: | :---------------: |
|   `==`​    |      égalité      |
|   `!=`    |    différence     |
|    `<`​    |     inférieur     |
|   `<=`​    | inférieur ou égal |
|    `>`​    |     supérieur     |
|   `>=`    | supérieur ou égal |

*Remarque : il ne faut pas confondre l'opérateur de comparaison pour l'égalité `==` avec l'affectation `=`.*

*Moyen mnémotechnique : pour se souvenir de l'ordre des caractères pour `>=`, il suffit de le prononcer à l'oral. On dit bien supérieur `>` ou égal `=` : supérieur d'abord, égal ensuite. Idem pour inférieur ou égal `<=`, et idem pour la différence (pas égal) `!=`.*

Nous allons tester ces opérateurs sur les types de base (*int*, *float*, *str*).

### 1. Comparer des nombres

La comparaison de 2 nombres entiers *int* ne pose aucune difficulté.

> **exercice 1 :**
>
> Testez les comparaisons suivantes :
>
> ```python
> >>> a = 6 
> >>> b = 4 
> >>> c = 2 
> >>> a < b 
> ?
> >>> a < b + c
> ?
> >>> a == b + c
> ?
> >>> a <= b + c
> ?
> >>> c != a - b
> ?
> >>> a - b >= c
> ?
> >>> a - b > c
> ?
> ```

La comparaison de 2 nombres flottants *float* pose par contre des problèmes.

> **exercice 2 :**
>
> Testez la comparaison suivante :
>
> ```python
> >>> 0.1 + 0.2 == 0.3
> ?
> ```
>
> Que remarquez-vous ?
>
> ```
> 
> 
> ```

Nous verrons lors de la séance sur la [représentation des flottants en machine](../representation_donnees/flottants.md) à quoi cela est dû.

Pour l'instant, il faut juste retenir que l'**on ne doit jamais tester l'égalité de 2 flottants avec `==`​**.

Il est également possible de comparer un entier *int* avec un flottant *float*.

> **exercice 3 :**
>
> Testez les comparaisons suivantes :
>
> ```python
> >>> 1.0 == 1
> ?
> >>> 4 == 2.0 * 2
> ?
> ```

Par contre, on rencontre parfois le même problème qu'avec la comparaison de 2 flottants.

> **exercice 4 :**
>
> Testez la comparaison suivante :
>
> ```python
> >>> 3 * 0.1 + 3 * 0.1 + 3 * 0.1 + 0.1 == 1
> ?
> ```
>
> Que remarquez-vous ?
>
> ```
> 
> 
> ```

Il faut donc retenir que dès qu'il y a des nombres flottants en jeu, on évite les comparaisons avec l'opérateur  `==`​.

### 2. Comparer des chaînes de caractères

Deux chaînes de caractères *str* sont égales si et seulement si :

* elles ont le même nombre de caractères
* le premier caractère de chacune des deux chaînes sont identiques
* le deuxième caractère de chacune des deux chaînes sont identiques
* etc.

On peut donc utiliser les opérateurs `==` et `!=` sur les chaînes.

> **exercice 5 :**
>
> Testez les comparaisons suivantes :
>
> ```python
> >>> "nsi" == "n" + "s" + "i"
> ?
> >>> "nsi" == "nsi "
> ?
> >>> "nsi" == "NSI"
> ?
> >>> "nsi" != 'nsi'
> ?
> >>> "123" != "123"
> ?
> >>> "1 2 3" != "123"
> ?
> >>> "j'aime la nsi !! :)" == "j'aime la nsi " + 2 * "!" + " :)"
> ?
> ```

On peut également utiliser les comparateurs `<`, `<=`, `>`, `>=` sur les chaînes.

On utilise alors l’ordre lexicographique, c’est-à-dire que l’on commence par comparer les deux premiers caractères de chacune des deux chaînes, puis en cas d’égalité on s’intéresse au second, et ainsi de suite. Le classement est donc le même que celui d’un dictionnaire.

Si, lors de la comparaison, on arrive à l’extrémité d’une des deux chaînes sans avoir
trouvé de caractères différents, alors la plus longue est la plus grande (là encore, comme
dans un dictionnaire).

> **exercice 6 :**
>
> Testez les comparaisons suivantes :
>
> ```python
> >>> "a" < "b"
> ?
> >>> "nsi" < "snt"
> ?
> >>> "science" < "savoir"
> ?
> >>> "information" < "informatique"
> ?
> >>> "info" < "informatique"
> ?
> ```

Cet ordre lexicographique fait que des chaînes de caractères contenant des chiffres ne sont pas forcément ordonnées selon la valeur des nombres.

> **exercice 7 :**
>
> Testez les comparaisons suivantes :
>
> ```python
> >>> "1" < "2"
> ?
> >>> "100" < "20"
> ?
> ```

Autre remarque, les majuscules sont inférieures aux minuscules. Nous verrons pourquoi lors de la séance sur la [représentation des caractères en machine](../representation_donnees/caracteres.md).

> **exercice 8 :**
>
> Testez les comparaisons suivantes :
>
> ```python
> >>> "A" < "a"
> ?
> >>> "NSI" < "nsi"
> ?
> >>> "Nsi" < "NSI"
> ?
> ```



## II. Opérateurs booléens

Grâce aux opérateurs de comparaisons, on peut maintenant vérifier certaines conditions : par exemple, est-ce que ma variable est positive ? Mais cela ne suffit pas, car dans certains cas on veut vérifier plusieurs conditions : par exemple, est-ce que ma variable est positive *et* inférieure à 10 ? On va donc devoir combiner nos conditions.

Les opérateurs booléens, aussi appelés opérateurs logiques, combinent des valeurs booléennes pour produire une nouvelle valeur booléenne.

Il existe principalement trois opérateurs logiques :

- la négation : *non* = `not`​
- la conjonction : *et* = `and`​
- la disjonction : *ou* = `or`​

### 1. Négation `not`

L’opérateur de **négation** permet d’obtenir la valeur inverse d’une valeur booléenne. En Python, l’opérateur de négation est noté `not`. Par exemple :

```python
>>> condition = True
>>> not condition
False
```

On décrit généralement les opérateurs booléens par leur **table de vérité**.

Voici la table de vérité de l'opérateur de négation :

|   x   | not x |
| :---: | :---: |
| False | True  |
| True  | False |

> **exercice 9 :**
>
> Evaluez à la main puis vérifiez avec Python les valeurs des conditions suivantes :
>
> ```python
> >>> not 2 < 7
> ?
> >>> not "nsi" == "NSI"
> ?
> >>> not "info" < "nsi"
> ?
> >>> not 10 * 2 > 5
> ?
> ```

### 2. Conjonction `and`​

L'opérateur de **conjonction** permet d’exprimer le fait que deux valeurs booléennes sont vraies simultanément. En Python, l’opérateur de conjonction est noté `and`.  Ainsi, `x and y` est vraie si et seulement si `x` est vraie, et `y` est vraie aussi. Par exemple :

```python
>>> condition1 = True
>>> condition2 = False
>>> condition1 and condition2
False
```

Voici la table de vérité de l'opérateur de conjonction :

|   x   |   y   | x and y |
| :---: | :---: | :-----: |
| False | False |  False  |
| False | True  |  False  |
| True  | False |  False  |
| True  | True  |  True   |

> **exercice 10 :**
>
> Evaluez à la main puis vérifiez avec Python les valeurs des conditions suivantes :
>
> ```python
> >>> nombre = 7
> >>> nombre > 0 and nombre < 10
> ?
> >>> nombre > 0 and nombre % 2 == 0
> ?
> >>> nombre // 3 != 2 and nombre != 0
> ?
> >>> nombre < 0 and nombre * 2 == 10
> ?
> >>> nombre < 0 and (nombre * 5 - 15) % 2 == nombre // 7 - 1
> ?
> ```

L'opérateur `and`​ est un opérateur **séquentiel**. Cela veut dire que Python évalue les valeurs booléennes de gauche à droite et, s'il n'est pas nécessaire d'examiner toutes les valeurs, il ne le fait pas.

Par exemple pour `1 == 2 and 2 == 3`​ :

* Python évalue d'abord `1 == 2`, il trouve `False`​
* comme `False and` n'importe quelle valeur vaut toujours `False`, Python n'évalue pas la suite.

> **exercice 11 :**
>
> Tentez de diviser 2 par 0 :
>
> ```python
> >>> 2 / 0
> ?
> ```
>
> Vous venez de rencontrer votre deuxième exception, **ZeroDivisionError** : comme en maths, il est impossible de diviser un nombre par 0.
>
> Testez maintenant la condition suivante :
>
> ```python
> >>> nombre = 7
> >>> nombre < 0 and 2 / 0 == 0
> ?
> ```
>
> Comment expliquez-vous que Python ne déclenche pas d'exception cette fois ?
>
> ```
> 
> 
> 
> ```

### 3. Disjonction `or`​

L'opérateur de **disjonction** permet d’exprimer le fait qu'au moins une valeur booléenne est vraie. En Python, l’opérateur de conjonction est noté `or`.  Ainsi, `x or y` est vraie si soit `x` est vraie, soit `y` est vraie, soit les deux sont vraies. Par exemple :

```python
>>> condition1 = True
>>> condition2 = False
>>> condition1 or condition2
True
```

Voici la table de vérité de l'opérateur de disjonction :

|   x   |   y   | x or y |
| :---: | :---: | :----: |
| False | False | False  |
| False | True  |  True  |
| True  | False |  True  |
| True  | True  |  True  |

> **exercice 12 :**
>
> Evaluez à la main puis vérifiez avec Python les valeurs des conditions suivantes :
>
> ```python
> >>> nombre = 7
> >>> nombre > 0 or nombre < 10
> ?
> >>> nombre > 0 or nombre % 2 == 0
> ?
> >>> nombre // 3 != 2 or nombre != 0
> ?
> >>> nombre < 0 or nombre * 2 == 10
> ?
> >>> nombre > 0 or (nombre * 5 - 15) % 2 == nombre // 7 - 1
> ?
> ```

L'opérateur `or`​ est également un opérateur **séquentiel**.

Par exemple pour `1 + 1 == 2 or 2 == 3`​ :

* Python évalue d'abord `1 + 1 == 2`, il trouve `True`​
* comme `True or` n'importe quelle valeur vaut toujours `True`, Python n'évalue pas la suite.

> **exercice 13 :**
>
> Donnez une expression qui devrait déclencher une exception, mais ne le fait pas grâce à la séquentialité de l'opérateur de disjonction.
>
> ```
> 
> 
> ```

### 4. Expressions booléennes

Une expression qui combinent des valeurs booléennes avec les opérateurs logiques et dont le résultat est une valeur booléenne s'appelle une **expression booléenne**.

Toutes les expressions que l'on a vu au-dessus sont donc des expressions booléennes. On peut combiner autant de valeurs que l'on veut dans une expression booléenne. Par exemple, `(not x and y) or z`​ est une expression booléenne.

*Remarque : il n'y a pas d'ordre de priorité entre le and et le or, les parenthèses sont donc obligatoires. Par contre, le not ne s'applique qu'à la valeur qui le suit (dans l'exemple précédent, le not s'applique à `x`​).*

Pour évaluer une expression booléenne, on dresse sa table de vérité. Par exemple pour `(not x and y) or z`​ :

|   x   |   y   |   z   | not x | not x and y | (not x and y) or z |
| :---: | :---: | :---: | :---: | :---------: | :----------------: |
| False | False | False | True  |    False    |       False        |
| False | False | True  | True  |    False    |        True        |
| False | True  | False | True  |    True     |        True        |
| False | True  | True  | True  |    True     |        True        |
| True  | False | False | False |    False    |       False        |
| True  | False | True  | False |    False    |        True        |
| True  | True  | False | False |    False    |       False        |
| True  | True  | True  | False |    False    |        True        |

> **exercice 14 :**
>
> Dressez la table de vérité des expressions booléennes suivantes :
>
> * x and not y
> * not x or not y
> * (x or y) and not z
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

Il existe 2 lois, appelées **lois de De Morgan** qui permettent de simplifier des expressions booléennes :

* `not (x or y)` est équivalent à `not x and not y`​
* `not (x and y)` est équivalent à `not x or not y`​

> **exercice 15 :**
>
> Simplifiez les expressions suivantes :
>
> * x and (not (x or y))
> * (not x or not y) or (not (x and y))
>
> ```
> 
> 
> 
> 
> 
> 
> ```

Pour vérifiez que 2 expressions sont bien équivalentes, on dresse leurs tables de vérité. Si les tables de vérité sont les mêmes, alors les expressions sont équivalentes.

> **exercice 16 :**
>
> Montrez que la première loi de De Morgan est vraie en dressant la table de vérité de `not (x or y)` et celle de `not x and not y`.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **Pour aller plus loin :**
>
> S'il vous reste du temps, vous pouvez faire cet exercice.
>
> Pour chacun des problèmes suivants, donnez une expression booléenne qui permet de vérifier si :
>
> * un nombre `x`​ est pair
> * un nombre `x` est un multiple de 5
> * une chaîne de caractères `chaine` est vide
> * un nombre `x`​ est du type *float*
> * deux nombres `x` et `y` sont tous les deux positifs
> * deux nombres `x` et `y`​ sont tous deux de même signe
> * un nombre `x` appartient à un intervalle [`a`,`b`]
> * l'intersection de 2 intervalles [`a`,`b`] et [`c`,`d`​] est vide



## III. Instructions conditionnelles

### 1. If

Comme dit plus-haut, une instruction conditionnelle permet de réaliser une séquence si une certaine condition est vérifiée. 

```
début du programme
si condition
	séquence à réaliser si la condition est vraie
fin si
suite du programme
```

Voilà comment on l'écrit en Python :

* le mot-clef pour cette instruction est **if** (=si en anglais)

* la condition s'exprime sous la forme d'une expression booléenne
* la condition se termine par le signe deux-points **:** qui marque le début de la séquence à réaliser
* la séquence à réaliser est repérée par une **indentation** supplémentaire

Cela donne :

```python
début du programme
if condition:
    première instruction de la séquence
    deuxième instruction de la séquence
    ...
    dernière instruction de la séquence
suite du programme
```

L'expression booléenne de la condition est évaluée, et si elle est vraie, la séquence indentée est réalisée.

Pour sortir de l'instruction conditionnelle, il suffit de continuer à écrire notre programme en revenant à une indentation normale.

![if schéma](img/if.png)

> **exercice 17 :**
>
> Exécutez le programme suivant :
>
> ```python
> nombre1 = 4
> nombre2 = 7
> if nombre1 < nombre2:
>     resultat = "inferieur"
> nombre3 = 3
> ```
>
> Regardez dans le shell la valeur de `resultat`​. L'affectation a-t-elle bien été réalisée ? Pourquoi ?
>
> ```
> 
> 
> 
> ```
>
> Regardez aussi la valeur de `nombre3`​. L'affectation a-t-elle bien été réalisée ? Pourquoi ?
>
> ```
> 
> 
> 
> ```

> **exercice 18 :**
>
> Recommencez l'exercice 17 mais en prenant nombre1 = 7 et nombre2 = 4.
>
> ```
> 
> 
> 
> 
> 
> ```

> **exercice 19 :**
>
> Ecrivez un programme qui initialise une variable `nombre`, puis affecte "positif" à une variable `resultat` si le nombre est positif.
>
> ```
> 
> 
> 
> ```
>
> Testez votre programme avec les nombres 42, -7 et 0.
>
> ```
> 
> 
> ```

Il est également possible d'imbriquer des instructions conditionnelles. C'est toujours l'**indentation** qui détermine le début/la fin d'une instruction.

> **exercice 20 :**
>
> Exécutez le programme suivant :
>
> ```python
> nombre = 10
> if nombre > 0:
>     resultat = "positif"
>     if nombre % 2 == 0:
>         resultat = "positif pair"
> ```
>
> Regardez la valeur de la variable `resultat`.
>
> ```
> 
> 
> ```
>
> Donnez l'état de la mémoire à chaque étape du programme :
>
> ```
> 
> 
> 
> 
> 
> ```
>
> Recommencez en affectant à la variable `nombre`​ les valeurs 7, puis -10.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```



### 2. Else

L’instruction conditionnelle *if* permet d’exécuter une séquence si et seulement si une condition est vraie. Il existe d’autres situations où l’on peut vouloir exécuter une séquence si une condition est vraie, et exécuter une autre séquence si cette même condition n’est pas vraie.

```
début du programme
si condition
	séquence à réaliser si la condition est vraie
sinon
	séquence à réaliser si la condition est fausse
fin si
suite du programme
```

Voilà comment on l'écrit en Python :

* les mots-clefs pour cette instruction sont **if** et **else**(=sinon en anglais)
* la condition s'exprime toujours sous la forme d'une expression booléenne
* le signe deux-points **:** permet de marquer le début d'une séquence
* la séquence à réaliser pour le if est repérée par une **indentation** supplémentaire
* la séquence à réaliser pour le else est repérée par une **indentation** supplémentaire

Cela donne :

```python
début du programme
if condition:
    première instruction de la séquence si la condition est vraie
    deuxième instruction de la séquence si la condition est vraie
    ...
    dernière instruction de la séquence si la condition est vraie
else:
    première instruction de la séquence si la condition est fausse
    deuxième instruction de la séquence si la condition est fausse
    ...
    dernière instruction de la séquence si la condition est fausse
suite du programme
```

L'expression booléenne de la condition est évaluée, et si elle est vraie, la séquence indentée est réalisée. Si elle est fausse, c'est la séquence indentée après le else qui est réalisée.

![else schéma](img/else.png)

> **exercice 21 :**
>
> Exécutez le programme suivant :
>
> ```python
> nombre1 = 4
> nombre2 = 7
> if nombre1 < nombre2:
>     resultat = "inferieur"
> else:
>     resultat = "superieur"
> nombre3 = 3
> ```
>
> Regardez dans le shell la valeur de `resultat`. Expliquez d'où vient cette valeur.
>
> ```
> 
> 
> ```
>
> Regardez aussi la valeur de `nombre3`. L'affectation a-t-elle bien été réalisée ? Pourquoi ?
>
> ```
> 
> 
> ```

> **exercice 22 :**
>
> Recommencez l'exercice 21 mais en prenant nombre1 = 7 et nombre2 = 4.
>
> ```
> 
> 
> 
> 
> ```

> **exercice 23 :**
>
> Ecrivez un programme qui initialise une variable `nombre`, puis affecte à une variable `resultat` la chaîne "positif" si le nombre est positif, et "negatif" si le nombre est négatif.
>
> ```
> 
> 
> 
> 
> ```
>
> Testez votre programme avec les nombres 42, -7 et 0.
>
> ```
> 
> 
> ```

### 3. Elif

Nous avons vu que :

- l’instruction conditionnelle *if* permet d’exécuter une séquence si et seulement si une condition est vraie
- l’alternative *else* permet d’exécuter une autre séquence si cette condition n'est pas vraie.

Il existe d’autres situations où l’on peut vouloir exécuter d’autres séquences si d’autres conditions sont vraies. Nous sommes face à une série de situations, et pour chacune d’elles voulons exécuter des séquences particulières.

```
début du programme
si condition 1
	séquence à réaliser si la condition 1 est vraie
sinon si condition 2
	séquence à réaliser si la condition 2 est vraie
sinon si condition 3
	séquence à réaliser si la condition 3 est vraie
sinon
	séquence à réaliser si toutes les conditions sont fausses
fin si
suite du programme
```

Voilà comment on l'écrit en Python :

* les mots-clefs pour cette instruction sont **if**, **else** et **elif**
* toutes les conditions s'expriment sous la forme d'expressions booléennes
* le signe deux-points **:** permet de marquer le début d'une séquence
* la séquence à réaliser pour le if est repérée par une **indentation** supplémentaire
* la séquence à réaliser pour chacun des elif est repérée par une **indentation** supplémentaire
* la séquence à réaliser pour le else est repérée par une **indentation** supplémentaire

Cela donne :

```python
début du programme
if condition1:
    première instruction de la séquence si la condition1 est vraie
    deuxième instruction de la séquence si la condition1 est vraie
    ...
    dernière instruction de la séquence si la condition1 est vraie
elif condition2:
    première instruction de la séquence si la condition1 est fausse et la condition2 est vraie
    deuxième instruction de la séquence si la condition1 est fausse et la condition2 est vraie
    ...
    dernière instruction de la séquence si la condition1 est fausse et la condition2 est vraie
elif condition3:
    première instruction de la séquence si la condition1 est fausse et la condition2 est fausse et la condition3 est vraie
    deuxième instruction de la séquence si la condition1 est fausse et la condition2 est fausse et la condition3 est vraie
    ...
    dernière instruction de la séquence si la condition1 est fausse et la condition2 est fausse et la condition3 est vraie
elif condition4:
    ...
else:
    première instruction de la séquence si toutes les conditions sont fausses
    deuxième instruction de la séquence si toutes les conditions sont fausses
    ...
    dernière instruction de la séquence si toutes les conditions sont fausses
suite du programme
```

L'expression booléenne de la condition1 est évaluée, et si elle est vraie, la séquence indentée est réalisée. Si elle est fausse, l'expression booléenne de la condition2 est évaluée : si elle est vraie, la séquence indentée est réalisée, si elle est fausse, l'expression booléenne de la condition3 est évaluée... On peut mettre autant de elif qu'on veut. Finalement, si toutes les conditions sont fausses, c'est la séquence indentée après le else qui est réalisée.

![elif schéma](img/elif.png)

> **exercice 24 :**
>
> Exécutez le programme suivant :
>
> ```python
> nombre1 = 4
> nombre2 = 7
> if nombre1 < nombre2:
>     resultat = "inferieur"
> elif nombre1 == nombre2:
>     resultat = "egal"
> else:
>     resultat = "superieur"
> nombre3 = 3
> ```
>
> Regardez dans le shell la valeur de $`resultat`$. Expliquez d'où vient cette valeur.
>
> ```
> 
> 
> ```
>
> Regardez aussi la valeur de $`nombre3`$. L'affectation a-t-elle bien été réalisée ? Pourquoi ?
>
> ```
> 
> 
> ```

> **exercice 25 :**
>
> Recommencez l'exercice 24 : 
>
> * en prenant nombre1 = 7 et nombre2 = 4
> * en prenant nombre1 = 7 et nombre2 = 7
>
> ```
> 
> 
> 
> ```

> **exercice 26 :**
>
> Ecrivez un programme qui initialise une variable `nombre`, puis affecte à une variable `resultat` la chaîne "positif" si le nombre est strictement positif, "negatif" si le nombre est strictement négatif, et "nul" si le nombre est nul.
>
> ```
> 
> 
> 
> 
> 
> 
> ```
>
> Testez votre programme avec les nombres 42, -7 et 0.
>
> ```
> 
> 
> ```

*Remarque : quand il y a plusieurs elif, l'ordre des conditions peut être très important (exemple avec l'exercice 30).*

### 4. Pour aller plus loin

Si vous avez terminé, vous pouvez vous entraîner sur les exercices suivants.

> **exercice 27 :**
>
> Ecrivez un programme qui initialise deux nombres et affecte à une variable `resultat`​ le plus grand de ces deux nombres.
>
> ```
> 
> 
> 
> 
> ```
>
> Ecrivez un programme qui initialise deux nombres et affecte à une variable `resultat`​ le plus petit de ces deux nombres.
>
> ```
> 
> 
> 
> 
> ```

> **exercice 28 :**
>
> Ecrivez un programme qui initialise un nombre et affecte à une variable `resultat`​ la valeur absolue de ce nombre.
>
> ```
> 
> 
> 
> 
> ```

> **exercice 29 :**
>
> Ecrivez un programme qui initialise 2 nombres : un numéro de jour et un numéro de mois, et affecte à une variable `saison`​ une des chaînes de caractères "printemps", "été", "automne", "hiver" en fonction de la saison.
>
> *Indice :*
>
> * le printemps commence le 20 mars
> * l'été le 21 juin
> * l'automne le 22 septembre
> * l'hiver le 21 décembre
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 30 :**
>
> Au tir à l’arc, les catégories sont les suivantes :
>
> - Poussins : 10 ans et moins
> - Benjamins : 11 à 12 ans
> - Minimes : 13 à 14 ans
> - Cadets : 15 à 17 ans
> - Juniors : 18 à 20 ans
> - Seniors : 21 ans et plus
>
> Les quatre programmes ci-dessous contiennent des instructions conditionnelles afin d’essayer de déterminer la catégorie d’un sportif en fonction de son âge. 
>
> Pour chaque programme, trouvez à la main la valeur de la variable `categorie` avec les âges 10, 15 et 23.
>
> ```python
> age = ????????????
> if age <= 10:
>     categorie = "poussin"
> if age <= 12:
>     categorie = "benjamin"
> if age <= 14:
>     categorie = "minime"
> if age <= 17:
>     categorie = "cadet"
> if age <= 20:
>     categorie = "junior"
> else:
>     categorie = "senior"
> ```
>
> ```python
> age = ????????????
> if age <= 10:
>     categorie = "poussin"
> elif age <= 12:
>     categorie = "benjamin"
> elif age <= 14:
>     categorie = "minime"
> elif age <= 17:
>     categorie = "cadet"
> elif age <= 20:
>     categorie = "junior"
> else:
>     categorie = "senior"
> ```
>
> ```python
> age = ????????????
> if age <= 10:
>     categorie = "poussin"
> elif age <= 20:
>     categorie = "junior"
> elif age <= 14:
>     categorie = "minime"
> elif age <= 17:
>     categorie = "cadet"
> elif age <= 12:
>     categorie = "benjamin"
> else:
>     categorie = "senior"
> ```
>
> ```python
> age = ????????????
> if age <= 10:
>     categorie = "poussin"
> elif age <= 12:
>     categorie = "benjamin"
> elif age <= 14:
>     categorie = "minime"
> elif age > 20: 
>     categorie = "senior"
> elif age > 17:
>     categorie = "junior"
> else:
>     categorie = "cadet"
> ```
>
> ```
> 
> 
> 
> 
> 
> 
> 
> ```
>
> Quel(s) programme(s) donne(nt) les bons résultats ?
>
> ```
> 
> 
> ```

> **exercice 31 :**
>
> Ecrivez un programme qui initialise trois nombres et :
>
> * affecte à une variable `minimum`​ le plus petit de ces trois nombres
> * affecte à une variable `maximum` le plus grand de ces trois nombres
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 32 :**
>
> Ecrivez un programme qui initialise trois nombres et affecte à une variable `mediane`​ le nombre médian de ces trois nombres.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```

> **exercice 33 :**
>
> Une année est bissextile :
>
> - si l’année est divisible par 4 et non divisible par 100, ou
> - si l’année est divisible par 400.
>
> Ainsi, 2018 n’était pas bissextile. L’année 2020 est bissextile suivant la première règle : divisible par 4 et non divisible par 100. L’an 1900 n’était pas bissextile car divisible par 4 mais aussi par 100, et non divisible par 400. L’an 2000 était bissextile car divisible par 400.
>
> Ecrivez un programme qui initialise un nombre $`annee`$ et affecte à une variable `bissextile`​ le booléen True si l'année est bissextile, False sinon.
>
> ```
> 
> 
> 
> 
> 
> 
> ```

> **exercice 34 :**
>
> Ecrivez un programme qui initialise 3 nombres `jour`, `mois` et `annee`​ et vérifie si les nombres correspondent bien à une date.
>
> Il faut vérifier si :
>
> * le numéro de mois est compris entre 1 et 12
> * le numéro de jour est compris entre 1 et 30/31/28/29 en fonction du mois et de l'année
>
> Vous affecterez à une variable `resultat` la chaîne "la date est valide" si les nombres correspondent bien à une date, la chaîne "la date n'est pas valide" sinon.
>
> ```
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> 
> ```
>
> Testez votre programme avec les dates suivantes :
>
> * 2/-5/2000 (non valide)
> * -2/5/2000 (non valide)
> * 2/5/2000 (valide)
> * 31/5/2000 (valide)
> * 31/4/2000 (non valide)
> * 29/2/2000 (valide)
> * 29/2/2018 (non valide)
> * 29/2/1900 (non valide)



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*

Source du III.4. : [FIL](https://www.fil.univ-lille1.fr/portail/)